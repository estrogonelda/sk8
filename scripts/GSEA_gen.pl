#!/usr/bin/env perl 
############################## HEADER ##########################################
#
#         FILE: GSEA_gen.pl
#
#        USAGE: ./GSEA_gen.pl  
#
#  DESCRIPTION: 
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: YOUR NAME (), 
# ORGANIZATION: 
#      VERSION: 1.0
#      CREATED: 01/21/2020 06:11:59 PM
#     REVISION: ---
#
################################################################################





############################## PREAMBLE ########################################

# Pragma.
use utf8;
use strict;
use warnings;

# Packages.
use Data::Dumper;
use Statistics::Basic qw(:all);



# Command line arguments.
# 0. File with all annotations, gencode v22 (GTF).
# .. Files with raw RNAseq counts (COUNTS).


# Verifications.
die "Incorrect number of input files (must be at least 2)" if scalar @ARGV < 2;

# More verifications.
foreach ( @ARGV ) {
	die "Nonexistent file \"$_\"" unless -f $_;
}



############################## MAIN ############################################



# Global variable declarations.
my %genes;
my %acm;
my @samples = ();
my $annot = shift;
my $actual = "";


# Load protein coding genes from GTF file.
open my $fh, '<', $annot or die "Can't open file '$annot'";
while (<$fh>) {
	next if /^#/;
	my @line = split;
	next unless ($line[2] =~ /gene/ and $line[11] =~ /protein_coding/);
	
	my $ensembl = $1 if $_ =~ /gene_id "(.*?)";/;
	my $name = $1 if $_ =~ /gene_name "(.*?)";/;
	push @{ $genes{$ensembl} } => [ $name, $line[3], $line[4] ];
}
close $fh;


# Load raw counts.
while (<>) {
	my @line = split;
	next unless exists $genes{$line[0]};
	chomp $line[1];
	
	# Calculate RPK.
	push @{ $genes{$line[0]}->[1] } => $line[1] / (abs($genes{$line[0]}->[0][2] - $genes{$line[0]}->[0][1]) / 1000);
	$acm{$ARGV} += $genes{$line[0]}->[1][-1];
	push @samples => $ARGV if $actual ne $ARGV;
	$actual = $ARGV;
}

# Calculate TPM.
foreach my $k ( keys %genes ) {
	$genes{$k}->[1][$_] /= ($acm{$samples[$_]} / 1000000) for 0..$#samples;
}

# Calculate medians for each gene.
$genes{$_}->[2] = median($genes{$_}->[1]) foreach keys %genes;



# Results.
print "$genes{$_}->[0][0]\t$genes{$_}->[2]\n" foreach sort { $genes{$b}->[2] <=> $genes{$a}->[2] } keys %genes;
