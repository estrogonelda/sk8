
$act = $ARGV, $ck++ unless $ck;
next if /^#/;

if ( $act eq $ARGV ) {
	if ( $F[2] eq "gene" ) {
		$ensembl = $1 if $F[8] =~ /gene_id "(.*?)";/;
		$name = $1 if $F[8] =~ /gene_name "(.*?)";/;
		$type = $1 if $F[8] =~ /gene_type "(.*?)";/;
		$genes{$ensembl} = [ $name, 1, $type, $F[0] ];
		$gene_ck = 0;
	}
	elsif ( $F[2] eq "transcript" ) {
		if ( $gene_ck ) {
			$gene_keep = $gene_acm if $gene_acm > $gene_keep;
		}
		else {
			$genes{$gene_prev}->[1] = max($gene_keep, 1) if defined $gene_prev;
			$genes{$gene_prev}->[1] = max($gene_acm, 1) if defined $gene_prev and $gene_keep == 0;
			$gene_keep = 0;
		}
		$gene_prev = $ensembl;
		$gene_ck++;
		$gene_acm = 0;
	}
	elsif ( $F[2] eq "exon" ) {
		$gene_acm += abs($F[3] - $F[4]);
	}
	else {}
}
else {
	if ( $ARGV =~ /GTEx/ or $ARGV =~ /\/dev\/fd/ ) {
		next if scalar(@F) < 3;
		if ( /^Name/ ) {
			@names = @F;
			chomp $names[-1];
			next;
		}

		next unless exists $genes{$F[0]};
		push @gns => $F[0];

		$rpk = $genes{$F[0]}->[1];
		foreach ( 2..$#names ) {
			$mtx{$names[$_]}{$F[0]} = $F[$_]/$rpk;
			$mtx{$names[$_]}{"acm"} += $F[$_]/$rpk;
		}
	}
	else {
		next if /^_/;
		next unless exists $genes{$F[0]};

		push @names => $ARGV if $argv ne $ARGV;
		$argv = $ARGV;
		push @gns => $F[0] if scalar @names == 1;

		$rpk = $genes{$F[0]}->[1];
		$mtx{$names[-1]}{$F[0]} = $F[1]/$rpk;
		$mtx{$names[-1]}{"acm"} += $F[1]/$rpk;
	}
}

END {
	$mtx{$names[$_]}{"acm"} /= 1000000 for 2..$#names;
	print "## Annotation file: $act\n";
	print "GENE_ID\tGENE_NAME\tGENE_TYPE\tGENE_LENGTH\t", join("\t", @names[2..$#names]), "\n";
	foreach my $gn ( @gns ) {
		$xtra = "";
		$xtra = "(MT)" if $genes{$gn}->[3] eq "chrM";
		print "$gn\t$genes{$gn}->[0]\t$genes{$gn}->[2]$xtra\t$genes{$gn}->[1]";
		foreach my $nm ( @names[2..$#names] ) {
			$res = 0;
			if ( ($mtx{$nm}{$gn} / $mtx{$nm}{"acm"}) < .001 ) {
				print "\t0";
			}
			else {
				$res = $mtx{$nm}{$gn} / $mtx{$nm}{"acm"};
				printf("\t%06.4f", $res);
			}
		}
		print "\n";
	}
}
