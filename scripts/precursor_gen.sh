#!/bin/bash
############################## HEADER ##########################################
#
# Template for a general script.
#
#
# Usage:
#
#    $ ./precursor_gen.sh <payload_file>
#
# Arguments:
#
#    1. Payload file generated by payload_gen.sh script.
#
################################################################################



############################## PREAMBLE ########################################

# Define a help message.
usage () {
	cat << EOF

Usage:
	
	\$ $0 <payload_file>
EOF
	return
}


# Verify input arguments number.
if [ $# -lt 1 ]; then
	echo -e "ERROR: Input arguments are missing (there must be at least 1)." >&2
	usage >&2
	exit 1
fi



for fl in $@; do
	if [ ! -f "$fl" ]; then
		echo -e "ERROR: Nonexistent file '$fl'" >&2
		exit 1
	fi
done


# Verify external requisisites: GNU parallel and curl.
[ -n "$(which parallel 2> /dev/null)" -a -n "$(which curl 2> /dev/null)" ] ||
	{ echo "Some external requisites are pending: 'parallel' or 'curl'." >&2; exit 1; }



############################## MAIN ############################################

# Extract file radical.
rad=${1%.*}


# Process init message.
#echo -e "Beginning process: $0." >&2


# Create manifest file.
echo -e "Creating precursor file: '$rad.prec'." >&2
echo -e "curl --request POST --header \"Content-Type: application/json\" --data @$1 'https://api.gdc.cancer.gov/files' > $rad.prec" >&2
curl --request POST --header "Content-Type: application/json" --data @$1 'https://api.gdc.cancer.gov/files' > $rad.prec
[ -z "$(perl -ne 'print if /^cases\.0/' $rad.prec)" ] && echo -e "ERROR: Precursor file creation failed." >&2
echo -e "Done." >&2


# Process final message.
#echo -e "\nFinished process: $0." >&2
