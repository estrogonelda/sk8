#!/bin/bash
############################## HEADER #########################################
# Simple script ....
###############################################################################



############################## PREAMBLE #######################################

# Auto referencing variables.
package=`basename $0`
version='0.1.0'
id=$RANDOM


greetings="
===============================================================================
=                              $package                                      =
===============================================================================
v$version

"

usage () {
	echo -e "ERROR: Arguments missing.\n
Usage:

	$ $package [opt1 val1 ...] arg1 ...
	" >&2
	exit 1;
}
[ $# -gt 0 ] || usage

err () {
	echo -e "ERROR: $package => $1\n" >&2
	exit 1;
}

wrn () {
	echo -e "WARNING: $package => $1\n" >&2
}

msg () {
	echo -e "MESSAGE: $package => $1\n" >&2
}


# All pre-defined short options (whole alphabeti, upper and lower case).
shopts="@:?:!:_:0:1:2:3:4:5:6:7:8:9:a:b:c:d:e:f:g:h:i:j:k:l:m:n:o:p:q:r:s:t:u:v:w:x:y:z:A:B:C:D:E:F:G:H:I:J:K:L:M:N:O:P:Q:R:S:T:U:V:W:X:Y:Z:"

# Corresponding pre-defined long options.
declare -A lopts=(
	[data-file]=""
	[data-dir]=""
	[data]=""
	[verbose]=""
	[version]=""
	[help]=""
	)


# Parse CLI options.
parsed=$(getopt \
	--name $package \
	--options $shopts \
	--longoptions $(printf "%s:," ${!lopts[@]}) \
	-- "$@")
[ $? -eq 0 ] || msg "Can not parse options."
eval set -- $parsed
echo "All antes: $@."


num=0
args=($parsed)
for opt in "${args[@]}"; do
	echo "Opcao $opt."
	case $opt in
		-@) lopts[]=${args[$num+1]//\'/} ;;
		-!) lopts[]=${args[$num+1]//\'/} ;;
		-?) lopts[]=${args[$num+1]//\'/} ;;
		-_) lopts[]=${args[$num+1]//\'/} ;;
		-0) lopts[]=${args[$num+1]//\'/} ;;
		-1) lopts[]=${args[$num+1]//\'/} ;;
		-2) lopts[]=${args[$num+1]//\'/} ;;
		-3) lopts[]=${args[$num+1]//\'/} ;;
		-4) lopts[]=${args[$num+1]//\'/} ;;
		-5) lopts[]=${args[$num+1]//\'/} ;;
		-6) lopts[]=${args[$num+1]//\'/} ;;
		-7) lopts[]=${args[$num+1]//\'/} ;;
		-8) lopts[]=${args[$num+1]//\'/} ;;
		-9) lopts[]=${args[$num+1]//\'/} ;;
		-a) lopts[]=${args[$num+1]//\'/} ;;
		-b) lopts[]=${args[$num+1]//\'/} ;;
		-c) lopts[]=${args[$num+1]//\'/} ;;
		-d) lopts[data-file]=${args[$num+1]//\'/} ;;
		-e) lopts[]=${args[$num+1]//\'/} ;;
		-f) lopts[]=${args[$num+1]//\'/} ;;
		-g) lopts[]=${args[$num+1]//\'/} ;;
		-h) lopts[]=${args[$num+1]//\'/} ;;
		-i) lopts[]=${args[$num+1]//\'/} ;;
		-j) lopts[]=${args[$num+1]//\'/} ;;
		-k) lopts[]=${args[$num+1]//\'/} ;;
		-l) lopts[]=${args[$num+1]//\'/} ;;
		-m) lopts[]=${args[$num+1]//\'/} ;;
		-n) lopts[]=${args[$num+1]//\'/} ;;
		-o) lopts[]=${args[$num+1]//\'/} ;;
		-p) lopts[]=${args[$num+1]//\'/} ;;
		-q) lopts[]=${args[$num+1]//\'/} ;;
		-r) lopts[]=${args[$num+1]//\'/} ;;
		-s) lopts[]=${args[$num+1]//\'/} ;;
		-t) lopts[]=${args[$num+1]//\'/} ;;
		-u) lopts[]=${args[$num+1]//\'/} ;;
		-v) lopts[]=${args[$num+1]//\'/} ;;
		-w) lopts[]=${args[$num+1]//\'/} ;;
		-x) lopts[]=${args[$num+1]//\'/} ;;
		-y) lopts[]=${args[$num+1]//\'/} ;;
		-z) lopts[]=${args[$num+1]//\'/} ;;
		-A) lopts[]=${args[$num+1]//\'/} ;;
		-B) lopts[]=${args[$num+1]//\'/} ;;
		-C) lopts[]=${args[$num+1]//\'/} ;;
		-D) lopts[data-dir]=${args[$num+1]//\'/} ;;
		-E) lopts[]=${args[$num+1]//\'/} ;;
		-F) lopts[]=${args[$num+1]//\'/} ;;
		-G) lopts[]=${args[$num+1]//\'/} ;;
		-H) lopts[]=${args[$num+1]//\'/} ;;
		-I) lopts[]=${args[$num+1]//\'/} ;;
		-J) lopts[]=${args[$num+1]//\'/} ;;
		-K) lopts[]=${args[$num+1]//\'/} ;;
		-L) lopts[]=${args[$num+1]//\'/} ;;
		-M) lopts[]=${args[$num+1]//\'/} ;;
		-N) lopts[]=${args[$num+1]//\'/} ;;
		-O) lopts[]=${args[$num+1]//\'/} ;;
		-P) lopts[]=${args[$num+1]//\'/} ;;
		-Q) lopts[]=${args[$num+1]//\'/} ;;
		-R) lopts[]=${args[$num+1]//\'/} ;;
		-S) lopts[]=${args[$num+1]//\'/} ;;
		-T) lopts[]=${args[$num+1]//\'/} ;;
		-U) lopts[]=${args[$num+1]//\'/} ;;
		-V) lopts[]=${args[$num+1]//\'/} ;;
		-W) lopts[]=${args[$num+1]//\'/} ;;
		-X) lopts[]=${args[$num+1]//\'/} ;;
		-Y) lopts[]=${args[$num+1]//\'/} ;;
		-Z) lopts[]=${args[$num+1]//\'/} ;;
		--data-file) lopts[data-file]=${args[$num+1]//\'/} ;;
		--data-dir) lopts[data-dir]=${args[$num+1]//\'/} ;;
		--verbose) ;;
		--version) ;;
		--help) ;;
		--) lopts[args]="${args[@]:$num+1}" ;;
		*) ;;
	esac
	((num++))
done
echo "All depois: $@."


for i in "${!lopts[@]}"; do
	echo "$i => ${lopts[$i]}."
done
