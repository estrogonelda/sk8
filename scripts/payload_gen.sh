#!/bin/bash
############################## HEADER ##########################################
#
# Template for a general script.
#
#
# Usage:
#
#    $ ./payload_gen.sh <tumor_type> <manifest_type>
#
# Arguments:
#
#    1. Tumor type id from TCGA. ("BRCA", "COAD", etc)
#    2. Manifest type. ("WGS", "WXS", "WTS" or "TC")
#
################################################################################



############################## PREAMBLE ########################################

# Define a help message.
usage () {
	cat << EOF

Usage:
	
	\$ $0 <tummor_type> <manifest_type>
EOF
	return
}


# Verify input arguments number.
if [ $# -lt 2 ]; then
	echo -e "ERROR: Input arguments are missing (there must be at least 2)." >&2
	usage >&2
	exit 1
fi



for fl in $@; do
	if [ -z "$fl" ]; then
		echo -e "ERROR: Input argument is empty" >&2
		exit 1
	fi
done


# Verify external requisisites: GNU parallel and curl.
[ -n "$(which parallel 2> /dev/null)" -a -n "$(which curl 2> /dev/null)" ] ||
	{ echo "Some external requisites are pending: 'parallel' or 'curl'." >&2; exit 1; }



############################## MAIN ############################################

# Process init message.
#echo -e "Beginning process: $0." >&2

# Choose between specific tumor type or all.
opt="
            {
                \"op\":\"=\",
                \"content\":{
                    \"field\":\"cases.project.project_id\",
                    \"value\":[\"TCGA-$1\"]
                 }
            },";
[ "$1" == "all" ] && opt=""


# Payload file templates.
wgs_manifest () {
	cat <<- EOF
{
    "filters":{
        "op":"and",
        "content":[
            {
                "op":"=",
                "content":{
                    "field":"cases.project.program.name",
                    "value":["TARGET"]
                }
            },$opt
            {
                "op":"in",
                "content":{
                    "field":"cases.samples.sample_type",
                    "value":["primary tumor","blood derived normal"]
                }
            },
            {
                "op":"=",
                "content":{
                    "field":"files.data_format",
                    "value":["bam"]
                }
            },
            {
                "op":"=",
                "content":{
                    "field":"files.data_category",
                    "value":["sequencing reads"]
                }
            },
            {
                "op":"=",
                "content":{
                    "field":"files.data_type",
                    "value":["Aligned Reads"]
                }
            },
            {
                "op":"=",
                "content":{
                    "field":"files.experimental_strategy",
                    "value":["WGS"]
                }
            }
        ]
    },
    "format":"tsv",
    "fields":"file_id,file_name,cases.case_id,cases.samples.sample_type,cases.project.project_id",
    "size":"90000"
}
EOF
	
	return
}

wxs_manifest () {
	cat <<- EOF
{
    "filters":{
        "op":"and",
        "content":[
            {
                "op":"=",
                "content":{
                    "field":"cases.project.program.name",
                    "value":["TCGA"]
                }
            },$opt
            {
                "op":"in",
                "content":{
                    "field":"cases.samples.sample_type",
                    "value":["primary tumor","blood derived normal"]
                }
            },
            {
                "op":"=",
                "content":{
                    "field":"files.data_format",
                    "value":["bam"]
                }
            },
            {
                "op":"=",
                "content":{
                    "field":"files.data_category",
                    "value":["sequencing reads"]
                }
            },
            {
                "op":"=",
                "content":{
                    "field":"files.data_type",
                    "value":["Aligned Reads"]
                }
            },
            {
                "op":"=",
                "content":{
                    "field":"files.experimental_strategy",
                    "value":["WXS"]
                }
            }
        ]
    },
    "format":"tsv",
    "fields":"file_id,file_name,cases.case_id,cases.samples.sample_type,cases.project.project_id",
    "size":"90000"
}
EOF
	
	return
}

wts_manifest () {
	cat <<- EOF
{
    "filters":{
        "op":"and",
        "content":[
            {
                "op":"=",
                "content":{
                    "field":"cases.project.program.name",
                    "value":["TCGA"]
                }
            },$opt
            {
                "op":"in",
                "content":{
                    "field":"cases.samples.sample_type",
                    "value":["primary tumor","solid tissue normal"]
                }
            },
            {
                "op":"=",
                "content":{
                    "field":"files.data_format",
                    "value":["bam"]
                }
            },
            {
                "op":"=",
                "content":{
                    "field":"files.data_category",
                    "value":["sequencing reads"]
                }
            },
            {
                "op":"=",
                "content":{
                    "field":"files.data_type",
                    "value":["Aligned Reads"]
                }
            },
            {
                "op":"=",
                "content":{
                    "field":"files.experimental_strategy",
                    "value":["RNA-Seq"]
                }
            }
        ]
    },
    "format":"tsv",
    "fields":"file_id,file_name,cases.case_id,cases.samples.sample_type,cases.project.project_id",
    "size":"90000"
}
EOF
	
	return
}


tc_manifest () {
	cat <<- EOF
{
    "filters":{
        "op":"and",
        "content":[
            {
                "op":"=",
                "content":{
                    "field":"cases.project.program.name",
                    "value":["TCGA"]
                }
            },$opt
            {
                "op":"in",
                "content":{
                    "field":"cases.samples.sample_type",
                    "value":["primary tumor","solid tissue normal"]
                }
            },
            {
                "op":"=",
                "content":{
                    "field":"files.analysis.workflow_type",
                    "value":["HTSeq - Counts"]
                }
            },
            {
                "op":"=",
                "content":{
                    "field":"files.data_category",
                    "value":["transcriptome profiling"]
                }
            },
            {
                "op":"=",
                "content":{
                    "field":"files.data_type",
                    "value":["Gene Expression Quantification"]
                }
            },
            {
                "op":"=",
                "content":{
                    "field":"files.experimental_strategy",
                    "value":["RNA-Seq"]
                }
            }
        ]
    },
    "format":"tsv",
    "fields":"file_id,file_name,cases.case_id,cases.samples.sample_type,cases.project.project_id",
    "size":"90000"
}
EOF
	
	return
}


# Create payload file.
echo -e "Creating payload file for manifest generation of '$1' tumor type." >&2
case $2 in
	WGS|wgs)
			wgs_manifest $1 ;;
	WXS|wxs)
			wxs_manifest $1 ;;
	WTS|wts)
			wts_manifest $1 ;;
	TC|tc)
			tc_manifest $1 ;;
	*)
			echo -e "ERROR: Invalid manifest type option '$2'." >&2
			exit 1
			;;
esac
echo -e "Done." >&2


# Process final message.
#echo -e "\nFinished process: $0." >&2
