#!/bin/bash
############################## HEADER #########################################
# A General Bash script template.
###############################################################################



############################## PREAMBLE #######################################

usg () {
	echo -e "Usage:
		
	\$ $(basename $0) [options ...] [cmd ...] [args ...]\n" >&2
	
	return
}

err () {
	echo -e "ERROR: $(basename $0): $1.\n" >&2
	
	exit 1
}

wrn () {
	echo -e "WARNING: $(basename $0): $1.\n" >&2
	
	return
}

msg () {
	echo -e "MESSAGE: $(basename $0): $1.\n"
	
	return
}


# Parse arguments.
usg
msg asdcasdc 4
echo "ahh $?."



############################## MAIN ###########################################

obj = new "$@"
############################## DOCUMENTATION ##################################
