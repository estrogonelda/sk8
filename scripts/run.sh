#!/bin/bash


[ -z "$1" -o -z "$2" ] && exit 1;

rootdir=/run/media/leonel/43697b55-725f-4c06-ab74-35e2a4ff375b/Workbench/SK8
datadir=/run/media/leonel/43697b55-725f-4c06-ab74-35e2a4ff375b/Workbench/data


echo "make -f $rootdir/SK8.mk
	module=$rootdir/modules/biotools.mk
	datatadir=$datadir
	annotation_ref=$datadir/gencode/gencode.v22.annotation.gtf
	genome_ref=$datadir/misc/hg38.fa
	blacklist=$datadir/misc/blacklist.bed
	tumor_type=$1
	data_type=wxs
	data_control=\"blood derived normal\"
	datamode=groups
	pairing=yes
	gdc_token=$datadir/gdc/gdc_token.txt
	$2"

make -f $rootdir/SK8.mk \
	module=$rootdir/modules/biotools.mk \
	datatadir=$datadir \
	annotation_ref=$datadir/gencode/gencode.v22.annotation.gtf \
	genome_ref=$datadir/misc/hg38.fa \
	blacklist=$datadir/misc/blacklist.bed \
	tumor_type=$1 \
	data_type=wxs \
	data_control="blood derived normal" \
	data_mode=groups \
	pairing=yes \
	gdc_token=$datadir/gdc/gdc_token.txt \
	$2
