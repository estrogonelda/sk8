#!/bin/bash
############################## HEADER ##########################################
# Template for a general script.
#
#
# Usage:
#
#    $ ./source <tumor_type> <lists_dir>
#
# Arguments:
#
#    1. Tumor type (e.g. "GBM", "COAD", "BLCA").
#    2. GO file (GMT).
#
################################################################################



############################## PREAMBLE ########################################

# Define help message.
usage () {
	cat << EOF

Usage:
	
	\$ $0 <tumor_type> <lists_dir> ...
EOF
	return
}



# Verify input arguments.
if [ $# -lt 2 ]; then
	echo -e "ERROR: Input arguments are missing (there must be at least 2)." >&2
	usage >&2
	exit 1
fi



for fl in $@; do
	if [ ! -f "$fl" ]; then
		echo -e "ERROR: Nonexistent file '$fl'" >&2
		#exit 1
	fi
done


# Verify external requisisites: GNU parallel and curl.
[ -n "$(which parallel 2> /dev/null)" -a -n "$(which curl 2> /dev/null)" ] ||
	{ echo "Some external requisites are pending: 'parallel' or 'curl'." >&2; exit 1; }



############################## MAIN ############################################

# Process init message.
echo -e "Beginning process: $0." >&2


# Set maximun number of jobs.
jbs=10
gencode=/run/media/leonel/43697b55-725f-4c06-ab74-35e2a4ff375b/Workbench/data/gencode.v22.annotation.gtf
gmt=/run/media/leonel/43697b55-725f-4c06-ab74-35e2a4ff375b/Workbench/data/c5.gmt
gsia=/run/media/leonel/43697b55-725f-4c06-ab74-35e2a4ff375b/Workbench/.staged/tarballs/gsia/build/usr/local/bin/gsia
universe=universe.txt


# Create manifest file.
echo -e "\nCreating manifest file: '$1.manifest'." >&2
./manifest_gen.sh $1 TP
echo -e "Done." >&2


# Download data.
echo -e "\nDownloading data from TCGA." >&2
echo -e "parallel -j$jbs curl --remote-name --remote-header-name 'https://api.gdc.cancer.gov/data/{}' ::: \$(perl -F'\t' -lane 'next if \$F[0] =~ /id/; print \$F[2]' $1.manifest)" >&2
parallel -j$jbs curl --remote-name --remote-header-name 'https://api.gdc.cancer.gov/data/{}' ::: "$(perl -F'\t' -lane 'next if $F[0] =~ /id/; print $F[2]' $1.manifest)"
echo -e "Done." >&2


# Read '*.gz' files.
echo -e "\nExtracting files." >&2
echo -e "gunzip *.gz" >&2
gunzip *.gz
echo -e "Done." >&2


echo -e "\nCalculating the most expressed gene set in $1." >&2
./GSEA_gen.pl $gencode *.counts | sort -n -k2 > $1.exp 
echo -e "Done." >&2


echo -e "\nGenerating GO lists for Enrichment Analysis." >&2
mkdir -p lists
cp $1.exp lists/${1}_expr.lst
perl -lane '
	open $fh, ">", "lists/$F[0].lst" or die "Cant open file";
	print $fh join("\n", @F[2..$#F]);
	close $fh;
	' $gmt
echo -e "Done." >&2


echo -e "\nDoing GSEA on user list $2." >&2
parallel -j1 "$gsia -i -u $universe -l $2 -f {} >> $1.hyperg" ::: lists/*.lst
echo -e "Done." >&2


echo -e "\nWriting pathways found.." >&2
perl -lane 'next unless $F[0] =~ /^1/; print "$F[1]\t$F[-1]" if $F[-1] < 0.05' $1.hyperg | sort -n -k2 > $1.pathw
echo -e "Done." >&2


# Process final message.
echo -e "\nFinished process: $0." >&2
