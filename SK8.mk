#!/usr/bin/make
############################## HEADER ##########################################
#
# SK8 - A Swiss Knife Makefile utility for Linux command line
#
#
# Metadata:
#
#	Project:		SK8
#	Module:			main
#	File:			SK8.mk
#	Default Goal:	help
#	
#	Source:			https://gitlab.com/estrogonelda/sk8.git
#	
#	Package:		SK8
#	Version:		0.1.0
#	Id:				random
#	License:		GPLv4
#	
#	DocType:		Makefile
#	Title:			SK8
#	Subtitle:		A Swiss Knife Makefile utility for Linux command line
#	Authors:		Jose L. L. Buzzo
#	Organization:	Bards Agrotechnologies
#	Local:			Brazil
#	Year:			2021
#	Publisher:		GitLab
#	DOI:			{}
#
# Requisites:
#
#	which, curl and perl
#
# Usage:
#
#	$ sk8 [--make_option1 ...] [var_option1=val1 ...] [target1 ...]
#
################################################################################





############################## PREAMBLE ########################################

SHELL                          := /bin/bash
#VPATH                          := ./.local:~/.local

# The default goal! Before any module or configuration file inclusion.
help:


# SK8 main variables.
sk8_package                    := SK8
sk8_version                    := 0.1.0
sk8_id                         := $(shell echo $$RANDOM)
sk8_repo                       := https://gitlab.com/estrogonelda/sk8.git

# Sanity paths.
sk8_distname                   := $(sk8_package)-$(sk8_version)
sk8_mainfile                   := $(abspath $(strip $(firstword $(MAKEFILE_LIST))))
sk8_rootdir                    := $(dir $(sk8_mainfile))
sk8_compiler                   := $(sk8_rootdir)/bin/tcc
sk8_modules                    := $(module)
sk8                            := $(MAKE) -f $(sk8_mainfile)
bards                          := estrogonelda



# Common command macros.
USER                           := $(shell whoami)
DATE                           := $(shell date --utc)
ECHO                           := echo -e
MKDIR                          := mkdir -p
PING                           := ping -c1


# Common color macros.
GREEN                          := "\033[0;32m"
YELLOW                         := "\033[0;33m"
RED                            := "\033[0;31m"
RESET                          := "\033[0;m"


# Internal URL adresses for packages known to be generically installable.
# They can have additional dependencies.
hello_url                      := https://ftp.gnu.org/gnu/hello/hello-2.10.tar.gz
tcc_url                        := http://download.savannah.nongnu.org/releases/tinycc/tcc-0.9.27.tar.bz2
gcc_url                        := https://ftp.gnu.org/gnu/gcc/gcc-11.2.0/gcc-11.2.0.tar.gz
make_url                       := https://ftp.gnu.org/gnu/make/make-4.3.tar.gz
zlib_url                       := https://zlib.net/zlib-1.2.11.tar.gz



############################## SYSTEM CHECKS ###################################

# NOTE: The user's system MUST have, at least, the requisites below.
# 1. A terminal emulator: bash (v5.0 for bash or later).
# 2. A build system: GNU make only (v4.1 or later).
# 3. A download utility: curl or wget (v1.20.0 for wget or later).

# NOTE: Also, it is recommend to have any C compilers, Perl and internet
# connection.


# Test for the 'which' command.
$(if $(shell which which 2> /dev/null),,\
	$(error ERROR: System has no 'which' command.))

# Test for the 'perl' command.
$(if $(shell which perl 2> /dev/null),,\
    $(error ERROR: System has no 'perl' command.))

# Test for GNU Make version: must be >= 3.8.
mk := $(shell perl -e 'print 1 if "$(MAKE_VERSION)" >= "3.8"' 2> /dev/null)
$(if $(mk),,$(error ERROR: System has an unsupported version of Make))

# Test for the 'wget' command.
$(if $(shell which wget 2> /dev/null),,\
    $(warning WARNING: System has no 'wget' command.))

# Test for the 'curl' command.
$(if $(shell which curl 2> /dev/null),,\
    $(warning WARNING: System has no 'curl' command.))


# Test for compilers: c99, cc, gcc, g++ and gfortran.
$(if $(shell which cc c99 gcc g++ gfortran clang 2> /dev/null),,\
    $(warning WARNING: System has no compilers at all.))

# Test for downloaders: wget, curl or even git.
$(if $(shell which wget curl git 2> /dev/null),,\
    $(warning WARNING: System has no downloaders at all.))

# Test for internet connection.
#$(if $(or \
#		$(shell wget www.google.com -O- 2> /dev/null),\
#		$(shell curl www.google.com 2> /dev/null)\
#	),,\
#	$(warning WARNING: System has no internet connection.))



############################## CONFIGURATION FILE PARSING ######################

# Configuration files inclusion.
dft_config				:= $(abspath $(strip $(wildcard $(CURDIR)/config.mk)))
cfg						:= $(abspath $(strip $(wildcard $(configfile))))
cfg						:= $(if $(configfile),$(or $(cfg),ERROR),$(dft_config))

ifeq ($(cfg),ERROR)
      $(error ERROR: Nonexistent configuration file '$(configfile)')
endif

ifneq ($(cfg),)
      $(warning MESSAGE: Configuration files included: '$(cfg)'.)
      override configfile := $(cfg)
      include $(configfile)
endif


# NOTE: Optional validation code would go here!
#ifneq($(words $(wildcard $(essentialfiles))),x) ...


# NOTE: From here on, option variables must be assigned with ?= to not overwrite
# previous user defined values.



############################## DEFAULTS ########################################

# Generic package main variables.
package                        ?= mypkg
version                        ?= 0.1.0
id                             ?= $(shell echo $$RANDOM)
repo                           ?=

distname                       ?= $(package)-$(version)
distfile                       ?= $(distname).tar.gz
distdir                        ?= $(if $(package),$(distname),.)


# GNU Standard Paths and other variables.
rootdir                        ?= $(distdir)

srcdir                         ?= $(rootdir)/src
srclibdir                      ?= $(rootdir)/lib
srcincludedir                  ?= $(rootdir)/include
srcconfigdir                   ?= $(rootdir)/config
srcdatadir                     ?= $(rootdir)/data
srcbindir                      ?= $(rootdir)/bin
srccachedir                    ?= $(rootdir)/cache
srcscriptsdir                  ?= $(rootdir)/scripts
srcmodulesdir                  ?= $(rootdir)/modules
srctemplatesdir                ?= $(rootdir)/templates
srccontainerdir                ?= $(rootdir)/containers
srcdocsdir                     ?= $(rootdir)/docs
srctestsdir                    ?= $(rootdir)/tests

testdir                        ?= $(rootdir)/t
builddir                       ?= $(rootdir)/build
containerdir                   ?= $(rootdir)/docker

prefix                         ?= /usr/local
oldincludedir                  ?= /usr/include
user_prefix                    ?= ~/.local
exec_prefix                    ?= $(prefix)
includedir                     ?= $(prefix)/include
localstatedir                  ?= $(prefix)/var
sysconfdir                     ?= $(prefix)/etc
sharedstatedir                 ?= $(prefix)/com
datarootdir                    ?= $(prefix)/share

bindir                         ?= $(exec_prefix)/bin
sbindir                        ?= $(exec_prefix)/sbin
libdir                         ?= $(exec_prefix)/lib
libexecdir                     ?= $(exec_prefix)/libexec
scriptsdir                     ?= $(exec_prefix)/scripts

#datadir                        ?= $(datarootdir)
mandir                         ?= $(datarootdir)/man
infodir                        ?= $(datarootdir)/info
localedir                      ?= $(datarootdir)/locale
lispdir                        ?= $(datarootdir)/emacs/site-lisp
docdir                         ?= $(datarootdir)/doc/$(package)
htmldir                        ?= $(docdir)
dvidir                         ?= $(docdir)
pdfdir                         ?= $(docdir)
psdir                          ?= $(docdir)


# Verify prefix existence.
ifeq ($(abspath $(strip $(wildcard $(dir $(prefix))))),)
      $(error ERROR: Invalid value "$(prefix)" for 'prefix' variable)
endif


# Runtime paths.
workdir                        ?= $(CURDIR)
stageddir                      ?= $(workdir)/.local
tarballsdir                    ?= $(stageddir)/tarballs
installdir                     ?= $(if $(usermode),$(stageddir)/bin,$(bindir))
installdir                     ?= $(if $(DESTDIR),$(DESTDIR)$(prefix)/bin,$(installdir))


# Runtime metadata.
dft_user                       ?= $(USER)
dft_date                       ?= $(DATE)
dft_path                       ?= ./.local/bin


# Main components of a package.
sanityfile                     := $(rootdir)/.$(package)env
readme                         := $(rootdir)/README.md
copying                        := $(rootdir)/COPYING
installdoc                     := $(rootdir)/INSTALL
authorsfile                    := $(rootdir)/AUTHORS
news                           := $(rootdir)/NEWS
changelog                      := $(rootdir)/ChangeLog
cifile                         := $(rootdir)/.gitlab-ci.yml

version_system                 := git
ignorefile                     := $(rootdir)/.gitignore

repository                     := $(source)
tarball                        := $(distfile)
builddir                       := $(distdir)
configurefile                  := $(builddir)/configure
buildfile                      := $(builddir)/Makefile
execfile                       := $(builddir)/$(package)
installfile                    := $(installdir)/$(package)
srcfile                        := $(srcdir)/$(package).c
libfile                        := $(srclibdir)/$(package)/lib$(package).a
headerfile                     := $(srcincludedir)/$(package).h

folders                        := src lib include tests assets scripts


# Generic package documentation variables.
language                       ?= C
name                           ?= Ponga
abstract                       ?= Some awesome text ...
authors                        ?= Bards Agrotechnologies
email                          ?= estrogonelda@gmail.com
repository                     ?= $(source)
dependecies                    ?= zlib


# App Engines metadata.
workflowfile                   ?= $(cifile)

builderize                     ?= yes
buildfile                      ?= Makefile
buildengine                    ?= make
buildhub                       ?= gitlab
tarball                        ?= $(distname)

environize                     ?= yes
envfile                        ?= $(package)env.yml
envengine                      ?= conda
envhub                         ?= anaconda
env                            ?= default_env

conteinerize                   ?= yes
containerfile                  ?= Dockerfile
containerengine                ?= docker
containerhub                   ?= dockerhub
image                          ?= $(bards)/blunt

clouderize                     ?= yes
cloudfile                      ?= .aws
cloudengine                    ?= aws
cloudhub                       ?= amazonhub
instance                       ?= ip



# System utilities' paths.
CC                             ?= $(shell which cc 2> /dev/null)
GCC                            ?= $(shell which gcc 2> /dev/null)
TAR                            ?= $(shell which tar 2> /dev/null)
MAKE                           ?= $(shell which make 2> /dev/null)
CMAKE                          ?= $(shell which cmake 2> /dev/null)
NINJA                          ?= $(shell which ninja 2> /dev/null)
CURL                           ?= $(shell which curl 2> /dev/null)
WGET                           ?= $(shell which wget 2> /dev/null)
GIT                            ?= $(shell which git 2> /dev/null)
PERL                           ?= $(shell which perl 2> /dev/null)
CPAN                           ?= $(shell which cpan 2> /dev/null)
CPANM                          ?= $(shell which cpanm 2> /dev/null)


# Default input file suffix.
mode                           ?= native
sfx                            ?= .tar.gz
dwlr                           ?= $(or $(downloader),$(CURL),$(WGET))
cplr                           ?= $(or $(compiler),$(CC),$(sk8_compiler))
jobs                           ?= 1


# Downloads utility decision:
# $1: Downloader. [WGET curl git custom]
# $2: Action. [DOWNLOAD download_token query status]
# $3: Destination URL.
# $4: Target or source file.
# $5: Extra arg (ex. a custom downloader path).
define downloader_cmd
	case $1 in                                                                 \
		*wget)                                                                 \
			if [ "$2" == "status" ]; then                                      \
				$1 "$3" -O- 2> /dev/null | perl -ne "print if /status...OK/";  \
			elif [ "$2" == "download" ]; then                                  \
				$1 "$3" -O $4 2> /dev/null;                                    \
			elif [ "$2" == "download_token" ]; then                            \
				$1 "$3" -O $4 --header "X-Auth-Token: $$(< $(token))" 2> /dev/null; \
			elif [ "$2" == "query" ]; then                                     \
				$1 "$3" -O $4 --header "Content-Type: application/json" --post-file $5; \
			else                                                               \
				echo "ERROR: Invalid downloader action '$2'." >&2; exit 7;     \
			fi                                                                 \
			;;                                                                 \
		*curl)                                                                 \
			if [ "$2" == "status" ]; then                                      \
				$1 "$3" 2> /dev/null | perl -ne "print if /status...OK/";      \
			elif [ "$2" == "download" ]; then                                  \
				$1 "$3" -L -o $4 2> /dev/null;                                 \
			elif [ "$2" == "download_token" ]; then                            \
				$1 "$3" -L -o $4 -O -J -H "X-Auth-Token: $$(< $(token))" 2> /dev/null; \
			elif [ "$2" == "query" ]; then                                     \
				$1 "$3" -L -o $4 --header "Content-Type: application/json" --request POST -d @$5; \
			else                                                               \
				echo "ERROR: Invalid downloader action '$2'." >&2; exit 7;     \
			fi                                                                 \
			;;                                                                 \
		*git) $1 clone "$3" $4 ;;                                              \
		custom)                                                                \
			[ -x "$1" ] || exit 7;                                             \
			$1 '$3' "$(args)" -o $4;                                           \
			;;                                                                 \
		*) echo "Invalid downloader option '$1'." >&2;                         \
			exit 7;                                                            \
			;;                                                                 \
	esac
endef

define test_bin
	if $2/bin/$1 $3; then                                                      \
		echo -e "\nPackage '$1' PASSED the test.";                             \
	else                                                                       \
		err=$$?;                                                               \
		echo -e "\nERROR: Package '$1' wasn't properly installed.";            \
		echo "Exit code: $$err.";                                              \
		exit 7;                                                                \
	fi
endef

define test_lib
	if echo "int main() {}" | cc -xc - -L$2/lib -l$1 -o /dev/null; then        \
		echo -e "\nPackage '$1' PASSED the test.";                             \
	else                                                                       \
		err=$$?;                                                               \
		echo -e "\nERROR: Package '$1' wasn't properly installed.";            \
		echo "Exit code: $$err.";                                              \
		exit 7;                                                                \
	fi
endef


# App specific tests.
dockerfile_has			:= [[ -f "$(containerfile)" ]] && echo "1"
dockerfile_get			:= $(if $(shell $(dockerfile_has)),,dockerfile_gen)

image_get_mode			:= $(if $(shell $(dockerfile_has)),image_build,image_pull)

image_has				:= echo -n "$$(sed -n '\|^$(image) |{p}' <<< "$$(docker images 2> /dev/null)" 2> /dev/null)" | grep -w 'latest'
image_get				:= $(if $(shell $(image_has)),,$(image_get_mode))

tarball_has				:= [[ -f "$(tarball)" ]] && echo "1"
tarball_get				:= $(if $(shell $(tarball_has)),,$(tarball))

app						:= $(package)
app_has					:= [[ -n "$$(which $(app) 2> /dev/null)" ]] && echo "1"
app_has_mode			:= $(if $(containerize),$(image_has),$(app_has))
app_get_mode			:= $(if $(containerize),$(image_get),app_install)
app_get					:= $(if $(shell $(app_has_mode)),,$(app_get_mode))



############################## INFRASTRUCTURE ##################################

# Default base directory for data encapsulation, if not given.
dft_basedir                    := $(workdir)
bsd                            := $(abspath $(strip $(wildcard $(dir $(basedir)))))
bsd                            := $(if $(basedir),\
			$(if $(bsd),$(abspath $(strip $(basedir))),ERROR),$(dft_basedir))

ifeq ($(bsd),ERROR)
      $(error ERROR: Nonexistent parent directory for '$(basedir)')
endif

ifeq ($(bsd),)
      $(error ERROR: No base directory specified.)
else
      override basedir         := $(bsd)
endif


rnd_str                        := 'asdmkkm_a5*'
ifeq ($(basedir),$(if $(capsule),$(rnd_str),$(workdir)))

# Host base directory (outside the container). Defaults to '$(workdir)'.
h_basedir                      := $(basedir)
h_configdir                    := $(h_basedir)
h_inputsdir                    := $(h_basedir)
h_outputsdir                   := $(h_basedir)
h_datadir                      := $(h_basedir)
h_referencedir                 := $(h_basedir)
h_annotationdir                := $(h_basedir)
h_extradir                     := $(h_basedir)
h_tmpdir                       := $(h_basedir)

# Container base directory: Defaults to '/home'.
c_basedir                      := /home/lion
c_configdir                    := $(c_basedir)
c_inputsdir                    := $(c_basedir)
c_outputsdir                   := $(c_basedir)
c_datadir                      := $(c_basedir)
c_referencedir                 := $(c_basedir)
c_annotationdir                := $(c_basedir)
c_extradir                     := $(c_basedir)
c_tmpdir                       := $(c_basedir)

else

# Host base directory (outside the container). Defaults to '$(workdir)'.
h_basedir                      := $(basedir)
h_configdir                    := $(h_basedir)/config
h_inputsdir                    := $(h_basedir)/inputs
h_outputsdir                   := $(h_basedir)/outputs
h_datadir                      := $(h_basedir)/data
h_referencedir                 := $(h_basedir)/reference
h_annotationdir                := $(h_basedir)/annotation
h_extradir                     := $(h_basedir)/extra
h_tmpdir                       := $(h_basedir)/tmp

# Container base directory: Defaults to '/home'.
c_basedir                      := /home/lion
c_configdir                    := $(c_basedir)/config
c_inputsdir                    := $(c_basedir)/inputs
c_outputsdir                   := $(c_basedir)/outputs
c_datadir                      := $(c_basedir)/data
c_referencedir                 := $(c_basedir)/reference
c_annotationdir                := $(c_basedir)/annotation
c_extradir                     := $(c_basedir)/extra
c_tmpdir                       := $(c_basedir)/tmp

endif


# Environment determination (container or host).
switch                         := $(shell [ -f "/.dockerenv" ] && echo "1")


# NOTE: Inside a container, $(workdir), $(stageddir) and $(installdir) were auto
# setted but they need to be maped.
c_stageddir                    ?= $(c_basedir)/.local

# A common internal representation for paths, based on 'switch'.
configdir                      ?= $(if $(switch),$(c_configdir),$(h_configdir))
inputsdir                      ?= $(if $(switch),$(c_inputsdir),$(h_inputsdir))
outputsdir                     ?= $(if $(switch),$(c_outputsdir),$(h_outputsdir))
datadir                        ?= $(if $(switch),$(c_datadir),$(h_datadir))
referencedir                   ?= $(if $(switch),$(c_referencedir),$(h_referencedir))
annotationdir                  ?= $(if $(switch),$(c_annotationdir),$(h_annotationdir))
extradir                       ?= $(if $(switch),$(c_extradir),$(h_extradir))
tmpdir                         ?= $(if $(switch),$(c_tmpdir),$(h_tmpdir))


ifeq ($(basedir),$(if $(capsule),$(rnd_str),$(workdir)))
infrastructure                 := $(stageddir) $(tarballsdir)
else
infrastructure                 := $(stageddir) $(tarballsdir)
infrastructure                 += $(configdir) $(inputsdir) $(outputsdir) $(datadir)
infrastructure                 += $(referencedir) $(annotationdir) $(extradir) $(tmpdir)
endif



############################## INPUT TREATMENT #################################

# NOTE 1: There are two mutual exclusive ways for the user to populate the final
# internal input-list variable $(inputfl):
# 1. Using the default behavior, which is to search for suffix containing files
#    (e.g. `ls *$(sfx)`, with sfx=".json" by default) in the default inputs
#    directory $(inputsdir) which is, by default, set to $(workdir).
# 2. Setting one or more of the four input variables available, whose values
#    will be previously expanded, validated and concatenated:
#    * input-file
#    * input-dir
#    * input-list
#    * input


# Variable 'input' just take a list of terms and filter by the $(sfx) pattern
# in it.
tmp						:= $(filter %$(sfx), $(input))
tmp						:= $(if $(input),$(if $(tmp),$(tmp),ERROR),)
acm						:= $(tmp)

ifeq ($(tmp),ERROR)
      $(error ERROR: No '$(sfx)' terms supplied in variable 'input')
endif


# Variable 'input-file' take a list of files and filter for existent $(sfx)
# file references inside them.
# ATTENTION: Do not use the 'foreach' command. Never!
tmp						:= $(abspath $(strip $(wildcard $(input-file))))
tmp						:= $(if $(tmp),$(filter %$(sfx), $(shell cat $(tmp))),)
tmp						:= $(abspath $(strip $(wildcard $(tmp))))
tmp						:= $(if $(input-file),$(if $(tmp),$(tmp),ERROR),)
acm						+= $(tmp)

ifeq ($(tmp),ERROR)
      $(error ERROR: No '$(sfx)' files exists referenced from those in variable 'input-file')
endif


# Variable 'input-dir' take a list of directories and search for existent $(sfx)
# files inside them.
tmp						:= $(abspath $(strip $(wildcard $(input-dir)/*$(sfx))))
tmp						:= $(if $(input-dir),$(if $(tmp),$(tmp),ERROR),)
acm						+= $(tmp)

ifeq ($(tmp),ERROR)
      $(error ERROR: No '$(sfx)' files exists in the directories listed in variable 'input-dir')
endif


# Variable 'input-list' take a list of terms and filters for existent $(sfx)
# files among them.
tmp						:= $(abspath $(strip $(wildcard $(filter %$(sfx), $(input-list)))))
tmp						:= $(if $(input-list),$(if $(tmp),$(tmp),ERROR),)
acm						+= $(tmp)

ifeq ($(tmp),ERROR)
      $(error ERROR: No '$(sfx)' files exists among those listed in variable 'input-list')
endif


# Search $(sfx) files in current directory if no one else is given.
tmp						:= $(abspath $(strip $(wildcard $(inputsdir)/*$(sfx))))
inputfl					:= $(or $(strip $(acm)), $(tmp))


# Final total verification and expansion.
ifeq ($(inputfl),)
      $(warning MESSAGE: No inputs specified.)
endif



############################## GENERAL TARGETS #################################

help: version
	@$(ECHO) "\n\n\
	Usage:\n\
		$$ sk8 [--make_option1 ...] [var_option1=val1 ...] [target1 ...]\n\
	\n\
	Manual:\n\
		$$ sk8 manual\n\
	\n\n\
	NOTE: Here, 'sk8' is an alias to 'make -f /path/to/SK8.mk'."


version:
	@mach=($$(uname -a)); \
	$(ECHO) "\n\
	SK8 0.1.0\n\
	Built for $${mach[-2]} $${mach[-1]}\n\
	\n\
	Copyright (C) Bards Agrotechnologies, 2011-2021.\n\
	License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>\n\
	This is free software you are free to change and redistribute it.\n\
	There is NO WARRANTY, to the extent permitted by law.\n\
	\n\
	Please, visit $(sk8_repo) for full documentation."


manual:
	@$(ECHO) "\n\
	Sorry! Target 'manual' was not implemented yet.\n\
	\n\
	Try:\n\
		$$ make -f SK8.mk help"


test:
	@$(ECHO) "\nThis is a simple test." >&2
	@if [[ -n "$(args)" ]]; then \
		$(ECHO) "\n\tInput arguments: \"$(args)\"."; \
	else \
		$(ECHO) "\n\tInput arguments: none."; \
	fi


diagnostics:
	@mach=($$(uname -a)); \
	$(ECHO) "\n\
				System Diagnostics\n\n\
	System:\n\
		system:                    $${mach[0]}\n\
		kernel:                    $${mach[2]}\n\
		arch:                      $${mach[-2]}\n\
		host:                      $${mach[1]}\n\
		date:                      $$(date)\n\
	User:\n\
		user:                      $$(whoami)\n\
		user_id:                   $$(id -u)\n\
		group_id:                  $$(id -g)\n\
		groups:                    $$(groups)\n\
		home:                      $$HOME\n\
	Shells:\n\
		sh:                        $$(which sh 2> /dev/null)\n\
		bash:                      $$(which bash 2> /dev/null)\n\
		zsh:                       $$(which zsh 2> /dev/null)\n\
		csh:                       $$(which csh 2> /dev/null)\n\
		fish:                      $$(which fish 2> /dev/null)\n\
	Commands:\n\
		which:                     $$(which which 2> /dev/null)\n\
		less:                      $$(which less 2> /dev/null)\n\
		install:                   $$(which install 2> /dev/null)\n\
		docker:                    $$(which docker 2> /dev/null)\n\
		conda:                     $$(which conda 2> /dev/null)\n\
		aws:                       $$(which aws 2> /dev/null)\n\
	Compilers:\n\
		c99:                       $$(which c99 2> /dev/null)\n\
		cc:                        $$(which cc 2> /dev/null)\n\
		gcc:                       $$(which gcc 2> /dev/null)\n\
		g++:                       $$(which g++ 2> /dev/null)\n\
		gfortran:                  $$(which gfortran 2> /dev/null)\n\
		clang:                     $$(which clang 2> /dev/null)\n\
	Downloaders:\n\
		wget:                      $$(which wget 2> /dev/null)\n\
		curl:                      $$(which curl 2> /dev/null)\n\
		git:                       $$(which git 2> /dev/null)\n\
	Builders:\n\
		make:                      $$(which make 2> /dev/null)\n\
		cmake:                     $$(which cmake 2> /dev/null)\n\
		meson:                     $$(which meson 2> /dev/null)\n\
		ninja:                     $$(which ninja 2> /dev/null)\n\
	Interpreters:\n\
		perl:                      $$(which perl 2> /dev/null)\n\
		cpan:                      $$(which cpan 2> /dev/null)\n\
		cpanm:                     $$(which cpanm 2> /dev/null)\n\
		python3:                   $$(which python3 2> /dev/null)\n\
		pip3:                      $$(which pip3 2> /dev/null)\n\
		R:                         $$(which R 2> /dev/null)\n\
		php:                       $$(which php 2> /dev/null)\n\
		java:                      $$(which java 2> /dev/null)\n\
	Zippers:\n\
		zip:                       $$(which zip 2> /dev/null)\n\
		unzip:                     $$(which unzip 2> /dev/null)\n\
		gzip:                      $$(which gzip 2> /dev/null)\n\
		gunzip:                    $$(which gunzip 2> /dev/null)\n\
		bzip2:                     $$(which bzip2 2> /dev/null)\n\
		bunzip2:                   $$(which bunzip2 2> /dev/null)\n\
		xz:                        $$(which xz 2> /dev/null)\n\
		unxz:                      $$(which unxz 2> /dev/null)\n\
		7z:                        $$(which 7z 2> /dev/null)\n\
	Development:\n\
		m4:                        $$(which m4 2> /dev/null)\n\
		autoconf:                  $$(which autoconf 2> /dev/null)\n\
		automake:                  $$(which automake 2> /dev/null)\n\
		libtool:                   $$(which libtool 2> /dev/null)\n\
		pkg-config:                $$(which pkg-config 2> /dev/null)\n\
		ldconfig:                  $$(which ldconfig 2> /dev/null)\n\
		ld:                        $$(which ld 2> /dev/null)\n\
	Environment:\n\
		CPATH:                     $$CPATH\n\
		LIBRARY_PATH:              $$LIBRARY_PATH\n\
		LD_LIBRARY_PATH:           $$LD_LIBRARY_PATH\n\
		PERL5LIB:                  $$PERL5LIB\n\
		PATH:                      $$PATH"


.PHONY: version manual test



############################## UTILITY TARGETS #################################

# Internet.
internet_status                := $(if $(shell $(dwlr) www.google.com 2> /dev/null),,\
									internet_configure)

internet_configure:
	@$(ECHO) "\nERROR: You MUST have internet access to proceed." >&2
	exit 7


# GNU Compiler Collection.
cc_status                      := $(if $(CC),,cc_install)
gcc_status                     := $(if $(GCC),,gcc_install)
compiler_status                := $(if $(cplr),,$(gcc_status))
downloader_status              := $(if $(dwlr),,$($(dwlr)_install))

gcc_install: $(sk8_rootdir)/bin/tcc
	@$(ECHO) "\nGenerating link for compiler '$<' ..." >&2
	ln -s $< $(installdir)/cc
	@$(ECHO) "Done." >&2


# SK8 module creation.
autoclone:
	@$(ECHO) "\nCloning 'SK8' from '$(sk8_repo)'." >&2
	$(WGET) $(basename $(sk8_repo))/-/raw/master/SK8.mk -O SK8_new.mk
	@$(ECHO) "Done." >&2


create_module: $(module_name).mk

$(module_name).mk: $(sk8_mainfile)
	@$(ECHO) "\nGenerating module file '$@' ..." >&2
	[ -n "$(module_name)" ] || { echo "Variable 'module_name' cannot be empty." >&2; exit 7; }
	perl -lne \
		'$$a-- if /^#\<end\>/; print if $$a; $$a++ if /^#\<begin\> GENERIC/' $< > $@
	perl -i -lpe 's/generic/$(notdir $(basename $@))/g' $@
	@$(ECHO) "Done." >&2


run: | $($(mode)_status)
	@$(ECHO) "\nRunning command '$(args)' in '$(mode)'." >&2
	@if [ "$(mode)" == "$(container)" ]; then \
		$(MAKE) -f $(firstword $(MAKEFILE_LIST)) -s $(mode)_run args="$(args)"; \
	elif [ "$(mode)" == "$(venv)" ]; then \
		$(MAKE) -f $(firstword $(MAKEFILE_LIST)) -s $(mode)_run args="$(args)"; \
	elif [ "$(mode)" == "native" ]; then \
		$(MAKE) -f $(firstword $(MAKEFILE_LIST)) -s $(mode)_run args="$(args)"; \
	else \
		echo "ERROR: Invalide mode '$(mode)'." >&2; \
		exit 7; \
	fi
	@$(ECHO) "Done." >&2


native_run:
	@$(ECHO) "\nNatively Running command '$(args)'." >&2
	@[ -n "$(args)" ] || ( echo "ERROR: Variable 'args' is empty. User must provide a command to run." >&2; exit 7; )
	@$(args); \
	[ $$? -eq 0 ] || ( echo "ERRROR: Command '$(args)' failed." >&2; exit 7 )
	@$(ECHO) "Done." >&2


native_test_build:
	#native run ...


download: | $(internet_status) $($(mode)_status)
	@$(ECHO) "\nDownloading with '$(dwlr)' from URL '$(args)'." >&2
	[ -n "$(args)" ] || ( echo "ERROR: Variable 'args' is empty. User must provide an URL to download from." >&2; exit 7 )
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) $(mode)_run args="$(dwlr) $(args)"
	@$(ECHO) "Done." >&2



############################## PACKAGE CREATION ################################

push_dist: $(dist) | $(git_status)
	@$(ECHO) "\nPush package distribution to '$@' for package '$(distname)'." >&2
	[ -n "$(source)" ] || ( echo "ERROR: Source is empty. There must be a remote to push to." >&2; exit 7; )
	$(GIT) push
	@$(ECHO) "Done." >&2


create_tarball tarball: $(tarball)

$(tarball): $(dist)
	@$(ECHO) "\nCreating tarball '$@' for package '$(distname)'." >&2
	tar -czvf $@ $<
	@$(ECHO) "Done." >&2


create_dist dist: $(dist)

$(dist): $(components) | $(distdir)
	@$(ECHO) "\nCreating package distribution '$(distname)'." >&2
	case "$(language)" in \
		C) \
			echo "Under development" >&2; \
			exit 7; \
			;; \
		perl) \
			module-starter --module=$(package) --author=$(authors) \
				--email=$(email); \
			;; \
		python) ;; \
		php) ;; \
		R) ;; \
		bash) ;; \
		*) \
			echo "ERROR: Invalid language option '$(language)'." >&2; \
			exit 7; \
			;; \
	esac
	touch $@
	@$(ECHO) "Done." >&2


create_package_empty: $(readme)

$(readme): $(readme_tt) | $(distdir) $(perl_Template_status)
	@$(ECHO) "\nCreating a minimal package environment in directory '$(distdir)'." >&2
	if [ -n "$(fill_templates)" ]; then \
		perl -I~/perl5/lib/perl5 -MTemplate -le \
			'print "Under development ..."' $<; \
	else \
		touch $@; \
	fi
	@$(ECHO) "Done." >&2


create_package_dir dir: $(distdir)

# Infrastructure creation.
$(infrastructure) $(installdir) $(distdir) $(srctestsdir):
	$(ECHO) "\nCreating directory '$@' ..." >&2
	$(MKDIR) $@
	$(ECHO) "Done." >&2



############################## GENERIC PACKAGE #################################

# Default example for a 'generic' package: GNU hello v2.10.
src                            ?= $(hello_url)
ext                            ?= .tar.gz


#<begin> GENERIC
############################## HEADER ##########################################
# SK8 module for 'generic' package.
################################################################################



############################## PREAMBLE ########################################
# Non-standalone run verification.
this                           ?= generic.mk
ifeq ($(notdir $(firstword $(MAKEFILE_LIST))),$(this))
      $(error ERROR: Module '$(this)' cannot run in standalone mode, \
      it needs to be loaded by 'SK8')
endif

# Info to download, configure, build, install and use 'generic' package.
generic_src                    ?= $(src)
generic_ext                    ?= $(ext)

generic_package                ?= $(shell perl -ne 'print "@{[ (split(/-/, $$_))[0] ]}"' <<< "$(notdir $(generic_src))")
generic_version                ?= $(shell perl -pe 's/^$(generic_package)-(.*?)\$(generic_ext)/$$1/g' <<< "$(notdir $(generic_src))")
generic_id                     ?= $(generic_package)-$(shell echo $$RANDOM)
generic_distname               ?= $(generic_package)-$(generic_version)


# Intended installation.
generic_repository             ?= $(generic_src)
generic_tarball                ?= $(tarballsdir)/$(generic_distname)$(generic_ext)
generic_builddir               ?= $(tarballsdir)/$(generic_distname)
generic_configurefile          ?= $(generic_builddir)/configure
generic_buildfile              ?= $(generic_builddir)/Makefile
generic_execfile               ?= $(generic_builddir)/$(generic_package)

generic_prefix                 ?= $(if $(supprefix),$(dir $(installdir))/$(generic_package),$(dir $(installdir)))
generic_install_bin            ?= $(generic_prefix)/bin/$(generic_package)
generic_install_lib            ?= $(generic_prefix)/lib/lib$(generic_package).a
generic_install_include        ?= $(generic_prefix)/include/$(generic_package).h
generic_install_share          ?= $(generic_prefix)/share/man

generic_bins                   = $(wildcard $(generic_builddir)/bin/*)
generic_libs                   = $(wildcard $(generic_builddir)/lib/lib*)
generic_includes               = $(wildcard $(generic_builddir)/include/*.h)


# Actual installation.
ifeq ($(islib),)
generic_installfile            ?= $(generic_install_bin)
generic_check                  ?= $(shell which $(generic_package) 2> /dev/null)
else
generic_installfile            ?= $(generic_install_lib)
generic_check                  ?= $(abspath $(strip $(wildcard $(basename $(generic_installfile)).*)))
endif

generic_get                    ?= $(if $(generic_check),,generic_install)
generic                        ?= $(or $(generic_check), $(generic_installfile))


# FORCE data.
generic_force                  ?= $(if $(FORCE),generic_force,)
generic_status                 ?= $(if $(FORCE),generic_install,$(generic_get))
generic                        ?= $(if $(FORCE),$(generic_installfile),$(generic))


# Include dependency modules.
#include $(addprefix $(sk8_rootdir)/modules/, gcc.mk)

generic_deps_status            ?= $(if $(recursive),generic_deps,)
generic_deps                   ?= $(gcc_status)



############################## MAIN ############################################

# Using '$(generic_package)' package.
generic_help:
	@$(ECHO) "\n\
	NAME\n\
		\n\
		generic_package - $(generic_package), version $(generic_version)\n\
		\n\
	SYNOPSIS\n\
		\n\
		$$ sk8 [make_option ...] [variable=value ...] <generic_target ...>\n\
		\n\
	DESCRIPTION\n\
		\n\
		This is an engine to download, configure, build, install and test packages\n\
		in a generic way. So, the package must be compliant with the GNU Coding\n\
		Standards in order to be handled by SK8.\n\
		Now, the package in question is '$(generic_distname)'.\n\
		\n\
	OPTIONS\n\
		\n\
		Targets:\n\
		\n\
		generic_help                   Print this help text.\n\
		generic_version                Print '$(generic_package)' package version.\n\
		generic_status                 Verifies '$(generic_package)' current installation status.\n\
		generic_run                    Run '$(generic_package)' with the arguments given in the 'args' variable.\n\
		generic_install                Install '$(generic_package)' binary in '\$$(prefix)/bin' directory.\n\
		generic_build                  Compile the '$(generic_package)' binary in the build directory.\n\
		generic_configure              Configure '$(generic_package)' build files for this system.\n\
		generic_tarball                Download the '$(generic_package)' tarball from its repository: $(generic_src)\n\
		generic_uninstall              Uninstall '$(generic_package)' from the '\$$(prefix)/bin' directory.\n\
		generic_clear                  Remove '$(generic_package)' tarball from tarball''s directory.\n\
		generic_purge                  Uninstall and clear '$(generic_package)' from the system.\n\
		generic_refresh                Uninstall and then re-install '$(generic_package)' again.\n\
		generic_deps                   Install a '$(generic_package)' dependency recursivelly as passed by the 'args' variable.\n\
		generic_test                   Test '$(generic_package)' installation.\n\
		generic_test_build_native      Test '$(generic_package)' build process, natively.\n\
		generic_test_build_conda       Test '$(generic_package)' build process in a new Conda environment.\n\
		generic_test_build_docker      Test '$(generic_package)' build process inside a vanilla Docker container.\n\
		generic_test_build_aws         Test '$(generic_package)' build process inside an AWS instance. NOT WORKING!!\n\
		generic_create_img             Create a Docker image for '$(generic_package)', based on '$(image)' image.\n\
		generic_create_dockerfile      Create a Dockerfile to build an image for '$(generic_package)'.\n\
		generic_create_module          Create a pre-filled SK8 module for '$(generic_package)' package.\n\
		\n\
		conda_run                      Run '$(generic_package)' in a new Conda environment. See 'conda_help' for more info.\n\
		docker_run                     Run '$(generic_package)' in a vanilla Docker image. See 'docker_help' for more info.\n\
		aws_run                        Run '$(generic_package)' in an AWS instance. See 'aws_help' for more info. NOT WORKING!!\n\
		\n\
		Variables:\n\
		\n\
		args                           String of arguments passed to '$(generic_package)' when running.               [ '' ]\n\
		prefix                         Path to the directory where to put the built '$(generic_packages)' binaries.   [ /usr/local ]\n\
		DESTDIR                        Path to an alternative staged installation directory.                          [ '' ]\n\
		workdir                        The current directory.                                                         [ . ]\n\
		stageddir                      Path to a staged directory for tarballs and package builds.                    [ ./.local ]\n\
		usermode                       Boolean to switch between local or system-wide installations paths.            [ '' ]\n\
		recursive                      Boolean to enable recursive dependency installations.                          [ '' ]\n\
		FORCE                          Boolean to force a fresh installation over an existent one.                    [ '' ]\n\
		\n\
		src                            URL of the '$(generic_package)' repository.                                    [ '$(generic_src)' ]\n\
		ext                            The tarball extension. Only for extensions other than '.tar.gz'.               [ '.tar.gz' ]\n\
		generic_package                The name of the package, derived from its tarball''s name.                     [ '$(generic_package)' ]\n\
		generic_version                The version of the package, derived from its tarball''s name.                  [ '$(generic_version)' ]\n\
		\n\
		Make options:\n\
		\n\
		\n\
		NOTE: Some packages cannot build generically. So, user must test the chosen target first.\n\
		For a list of known generically installable packages, please read the\n\
		'$(sk8_rootdir)/modules/modules_urls.txt' file.\n\
		\n\
		\n\
	COPYRIGHT\n\
		\n\
		$(sk8_package), version $(sk8_version) for GNU/Linux.\n\
		Copyright (C) Bards Agrotechnologies, 2011-2021.\n\
		\n\
	SEE ALSO\n\
		\n\
		Search for 'help', 'docker_help', 'conda_help' and 'miscellaneus'\n\
		for other sources of command line help or visit <$(basename $(sk8_source))>\n\
		for full documentation."

generic_manual:
	@$(ECHO) "\n$(generic_package) v$(generic_version) for GNU/Linux.\n\
	Please, see <$(generic_src)> for full documentation." >&2

generic_version:
	@$(ECHO) "\n\
	Package:              $(generic_package)\n\
	Version:              $(generic_version)\n\
	Id:                   $(generic_id)\n\
	\n\
	Source:               $(generic_src)\n\
	Installed             $(generic)\n\
	Status:               $(if $(generic_status),Not Installed,Ok)\n\
	Target Installdir:    $(installdir)\n\
	\n\
	Runned with $(sk8_package), version $(sk8_version) for GNU/Linux.\n\
	Copyright (C) Bards Agrotechnologies, 2011-2021." >&2

generic_run: $(generic_requisites) | $(generic_status)
	@$(ECHO) "\nRunning '$(generic)' with arguments '$(args)' ..." >&2
	$(generic) $(args)
	@$(ECHO) "Done." >&2


# Verification.
generic_status: $(generic_status) # 'generic_install' or none.
	@$(ECHO) "\nSystem has '$(generic_package)' up and running." >&2


# Installation.
generic_install: $(generic_installfile) # $(installdir)/$(generic_package)
	@$(ECHO) "\nSystem has '$(generic_package)' in PATH: '$<'." >&2

$(generic_installfile): $(generic_execfile) | $(installdir) # just copy the executable file to $(installdir).
	@$(ECHO) "\nInstalling '$(generic_package)' on directory '$(generic_prefix)/bin' ..." >&2
	$(MAKE) -C $(generic_builddir) install DESTDIR=$(DESTDIR) prefix=$(generic_prefix) -j$(jobs)
	if [ -z "$$(perl -ne 'print if s#.*$(installdir).*#AA#g' <<< $${PATH})" ]; then \
		echo -e "export PATH=$(installdir):\$$PATH" >> ~/.bashrc; \
		source ~/.bashrc; \
		echo -e "WARNING: You may need to run 'source ~/.bashrc' to release the new \$$PATH."; \
	elif [ "$(generic)" != "$(generic_installfile)" ]; then \
		echo "WARNING: Previuos installation of '$(generic_package)' may be shadowing the new one in the \$$PATH."; \
	fi
	touch $@
	@$(ECHO) "Done." >&2


# Build.
generic_build: $(generic_execfile) # $(generic_builddir)/$(generic_package)

$(generic_execfile): $(generic_buildfile) | $(gcc_status) # a Makefile generates the executable file.
	@$(ECHO) "\nBuilding '$(generic_package)' on directory '$(generic_builddir)' ..." >&2
	$(MAKE) -C $(generic_builddir) -j$(jobs)
	touch $@
	@$(ECHO) "Done." >&2


# Configuration.
generic_configure: $(generic_buildfile) # $(generic_builddir)/Makefile

$(generic_buildfile): generic_configure_cmd := ./configure --prefix=$(generic_prefix) $(generic_args)
$(generic_buildfile): $(generic_configurefile)
	@$(ECHO) "\nConfiguring '$(generic_package)' on directory '$(generic_builddir)' ..." >&2
	cd $(generic_builddir); \
	$(generic_configure_cmd)
	touch $@
	@$(ECHO) "Done." >&2

$(generic_configurefile): $(generic_tarball) | $(generic_builddir)
	@$(ECHO) "\nExtracting '$(generic_package)' tarball to directory '$(generic_builddir)' ..." >&2
	if [ "$(generic_ext)" == ".tar.gz" ]; then \
		tar -xzvf $< -C $(generic_builddir) --strip-components=1; \
	elif [ "$(generic_ext)" == ".tar.xz" ]; then \
		tar -xJvf $< -C $(generic_builddir) --strip-components=1; \
	else \
		tar -xvf $< -C $(generic_builddir) --strip-components=1; \
	fi
	[ -s "$@" ] || { echo "ERROR: No file '$@' was extracted." >&2; exit 8; }
	touch $@
	@$(ECHO) "Done." >&2


# Download.
generic_tarball: $(generic_tarball) # $(tarballsdir)/$(generic_tarball).

$(generic_tarball): $(generic_force) | $(tarballsdir) $(internet_status) $(generic_deps_status) # none or 'generic_deps'
	@$(ECHO) "\nDownloading '$(generic_package)' tarball from '$(generic_repository)' ..." >&2
	[ -n "$(generic_repository)" ] || { echo "ERROR: Empty URL for '$(generic_package)' package." >&2; exit 7; }
	#wget -c $(generic_repository) -P $(tarballsdir) -O $@
	$(call downloader_cmd,$(dwlr),download,$(generic_repository),$@,)
	touch $@
	@$(ECHO) "Done." >&2

generic_deps: $(generic_deps)
	@$(ECHO) "\nRecursively installing dependecies for '$(generic_distname)' ..." >&2
	if [ -n "$(args)" ]; then \
		$(MAKE) -f $(firstword $(MAKEFILE_LIST)) recursive= $(args); \
	fi
	@$(ECHO) "Done." >&2

generic_force:
	@$(ECHO) "\nForce reinstallation of '$(generic_package)' on directory '$(installdir)'." >&2
	[ -f "$(generic_tarball)" ] && rm -f $(generic_tarball)
	@$(ECHO) "Done." >&2


# Uninstallation and cleanning.
generic_refresh: generic_uninstall
	if [ -z "$(usermode)" ]; then \
		$(MAKE) -f $(firstword $(MAKEFILE_LIST)) DESTDIR=$(DESTDIR) prefix=$(prefix) generic_install; \
	else \
		$(MAKE) -f $(firstword $(MAKEFILE_LIST)) DESTDIR=$(DESTDIR) prefix=$(prefix) generic_install usermode=1; \
	fi

generic_purge: generic_uninstall generic_clean

generic_uninstall:
	@$(ECHO) "\nUninstalling '$(generic_package)' from directory '$(installdir)' ..." >&2
	[ -d "$(installdir)" ] || exit 1;
	if [ -f "$(generic_buildfile)" ]; then \
		$(MAKE) -C $(generic_builddir) uninstall DESTDIR=$(DESTDIR) prefix=$(dir $(installdir)) -j4; \
	else \
		rm -f $(generic_install_bin); \
		rm -f $(generic_install_include); \
		rm -f $(dir $(generic_install_lib))/pkgconfig/$(generic_package).pc; \
		rm -rf $(basename $(generic_install_lib)).*; \
		rm -rf $(generic_installfile)*; \
	fi
	@$(ECHO) "User must resolve \$$PATH in '~/.bashrc' file." >&2
	@$(ECHO) "Done." >&2

generic_clean:
	@$(ECHO) "\nCleaning '$(generic_package)' from staged directory '$(stageddir)' ..." >&2
	rm -rf $(tarballsdir)/$(generic_package)*
	@$(ECHO) "Done." >&2


# Unit Tests.
generic_test_tarball: generic_tarball
	$(ECHO) "\nTesting tarball integrity of '$(generic_package)' package ..." >&2
	# Dowload and compare md5sum ...
	$(ECHO) "Done." >&2

generic_test_build: generic_build
	$(ECHO) "\nTesting building of '$(generic_package)' package ..." >&2
	if [ -f "$(generic_buildfile)" ]; then \
		if $(MAKE) -C $(generic_builddir) check; then \
			echo -e "\nBuild Test: PASSED!"; \
		else \
			echo -e "\nBuild Test: NOT PASSED!"; \
		fi \
	else \
		if $(generic_execfile) --version; then \
			echo -e "\nBuild Test: PASSED!"; \
		else \
			echo -e "\nBuild Test: NOT PASSED!"; \
		fi \
	fi
	$(ECHO) "Done." >&2

generic_test_install: generic_install
	$(ECHO) "\nTesting installation of '$(generic_distname)' package ..." >&2
	if [ -f "$(generic_buildfile)" ]; then \
		if $(MAKE) -C $(generic_builddir) installcheck; then \
			echo -e "\nBuild Test: PASSED!"; \
		else \
			echo -e "\nBuild Test: NOT PASSED!"; \
		fi \
	else \
		if $(generic_installfile) --version; then \
			echo -e "\nBuild Test: PASSED!"; \
		else \
			echo -e "\nBuild Test: NOT PASSED!"; \
		fi \
	fi >&2
	$(ECHO) "Done." >&2


# Build tests.

generic_test_native: override args := $(args2)
generic_test_native: native_test_build | $(srctestsdir)
	$(ECHO) "\nFinished build test for '$(generic_package)' package in 'native' mode." >&2


generic_test_conda: override args := $(args2)
generic_test_conda: conda_test_build | $(srctestsdir)
	$(ECHO) "\nFinished build test for '$(generic_package)' package in 'conda' mode." >&2


args_docker = $(MAKE) -f $(c_basedir)/SK8/$(notdir $(firstword $(MAKEFILE_LIST))) \
	usermode=1 \
	module=$(module) \
	recursive=$(recursive) \
	islib=$(islib) \
	generic_src=$(generic_src) \
	generic_ext=$(generic_ext) \
	generic_package=$(generic_package) \
	generic_version=$(generic_version) \
	generic_alt_name=$(generic_alt_name) \
	generic_args='$(generic_args)' \
	args='$(or $(args),--version)' \
	generic_test_install

generic_test_docker: override args := $(args_docker)
generic_test_docker: docker_run
	$(ECHO) "\nFinished build test for '$(generic_package)' package in 'docker' mode." >&2


generic_test_all: generic_test_docker generic_test_conda generic_test_native


$(generic_builddir) $(generic_installdir):
	@$(ECHO) "\nCreating directory '$@' ..." >&2
	mkdir -p $@
	@$(ECHO) "Done." >&2


.PHONY: generic_help generic_version generic_manual generic_force



############################## DOCUMENTATION ###################################
# Documentation was already written in the targets above.
#<end> GENERIC

# An example to test samtools recursive installation would be:
#time make -f SK8/SK8.mk generic_test_docker \
#	src=https://github.com/samtools/samtools/releases/download/1.11/samtools-1.11.tar.bz2 \
#	ext=.tar.bz2 \
#	recursive=1 \
#	image=bards/blunt \
#	docker_more_opts="-e CPATH=/home/lion/.local/include -e LIBRARY_PATH=/home/lion/.local/lib" \
#	generic_args="--without-curses --disable-bz2 --disable-lzma" \
#	args="generic_install generic_src=https://zlib.net/zlib-1.2.12.tar.gz generic_ext=.tar.gz generic_package=zlib generic_version=1.2.12 islib=1 generic_args="




############################## DOCKER ##########################################

# Info to download, configure, build, install and use 'generic' package.
docker_src                     ?= https://download.docker.com/linux/static/stable/x86_64/docker-20.10.9.tgz
docker_ext                     ?= .tgz


# Info to download, configure, build, install and use 'docker-19.03.5'.
docker_package                 := $(shell perl -ne 'print "@{[ (split(/-/, $$_))[0] ]}"' <<< "$(notdir $(docker_src))")
docker_version                 := $(shell perl -pe 's/^$(docker_package)-(.*?)\$(docker_ext)/$$1/g' <<< "$(notdir $(docker_src))")
docker_id                      := $(docker_package)-xxxxxx
docker_distname                := $(docker_package)-$(docker_version)
docker_license                 := GPLv4


# Potential installation path.
docker_repository              := $(docker_src)
docker_tarball                 := $(tarballsdir)/$(docker_distname)$(docker_ext)
docker_builddir                := $(tarballsdir)/$(docker_distname)
docker_configurefile           := $(docker_builddir)/containerd
docker_buildfile               := $(docker_builddir)/dockerd
docker_execfile                := $(docker_builddir)/docker
docker_installfile             := $(installdir)/docker
docker_installdaemon           := $(installdir)/dockerd


# Actual installation path.
docker_has                     := $(shell which docker 2> /dev/null)
docker_get                     := $(if $(docker_has),,docker_install)
docker                         := $(if $(docker_has),$(docker_has),$(docker_installfile))

dockerd_has                    := $(shell which dockerd 2> /dev/null)
dockerd_get                    := $(if $(dockerd_has),,docker_install)
dockerd                        := $(if $(dockerd_has),$(dockerd_has),$(docker_installdaemon))

docker_status                  := $(if $(shell docker images 2> /dev/null),,docker_activate)

docker_force                   := $(if $(FORCE),docker_force,)
#docker_status                  := $(if $(FORCE),docker_activate,$(docker_status))
docker_activate                := $(if $(FORCE),docker_install,$(docker_get))

ifneq ($(dockerd),$(docker_installdaemon))
      docker_activate          := docker_install
endif


# Runtime options.
docker_opts                    ?= --rm --name SK8container_$(shell echo $$RANDOM) -ti -u $$(id -u):$$(id -g) -w $(c_basedir) $(docker_more_opts)
ifeq ($(basedir),$(workdir))
  docker_maps                  += -v $(workdir):$(c_basedir)
else
  docker_maps                  += -v $(configdir):$(c_configdir)
  docker_maps                  += -v $(inputsdir):$(c_inputsdir)
  docker_maps                  += -v $(outputsdir):$(c_outputsdir)
  docker_maps                  += -v $(datadir):$(c_datadir)
  docker_maps                  += -v $(extradir):$(c_extradir)
  docker_maps                  += -v /tmp:$(c_tmpdir)
endif
docker_maps                    = -v $(sk8_rootdir):$(c_basedir)/SK8:ro
docker_img                     ?= $(image)
docker_args                    ?= $(args)


# Usage.
docker_run: $(image_get) $(docker_alterate_image) | $(docker_status) $(docker_infra)
	@$(ECHO) "\nRunning Docker image '$(image)' with command: '$(docker_img_args)' ..." >&2
	docker run \
		$(docker_opts) \
		$(docker_maps) \
		$(docker_img) \
		$(docker_args) >&2
	@$(ECHO) "Done." >&2


docker_create_image: docker_opts := --name mycontainer$(shell echo $$RANDOM) -ti -u $$(id -u):$$(id -g) -w $(c_basedir)
docker_create_image: docker_maps := -v $(sk8_rootdir):$(c_basedir)/SK8 $(inspect_map)
docker_create_image: $(image_get) $(docker_img_alt) | $(infrastructure) $(docker_status)
	@$(ECHO) "\nRunning docker image '$(image)' with arguments: '$(args)' ..." >&2
	docker run \
		$(docker_opts) \
		$(docker_more_opts) \
		$(docker_maps) \
		$(docker_img) \
		$(docker_img_args)
	docker commit mycontainer $(docker_img):new
	docker rm mycontainer
	#container_id=$$(perl -lne 'next if /^CONTAINER ID/; @a = split /\s{2,}/, $$_; print $$a[0] if $$a[1] =~ m#$(docker_img)#' <<< $$(docker ps -a))
	#docker commit $${container_id} $(docker_img):new
	#docker rm $${container_id}
	@$(ECHO) "Done." >&2

docker_alterate_image: docker_opts := --name mycontainer -ti -u $$(id -u):$$(id -g) -w $(c_basedir)
docker_alterate_image: $(image_get) | $(docker_status)
	$(ECHO) "Commit the altered image $(IMAGE) as $(IMAGE):alt."
	docker run \
		$(docker_opts) \
		$(docker_more_opts) \
		$(docker_maps) \
		--entrypoint /bin/bash \
		$(docker_img) \
		$(docker_img_args)
	docker commit mycontainer $(docker_img):alt
	docker rm mycontainer
	#container_id=$$(perl -lne 'next if /^CONTAINER ID/; @a = split /\s{2,}/, $$_; print $$a[0] if $$a[1] =~ m#$(docker_img)#' <<< $$(docker ps -a))
	#docker commit $${container_id} $(docker_img):alt
	#docker rm $${container_id}
	$(ECHO) "Done.\n"


image_pull: | $(internet_status) $(docker_status)
	@$(ECHO) "\nPulling Docker image: '$(docker_img)' from DockerHub ..."
	docker pull $(docker_img)
	@$(ECHO) "Done."

image_build: $(containerfile) | $(internet_status) $(docker_status)
	@$(ECHO) "Building image '$(image)' from dockerfile '$(containerfile)' ..."
	[ -f "$<" ] || exit 1
	docker build -t $(docker_img):latest - < $<
	@$(ECHO) "Done.\n"

dockerfile_gen: $(containerfile)
	@$(ECHO) "\nProcessing Dockerfile for '$(app)'.\n"
	@$(ECHO) "Done."


docker_status: $(docker_status) # 'docker_activate' or none.
	@$(ECHO) "\nSystem has '$(docker_package)' up and running." >&2


# Activation and deactivation.
docker_deactivate:
	@$(ECHO) "\nDeactivating '$(docker_package)' and its daemon ($(docker_installdaemon)) ..." >&2
	[ -f "$(docker_installdaemon)" ] \
		|| ( echo "ERROR: Nonexistent file '$(docker_installdaemon)'." >&2; exit 1; )
	-pkill "dockerd"
	rm -f /var/run/docker.pid
	@$(ECHO) "Done." >&2

docker_activate: $(docker_activate) # 'docker_install' or none.
	@$(ECHO) "\nActivating '$(docker_package)' and its daemon ($(docker_installdaemon)) ..." >&2
	groupadd -f -g 9999 docker
	usermod -aG docker $(USER)
	-pkill "dockerd"
	rm -f /var/run/docker.pid
	sleep 2
	$(docker_installdaemon) 2> /dev/null &
	#sudo newgrp docker; exit
	#sudo systemctl enable docker
	if [ "$(dockerd)" != "$(docker_installdaemon)" ]; then \
		echo "WARNING: Previuos installation of '$(docker_package)' may be shadowing the new one in the \$$PATH."; \
	fi
	@$(ECHO) "Done." >&2


# Installation process.
docker_install: $(docker_installfile) # $(installdir)/docker
	@$(ECHO) "\nSystem has '$(docker_package)' and its daemon in PATH: '$<'." >&2

$(docker_installfile): $(docker_execfile) # just copy the executable file to $(installdir).
	@$(ECHO) "\nInstalling 'Docker' on directory '$(installdir)' ..." >&2
	echo "$$(ls $(docker_builddir)/*)" \
		| perl -F/ -ane 'print $$F[-1]' > $(tarballsdir)/docker.manifest;
	install -d $(installdir);
	install -t $(installdir) -m 0755 $(docker_builddir)/*;
	if [ -z "$$(perl -ne 'print if s#.*$(installdir).*#AA#g' <<< $${PATH})" ]; then \
		echo -e "export PATH=$(installdir):\$$PATH" >> ~/.bashrc; \
		source ~/.bashrc; \
		echo -e "WARNING: You may need to run 'source ~/.bashrc' to release the new \$$PATH."; \
	elif [ "$(docker)" != "$(docker_installfile)" ]; then \
		echo "WARNING: Previuos installation of '$(docker_package)' may be shadowing the new one in the \$$PATH."; \
	fi
	@$(ECHO) "Done." >&2


# Build process.
docker_build: $(docker_execfile) # $(docker_builddir)/docker

$(docker_execfile): $(docker_buildfile) # a Makefile generates the executable file.
	@$(ECHO) "\nBuilding 'Docker' on directory '$(docker_builddir)' ..."
	#$(MAKE) -f $< -C $(docker_builddir)
	touch $@
	@$(ECHO) "Done."


# Configuration process:
docker_configure: $(docker_buildfile) # $(docker_builddir)/configure

$(docker_buildfile): $(docker_configurefile)
	@$(ECHO) "\nConfiguring 'Docker' on directory '$(docker_builddir)' ..."
	#cd $(docker_builddir) && ./$(notdir $<)
	touch $@
	@$(ECHO) "Done."

$(docker_configurefile): $(docker_tarball)
	@$(ECHO) "\nExtracting 'Docker' tarball to directory '$(docker_builddir)' ..."
	mkdir -p $(docker_builddir)
	tar -xvf $< -C $(docker_builddir) --strip-components=1
	touch $@
	@$(ECHO) "Done."


# Tarball download.
docker_tarball: $(docker_tarball) # $(tarballsdir)/$(docker_distname).tgz.
	@$(ECHO) "\nSystem has 'Docker' tarball '$<'."

$(docker_tarball): $(docker_force) | $(tarballsdir) $(internet_status) $(docker_depscheck)
	@$(ECHO) "\nDownloading 'Docker' tarball from '$(docker_repository)' ..."
	wget -c $(docker_repository) -P $(tarballsdir) -O $@
	touch $@
	@$(ECHO) "Done."

docker_force:
	@$(ECHO) "\nForce reinstallation for 'Docker' on directory '$(installdir)'."
	rm -f $(docker_tarball)
	@$(ECHO) "Done."


# Maintenance process.
docker_refresh: docker_uninstall
	if [ -z "$(usermode)" ]; then \
		$(MAKE) -f $(firstword $(MAKEFILE_LIST)) DESTDIR=$(DESTDIR) prefix=$(prefix) docker_activate; \
	else \
		$(MAKE) -f $(firstword $(MAKEFILE_LIST)) DESTDIR=$(DESTDIR) prefix=$(prefix) docker_activate usermode=1; \
	fi


# Uninstall and clean.
docker_purge: docker_uninstall docker_clean

docker_uninstall:
	@$(ECHO) "\nUninstalling '$(docker_package)' from directory '$(installdir)' ..." >&2
	#systemctl disable docker
	[ -d "$(installdir)" ] \
		|| ( echo "ERROR: Nonexistent directory '$(installdir)'." >&2; exit 1; )
	-pkill "dockerd";
	rm -rf /run/docker*;
	rm -rf /var/run/docker*;
	rm -rf /var/lib/docker*;
	rm -rf /usr/lib/systemd/system/docker*;
	rm -rf /etc/systemd/system/docker*;
	if [ -f "$(tarballsdir)/docker.manifest" ]; then \
		while read fl; do \
			rm -f $(installdir)/$${fl}; \
		done < $(tarballsdir)/docker.manifest; \
	else \
		echo "No such file '$(tarballsdir)/docker.manifest'."; \
		rm -f $(installdir)/docker*; \
		rm -f $(installdir)/containerd*; \
		rm -f $(installdir)/ctr $(installdir)/runc; \
	fi
	source ~/.bashrc
	@$(ECHO) "User must resolve \$$PATH in '~/.bashrc' file." >&2
	@$(ECHO) "Done." >&2

docker_clean:
	@$(ECHO) "\nCleaning 'Docker' from staged directory '$(stageddir)' ..." >&2
	rm -rf $(tarballsdir)/docker*
	@$(ECHO) "Done." >&2


# Unit Test.
docker_test: docker_img_args := pwd
docker_test:
	$(ECHO) "\nTesting '$(docker_package)' installation from $(docker) ..." >&2
	log="$$($(docker) run                                                      \
		$(docker_opts)                                                         \
		$(docker_more_opts)                                                    \
		$(docker_img)                                                          \
		$(docker_img_args) 2>&1)";                                             \
	if [[ "$$log" =~ "home" ]]; then                                           \
		echo "Package '$(docker_package)' passed the test.";                   \
	else                                                                       \
		echo "ERROR: Package '$(docker_package)' wasn't properly installed.";  \
		echo -e "Error log:\n$$log";                                           \
		exit 7;                                                                \
	fi >&2
	$(ECHO) "Done." >&2

docker_test_run: docker_maps := -v $(sk8_rootdir):$(c_basedir)/SK8 $(inspect_map)
docker_test_run: $(image_get) $(docker_img_alt) | $(docker_status)
	@$(ECHO) "\nRunning docker image '$(image)' with arguments: '$(args)' ..." >&2
	docker run \
		$(docker_opts) \
		$(docker_more_opts) \
		$(docker_maps) \
		$(docker_img) \
		$(docker_args) >&2
	@$(ECHO) "Done." >&2



############################## MINICONDA3 ######################################

# Info to download, configure, build, install, test and use 'Miniconda3' package.
conda_src                      ?= https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
conda_ext                      ?=

conda_package                  := $(shell perl -ne 'print "@{[ (split(/-/, $$_))[0] ]}"' <<< "$(notdir $(conda_src))")
conda_version                  := 4.10.3
conda_id                       := $(conda_package)-xxxxxx
conda_distname                 := $(conda_package)-$(conda_version)


# Intended installation.
conda_repository               := $(conda_src)
conda_tarball                  := $(tarballsdir)/$(notdir $(conda_src))
conda_builddir                 := $(tarballsdir)/$(conda_distname)
conda_configurefile            := $(conda_builddir)/configure.fk
conda_buildfile                := $(conda_builddir)/Makefile.fk
conda_execfile                 := $(conda_builddir)/conda.fk
conda_prefix                   := $(dir $(installdir))/$(conda_package)
conda_installfile              := $(conda_prefix)/bin/conda


# Actual installation.
conda_check                    := $(shell which conda 2> /dev/null)
conda_get                      := $(if $(conda_check),,conda_install)
conda                          := $(or $(conda_check), $(conda_installfile))


# FORCE data.
conda_force                    := $(if $(FORCE),conda_force,)
conda_status                   := $(if $(FORCE),conda_install,$(conda_get))
conda                          := $(if $(FORCE),$(conda_installfile),$(conda))


# Include dependency modules.
#include $(addprefix $(sk8_rootdir)/modules/, gcc.mk)

conda_deps_status              := $(if $(recursive),conda_deps,)
conda_deps                     :=


# Using 'Miniconda3' package.
conda_env_name                 := $(env)
conda_env_file                 := $(conda_env_name).yml
conda_env_args                 := $(conda_requisites)
conda_env_dir                  := $(or $(wildcard $(dir $(conda))/../envs/$(conda_env_name)),conda_create)
conda_channels                 := conda-forge
conda_opts                     :=
conda_args                     := $(args)


conda_help: | $(conda_status)
	$(conda) --help >&2

conda_version: | $(conda_status)
	$(conda) --version >&2

conda_run: $(conda_requisites) | $(conda_status) $(conda_env_dir)
	@$(ECHO) "\nPreparing conda environment '$(conda_env_name)' to run command '$(conda_run_args)' ..." >&2
	#[ -n "$(conda_run_args)" ] || ( echo "ERROR: Variable '$(conda_run_args)' is empty." >&2; exit 7 )
	$(conda) run \
		-n $(conda_env_name) \
		$(conda_opts) \
		$(conda_args)
	@$(ECHO) "Done." >&2

conda_create: $(conda_env_file) | $(conda_status) $(internet_status)
	@$(ECHO) "\nCreating conda environment '$(conda_env)' with arguments: '$(conda_env_args)' ..." >&2
	[ -d "$(conda_env_dir)" ] && echo "WARNING: Environment '$(conda_env)' will be overwritten." >&2
	if [ -s "$<" ]; then \
		$(conda) create $(conda_opts) -c $(conda_channels) -f $(conda_env_file); \
	elif [ -n "$(conda_env_args)" ]; then \
		$(conda) create $(conda_opts) -c $(conda_channels) $(conda_env_args); \
	else \
		$(conda) create $(conda_opts) -c $(conda_channels) gcc make perl wget curl git; \
	fi
	@$(ECHO) "Use 'conda activate $(conda_env)' to activate the environment." >&2
	@$(ECHO) "Done." >&2

conda_remove: | $(conda_status)
	@$(ECHO) "\nRemoving conda environment '$(conda_env)' ..." >&2
	[ -n "$(conda_opts)" ] || exit 1
	$(conda) remove $(conda_opts) --all
	@$(ECHO) "Done." >&2

conda_activate: $(conda_status)
	@$(ECHO) "\nActivating conda environment '$(conda_env)' ..." >&2
	[ -d "$(dir $(conda))/../envs/$(conda_env)" ] \
		|| ( echo "ERROR: Non-existent environment '$(conda_env)'."; exit 1; )
	$(conda) activate $(conda_env)
	@$(ECHO) "Done." >&2

conda_install_env: $(conda_status)
	@$(ECHO) "\nInstalling on conda environment '$(conda_env)' with arguments: '$(args)' ..." >&2
	[ -n "$(args)" ] || ( echo "ERROR: Variable 'args' is empty."; exit 1 ) >&2
	if [ -d "$(dir $(conda))/../envs/$(conda_env)" ]; then \
		$(conda) install -y -n $(conda_env) -c $(conda_channels) $(args); \
	else \
		$(conda) create -y -n $(conda_env) -c $(conda_channels) $(args); \
	fi
	@$(ECHO) "Use 'conda activate $(conda_env)' to activate the environment." >&2
	@$(ECHO) "Done." >&2


# Verification.
conda_status: $(conda_status) # 'conda_install' or none.
	@$(ECHO) "\nSystem has '$(conda_package)' up and running." >&2


# Installation.
conda_install: $(conda_installfile) # $(installdir)/conda
	@$(ECHO) "\nSystem has '$(conda_package)' in PATH: '$<'."

$(conda_installfile): $(conda_tarball) # just copy the executable file to $(installdir)
	@$(ECHO) "\nInstalling '$(conda_package)' on directory '$(conda_prefix)/bin' ..." >&2
	chmod +x $<
	$< -bf -p $(conda_prefix)
	if [ -z "$$(perl -ne 'print if s#.*$(conda_prefix)/bin.*#AA#g' <<< $${PATH})" ]; then \
		$@ init; \
		source ~/.bashrc; \
		echo -e "WARNING: You may need to run 'source ~/.bashrc' to release the new \$$PATH."; \
	elif [ "$(conda)" != "$(conda_installfile)" ]; then \
		echo "User must resolve \$$PATH for conflicting installations: '$(conda)' != '$(conda_installfile)'."; \
	fi
	touch $@
	@$(ECHO) "Done." >&2


# Build.


# Download.
conda_tarball: $(conda_tarball) # $(tarballsdir)/Miniconda3-latest-Linux-x86_64.sh.
	@$(ECHO) "\nSystem has '$(conda_package)' tarball '$<'."

$(conda_tarball): $(conda_force) | $(tarballsdir) $(internet_status) $(conda_deps_status) # 'conda_deps' or none
	@$(ECHO) "\nDownloading '$(conda_package)' tarball from '$(conda_repository)' ..."
	[ -n "$(conda_repository)" ] || { echo "ERROR: Empty URL for '$(conda_package)' package." >&2; exit 7; }
	#wget -c $(conda_repository) -P $(tarballsdir) -O $@
	$(call downloader_cmd,$(dwlr),download,$(conda_repository),$@,)
	touch $@
	@$(ECHO) "Done."

conda_deps: $(conda_deps)

conda_force:
	@$(ECHO) "\nForce reinstallation of '$(conda_package)' on directory '$(conda_prefix)'." >&2
	[ -f "$(conda_tarball)" ] && rm -f $(conda_tarball)
	@$(ECHO) "Done." >&2


# Uninstallation and cleanning.
conda_refresh: conda_uninstall conda_install

conda_purge: conda_uninstall conda_clean

conda_uninstall:
	@$(ECHO) "\nUninstalling '$(conda_package)' from directory '$(conda_prefix)' ..." >&2
	[ -d "$(conda_prefix)" ] || exit 1;
	rm -rf $(conda_prefix)
	source ~/.bashrc
	@$(ECHO) "User must resolve \$$PATH in '~/.bashrc' file." >&2
	@$(ECHO) "Done." >&2

conda_clean:
	@$(ECHO) "\nCleaning '$(conda_package)' from staged directory '$(stageddir)' ..." >&2
	rm -rf $(tarballsdir)/$(conda_package)*
	@$(ECHO) "Done." >&2


# Unit test.
conda_test:
	@$(ECHO) "\nTesting '$(conda_package)' installation ..." >&2
	$(conda) --version 2> /dev/null; \
	if [ $$? -eq 0 ]; then \
		echo "PASS!"; \
	else \
		echo "Package '$(conda_package)' isn't properly installed."; \
		exit 1; \
	fi
	@$(ECHO) "Done." >&2


# Build test.
conda_test_build: $(conda_requisites) | $(conda_status) $(srctestsdir)
	@$(ECHO) "\nPreparing conda environment '$(conda_env_name)' to run command '$(conda_run_args)' ..." >&2
	#[ -n "$(conda_run_args)" ] || ( echo "ERROR: Variable '$(conda_run_args)' is empty." >&2; exit 7 )
	$(conda) run \
		-n $(conda_env_name) \
		$(conda_opts) \
		$(conda_run_args)
	@$(ECHO) "Done." >&2



############################## MODULE INCLUSIONS ###############################

# NOTE: Module inclusion must be the last thing to hapen.


# Module files validation.
tmp                            := $(abspath $(strip $(wildcard $(module))))
tmp                            := $(if $(module),$(if $(tmp),$(tmp),ERROR),)

ifeq ($(tmp),ERROR)
      $(error ERROR: Nonexistent module file '$(module)')
endif

ifneq ($(tmp),)
      $(warning MESSAGE: Modules included: '$(tmp)'.)
      include $(tmp)
endif

tmp                            :=


ifneq ($(biotools),)
      $(warning MESSAGE: Module included 'biotools.mk'.)
      include $(sk8_rootdir)/modules/biotools.mk
endif



############################## DEBUG CODE ######################################

ifeq ($(dbg_general),yes)
$(info )
$(info ****************************** DEBUG GENERAL ***********************************)
$(info Runtime data.)
$(info User:                          $(USER).)
$(info Date:                          $(DATE).)
$(info Command Line:                  $(CMD).)
$(info )

$(info General App data.)
$(info package:                       $(package).)
$(info version:                       $(version).)
$(info id:                            $(id).)
$(info distname:                      $(distname).)
$(info license:                       $(license).)
$(info )

$(info Source url.)
$(info src:                           $(src).)
$(info source:                        $(source).)
$(info )

$(info LEVEL: $(MAKELEVEL))
$(info ********************************************************************************)
endif

ifeq ($(dbg_engine),yes)
$(info )
$(info ****************************** DEBUG ENGINE ************************************)
$(info configfile: $(configfile).)
$(info builderize: $(builderize).)
$(info builder: $(builder).)
$(info buildfile: $(buildfile).)
$(info buildrepository: $(buildrepository).)
$(info tarball: $(tarball).)
$(info conteinerize: $(containerize).)
$(info container: $(container).)
$(info containerfile: $(containerfile).)
$(info containerhub: $(containerhub).)
$(info image: $(image).)
$(info clouderize: $(clouderize).)
$(info clouder: $(clouder).)
$(info cloudfile: $(cloudfile).)
$(info cloud: $(cloud).)
$(info bucket: $(bucket).)
$(info subprojects: $(subprojects).)
$(info dependecies: $(dependencies).)
$(info )

$(info LEVEL: $(MAKELEVEL))
$(info ********************************************************************************)
endif

ifeq ($(dbg_paths),yes)
$(info )
$(info ****************************** DEBUG PATHS *************************************)
$(info General paths.)
$(info CURDIR:                        $(CURDIR).)
$(info PWD:                           $(PWD).)
$(info ./:                            $(abspath .).)
$(info workdir:                       $(workdir).)
$(info sk8_rootdir:                   $(sk8_rootdir).)
$(info sk8_modules:                   $(MAKEFILE_LIST).)
$(info LD_LIBRARY_PATH:               $(LD_LIBRARY_PATH))
$(info PATH:                          $(PATH))
$(info )

$(info Build paths.)
$(info DESTDIR:                       $(DESTDIR).)
$(info prefix:                        $(prefix).)
$(info installdir:                    $(installdir).)
$(info stageddir:                     $(stageddir).)
$(info rootdir:                       $(rootdir).)
$(info srcdir:                        $(srcdir).)
$(info builddir:                      $(builddir).)
$(info datadir:                       $(datadir).)
$(info )

$(info Environment paths.)
$(info switch:                        $(switch).)
$(info capsule:                       $(capsule).)
$(info basedir:                       $(basedir).)
$(info configdir:                     $(configdir).)
$(info inputsdir:                     $(inputsdir).)
$(info outputsdir:                    $(outputsdir).)
$(info datadir:                       $(datadir).)
$(info referencedir:                  $(referencedir).)
$(info annotationdir:                 $(annotationdir).)
$(info extradir:                      $(extradir).)
$(info tmpdir:                        $(tmpdir).)
$(info infrastructure:                $(infrastructure).)
$(info )

$(info Runtime data.)
$(info default goal:                  $(DEFAULT_GOAL).)
$(info FORCE:                         $(FORCE).)
$(info Command Line:                  $(CMD).)
$(info )

$(info Configuration files.)
$(info configfile: $(configfile).)
$(info dft_config: $(dft_config).)
$(info cfg: $(cfg).)
$(info )

$(info Input data.)
$(info suffix:                        $(sfx).)
$(info inputfl:                       $(inputfl).)
$(info args:                          $(args).)
$(info )

$(info Misc data.)
$(info sdfvsdfv...)
$(info )

$(info LEVEL: $(MAKELEVEL))
$(info ********************************************************************************)
endif

ifeq ($(dbg_env),yes)
$(info )
$(info ****************************** DEBUG ENV ***************************************)
$(info Environment variables.)
$(info ARCH:                          $(shell uname -a).)
$(info SHELL:                         $(SHELL).)
$(info MAKE:                          $(MAKE).)
$(info CC:                            $(CC).)
$(info PATH:                          $(PATH).)
$(info )

$(info System tools.)
$(info which:                         $(shell which which 2> /dev/null).)
$(info sh:                            $(shell which sh 2> /dev/null).)
$(info bash:                          $(shell which bash 2> /dev/null).)
$(info make:                          $(shell which make 2> /dev/null).)
$(info cc:                            $(shell which cc 2> /dev/null).)
$(info gcc:                           $(shell which gcc 2> /dev/null).)
$(info gfortran:                      $(shell which gfortran 2> /dev/null).)
$(info ninja:                         $(shell which ninja 2> /dev/null).)
$(info wget:                          $(shell which wget 2> /dev/null).)
$(info curl:                          $(shell which curl 2> /dev/null).)
$(info git:                           $(shell which git 2> /dev/null).)
$(info tar:                           $(shell which tar 2> /dev/null).)
$(info nano:                          $(shell which nano 2> /dev/null).)
$(info perl:                          $(shell which perl 2> /dev/null).)
$(info python:                        $(shell which python 2> /dev/null).)
$(info conda:                         $(shell which conda 2> /dev/null).)
$(info docker:                        $(shell which docker 2> /dev/null).)
$(info Internet Conection:            $(internet).)
$(info )

$(info LEVEL: $(MAKELEVEL))
$(info ********************************************************************************)
endif


ifeq ($(dbg_generic),yes)
$(info )
$(info ****************************** DEBUG GENERIC PACK ******************************)
$(info Precursor variables for a 'generic' package src.)
$(info workdir:                       $(workdir).)
$(info stageddir:                     $(stageddir).)
$(info tarballsdir:                   $(tarballsdir).)
$(info DESTDIR:                       $(DESTDIR).)
$(info installdir:                    $(installdir).)
$(info usermode:                      $(usermode).)
$(info src:                           $(src).)
$(info ext:                           $(ext).)
$(info )

$(info Info to download, configure, build, install and use 'generic' package.)
$(info generic_src:                   $(generic_src).)
$(info generic_ext:                   $(generic_ext).)
$(info )

$(info generic_package:               $(generic_package).)
$(info generic_version:               $(generic_version).)
$(info generic_id:                    $(generic_id).)
$(info generic_distname:              $(generic_distname).)
$(info generic_license:               $(generic_license).)
$(info )

$(info Potential installation data.)
$(info generic_dependecies:           $(generic_dependecies).)
$(info generic_repository:            $(generic_repository).)
$(info generic_tarball:               $(generic_tarball).)
$(info generic_builddir:              $(generic_builddir).)
$(info generic_configurefile:         $(generic_configurefile).)
$(info generic_buildfile:             $(generic_buildfile).)
$(info generic_execfile:              $(generic_execfile).)
$(info generic_installfile:           $(generic_installfile).)
$(info )


$(info Actual installation data.)
$(info generic_has:                   $(generic_has).)
$(info generic_get:                   $(generic_get).)
$(info generic:                       $(generic).)
$(info )


$(info Status and FORCE data.)
$(info generic_force:                 $(generic_force).)
$(info generic_status:                $(generic_status).)
$(info )

$(info LEVEL: $(MAKELEVEL))
$(info ********************************************************************************)
endif


# Emergency estop.
ifeq ($(stop),yes)
    $(error ERROR: Emergency stop)
endif
