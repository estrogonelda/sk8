#!/bin/bash
############################## HEADER #########################################
# Simple script ....
###############################################################################



############################## PREAMBLE #######################################

# Auto referencing variables.
package=`basename $0`
version='0.1.0'
id=$RANDOM


greetings="
===============================================================================
=                              $package                                      =
===============================================================================
v$version

"

usage () {
	echo -e "ERROR: Arguments missing.\n
Usage:

	$ $package [opt1 val1 ...] arg1 ...
	" >&2
	exit 1;
}
[ $# -gt 0 ] || usage

err () {
	echo -e "ERROR: $package => $1\n" >&2
	exit 1;
}

wrn () {
	echo -e "WARNING: $package => $1\n" >&2
}

msg () {
	echo -e "MESSAGE: $package => $1\n" >&2
}


# All pre-defined short options (whole alphabeti, upper and lower case).
shopts="@:?!:_:0:1:2:3:4:5:6:7:8:9:a:b:c:d:e:f:g:hi:j:k:l:m:n:o:p:q:r:s:t:u:vw:x:y:z:A:B:C:D:E:F:G:H:I:J:K:L:M:N:O:P:Q:R:S:T:U:V:W:X:Y:Z:"

# Corresponding pre-defined long options.
declare -A lopts=(
	[auxiliary-file]=""
	[annotation-file]=""
	[bed-file]=""
	[build-file]=""
	[batch-file]=""
	[cfg]=""
	[cmd]=""
	[ci-file]=""
	[cloud-file]=""
	[configuration-file]=""
	[credential-file]=""
	[db]=""
	[document-file]=""
	[database]=""
	[data-file]=""
	[expression]=""
	[extension]=""
	[file]=""
	[filter-file]=""
	[grp]=""
	[group]=""
	[gtf-file]=""
	[help]=""
	[header-file]=""
	[id]=""
	[ip]=""
	[idx]=""
	[index-file]=""
	[input-file]=""
	[include-file]=""
	[jobs]=""
	[key-file]=""
	[list-file]=""
	[library-file]=""
	[mdl]=""
	[module-file]=""
	[merge]=""
	[no]=""
	[name]=""
	[number]=""
	[output-file]=""
	[port]=""
	[passwd]=""
	[project]=""
	[phase]=""
	[quality]=""
	[recursive]=""
	[reference-file]=""
	[src]=""
	[source-file]=""
	[tag]=""
	[type]=""
	[tst]=""
	[tmp]=""
	[temporary-file]=""
	[tarball-file]=""
	[template-file]=""
	[test-file]=""
	[url]=""
	[user]=""
	[unit]=""
	[verbose]=""
	[version]=""
	[volume]=""
	[extra-file]=""
	[yes]=""
	[zip]=""
	[args]=""
	[build]=""
	[build-dir]=""
	[config-dir]=""
	[data]=""
	[data-dir]=""
	[prefix-dir]=""
	)


# Parse CLI options.
parsed=$(getopt \
	--name $package \
	--options $shopts \
	--longoptions $(printf "%s:," ${!lopts[@]}) \
	-- "$@")
[ $? -eq 0 ] || msg "Can not parse options."
eval set -- $parsed
echo "All antes: $@."


num=0
args=($parsed)
for opt in "${args[@]}"; do
	echo "Opcao $opt."
	case $opt in
		-d) lopts[data-file]=${args[$num+1]//\'/} ;;
		-D) lopts[data-dir]=${args[$num+1]//\'/} ;;
		--annotation-file) lopts[annotation-file]=${args[$num+1]//\'/} ;;
		--data-file) lopts[data-file]=${args[$num+1]//\'/} ;;
		--data-dir) lopts[data-dir]=${args[$num+1]//\'/} ;;
		--verbose) ;;
		--version) ;;
		--help) ;;
		--) lopts[args]="${args[@]:$num+1}" ;;
		*) ;;
	esac
	((num++))
done
echo -e "All depois: $@.\n\n"


for k in $(for i in "${!lopts[@]}"; do echo $i; done | sort); do
	;#echo "$k => ${lopts[$k]}."
done
