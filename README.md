<p align="center"><a href="https://sideretro.readthedocs.io/en/latest/?badge=latest"><img src="docs/images/ollie.png" alt="SK8" width="200"></a></p>
<h2 align="center">SK8 - A Swiss Knife suite to manage pipeline environments</h2>

<p align="center">
  <a href="https://travis-ci.org/galantelab/sideRETRO"><img alt="" src="https://travis-ci.org/galantelab/sideRETRO.svg?branch=master" align="center"></a>
  <a href="https://sideretro.readthedocs.io/en/latest/?badge=latest"><img alt="" src="https://readthedocs.org/projects/sideretro/badge/?version=latest" align="center"></a>
  <a href="https://coveralls.io/github/galantelab/sideRETRO?branch=master"><img alt="" src="https://coveralls.io/repos/github/galantelab/sideRETRO/badge.svg?branch=master" align="center"></a>
  <a href="https://hub.docker.com/r/galantelab/sider"><img alt="" src="https://img.shields.io/docker/cloud/build/galantelab/sider?color=blue" align="center"></a>
</p>


**SK8** was first intended to be a helper command line tool on dependency
management for several Bioinformatics pipelines at a backyard level.
Interestingly, it came to the front porch!

It tries to assure that not only the needed system packages are installed, but
also that all the required annotatation and reference files are in their
expected places for a given pipeline. And, based on a language agnostic style
(it's *free style*!), SK8 makes just a few assumptions on the user's system
status, with which can do its *workarounds*.

In Brazil, workarounds are kind of *Behavioral Do*, like in *Karate Do*. So,
when incarned in this computational tool, that concept becames the *SK8 Do* --
the way of elusion!

Moreover, in an [internal flight](https://www.youtube.com/watch?v=6lIt07sBW4E)
it was revealed to this author by an [enlightened being](https://en.wikipedia.org/wiki/Richard_Stallman),
that any tool aiming to help people manage their dependecies must be, not only
language and OS agnostic, but also portable. And, since pipelines almost aways
creates chains of requirements, the unique portable, standard and optimal tool
to make and smartly execute dependecy trees that relies minimally on system's
freshnesses was the powerful [GNU Make](https://www.gnu.org/software/make/)
framework. That was the SK8's keystone.

Therefore, user can easily do simple tricks to have things done. And, even if
the needed functionality isn't implemented yet, the user can create a new SK8
module effortlessly.
The final philosophy is TGRTIDTDEL or *To Give Rise To an Increasing Desire To
Do Even Less*.

For full documentation, please visit <https://estrogonelda.gitlab/sk8>.

**Note:** The author doesn't care if some or all the commands discussed here
doesn't work on Windows.

> One Tool to rule them all,
> One Tool to find them,
> One Tool to bring them all,
> ... and in the darkness BUILD them!
>
> -- paraphrase from Tolkien's *The Lord of the Rings*



## Contents at a Glance
1. Getting Started
2. Maestry Usage
3. Legendary Tales
3. Experts' Opinions
4. See Also
5. Author
6. Copyright



## Getting Started

**SK8** is nothing more than a collection of [GNU Make](https://www.gnu.org/software/make/)
scripts, plain text files with `.mk` extension containing some concatenated
rules. It is funnny the fact that nowadays people don't even realize that
Makefiles were ever runned by their package managers. They were aways there,
and in ancient times of the world's youth it was used to automate UNIX programs'
compilations. And, if Linus Torvalds was the Linux's mother, Make was the
amniotic fluid and placenta (?!). In this context, the [GNU web site](https://www.gnu.org/software/)
is considered some kind of alien giant insect's ovopositor abdomen,
giving birth to wonderful new software!

Appart from breezes, first of all, user MUST install the
[GNU Make](https://www.gnu.org/software/make/) utility as well as these
**essential requisites**: `wget`and `curl` and `perl`. To discover if these are
already installed in the system, user may run this from the command line:
```sh
which make wget curl perl
```
If the command line didn't returned any of them, user must install it in order
to proceed.


### Installation

#### Ordinary downloads (command line deficient users)
Obtaining the SK8 suite is as easy as download an ordinary file. User can simply
click and chose from one of the compressed archive formats in the
[SK8 repository](https://gitlab.com/estrogonelda/sk8) at GitLab and save any
[tarball](https://gitlab.com/estrogonelda/sk8/-/archive/master/sk8-master.tar.gz).
This can be decompressed and extracted normally. The `SK8.mk` main script will
be inside this decompressed folder.

#### Automated downloads (the nerd way)
User can directly download any [tarball](https://gitlab.com/estrogonelda/sk8/-/archive/master/sk8-master.tar.gz)
using either `wget` or `curl` as follows:
```sh
	wget https://gitlab.com/estrogonelda/sk8/-/archive/master/sk8-master.tar.gz -O sk8.tar.gz
```
or
```sh
curl https://gitlab.com/estrogonelda/sk8/-/archive/master/sk8-master.tar.gz -o sk8.tar.gz
```
and extract it with `tar`:
```sh
	tar -xzvf sk8.tar.gz
```
this will create a `sk8-master/` folder in user's current directory with the
`SK8.mk` main script inside.

#### Automated downloads (the hipster way)
Just clone the *SK8* repository unsing `git`:
```sh
	git clone https://gitlab.com/estrogonelda/sk8.git
```
this will create a `sk8/` folder in user's current directory with the `SK8.mk`
main script inside.


### Usage

If the user already has GNU Make installed (version 3.8 or later), than he can
run any of its scripts (say a `ponga.mk` file) like bellow:
```sh
	make -f ponga.mk <target1> [vars=values ...]
```
That was a special case. Usually, script files to GNU Make are named `Makefile`
or `makefile` and are unique in their folder. If the user follows that
convention (which is recommendable), the command above becomes more succinct:
```sh
	make <target1> [vars=values ...]
```
Make will search and automatically run any `Makefile` in the current folder.

But, to for **SK8**, the name `SK8.mk` was a design decision to not conflict with
that previous convention. And here, the autho propose an even cleaner way to
call it. Doing the following commands only once in the user's system:
```sh
	echo 'alias sk8="make -f /path/to/SK8_root/SK8.mk "' >> ~/.bashrc
	source ~/.bashrc
```
being `/path/to/SK8_root/` the path to the downloaded and extracted **SK8**
repository. So, after this, running **SK8** would be just:
```sh
	sk8 <target1> [vars=values ...]
```
And now you rocks at the prom!!

There are myriads of possible commands, try to start with `sk8 help`. Users can
also [read the **SK8** docs]() once in a life (for a change ...).


## Mastery Usage


## Some Expert Opinions

> "Chic praw, chic prey, Charlie Brown!!"
>                             -- god's John apud Chorão, brazilian singer and skateboarder *(the last referred)*

> "Ehh, já vi melhores ..."
>                             -- Rafael M., bahian Bioinformatician

> "But there's nothing here concerning Design Patterns?!"
>                             -- João L. L. Buzzo, brazilian PHP advocate, Design Patterns priest and amateur skateboarder

> "It can be ugly. But it works?"
>                             -- Gallego, brazilian nerd

> "It's an imbecility!"
>                             -- Tatá, nerd girlfriend
>
> My Answer: "Imbecility?! This is SPARTA!!" *(kicking the computer)*

> "This is it!!"
>                             -- M.J., american pop singer

## See Also


## Author

*Estrogonelda* is the pseudonym adopted by this author as the last choice after
a long and exaustive unsuccessfully search for a valid username for a Gmail
account in 2008. He was trying to make an anagram of his name and sadly failled
because many (I said many, if not all) usernames like *j\o*,;~ès_nl^le* were
already in use. The unexpected solution comes from nowhere and was attributed
to a Cosmic Chaos emanation inside his mind.

In 2011, *Estrogonelda* became part of a quintet of eight (maybe more) distorted
kind of illuminated nerds called **The Bards**, when in a contemplative
reflection about agrotechnology in a scrapped university of his underdeveloped
country.

Estrogonelda is also an assiduous consumer of [De Cabron](https://www.lojadecabron.com.br/)
awesome products and dreams with the day when he'll cook an opíparous peper
sauce with the [ROSMT](https://www.youtube.com/user/SwedishMealTime) guys.



## Copyright

This is free software, licensed under:

`The GNU General Public License, Version 3, June 2007`
