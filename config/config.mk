############################## USER OPTIONS ####################################

ifneq ($(getopt),)

# Option files.
app                            := $a
annotfile                      := $a
buildfile                      := $b
batchfile                      := $b
configfile                     := $c
dbfile                         := $d
expression                     := $e
file                           := $f
groups                         := $g
help                           := $h
infile                         := $i
jobs                           := $j
keyfile                        := $k
libfile                        := $l
logfile                        := $l
modfile                        := $m
name                           := $n
num                            := $n
no                             := $n
outfile                        := $o
passwd                         := $p
quiet                          := $q
reffile                        := $r
srcfile                        := $s
silent                         := $s
templatefile                   := $t
testfile                       := $t
tmpfile                        := $t
tarfile                        := $t
user                           := $u
url                            := $u
verbose                        := $v
version                        := $v
xtrafile                       := $x
yes                            := $y


# Option Directories.
args                           := $A
builddir                       := $B
curdir                         := $C
datadir                        := $D
groupdir                       := $G
docdir                         := $H
includedir                     := $I
stageddir                      := $J
libdir                         := $L
modulesdir                     := $M
outputdir                      := $O
prefixdir                      := $P
rootdir                        := $R
srcdir                         := $S
tmpdir                         := $T
userdir                        := $U
virtualpath                    := $V
workdir                        := $W
xtradir                        := $X
templatesdir                   := $Y
testdir                        := $Z

endif
