#!/usr/bin/env bash
############################## HEADER ##########################################
# Test case for module 'biotools.mk'.
################################################################################



############################## PREAMBLE ########################################

# Get absolute paths.
curdir=$(pwd)
testsdir="$(dirname `realpath -e $0`)"
envdir=$testsdir/biotools
sk8dir="${testsdir%/*}"
sk8=$sk8dir/SK8.mk
module=$sk8dir/modules/biotools.mk


# Predefined values
silent=-s
name="GBM.manifest"
num_lines=965


# Verbosity control.
	cat << EOF
----------------------------- TEST ---------------------------------------------
Test script:                  $0
EOF

if [ -n "$1" ]; then
	cat << EOF

CURDIR:                       $curdir
envdir:                       $envdir

SK8:                          $sk8
modules:                      $module
testdir:                      $testdir

CMD:                          
make -C $envdir -f $sk8
	module=$module
	data_type=wxs
	data_sample=${name%.*}
	data_control="blood derived normal"
	paired=yes
	manifest


EOF

silent=
fi



############################## MAIN ############################################

# Set test environment.
rm -rf $envdir;
mkdir -p $envdir;
make -C $envdir -f $sk8 $silent \
	module=$module \
	data_type=wxs \
	data_sample=${name%.*} \
	data_control="blood derived normal" \
	paired=yes \
	manifest 2> $0.log

errno=$?


# Test
if [ $errno -eq 0 -a "$(wc -l $envdir/$name | perl -ane 'print $F[0]')" == $num_lines ]; then
	echo -e "\nPASSED!";
	echo "--------------------------------------------------------------------------------";
else
	echo -e "\nNOT PASSED!! Error: $?." >&2;
	echo "--------------------------------------------------------------------------------";
	exit 8;
fi


# Clear environment.
[ -n "$1" ] || rm -rf $envdir
