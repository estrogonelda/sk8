############################## GENERIC PACKAGE #################################

# Info to download, configure, build, install and use 'mysql' package.
mysql_src                      := https://dev.mysql.com/get/Downloads/MySQL-8.0/mysql-8.0.21.tar.gz
mysql_ext                      := .tar.gz

# Info to download, configure, build, install and use 'mysql' package.
mysql_package                  := $(shell perl -ne 'print "@{[ (split(/-/, $$_))[0] ]}"' <<< "$(notdir $(mysql_src))")
mysql_version                  := $(shell perl -pe 's/^$(mysql_package)-(.*?)\$(mysql_ext)/$$1/g' <<< "$(notdir $(mysql_src))")
mysql_id                       := $(mysql_package)-xxxxxx
mysql_distname                 := $(mysql_package)-$(mysql_version)
mysql_license                  := GPLv4

# Potential installation data.
mysql_repository               := $(mysql_src)
mysql_tarball                  := $(tarballsdir)/$(mysql_distname)$(mysql_ext)
mysql_builddir                 := $(tarballsdir)/$(mysql_distname)
mysql_configurefile            := $(mysql_builddir)/README
mysql_buildfile                := $(mysql_builddir)/bld/CMakeCache.txt
mysql_execfile                 := $(mysql_builddir)/bld/runtime_output_directory/$(mysql_package)
mysql_dir                      := $(dir $(installdir))/mysql
mysql_prefix                   := $(dir $(installdir))/$(mysql_package)
mysql_installfile              := $(mysql_dir)/bin/$(mysql_package)
mysql_installdaemon            := $(mysql_dir)/bin/$(mysql_package)d


# Actual installation data.
mysql_has                    := $(shell which $(mysql_package) 2> /dev/null)
mysql_get                    := $(if $(mysql_has),,mysql_install)
mysql                        := $(if $(mysql_has),$(mysql_has),$(mysql_installfile))

mysqld_has                   := which $(mysql_package)d 2> /dev/null
mysqld_get                   := $(if $(shell $(mysqld_has)),,mysql_install)
mysqld                       := $(if $(shell $(mysqld_has)),$(shell $(mysqld_has)),$(mysql_installdaemon))


aux                          := $(shell perl -ne 'print if /SUCCESS/' <<< "$$($(mysql_dir)/support-files/mysql.server status 2> /dev/null)")
mysql_status                 := $(if $(aux),,mysql_activate)


# FORCE data.
mysql_force                  := $(if $(FORCE),mysql_force,)
mysql_status                 := $(if $(FORCE),mysql_activate,$(mysql_status))
mysql_activate               := $(if $(FORCE),mysql_install,$(mysql_get))
mysql                        := $(if $(FORCE),$(mysql_installfile),$(mysql))


# Verification.
mysql_status: $(mysql_status) # 'mysql_install' or none.
	@$(ECHO) "\nSystem has '$(mysql_package)' up and running."

# Activation process.
mysql_activate: $(mysql_activate) # 'mysql_install' or none.
	@$(ECHO) "\nActivating '$(mysql_package)' and its daemon ($(mysql_installdaemon)) ..."
	-pkill "mysqld"
	sleep 2
	if [ -n "$(DESTDIR)" -o -n "$(usermode)" ]; then \
		cd $(mysql_dir); \
		[ -d "data" ] || bin/mysqld --initialize-insecure; \
		bin/mysqld_safe \
			--port=57777 \
			--socket=/tmp/mysql.sock \
			--datadir=$(mysql_dir)/data & disown -h; \
	else \
		groupadd -f mysql; \
		useradd -r -s /bin/false -g mysql mysql; \
		cd $(mysql_dir); \
		mkdir -p data; \
		chown -R mysql:mysql data; \
		chmod -R 750 data; \
		bin/mysqld --initialize-insecure --user=mysql; \
		bin/mysql_ssl_rsa_setup; \
		bin/mysqld_safe --user=mysql & disown -h; \
		cp $(mysql_dir)/support-files/mysql.server /etc/mysql.server; \
	fi
	@$(ECHO) "WARNING: User must set the MySQL's root password with 'mysqladmin' command."
	@$(ECHO) "Done."


# Install.
mysql_install: $(mysql_installfile) # $(installdir)/$(mysql_package)
	@$(ECHO) "\nSystem has '$(mysql_package)' in PATH: '$<'."

$(mysql_installfile): $(mysql_execfile) # just copy the executable file to $(installdir).
	@$(ECHO) "\nInstalling '$(mysql_package)' on directory '$(mysql_prefix)' ..."
	$(MAKE) -C $(mysql_builddir)/bld install DESTDIR=$(DESTDIR) prefix=$(mysql_prefix) -j2
	if [ -z "$$(perl -ne 'print if s#.*$(mysql_prefix)/bin.*#AA#g' <<< $${PATH})" ]; then \
		echo -e "export PATH=$(mysql_prefix)/bin:\$$PATH" >> ~/.bashrc; \
		source ~/.bashrc; \
		echo -e "WARNING: You may need to run 'source ~/.bashrc' to release the new \$$PATH."; \
	elif [ "$(mysql)" != "$(mysql_installfile)" ]; then \
		echo "WARNING: Previuos installation of '$(mysql_package)' may be shadowing the new one in the \$$PATH." >&2; \
	fi
	touch $@
	@$(ECHO) "Done."


# Build.
mysql_build: $(mysql_execfile) # $(mysql_builddir)/$(mysql_package)

$(mysql_execfile): $(mysql_buildfile) # a Makefile generates the executable file.
	@$(ECHO) "\nBuilding '$(mysql_package)' on directory '$(mysql_builddir)' ..."
	$(MAKE) -C $(mysql_builddir)/bld -j2
	touch $@
	@$(ECHO) "Done."


# Configure.
mysql_configure: $(mysql_buildfile) # $(mysql_builddir)/Makefile

$(mysql_buildfile): prefix := $(if $(DESTDIR),$(prefix),$(dir $(installdir))/mysql)
$(mysql_buildfile): $(mysql_configurefile)
	@$(ECHO) "\nConfiguring '$(mysql_package)' on directory '$(mysql_builddir)' ..."
	boost=https://dl.bintray.com/boostorg/release/1.72.0/source/boost_1_72_0.tar.gz; \
	boosttar=$(tarballsdir)/$$(basename $${boost}); \
	boostdir=$(tarballsdir)/boost; \
	mkdir -p $${boostdir}; \
	wget $${boost} -P $(tarballsdir); \
	tar -xzvf $${boosttar} -C $${boostdir} --strip-components=1
	mkdir -p $(mysql_builddir)/bld
	cd $(mysql_builddir)/bld; \
	cmake -DCMAKE_INSTALL_PREFIX=$(prefix) -DWITH_BOOST=$(tarballsdir)/boost ..
	touch $@
	@$(ECHO) "Done."

$(mysql_configurefile): $(mysql_tarball)
	@$(ECHO) "\nExtracting '$(mysql_package)' tarball to directory '$(mysql_builddir)' ..."
	mkdir -p $(mysql_builddir)
	tar -xzvf $< -C $(mysql_builddir) --strip-components=1
	touch $@
	@$(ECHO) "Done."


# Download.
mysql_tarball: $(mysql_tarball) # $(tarballsdir)/$(mysql_tarball).

$(mysql_tarball): $(internet_get) $(mysql_force) | $(tarballsdir)
	@$(ECHO) "\nDownloading '$(mysql_package)' tarball from '$(mysql_repository)' ..."
	wget -c $(mysql_repository) -P $(tarballsdir) -O $@
	touch $@
	@$(ECHO) "Done."


mysql_refresh: mysql_uninstall mysql_install


# Uninstall and clean.
mysql_uninstall:
	@$(ECHO) "\nUninstalling '$(mysql_package)' from directory '$(mysql_prefix)' ...";
	[ -d "$(mysql_prefix)/bin" ] || exit 1;
	-pkill $(mysqld)
	if [ -f "$(mysql_buildfile)" ]; then \
		$(MAKE) -C $(mysql_builddir) uninstall DESTDIR=$(DESTDIR) prefix=$(mysql_prefix) -j4; \
	else \
		rm -rf $(mysql_prefix); \
	fi
	echo "User may resolve \$$PATH in '~/.bashrc' file." >&2 
	@$(ECHO) "Done."

mysql_clean:
	@$(ECHO) "\nCleaning '$(mysql_package)' from staged directory '$(stageddir)' ...";
	rm -rf $(tarballsdir)/$(mysql_package)*
	@$(ECHO) "Done."

mysql_purge: mysql_uninstall mysql_clean


# FORCE!
mysql_force:
	@$(ECHO) "\nForce installation of '$(mysql_package)' on directory '$(installdir)'."
	-rm $(mysql_tarball)
	@$(ECHO) "Done."


# Test.
mysql_test:
	@$(ECHO) "\nTesting package '$(mysql_package)' ...";
	if [ -z "$$($(mysql_package) --version 2> /dev/null)" ]; then \
		echo "Package '$(mysql_package)' is not properly running." >&2; \
		exit 1; \
	else \
		echo "PASS!"; \
	fi
	@$(ECHO) "Done."



############################## DEBUG CODE ######################################

ifeq ($(dbg_mysql),yes)
$(info ****************************** DEBUG GENERIC PACK ******************************)
$(info Precursor variables for a 'mysql' package source.)
$(info workdir:                       $(workdir).)
$(info stageddir:                     $(stageddir).)
$(info tarballsdir:                   $(tarballsdir).)
$(info DESTDIR:                       $(DESTDIR).)
$(info installdir:                    $(installdir).)
$(info usermode:                      $(usermode).)
$(info src:                           $(src).)
$(info ext:                           $(ext).)
$(info )

$(info Info to download, configure, build, install and use 'mysql' package.)
$(info mysql_source:                $(mysql_source).)
$(info mysql_ext:                   $(mysql_ext).)
$(info )

$(info mysql_package:               $(mysql_package).)
$(info mysql_version:               $(mysql_version).)
$(info mysql_id:                    $(mysql_id).)
$(info mysql_distname:              $(mysql_distname).)
$(info mysql_license:               $(mysql_license).)
$(info )

$(info Potential installation data.)
$(info mysql_dependecies:           $(mysql_dependecies).)
$(info mysql_repository:            $(mysql_repository).)
$(info mysql_tarball:               $(mysql_tarball).)
$(info mysql_builddir:              $(mysql_builddir).)
$(info mysql_configurefile:         $(mysql_configurefile).)
$(info mysql_buildfile:             $(mysql_buildfile).)
$(info mysql_execfile:              $(mysql_execfile).)
$(info mysql_installfile:           $(mysql_installfile).)


$(info Actual installation data.)
$(info mysql_has:                   $(mysql_has).)
$(info mysql_get:                   $(mysql_get).)
$(info mysql:                       $(mysql).)
$(info )


$(info Status and FORCE data.)
$(info mysql_force:                 $(mysql_force).)
$(info mysql_status:                $(mysql_status).)
$(info ********************************************************************************)
endif
