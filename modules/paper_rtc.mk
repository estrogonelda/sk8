############################## HEADER ##########################################
#
################################################################################



############################## PREAMBLE ########################################
circos                         := /run/media/leonel/43697b55-725f-4c06-ab74-35e2a4ff375b/Workbench/.local/Circos/bin/circos
circos_conf                    := $(radical)_circos.conf
circos_plot                    := $(radical)_circos.png

piechart                       := $(radical)_piechart.pdf
barplot                        := $(radical)_barplot.pdf
boxplot                        := $(radical)_boxplot.pdf
upsetplot                      := $(radical)_upsetplot.pdf

rtc_per_individual             := tcga_$(radical)_wxs_p_ind.tsv

neopipe_rootdir                := $(workdir)/neopipe
neopipe_vcf                    := $(inputfl)
neopipe_tsv                    := neopipe.tsv
neopipe_rmd                    := neopipe.Rmd
neopipe_plot                   := neopipe.png

neopipe_cmd                    := $(noepipe_rootdir)/neopipe vcf_analyzer
neopipe_cmd                    += -a $(annotation_ref)
neopipe_cmd                    += -d $(database_ref)
neopipe_cmd                    += --filter $(cancer_genes)
neopipe_cmd                    += --mode paired -l $(database_ref)
neopipe_cmd                    += --tag
neopipe_cmd                    += -- $(neopipe_vcf)


define R_piechart
	options(tidyverse.quiet = T);
	library("tidyverse");
	args <- commandArgs(trailingOnly = T);
	
	print(paste("Importing files:", args[1]));
	tbl <- read.table(args[1], header = T, sep = "\t");
	tbl <- tbl[!tbl$$INFO %% 2, ];
	tbl <- tbl[!tbl$$INDIV == ".", ];
	
	dt <- data.frame(COUNTS = c(tbl$$G1_HM + tbl$$G1_HT, tbl$$G2_HM + tbl$$G2_HT));
	dt$$TISSUE <- factor(rep(c("Normal", "Tumor"), each = length(tbl$$ID)));
	dt$$TAG <- factor(rep(tbl$$TAG, 2));
	dt$$TYPE <- ifelse(tbl$$INFO >= 64, "Somatic", "Polymorphic");
	germ <- rep(tbl$$G12 == 1 & rowSums(tbl[, 22:31]) == 3, 2);
	dt[germ, "TYPE"] <- "Germline";
	dt$$TYPE <- factor(dt$$TYPE);
	str(dt)
	
	argv <- strsplit(" ", args[2])[[1]];
	
	g <- ggplot(dt, aes(x = TYPE, y = COUNTS, fill = TYPE)) \
		+ geom_rect() \
		+ coord_polar(theta = "y") \
		+ xlim(2, 4)

	pdf(paste0(sub("(.*?)\\.tsv", "\\1", args[1]), "_piechart.pdf"), height = 21*0.4, width = 29.7*0.4);
	print(g);
	dev.off();
endef

#		+ facet_wrap(~ TAG, nrow = 1, scales = "free") \

define R_barplot
	options(tidyverse.quiet = T);
	library("tidyverse");
	args <- commandArgs(trailingOnly = T);
	
	print(paste("Importing files:", args[1]));
	tbl <- read.table(args[1], header = T, sep = "\t");
	tbl <- tbl[!tbl$$INFO %% 2, ];
	tbl <- tbl[!tbl$$INDIV == ".", ];
	
	dt <- data.frame(COUNTS = c(tbl$$G1_HM + tbl$$G1_HT, tbl$$G2_HM + tbl$$G2_HT));
	dt$$TISSUE <- factor(rep(c("Normal", "Tumor"), each = length(tbl$$ID)));
	dt$$TAG <- factor(rep(tbl$$TAG, 2));
	dt$$TYPE <- ifelse(tbl$$INFO >= 64, "Somatic", "Polymorphic");
	germ <- rep(tbl$$G12 == 1 & rowSums(tbl[, 22:31]) == 3, 2);
	dt[germ, "TYPE"] <- "Germline";
	dt$$TYPE <- factor(dt$$TYPE);
	str(dt)
	
	argv <- strsplit(" ", args[2])[[1]];
	
	g <- ggplot(dt, aes(x = TISSUE, y = COUNTS, fill = TYPE)) \
		+ geom_bar(position = "fill", stat = "identity") \
		+ facet_wrap(~ TAG, nrow = 1, scales = "free") \
		+ labs(x = "", y = "# retroCNVs events", fill = "") \
		+ theme_classic() \
		+ theme(strip.background = element_blank(), strip.text.x = element_blank(), axis.line = element_blank(), panel.grid.minor = element_line(colour = "grey", size = 0.5), legend.position = "bottom", axis.ticks = element_blank(), axis.text.x = element_blank()) \
		+ scale_fill_manual(values = c("#d9f0a3", "#78c679", "#238443"))

	pdf(paste0(sub("(.*?)\\.tsv", "\\1", args[1]), "_barplot.pdf"), height = 21*0.4, width = 29.7*0.4);
	print(g);
	dev.off();
endef


define R_boxplot
	options(tidyverse.quiet = T);
	library("tidyverse");
	library(viridis);
	args <- commandArgs(trailingOnly = T);
	
	print(paste("Importing files:", args[1]));
	tbl <- read.table(args[1], header = T, sep = "\t", stringsAsFactors = T);
	tbl <- tbl[tbl$$INFO >= 64 & tbl$$TISSUE == "Primary Tumor" & tbl$$COUNT > 0, ];
	tbl$$ONES <- 1;
	tbl <- tbl %>% group_by(INDIV, TUMOR_TYPE) %>% summarize(RES = sum(ONES));
	
	g <- ggplot(tbl, aes(x = TUMOR_TYPE, y = RES, fill = TUMOR_TYPE)) \
		+ geom_violin() \
		+ ylim(0, 20) \
		+ labs(x = "", y = "# Somatic retroCNVs per individual", fill = "Tumor Type") \
		+ theme_classic() \
		+ theme(strip.background = element_blank(), strip.text.x = element_blank(), axis.line = element_blank(), panel.grid.minor = element_line(colour = "grey", size = 0.5), legend.position = "bottom", axis.ticks = element_blank(), axis.text.x = element_blank()) \
		+ facet_wrap(~ TUMOR_TYPE, nrow = 1, scales = "free");
	
	pdf(paste0(sub("(.*?)\\.tsv", "\\1", args[1]), "_boxplot.pdf"), height = 21*0.2, width = 29.7*0.4);
	print(g);
	dev.off();
endef

#		+ geom_jitter(size = 1, color = "black", alpha = 0.3) \

define perl_rtc_per_individual
	$$act = $$ARGV, $$ck++ unless $$ck; \
	next if /^#/; \
	
	if ( $$ARGV eq $$act ) { \
		next if /^CASE_ID/; \
		$$files{$$F[4]} = $$F[2]; \
	} \
	else { \
		next if /^ID/ or $$F[10] & 1 or $$F[31] eq "."; \
		push @{ $$ind{$$_} } => [ $$F[15], $$F[5], $$F[16], $$F[10] ] for split /\|/, $$F[-1]; \
	} \
	\
	END { \
		print "INDIV\tTUMOR_TYPE\tTISSUE\tINFO\tPARENT\tHOST\tCOUNT"; \
		for my $$i ( sort keys %ind ) { \
			next unless exists $$files{$$i}; \
			$$tissue = $$files{$$i}; \
			print "$$i\t$$_->[2]\t$$tissue\t$$_->[3]\t$$_->[0]\t$$_->[1]\t", scalar @{ $$ind{$$i} } for @{ $$ind{$$i} }; \
		} \
	}
endef

define perl_circos
	$$act = $$ARGV, $$ck++ unless $$ck; \
	next if /^#/; \
	
	if ( $$ARGV eq $$act ) { \
		next unless $$F[2] eq "gene" and $$F[8] =~ /protein_coding/; \
		$$name = $$1 if $$F[8] =~ /gene_name "(.*?)";/; \
		$$gene{$$name} = [ $$F[0], $$F[3], $$F[4] ]; \
	} \
	else { \
		next if /ID/ or $$F[10] & 1 or $$F[31] eq "."; \
		next unless exists $$gene{$$F[15]};
		$$start = $$gene{$$F[15]}; \
		$$end = [ $$F[1], $$F[2], $$F[2] + 10000 ]; \
		print join("\t", (@{ $$start }, @{ $$end })), "\tdata_sample=$$F[16],info=$$F[10],parent=$$F[15],host=$$F[5],normal=", sum(@F[21, 22]), ",tumor=", sum(@F[23, 24]), ",germline=$$F[27]"; \
		print STDERR join("\t", @{ $$start }), "\t$$F[15]\tdata_sample=$$F[16],info=$$F[10],parent=$$F[15],host=$$F[5],normal=", sum(@F[21, 22]), ",tumor=", sum(@F[23, 24]), ",germline=$$F[27]"; \
		print STDERR join("\t", @{ $$end }), "\t$$F[5]\tdata_sample=$$F[16],info=$$F[10],parent=$$F[15],host=$$F[5],normal=", sum(@F[21, 22]), ",tumor=", sum(@F[23, 24]), ",germline=$$F[27]"; \
	}
endef

define perlzim
	print "@ARGV"; last; END { print "Orra." }
endef


circos_plot: $(circos_plot)

$(circos_plot): $(annotation_ref) $(input) | $(circos_status) $(circos_conf)
	@$(ECHO) "\nCreating Circos plot from input file: '$(input)' ..." >&2
	perl -F"\t" -MList::Util=sum -lane '$(perl_circos)' $^ > lnk.txt 2> txt.txt
	perl -i -pe 's/chr/hs/g' lnk.txt
	perl -i -pe 's/chr/hs/g' txt.txt
	$(circos) -conf $(circos_conf) \
		-param links/link/file=lnk.txt \
		-param plots/plot/file=txt.txt \
		-file $(basename $@) \
		-noparanoid
	#rm -f lnk.txt txt.txt
	@$(ECHO) "Done." >&2

$(circos_conf):
	@$(ECHO) "\nERROR: There must be supplied a configuration file '$@' for Circos to proceed." >&2
	exit 1


pie_chart: $(piechart)

$(piechart): $(input) | $(R_status)
	@$(ECHO) "\nCreating barplot from input file: '$(input)' ..." >&2
	R --vanilla --slave --args $< "$(args)" <<< '$(R_piechart)'
	@$(ECHO) "Done." >&2


bar_plot: $(barplot)

$(barplot): $(input) | $(R_status)
	@$(ECHO) "\nCreating barplot from input file: '$(input)' ..." >&2
	R --vanilla --slave --args $< "$(args)" <<< '$(R_barplot)'
	@$(ECHO) "Done." >&2

box_plot: $(boxplot)

$(boxplot): $(rtc_per_individual) | $(R_status)
	@$(ECHO) "\nCreating boxplot from input file: '$(input)' ..." >&2
	R --vanilla --slave --args $< "$(args)" <<< '$(R_boxplot)'
	@$(ECHO) "Done." >&2


rtc_per_individual: $(rtc_per_individual)

$(rtc_per_individual): $(manifest) $(input)
	@$(ECHO) "\nCreating table of RTCs per Individual from input file: '$(input)' ..." >&2
	[ -f "$(input)" ] || exit 7
	perl -F"\t" -lane '$(perl_rtc_per_individual)' $^ > $@
	@$(ECHO) "Done." >&2


neopipe_html: $(neopipe_html)

$(neopipe_html): $(neopipe_rmd) | $(R_status)
	@$(ECHO) "\nRendering report of RTCs from input files: '$<' ..." >&2
	R --vanilla --slave --args "$<" <<< '$(R_renderize)'
	@$(ECHO) "Done." >&2


$(neopipe_rmd): $(neopipe_tsv)
	@$(ECHO) "\nCreating report template file '$@' ..." >&2
	perl -lane '$(perl_neopipe_report)' $< > $@
	@$(ECHO) "Done." >&2


neopipe_tsv: $(neopipe_tsv)

$(neopipe_tsv): $(neopipe_vcf) | $(neopipe_status)
	@$(ECHO) "\nCreating table of RTCs from input files: '$<' ..." >&2
	$(neopipe_cmd) -- $< > $@
	[ -s "$@" ] || (echo "ERROR: Neopipe output TSV file is empty." >&2; exit 1)
	@$(ECHO) "Done." >&2
