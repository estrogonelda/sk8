# Non-standalone run verification.
this                           := bedtools.mk
ifeq ($(notdir $(firstword $(MAKEFILE_LIST))),$(this))
      $(error ERROR: Module '$(this)' cannot run in standalone mode, \
      it needs to be loaded by 'SK8')
endif

# Info to download, configure, build, install and use 'bedtools' package.
bedtools_src                    := https://github.com/arq5x/bedtools2/releases/download/v2.29.1/bedtools-2.29.1.tar.gz
bedtools_ext                    := .tar.gz

bedtools_package                := $(shell perl -ne 'print "@{[ (split(/-/, $$_))[0] ]}"' <<< "$(notdir $(bedtools_src))")
bedtools_version                := $(shell perl -pe 's/^$(bedtools_package)-(.*?)\$(bedtools_ext)/$$1/g' <<< "$(notdir $(bedtools_src))")
bedtools_id                     := $(bedtools_package)-xxxxxx
bedtools_distname               := $(bedtools_package)-$(bedtools_version)
bedtools_license                := GPLv4

# Potential installation data.
bedtools_repository             := $(bedtools_src)
bedtools_tarball                := $(tarballsdir)/$(bedtools_distname)$(bedtools_ext)
bedtools_builddir               := $(tarballsdir)/$(bedtools_distname)
bedtools_configurefile          := $(bedtools_builddir)/configure
bedtools_buildfile              := $(bedtools_builddir)/Makefile
bedtools_execfile               := $(bedtools_builddir)/$(bedtools_package)
bedtools_installfile            := $(installdir)/$(bedtools_package)


# Actual installation data.
bedtools_has                    := $(shell which $(bedtools_package) 2> /dev/null)
bedtools_get                    := $(if $(bedtools_has),,bedtools_install)
bedtools                        := $(if $(bedtools_has),$(bedtools_has),$(bedtools_installfile))


# FORCE data.
bedtools_force                  := $(if $(FORCE),bedtools_force,)
bedtools_status                 := $(if $(FORCE),bedtools_install,$(bedtools_get))
bedtools                        := $(if $(FORCE),$(bedtools_installfile),$(bedtools))


# Include required modules.
#include $(addprefix $(sk8_rootdir)/modules/, gcc.mk)
bedtools_deps                   := $(if $(recursive),bedtools_deps,)
bedtools_depscheck              := $(gpp_status) $(bedtools_deps)


# Usage.
bedtools_help:
	@$(ECHO) "\n\
	NAME\n\
		\n\
		bedtools_package - $(bedtools_package), version $(bedtools_version)\n\
		\n\
	SYNOPSIS\n\
		\n\
		$$ sk8 [make_option ...] [variable=value ...] <bedtools_target ...>\n\
		\n\
	DESCRIPTION\n\
		\n\
		This is an engine to download, configure, build, install and test packages\n\
		in a bedtools way. So, the package must be compliant with the GNU Coding\n\
		Standards in order to be handled by SK8.\n\
		Now, the package in question is '$(bedtools_distname)'.\n\
		\n\
	OPTIONS\n\
		\n\
		Targets:\n\
		\n\
		bedtools_help                   Print this help text.\n\
		bedtools_version                Print '$(bedtools_package)' package info.\n\
		bedtools_status                 Verifies '$(bedtools_package)' current installation status.\n\
		bedtools_run                    Run '$(bedtools_package)' with the arguments given in 'args' variable.\n\
		bedtools_install                Install '$(bedtools_package)' binary in '$$(prefix)/bin' directory.\n\
		bedtools_build                  Compile the '$(bedtools_package)' binary in the build directory.\n\
		bedtools_configure              Configure '$(bedtools_package)' build files for this system.\n\
		bedtools_tarball                Download the '$(bedtools_package)' tarball from its repository: $(bedtools_src)\n\
		bedtools_uninstall              Uninstall '$(bedtools_package)' from the '$$(prefix)/bin' directory.\n\
		bedtools_clear                  Remove '$(bedtools_package)' tarball from tarball''s directory.\n\
		bedtools_purge                  Uninstall and clear '$(bedtools_package)' from the system.\n\
		bedtools_refresh                Uninstall and then re-install '$(bedtools_package)' again.\n\
		bedtools_deps                   Install a '$(bedtools_package)' dependency recursivelly as passed in 'args' variable.\n\
		bedtools_test                   Test '$(bedtools_package)' installation.\n\
		bedtools_test_build             Test '$(bedtools_package)' build process, natively.\n\
		bedtools_test_build_conda       Test '$(bedtools_package)' build process in a new Conda environment.\n\
		bedtools_test_build_docker      Test '$(bedtools_package)' build process inside a vanilla Docker container.\n\
		bedtools_test_build_aws         Test '$(bedtools_package)' build process inside an AWS instance. NOT WORKING!!\n\
		bedtools_create_img             Create a Docker image for '$(bedtools_package)', based on '$(image)' image.\n\
		bedtools_create_dockerfile      Create a Dockerfile to build an image for '$(bedtools_package)'.\n\
		bedtools_create_module          Create a pre-filled SK8 module for '$(bedtools_package)' package.\n\
		\n\
		conda_run                      Run '$(bedtools_package)' in a new Conda environment. See 'conda_help' for more info.\n\
		docker_run                     Run '$(bedtools_package)' in a vanilla Docker image. See 'docker_help' for more info.\n\
		aws_run                        Run '$(bedtools_package)' in an AWS instance. See 'aws_help' for more info. NOT WORKING!!\n\
		\n\
		Variables:\n\
		\n\
		args                           String of arguments passed to '$(bedtools_package)' when running.               [ '' ]\n\
		prefix                         Path to the directory where to put the built '$(bedtools_packages)' binaries.   [ /usr/local ]\n\
		DESTDIR                        Path to an alternative staged installation directory.                          [ '' ]\n\
		workdir                        The current directory.                                                         [ . ]\n\
		stageddir                      Path to a staged directory for tarballs and package builds.                    [ ./.local ]\n\
		usermode                       Boolean to switch between local or system-wide installations paths.            [ '' ]\n\
		recursive                      Boolean to enable recursive dependency installations.                          [ '' ]\n\
		FORCE                          Boolean to force a fresh installation over an existent one.                    [ '' ]\n\
		\n\
		src                            URL of the '$(bedtools_package)' repository.                                    [ '$(bedtools_src)' ]\n\
		ext                            The tarball extension. Only for extensions other than '.tar.gz'.               [ '.tar.gz' ]\n\
		bedtools_package                The name of the package, derived from its tarball''s name.                     [ '$(bedtools_package)' ]\n\
		bedtools_version                The version of the package, derived from its tarball''s name.                  [ '$(bedtools_version)' ]\n\
		\n\
		Make options:\n\
		\n\
		\n\
		NOTE: Some packages cannot build bedtoolsally. So, user must test the chosen target first.\n\
		For a list of known bedtoolsally installable packages, pleas read the\n\
		'$(sk8_rootdir)/modules/modules_urls.txt' file.\n\
		\n\
		\n\
	COPYRIGHT\n\
		\n\
		$(sk8_package), version $(sk8_version) for GNU/Linux.\n\
		Copyright (C) Bards Agrotechnologies, 2011-2021.\n\
		\n\
	SEE ALSO\n\
		\n\
		Search for 'help', 'docker_help', 'conda_help' and 'miscellaneus'\n\
		for other sources of command line help or visit <$(basename $(sk8_url))>\n\
		for full documentation."

bedtools_version:
	@$(ECHO) "\n\
	Package:    $(bedtools_package)\n\
	Version:    $(bedtools_version)\n\
	Id:         $(bedtools_id)\n\
	License:    $(bedtools_license)\n\
	\n\
	Status:     $(bedtools_status)\n\
	Source:     $(bedtools_src)\n\
	Installdir: $(installdir)\n\
	\n\
	Runned with $(sk8_package), version $(sk8_version) for GNU/Linux.\n\
	Copyright (C) Bards Agrotechnologies, 2011-2021."

bedtools_run: $(bedtools_status)
	@$(ECHO) "\nRunning '$(bedtools)' with arguments '$(args)' ..." >&2
	$(bedtools) $(args)
	@$(ECHO) "Done." >&2


# Verification.
bedtools_status: $(bedtools_status) # 'bedtools_install' or none.
	@$(ECHO) "\nSystem has '$(bedtools_package)' up and running." >&2


# Install.
bedtools_install: $(bedtools_installfile) # $(installdir)/$(bedtools_package)
	@$(ECHO) "\nSystem has '$(bedtools_package)' in PATH: '$<'." >&2

$(bedtools_installfile): $(bedtools_execfile) | $(installdir) # just copy the executable file to $(installdir).
	@$(ECHO) "\nInstalling '$(bedtools_package)' on directory '$(installdir)' ..." >&2
	$(MAKE) -C $(bedtools_builddir) install DESTDIR=$(DESTDIR) prefix=$(dir $(installdir)) -j4
	echo "$$(ls $(bedtools_builddir)/bin)" > $(tarballsdir)/$(bedtools_package).manifest
	if [ -z "$$(perl -ne 'print if s#.*$(installdir).*#AA#g' <<< $${PATH})" ]; then \
		echo -e "export PATH=$(installdir):\$$PATH" >> ~/.bashrc; \
		source ~/.bashrc; \
		echo -e "WARNING: You may need to run 'source ~/.bashrc' to release the new \$$PATH."; \
	elif [ "$(bedtools)" != "$(bedtools_installfile)" ]; then \
		echo "WARNING: Previuos installation of '$(bedtools_package)' may be shadowing the new one in the \$$PATH."; \
	fi
	touch $@
	@$(ECHO) "Done." >&2


# Build.
bedtools_build: $(bedtools_execfile) # $(bedtools_builddir)/$(bedtools_package)

$(bedtools_execfile): $(bedtools_buildfile) | $(gcc_status) # a Makefile generates the executable file.
	@$(ECHO) "\nBuilding '$(bedtools_package)' on directory '$(bedtools_builddir)' ..." >&2
	$(MAKE) -C $(bedtools_builddir) -j4
	touch $@
	@$(ECHO) "Done." >&2


# Configure.
bedtools_configure: $(bedtools_buildfile) # $(bedtools_builddir)/Makefile

$(bedtools_buildfile): $(bedtools_configurefile)
	@$(ECHO) "\nConfiguring '$(bedtools_package)' on directory '$(bedtools_builddir)' ..." >&2
	#cd $(bedtools_builddir); \
	#./configure --prefix=$(dir $(installdir)) $(bedtools_args)
	touch $@
	@$(ECHO) "Done." >&2

$(bedtools_configurefile): $(bedtools_tarball) | $(bedtools_builddir)
	@$(ECHO) "\nExtracting '$(bedtools_package)' tarball to directory '$(bedtools_builddir)' ..." >&2
	if [ "$(bedtools_ext)" == ".tar.gz" ]; then \
		tar -xzvf $< -C $(bedtools_builddir) --strip-components=1; \
	elif [ "$(bedtools_ext)" == ".tar.xz" ]; then \
		tar -xJvf $< -C $(bedtools_builddir) --strip-components=1; \
	else \
		tar -xvf $< -C $(bedtools_builddir) --strip-components=1; \
	fi
	touch $@
	@$(ECHO) "Done." >&2

$(bedtools_builddir):
	@$(ECHO) "\nCreating directory '$(bedtools_builddir)' ..." >&2
	mkdir -p $(bedtools_builddir)
	@$(ECHO) "Done." >&2


# Download.
bedtools_tarball: $(bedtools_tarball) # $(tarballsdir)/$(bedtools_tarball).

$(bedtools_tarball): $(bedtools_force) | $(tarballsdir) $(internet_status) $(bedtools_depscheck)
	@$(ECHO) "\nDownloading '$(bedtools_package)' tarball from '$(bedtools_repository)' ..." >&2
	wget -c $(bedtools_repository) -P $(tarballsdir) -O $@
	touch $@
	@$(ECHO) "Done." >&2

bedtools_deps:
	@$(ECHO) "\nRecursively installing dependecies for '$(bedtools_distname)' ..." >&2
	[ -n "$(args)" ] || ( echo "ERROR: Variable 'args' can not be empty." >&2; exit 1 )
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) recursive= $(args)
	@$(ECHO) "Done." >&2

bedtools_force:
	@$(ECHO) "\nForce reinstallation of '$(bedtools_package)' on directory '$(installdir)'." >&2
	rm -f $(bedtools_tarball)
	@$(ECHO) "Done." >&2


bedtools_refresh: bedtools_uninstall
	if [ -z "$(usermode)" ]; then \
		$(MAKE) -f $(firstword $(MAKEFILE_LIST)) DESTDIR=$(DESTDIR) prefix=$(prefix) bedtools_install; \
	else \
		$(MAKE) -f $(firstword $(MAKEFILE_LIST)) DESTDIR=$(DESTDIR) prefix=$(prefix) bedtools_install usermode=1; \
	fi


# Uninstall and clean.
bedtools_purge: bedtools_uninstall bedtools_clean

bedtools_uninstall:
	@$(ECHO) "\nUninstalling '$(bedtools_package)' from directory '$(installdir)' ..." >&2
	[ -d "$(installdir)" ] || exit 1;
	if [ -f "$(bedtools_buildfile)_no" ]; then \
		$(MAKE) -C $(bedtools_builddir) uninstall DESTDIR=$(DESTDIR) prefix=$(dir $(installdir)) -j4; \
	else \
		if [ -f "$(tarballsdir)/$(bedtools_package).manifest" ]; then \
			while read; do \
				rm -f $(installdir)/$${REPLY}; \
			done < $(tarballsdir)/$(bedtools_package).manifest; \
			rm -f $(tarballsdir)/$(bedtools_package).manifest; \
		else \
				rm -rf $(bedtools_installfile)*; \
		fi \
	fi
	@$(ECHO) "User must resolve \$$PATH in '~/.bashrc' file." >&2
	@$(ECHO) "Done." >&2

bedtools_clean:
	@$(ECHO) "\nCleaning '$(bedtools_package)' from staged directory '$(stageddir)' ..." >&2
	rm -rf $(tarballsdir)/$(bedtools_package)*
	@$(ECHO) "Done." >&2


# Tests.
bedtools_test:
	@$(ECHO) "\nTesting '$(bedtools_package)' ..." >&2
	if [ -n "$(is_lib)" ]; then \
		if [ ! -s "$$(find $(dir $(installdir))/lib -type f -name '$(bedtools_package)*')" ]; then \
			echo "Library '$(bedtools_package)' is not properly installed."; \
			exit 7; \
		fi \
	else \
		if [ -z "$$($(bedtools_package) --version 2> /dev/null)" ]; then \
			echo "Package '$(bedtools_package)' is not properly running."; \
			exit 7; \
		fi \
	fi
	@$(ECHO) "Package '$(bedtools_package)' test status: PASS!"
	@$(ECHO) "Done." >&2


bedtools_test_build_native: _install := $(basedir)/_install_$(bedtools_distname)
bedtools_test_build_native:
	@$(ECHO) "\nTesting native build of '$(bedtools_package)' module." >&2
	[ -n "$(_install)" ] || ( echo "ERROR: Variable '_install' is empty."; exit 7 )
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) \
		stageddir=$(_install) \
		usermode=1 \
		bedtools_src=$(bedtools_src) \
		bedtools_ext=$(bedtools_ext) \
		bedtools_package=$(bedtools_package) \
		bedtools_version=$(bedtools_version) \
		bedtools_args=$(bedtools_args) \
		recursive=$(recursive) args="$(args)" \
		bedtools_install bedtools_test
	[ -n "$(keep_install)" ] || rm -rf $(_install)
	@$(ECHO) "Done." >&2
	@$(ECHO) "\nFinished native build test for '$(bedtools_package)' module." >&2

bedtools_test_build_conda: conda_test_build
	@$(ECHO) "\nFinished Conda build test for '$(bedtools_package)' module." >&2

bedtools_test_build_docker: override args := make -f SK8/SK8.mk \
	module=$(addprefix SK8/modules/, $(notdir $(module))) \
	usermode=1 \
	bedtools_src=$(bedtools_src) \
	bedtools_ext=$(bedtools_ext) \
	bedtools_package=$(bedtools_package) \
	bedtools_version=$(bedtools_version) \
	bedtools_args=$(bedtools_args) \
	recursive=$(recursive) args="$(args)" \
	bedtools_install bedtools_test
bedtools_test_build_docker: docker_test_build
	@$(ECHO) "\nFinished Docker build test for '$(bedtools_package)' module." >&2

