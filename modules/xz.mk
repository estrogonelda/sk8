############################## HEADER ##########################################
# SK8 module for package 'xz'.
################################################################################



############################## PREAMBLE ########################################
# Non-standalone run verification.
this                           := xz.mk
ifeq ($(notdir $(firstword $(MAKEFILE_LIST))),$(this))
      $(error ERROR: Module '$(this)' cannot run in standalone mode, \
      it needs to be loaded by 'SK8')
endif

# Info to download, configure, build, install and use 'xz' package.
xz_src                    ?= https://downloads.sourceforge.net/project/lzmautils/xz-5.2.5.tar.gz
xz_ext                    ?= .tar.gz

xz_package                ?= $(shell perl -ne 'print "@{[ (split(/-/, $$_))[0] ]}"' <<< "$(notdir $(xz_src))")
xz_version                ?= $(shell perl -pe 's/^$(xz_package)-(.*?)\$(xz_ext)/$$1/g' <<< "$(notdir $(xz_src))")
xz_id                     ?= $(xz_package)-$(shell echo $$RANDOM)
xz_license                ?= GPLv4
xz_distname               ?= $(xz_package)-$(xz_version)

# Potential installation data.
xz_repository             ?= $(xz_src)
xz_tarball                ?= $(tarballsdir)/$(xz_distname)$(xz_ext)
xz_builddir               ?= $(tarballsdir)/$(xz_distname)
xz_configurefile          ?= $(xz_builddir)/configure
xz_buildfile              ?= $(xz_builddir)/Makefile
xz_execfile               ?= $(xz_builddir)/$(xz_package)

xz_prefix                 ?= $(if $(supprefix),$(dir $(installdir))/$(xz_package),$(dir $(installdir)))
xz_install_bin            ?= $(xz_prefix)/bin/$(xz_package)
xz_install_lib            ?= $(xz_prefix)/lib/lib$(xz_package).a
xz_install_include        ?= $(xz_prefix)/include/$(xz_package).h
xz_install_share          ?= $(xz_prefix)/share/man

xz_bins                   = $(wildcard $(xz_builddir)/bin/*)
xz_libs                   = $(wildcard $(xz_builddir)/lib/lib*)
xz_includes               = $(wildcard $(xz_builddir)/include/*.h)


# Actual installation data.
ifeq ($(islib),)
xz_installfile            ?= $(xz_install_bin)
xz_check                  ?= $(shell which $(xz_package) 2> /dev/null)
else
xz_installfile            ?= $(xz_install_lib)
xz_check                  ?= $(abspath $(strip $(wildcard $(basename $(xz_installfile)).*)))
endif

xz_get                    ?= $(if $(xz_check),,xz_install)
xz                        ?= $(or $(xz_check), $(xz_installfile))


# FORCE data.
xz_force                  ?= $(if $(FORCE),xz_force,)
xz_status                 ?= $(if $(FORCE),xz_install,$(xz_get))
xz                        ?= $(if $(FORCE),$(xz_installfile),$(xz))


# Include required modules.
#include $(addprefix $(sk8_rootdir)/modules/, gcc.mk)

xz_deps_status             ?= $(if $(recursive),xz_deps,)
xz_deps                    ?= $(gcc_status)



############################## MAIN ############################################
# Usage.
xz_help:
	@$(ECHO) "\n\
	NAME\n\
		\n\
		xz_package - $(xz_package), version $(xz_version)\n\
		\n\
	SYNOPSIS\n\
		\n\
		$$ sk9 [make_option ...] [variable=value ...] <xz_target ...>\n\
		\n\
	DESCRIPTION\n\
		\n\
		This is an engine to download, configure, build, install and test packages\n\
		in a xz way. So, the package must be compliant with the GNU Coding\n\
		Standards in order to be handled by SK8.\n\
		Now, the package in question is '$(xz_distname)'.\n\
		\n\
	OPTIONS\n\
		\n\
		Targets:\n\
		\n\
		xz_help                   Print this help text.\n\
		xz_version                Print '$(xz_package)' package version.\n\
		xz_status                 Verifies '$(xz_package)' current installation status.\n\
		xz_run                    Run '$(xz_package)' with the arguments given in the 'args' variable.\n\
		xz_install                Install '$(xz_package)' binary in '\$$(prefix)/bin' directory.\n\
		xz_build                  Compile the '$(xz_package)' binary in the build directory.\n\
		xz_configure              Configure '$(xz_package)' build files for this system.\n\
		xz_tarball                Download the '$(xz_package)' tarball from its repository: $(xz_src)\n\
		xz_uninstall              Uninstall '$(xz_package)' from the '\$$(prefix)/bin' directory.\n\
		xz_clear                  Remove '$(xz_package)' tarball from tarball''s directory.\n\
		xz_purge                  Uninstall and clear '$(xz_package)' from the system.\n\
		xz_refresh                Uninstall and then re-install '$(xz_package)' again.\n\
		xz_deps                   Install a '$(xz_package)' dependency recursivelly as passed by the 'args' variable.\n\
		xz_test                   Test '$(xz_package)' installation.\n\
		xz_test_build_native      Test '$(xz_package)' build process, natively.\n\
		xz_test_build_conda       Test '$(xz_package)' build process in a new Conda environment.\n\
		xz_test_build_docker      Test '$(xz_package)' build process inside a vanilla Docker container.\n\
		xz_test_build_aws         Test '$(xz_package)' build process inside an AWS instance. NOT WORKING!!\n\
		xz_create_img             Create a Docker image for '$(xz_package)', based on '$(image)' image.\n\
		xz_create_dockerfile      Create a Dockerfile to build an image for '$(xz_package)'.\n\
		xz_create_module          Create a pre-filled SK8 module for '$(xz_package)' package.\n\
		\n\
		conda_run                      Run '$(xz_package)' in a new Conda environment. See 'conda_help' for more info.\n\
		docker_run                     Run '$(xz_package)' in a vanilla Docker image. See 'docker_help' for more info.\n\
		aws_run                        Run '$(xz_package)' in an AWS instance. See 'aws_help' for more info. NOT WORKING!!\n\
		\n\
		Variables:\n\
		\n\
		args                           String of arguments passed to '$(xz_package)' when running.               [ '' ]\n\
		prefix                         Path to the directory where to put the built '$(xz_packages)' binaries.   [ /usr/local ]\n\
		DESTDIR                        Path to an alternative staged installation directory.                          [ '' ]\n\
		workdir                        The current directory.                                                         [ . ]\n\
		stageddir                      Path to a staged directory for tarballs and package builds.                    [ ./.local ]\n\
		usermode                       Boolean to switch between local or system-wide installations paths.            [ '' ]\n\
		recursive                      Boolean to enable recursive dependency installations.                          [ '' ]\n\
		FORCE                          Boolean to force a fresh installation over an existent one.                    [ '' ]\n\
		\n\
		src                            URL of the '$(xz_package)' repository.                                    [ '$(xz_src)' ]\n\
		ext                            The tarball extension. Only for extensions other than '.tar.gz'.               [ '.tar.gz' ]\n\
		xz_package                The name of the package, derived from its tarball''s name.                     [ '$(xz_package)' ]\n\
		xz_version                The version of the package, derived from its tarball''s name.                  [ '$(xz_version)' ]\n\
		\n\
		Make options:\n\
		\n\
		\n\
		NOTE: Some packages cannot build xzally. So, user must test the chosen target first.\n\
		For a list of known xzally installable packages, please read the\n\
		'$(sk8_rootdir)/modules/modules_urls.txt' file.\n\
		\n\
		\n\
	COPYRIGHT\n\
		\n\
		$(sk8_package), version $(sk8_version) for GNU/Linux.\n\
		Copyright (C) Bards Agrotechnologies, 2011-2021.\n\
		\n\
	SEE ALSO\n\
		\n\
		Search for 'help', 'docker_help', 'conda_help' and 'miscellaneus'\n\
		for other sources of command line help or visit <$(basename $(sk8_source))>\n\
		for full documentation."

xz_manual:
	@$(ECHO) "\n$(xz_package) v$(xz_version) for GNU/Linux.\n\
	Please, see <$(xz_src)> for full documentation." >&2

xz_version:
	@$(ECHO) "\n\
	Package:    $(xz_package)\n\
	Version:    $(xz_version)\n\
	Id:         $(xz_id)\n\
	License:    $(xz_license)\n\
	\n\
	Status:     $(xz_status)\n\
	Source:     $(xz_src)\n\
	Installdir: $(installdir)\n\
	\n\
	Runned with $(sk8_package), version $(sk8_version) for GNU/Linux.\n\
	Copyright (C) Bards Agrotechnologies, 2011-2021."


xz_run: $(xz_status)
	@$(ECHO) "\nRunning '$(xz)' with arguments '$(args)' ..." >&2
	$(xz) $(args)
	@$(ECHO) "Done." >&2


# Verification.
xz_status: $(xz_status) # 'xz_install' or none.
	@$(ECHO) "\nSystem has '$(xz_package)' up and running." >&2


# Install.
xz_install: $(xz_installfile) # $(installdir)/$(xz_package)
	@$(ECHO) "\nSystem has '$(xz_package)' in PATH: '$<'." >&2

$(xz_installfile): $(xz_execfile) | $(installdir) # just copy the executable file to $(installdir).
	@$(ECHO) "\nInstalling '$(xz_package)' on directory '$(xz_prefix)' ..." >&2
	$(MAKE) -C $(xz_builddir) install DESTDIR=$(DESTDIR) prefix=$(xz_prefix) -j4
	if [ -z "$$(perl -ne 'print if s#.*$(installdir).*#AA#g' <<< $${PATH})" ]; then \
		echo -e "export PATH=$(installdir):\$$PATH" >> ~/.bashrc; \
		source ~/.bashrc; \
		echo -e "WARNING: You may need to run 'source ~/.bashrc' to release the new \$$PATH."; \
	elif [ "$(xz)" != "$(xz_installfile)" ]; then \
		echo "WARNING: Previuos installation of '$(xz_package)' may be shadowing the new one in the \$$PATH."; \
	fi
	touch $@
	@$(ECHO) "Done." >&2


# Build.
xz_build: $(xz_execfile) # $(xz_builddir)/$(xz_package)

$(xz_execfile): $(xz_buildfile) | $(gcc_status) # a Makefile generates the executable file.
	@$(ECHO) "\nBuilding '$(xz_package)' on directory '$(xz_builddir)' ..." >&2
	$(MAKE) -C $(xz_builddir) -j4
	touch $@
	@$(ECHO) "Done." >&2


# Configure.
xz_configure: $(xz_buildfile) # $(xz_builddir)/Makefile

$(xz_buildfile): xz_configure_cmd := ./configure --prefix=$(dir $(installdir)) $(xz_args)
$(xz_buildfile): $(xz_configurefile)
	@$(ECHO) "\nConfiguring '$(xz_package)' on directory '$(xz_builddir)' ..." >&2
	cd $(xz_builddir); \
	$(xz_configure_cmd)
	touch $@
	@$(ECHO) "Done." >&2

$(xz_configurefile): $(xz_tarball) | $(xz_builddir)
	@$(ECHO) "\nExtracting '$(xz_package)' tarball to directory '$(xz_builddir)' ..." >&2
	if [ "$(xz_ext)" == ".tar.gz" ]; then \
		tar -xzvf $< -C $(xz_builddir) --strip-components=1; \
	elif [ "$(xz_ext)" == ".tar.xz" ]; then \
		tar -xJvf $< -C $(xz_builddir) --strip-components=1; \
	else \
		tar -xvf $< -C $(xz_builddir) --strip-components=1; \
	fi
	[ -s "$@" ] || { echo "ERROR: No file '$@' was extracted." >&2; exit 8; }
	touch $@
	@$(ECHO) "Done." >&2

$(xz_builddir):
	@$(ECHO) "\nCreating directory '$(xz_builddir)' ..." >&2
	mkdir -p $(xz_builddir)
	@$(ECHO) "Done." >&2


# Download.
xz_tarball: $(xz_tarball) # $(tarballsdir)/$(xz_tarball).

$(xz_tarball): $(xz_force) | $(tarballsdir) $(internet_status) $(xz_deps_status) # none or 'xz_deps'
	@$(ECHO) "\nDownloading '$(xz_package)' tarball from '$(xz_repository)' ..." >&2
	$(call downloader_cmd,$(dwlr),download,$(xz_repository),$@,)
	touch $@
	@$(ECHO) "Done." >&2

xz_deps: $(xz_deps)
	@$(ECHO) "\nRecursively installing dependecies for '$(xz_distname)' ..." >&2
	if [ -n "$(args)" ]; then \
		$(MAKE) -f $(firstword $(MAKEFILE_LIST)) recursive= $(args); \
	fi
	@$(ECHO) "Done." >&2

xz_force:
	@$(ECHO) "\nForce reinstallation of '$(xz_package)' on directory '$(installdir)'." >&2
	rm -f $(xz_tarball)
	@$(ECHO) "Done." >&2


xz_refresh: xz_uninstall
	if [ -z "$(usermode)" ]; then \
		$(MAKE) -f $(firstword $(MAKEFILE_LIST)) DESTDIR=$(DESTDIR) prefix=$(prefix) xz_install; \
	else \
		$(MAKE) -f $(firstword $(MAKEFILE_LIST)) DESTDIR=$(DESTDIR) prefix=$(prefix) xz_install usermode=1; \
	fi


# Uninstall and clean.
xz_purge: xz_uninstall xz_clean

xz_uninstall:
	@$(ECHO) "\nUninstalling '$(xz_package)' from directory '$(installdir)' ..." >&2
	[ -d "$(installdir)" ] || exit 1;
	if [ -f "$(xz_buildfile)" ]; then \
		$(MAKE) -C $(xz_builddir) uninstall DESTDIR=$(DESTDIR) prefix=$(dir $(installdir)) -j4; \
	else \
		rm -f $(xz_install_bin); \
		rm -f $(xz_install_include); \
		rm -f $(dir $(xz_install_lib))/pkgconfig/$(xz_package).pc; \
		rm -rf $(basename $(xz_install_lib)).*; \
		rm -rf $(xz_installfile)*; \
	fi
	@$(ECHO) "User must resolve \$$PATH in '~/.bashrc' file." >&2
	@$(ECHO) "Done." >&2

xz_clean:
	@$(ECHO) "\nCleaning '$(xz_package)' from staged directory '$(stageddir)' ..." >&2
	rm -rf $(tarballsdir)/$(xz_package)*
	@$(ECHO) "Done." >&2


# Unit Test.
xz_test: alt_name := $(or $(xz_alt_name),$(xz_package))
xz_test: $(xz_status)
	$(ECHO) "\nTesting installation of '$(xz_distname)' package ..." >&2
	if [ -f "$(xz_buildfile)" ]; then \
		if [ -n "$$(grep 'installcheck' $(xz_buildfile))" ]; then \
			$(MAKE) -C $(xz_builddir) installcheck; \
		else \
			$(MAKE) -C $(xz_builddir) check; \
		fi \
	else \
		if [ -n "$(islib)" ]; then \
			echo "int main() {}" | cc -xc - -L$(xz_installfile) -l$(alt_name) -o /dev/null; \
		else \
			$(xz_installfile) --version; \
		fi \
	fi >&2
	#$(call test_lib,$(alt_name),$(xz_prefix),$(args))
	$(ECHO) "Done." >&2


# Build tests.
args2 = $(MAKE) -f $(c_basedir)/SK8/$(notdir $(firstword $(MAKEFILE_LIST))) \
	FORCE=yes \
	usermode=1 \
	islib= \
	recursive= \
	module=/home/lion/SK8/modules/$(this) \
	xz_src=$(xz_src) \
	xz_ext=$(xz_ext) \
	xz_package=$(xz_package) \
	xz_version=$(xz_version) \
	xz_alt_name=$(xz_alt_name) \
	xz_args='$(xz_args)' \
	args='$(or $(args),--version)' \
	xz_test


xz_test_native: override args := $(args2)
xz_test_native: native_test_build | $(srctestsdir)
	$(ECHO) "\nFinished native build test for '$(xz_package)' package." >&2


xz_test_conda: override args := $(args2)
xz_test_conda: conda_test_build | $(srctestsdir)
	$(ECHO) "\nFinished Conda build test for '$(xz_package)' package." >&2


xz_test_docker: override args := $(args2)
xz_test_docker: docker_test_build | $(srctestsdir)
	$(ECHO) "\nFinished Docker build test for '$(xz_package)' package." >&2


xz_test_all: xz_test_docker xz_test_conda xz_test_native


.PHONY: xz_help xz_version xz_manual xz_force



############################## DOCUMENTATION ###################################
# Documentation was already written in the targets above.
