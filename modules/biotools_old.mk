############################## HEADER ##########################################
#
# SK8 module for Binformatics.
#
################################################################################





############################## PREAMBLE ########################################

# Non-standalone run verification.
this                           := biotools.mk
ifeq ($(notdir $(firstword $(MAKEFILE_LIST))),$(this))
      $(error ERROR: Module '$(this)' cannot run in standalone mode, \
      it needs to be loaded by 'SK8')
endif

# Include required modules.
$(warning MESSAGE: $(this) => Modules included: \
	$(addprefix $(sk8_rootdir)/modules/, parallel.mk samtools.mk bwa.mk))
include $(addprefix $(sk8_rootdir)/modules/, parallel.mk samtools.mk bwa.mk)


# Default variables for user interaction.
usermode                       := 1
data_mode                      ?= batch
data_type                      ?= tp
data_sample                    ?= all
data_control                   ?= solid tissue normal
data_control2                  ?=
data_control3                  ?=
data_treated                   ?= primary tumor
data_treated2                  ?=
data_treated3                  ?=
paired                         ?= no

ifeq ($(words $(data_sample)),1)
      radical                  ?= $(data_sample)
else
      radical                  ?= $(firstword $(data_sample))
endif

manifest                       ?= $(if $(inputfl),,$(radical).manifest)
manifest_type                  ?= tcga_manifest


# Important directories' paths.
rdir                           ?= $(sk8_rootdir)
gbgdir                         ?= /tmp


# Miscelaneous data.
misc_dir                       ?= $(datadir)/misc
gene_universe                  ?= $(misc_dir)/universe.tsv
blacklist                      ?= $(misc_dir)/blacklist.bed
cancer_genes                   ?= $(misc_dir)/cosmic_cancergenes.tsv

scripts_dir                    ?= $(rdir)/scripts
analyzer                       ?= $(scripts_dir)/rtc_analyser.pl
manifest_gen                   ?= $(scripts_dir)/manifest_gen.sh
payload_gen                    ?= $(scripts_dir)/payload_gen.sh


# UCSC data.
ucsc_latest_u                  := https://hgdownload.soe.ucsc.edu/goldenPath/hg38/bigZips/latest/hg38.fa.gz
ucsc_latest_chrom_sizes_u      := https://hgdownload.soe.ucsc.edu/goldenPath/hg38/bigZips/latest/hg38.chrom.sizes
ucsc_hg38_u                    := https://hgdownload.soe.ucsc.edu/goldenPath/hg38/bigZips/hg38.fa.gz
ucsc_hg19_u                    := https://hgdownload.soe.ucsc.edu/goldenPath/hg19/bigZips/hg19.fa.gz
ucsc_urls                      := $(ucsc_latest_u) $(ucsc_latest_chrom_sizes_u) $(ucsc_hg19_u)

ucsc_dir                       ?= $(datadir)/ucsc
ucsc_latest                    ?= $(ucsc_dir)/$(notdir $(ucsc_latest_u))
ucsc_latest_chrom_sizes        ?= $(ucsc_dir)/$(notdir $(ucsc_latest_chrom_sizes_u))
ucsc_hg38                      ?= $(ucsc_dir)/$(notdir $(ucsc_hg38_u))
ucsc_hg19                      ?= $(ucsc_dir)/$(notdir $(ucsc_hg19_u))
ucsc_all                       ?= $(ucsc_latest) $(ucsc_hg19)
genome_ref                     ?= $(ucsc_latest)
genome_idx                     ?= $(ucsc_latest).fai


# TCGA data.
tcga_endpoint_status           := https://api.gdc.cancer.gov/status
tcga_endpoint_manifest         := https://api.gdc.cancer.gov/manifest
tcga_endpoint_files            := https://api.gdc.cancer.gov/files
tcga_endpoint_data             := https://api.gdc.cancer.gov/data

tcga_radical                   := $(data_sample)
tcga_payload                   := $(tcga_radical).payl
tcga_precursor                 := $(tcga_payload:.payl=.prec)
tcga_manifest                  := $(tcga_payload:.payl=.manifest)
tcga_associations              := $(tcga_payload:.payl=.assoc)
tcga_tsv                       := $(tcga_payload:.payl=.tsv)
tcga_expr                      := $(tcga_payload:.payl=.expr)


# TCGA status verification.
tcga_has                       := $(shell echo `$(call downloader_cmd,$(dwlr),status,$(tcga_endpoint_status))`)
tcga_status                    := $(if $(tcga_has),,$(warning WARNING: TCGA server is unreachable!))


# Gencode data.
# NOTE: gencode v22 is for TCGA RNA-seq data and v26 is for GTEx.
gencode_latest_u               := ftp://ftp.ebi.ac.uk/pub/databases/gencode/Gencode_human/release_35/gencode.v35.annotation.gtf.gz
gencode_v22_u                  := ftp://ftp.ebi.ac.uk/pub/databases/gencode/Gencode_human/release_22/gencode.v22.annotation.gtf.gz
gencode_v26_collab_u           := https://storage.googleapis.com/gtex_analysis_v8/reference/gencode.v26.GRCh38.genes.gtf
gencode_urls                   := $(gencode_latest_u) $(gencode_v22_u) $(gencode_v26_collab_u)

gencode_dir                    ?= $(datadir)/gencode
gencode_latest                 ?= $(gencode_dir)/$(notdir $(gencode_latest_u))
gencode_v22                    ?= $(gencode_dir)/$(notdir $(gencode_v22_u))
gencode_v26                    ?= $(gencode_dir)/$(notdir $(gencode_v26_collab_u))
gencode_all                    ?= $(gencode_latest) $(gencode_v22) $(gencode_v26)
annotation_ref                 ?= $(gencode_latest)

ensembl_transcriptome_v103_u   := http://ftp.ensembl.org/pub/release-103/fasta/homo_sapiens/cdna/Homo_sapiens.GRCh38.cdna.all.fa.gz
ensembl_transcriptome_v103     ?= $(gencode_dir)/$(notdir $(ensembl_transcriptome_v103_u))
transcriptome_ref              ?= $(ensembl_transcriptome_v103)


# GTEx url and data.
gtex_attributes_u              := https://storage.googleapis.com/gtex_analysis_v8/annotations/GTEx_Analysis_v8_Annotations_SampleAttributesDS.txt
gtex_reads_u                   := https://storage.googleapis.com/gtex_analysis_v8/rna_seq_data/GTEx_Analysis_2017-06-05_v8_RNASeQCv1.1.9_gene_reads.gct.gz
gtex_tpm_u                     := https://storage.googleapis.com/gtex_analysis_v8/rna_seq_data/GTEx_Analysis_2017-06-05_v8_RNASeQCv1.1.9_gene_tpm.gct.gz
gtex_medians_u                 := https://storage.googleapis.com/gtex_analysis_v8/rna_seq_data/GTEx_Analysis_2017-06-05_v8_RNASeQCv1.1.9_gene_median_tpm.gct.gz

gtex_urls := $(gtex_attributes_u) $(gtex_reads_u) $(gtex_tpm_u) $(gtex_medians_u)

gtex_dir                       ?= $(datadir)/gtex
gtex_attributes                ?= $(gtex_dir)/$(notdir $(gtex_attributes_u))
gtex_reads                     ?= $(gtex_dir)/$(notdir $(gtex_reads_u))
gtex_tpm                       ?= $(gtex_dir)/$(notdir $(gtex_tpm_u))
gtex_medians                   ?= $(gtex_dir)/$(notdir $(gtex_medians_u))
gtex_all                       ?= $(gtex_attributes) $(gtex_reads) $(gtex_tpm) $(gtex_medians)


# GDC data.
gdc_dir                        ?= $(datadir)/gdc
gdc_token                      ?= $(gdc_dir)/gdc_token.txt
gdc_client                     ?= $(gdc_dir)/gdc-client
token                          ?= $(gdc_token)


# Operational vars.
jobs                           ?= 10
step                           ?= 10
aligner                        ?= bwa


lst                            := $(radical).lst
plt                            := $(radical).png
expr                           := $(radical)_tpm.tsv
mexpr                          := $(radical)_mexpr.tsv
deg                            := $(radical)_deg.tsv
gsea                           := $(radical)_gsea.tsv
dwl                            := $(addprefix $(outputsdir)/, $(notdir $(inputfl)))



############################## MACRO DEFINITIONS ###############################

define rec
      $(filter %$(1)$(sfx), $(inputfl))
endef


define opt
{                                                                              \
    "op":"=",                                                                  \
    "content":{                                                                \
        "field":"cases.project.project_id",                                    \
        "value":["TCGA-$(data_sample)"]                                        \
     }                                                                         \
},
endef

ifeq ($(data_sample),all)
      opt :=
endif


define payload_wgs
{                                                                              \
    "filters":{                                                                \
        "op":"and",                                                            \
        "content":[                                                            \
            {                                                                  \
                "op":"=",                                                      \
                "content":{                                                    \
                    "field":"cases.project.program.name",                      \
                    "value":["TARGET"]                                         \
                }                                                              \
            },$(opt)                                                           \
            {                                                                  \
                "op":"in",                                                     \
                "content":{                                                    \
                    "field":"cases.samples.sample_type",                       \
                    "value":["$(data_control)","$(data_treated)","$(data_treated2)"] \
                }                                                              \
            },                                                                 \
            {                                                                  \
                "op":"=",                                                      \
                "content":{                                                    \
                    "field":"files.data_format",                               \
                    "value":["bam"]                                            \
                }                                                              \
            },                                                                 \
            {                                                                  \
                "op":"=",                                                      \
                "content":{                                                    \
                    "field":"files.data_category",                             \
                    "value":["sequencing reads"]                               \
                }                                                              \
            },                                                                 \
            {                                                                  \
                "op":"=",                                                      \
                "content":{                                                    \
                    "field":"files.data_type",                                 \
                    "value":["Aligned Reads"]                                  \
                }                                                              \
            },                                                                 \
            {                                                                  \
                "op":"=",                                                      \
                "content":{                                                    \
                    "field":"files.experimental_strategy",                     \
                    "value":["WGS"]                                            \
                }                                                              \
            }                                                                  \
        ]                                                                      \
    },                                                                         \
    "format":"tsv",                                                            \
    "size":"90000",                                                            \
    "fields":"file_id,file_name,cases.case_id,cases.samples.sample_type,cases.project.project_id" \
}
endef

define payload_wxs
{                                                                              \
    "filters":{                                                                \
        "op":"and",                                                            \
        "content":[                                                            \
            {                                                                  \
                "op":"=",                                                      \
                "content":{                                                    \
                    "field":"cases.project.program.name",                      \
                    "value":["TCGA"]                                           \
                }                                                              \
            },$(opt)                                                           \
            {                                                                  \
                "op":"in",                                                     \
                "content":{                                                    \
                    "field":"cases.samples.sample_type",                       \
                    "value":["$(data_control)","$(data_treated)","$(data_treated2)"] \
                }                                                              \
            },                                                                 \
            {                                                                  \
                "op":"=",                                                      \
                "content":{                                                    \
                    "field":"files.data_format",                               \
                    "value":["bam"]                                            \
                }                                                              \
            },                                                                 \
            {                                                                  \
                "op":"=",                                                      \
                "content":{                                                    \
                    "field":"files.data_category",                             \
                    "value":["sequencing reads"]                               \
                }                                                              \
            },                                                                 \
            {                                                                  \
                "op":"=",                                                      \
                "content":{                                                    \
                    "field":"files.data_type",                                 \
                    "value":["Aligned Reads"]                                  \
                }                                                              \
            },                                                                 \
            {                                                                  \
                "op":"=",                                                      \
                "content":{                                                    \
                    "field":"files.experimental_strategy",                     \
                    "value":["WXS"]                                            \
                }                                                              \
            }                                                                  \
        ]                                                                      \
    },                                                                         \
    "format":"tsv",                                                            \
    "size":"90000",                                                            \
    "fields":"file_id,file_name,cases.case_id,cases.samples.sample_type,cases.project.project_id" \
}
endef

define payload_wts
{                                                                              \
    "filters":{                                                                \
        "op":"and",                                                            \
        "content":[                                                            \
            {                                                                  \
                "op":"=",                                                      \
                "content":{                                                    \
                    "field":"cases.project.program.name",                      \
                    "value":["TCGA"]                                           \
                }                                                              \
            },$(opt)                                                           \
            {                                                                  \
                "op":"in",                                                     \
                "content":{                                                    \
                    "field":"cases.samples.sample_type",                       \
                    "value":["$(data_control)","$(data_treated)","$(data_treated2)"] \
                }                                                              \
            },                                                                 \
            {                                                                  \
                "op":"=",                                                      \
                "content":{                                                    \
                    "field":"files.data_format",                               \
                    "value":["bam"]                                            \
                }                                                              \
            },                                                                 \
            {                                                                  \
                "op":"=",                                                      \
                "content":{                                                    \
                    "field":"files.data_category",                             \
                    "value":["sequencing reads"]                               \
                }                                                              \
            },                                                                 \
            {                                                                  \
                "op":"=",                                                      \
                "content":{                                                    \
                    "field":"files.data_type",                                 \
                    "value":["Aligned Reads"]                                  \
                }                                                              \
            },                                                                 \
            {                                                                  \
                "op":"=",                                                      \
                "content":{                                                    \
                    "field":"files.experimental_strategy",                     \
                    "value":["RNA-Seq"]                                        \
                }                                                              \
            }                                                                  \
        ]                                                                      \
    },                                                                         \
    "format":"tsv",                                                            \
    "size":"90000",                                                            \
    "fields":"file_id,file_name,cases.case_id,cases.samples.sample_type,cases.project.project_id" \
}
endef

define payload_tp
{                                                                              \
    "filters":{                                                                \
        "op":"and",                                                            \
        "content":[                                                            \
            {                                                                  \
                "op":"=",                                                      \
                "content":{                                                    \
                    "field":"cases.project.program.name",                      \
                    "value":["TCGA"]                                           \
                }                                                              \
            },$(opt)                                                           \
            {                                                                  \
                "op":"in",                                                     \
                "content":{                                                    \
                    "field":"cases.samples.sample_type",                       \
                    "value":["$(data_control)","$(data_treated)","$(data_treated2)"] \
                }                                                              \
            },                                                                 \
            {                                                                  \
                "op":"=",                                                      \
                "content":{                                                    \
                    "field":"files.analysis.workflow_type",                    \
                    "value":["HTSeq - Counts"]                                 \
                }                                                              \
            },                                                                 \
            {                                                                  \
                "op":"=",                                                      \
                "content":{                                                    \
                    "field":"files.data_category",                             \
                    "value":["transcriptome profiling"]                        \
                }                                                              \
            },                                                                 \
            {                                                                  \
                "op":"=",                                                      \
                "content":{                                                    \
                    "field":"files.data_type",                                 \
                    "value":["Gene Expression Quantification"]                 \
                }                                                              \
            },                                                                 \
            {                                                                  \
                "op":"=",                                                      \
                "content":{                                                    \
                    "field":"files.experimental_strategy",                     \
                    "value":["RNA-Seq"]                                        \
                }                                                              \
            }                                                                  \
        ]                                                                      \
    },                                                                         \
    "format":"tsv",                                                            \
    "size":"90000",                                                            \
    "fields":"file_id,file_name,cases.case_id,cases.samples.sample_type,cases.project.project_id" \
}
endef

# Decide payload type.
payload_gen                    := $(payload_tp)
ifeq ($(data_type),wgs)
      payload_gen              := $(payload_wgs)
endif
ifeq ($(data_type),wxs)
      payload_gen              := $(payload_wxs)
endif
ifeq ($(data_type),wts)
      payload_gen              := $(payload_wts)
endif
$(if $(filter $(data_type), wgs wxs wts tp),,\
	$(error ERROR: Unsuported data_type '$(data_type)'))


# Manifest filter. Never use comments in scripts like this (in the same line).
define manifest_gen
	perl -F"\t" -MList::Util=min,max -ane '                                    \
		chomp;                                                                 \
		print "CASE_ID\tTUMOR_TYPE\tSAMPLE_TYPE\tFILE_ID\tFILE_NAME\tID\n" if /file_id/; \
		next if /file_id/;                                                     \
		                                                                       \
		@groups = ("Solid Tissue Normal", "Blood Derived Normal",              \
			"Primary Tumor", "Metastatic");                                    \
		if ( "$(data_sample)" eq "all" ) {                                     \
			push @{ $$a{$$F[0]}{$$F[2]} } => $$_;                              \
		}                                                                      \
		else {                                                                 \
			push @{ $$a{$$F[0]}{$$F[2]} } => $$_ if $$F[1] =~ /TCGA-$(data_sample)/; \
		}                                                                      \
		$$gp{$$F[2]}++;                                                        \
		                                                                       \
		END {                                                                  \
			$$num = scalar keys %gp;                                           \
			foreach my $$k ( sort keys %a ) {                                  \
				$$j = 0;                                                       \
				$$prev = 0;                                                    \
				$$mn = 0;                                                      \
				$$mx = 0;                                                      \
				%prs = {};                                                     \
				@arr = ();                                                     \
				foreach my $$g ( @groups ) {                                   \
					next unless grep { $$_ eq $$g } keys %gp;                  \
					push @arr => $$g;                                          \
					@{ $$prs{$$g} } = sort { (split "\t", $$a)[4] cmp (split "\t", $$b)[4] } @{ $$a{$$k}{$$g} }; \
					$$mn = min($$prev, $$#{ $$a{$$k}{$$g} });                  \
					$$mx = max($$prev, $$#{ $$a{$$k}{$$g} });                  \
					$$prev = $$#{ $$a{$$k}{$$g} };                             \
				}                                                              \
				if ( $$mn != -1 and $$mn != $$mx ) {                           \
					$$ch_min = "";                                             \
					$$ch_max = "";                                             \
					for my $$g ( @arr ) {                                      \
						$$ch_min = $$g if $$#{ $$prs{$$g} } == $$mn;           \
						$$ch_max = $$g if $$#{ $$prs{$$g} } == $$mx;           \
					}                                                          \
					AGAIN: if ( $$ch_min and $$ch_max ) {                      \
						$$ptrn = (split "\t", @{ $$prs{$$ch_min} }[0])[4];     \
						$$ptrn = (split "-", $$ptrn)[0];                       \
						unless ( length($$ptrn) > 9                            \
							or @{ $$prs{$$ch_max} }[0] =~ /$$ptrn/             \
							or $$j > $$mx ) {                                  \
							$$tp = shift @{ $$prs{$$ch_max} };                 \
							push @{ $$prs{$$ch_max} } => $$tp;                 \
						}                                                      \
					}                                                          \
				}                                                              \
				PONGA: foreach my $$idx ( 0..$$mx ) {                          \
					for my $$g ( @arr ) {                                      \
						print "$$prs{$$g}->[$$idx]\n" if "$(paired)" ne "no"   \
							and $$idx <= $$mn;                                 \
						                                                       \
						if ( $$prs{$$g}->[$$idx] ) {                           \
							print STDERR (split /\t/, $$prs{$$g}->[$$idx])[4]; \
						}                                                      \
						else { print STDERR "."; }                             \
						print STDERR "\t" unless $$g eq $$arr[-1];             \
					}                                                          \
					print STDERR "\n";                                         \
				}                                                              \
				                                                               \
				unless ( "$(paired)" ne "no" ) {                               \
					foreach ( sort keys %{ $$a{$$k} } ) {                      \
						print join("\n", @{ $$a{$$k}{$$_} }), "\n" if @{ $$a{$$k}{$$_} }; \
					}                                                          \
				}                                                              \
			}                                                                  \
		}'
endef


define tpm_gen
	BEGIN {                                                                \
		$$ck = 0;                                                          \
	}                                                                      \
		                                                                   \
	$$act = $$ARGV, $$ck++ unless $$ck;                                    \
	next if /^#/;                                                          \
		                                                                   \
	if ( $$act eq $$ARGV ) {                                               \
		if ( $$F[2] eq "gene" ) {                                          \
			$$ensembl = $$1 if $$F[8] =~ /gene_id "(.*?)";/;               \
			$$name = $$1 if $$F[8] =~ /gene_name "(.*?)";/;                \
			$$type = $$1 if $$F[8] =~ /gene_type "(.*?)";/;                \
			$$genes{$$ensembl} = [ $$name, 1, $$type, $$F[0] ];            \
			$$gene_ck = 0;                                                 \
		}                                                                  \
		elsif ( $$F[2] eq "transcript" ) {                                 \
			if ( $$gene_ck ) {                                             \
				$$gene_keep = $$gene_acm if $$gene_acm > $$gene_keep;      \
			}                                                              \
			else {                                                         \
				$$genes{$$gene_prev}->[1] = max($$gene_keep, 1) if defined $$gene_prev; \
				$$genes{$$gene_prev}->[1] = max($$gene_acm, 1) if defined $$gene_prev and $$gene_keep == 0; \
				$$gene_keep = 0;                                           \
			}                                                              \
			$$gene_prev = $$ensembl;                                       \
			$$gene_ck++;                                                   \
			$$gene_acm = 0;                                                \
		}                                                                  \
		elsif ( $$F[2] eq "exon" ) {                                       \
			$$gene_acm += abs($$F[3] - $$F[4]);                            \
		}                                                                  \
		else {}                                                            \
	}                                                                      \
	else {                                                                 \
		if ( $$ARGV =~ /GTEx/ or $$ARGV =~ /.dev.fd/ ) {                   \
			next if scalar(@F) < 3;                                        \
			if ( /^Name/ ) {                                               \
				@names = @F[2..$$#F];                                      \
				chomp $$names[-1];                                         \
				next;                                                      \
			}                                                              \
				                                                           \
			next unless exists $$genes{$$F[0]};                            \
				                                                           \
			$$rpk = $$genes{$$F[0]}->[1];                                  \
			next if $$rpk == 0;                                            \
				                                                           \
			push @gns => $$F[0];                                           \
			foreach ( 0..$$#names ) {                                      \
				$$mtx{$$names[$$_]}{$$F[0]} = $$F[$$_+2]/$$rpk;            \
				$$mtx{$$names[$$_]}{"acm"} += $$F[$$_+2]/$$rpk;            \
			}                                                              \
		}                                                                  \
		else {                                                             \
			next if /^_/;                                                  \
			next unless exists $$genes{$$F[0]};                            \
				                                                           \
			push @names => (split(m#/#, $$ARGV))[-1] if $$argv ne $$ARGV;  \
			$$argv = $$ARGV;                                               \
			push @gns => $$F[0] if scalar @names == 1;                     \
				                                                           \
			$$rpk = $$genes{$$F[0]}->[1];                                  \
			$$mtx{$$names[-1]}{$$F[0]} = $$F[1]/$$rpk;                     \
			$$mtx{$$names[-1]}{"acm"} += $$F[1]/$$rpk;                     \
		}                                                                  \
	}                                                                      \
		                                                                   \
	END {                                                                  \
		$$mtx{$$_}{"acm"} /= 1000000 for @names;                           \
		print "## Annotation file: $$act\n";                               \
		print "GENE_ID\tGENE_NAME\tGENE_TYPE\tGENE_LENGTH\t", join("\t", sort @names), "\n"; \
		foreach my $$gn ( sort @gns ) {                                    \
			$$xtra = "";                                                   \
			$$xtra = "(MT)" if $$genes{$$gn}->[3] eq "chrM";               \
			print "$$gn\t$$genes{$$gn}->[0]\t$$genes{$$gn}->[2]$$xtra\t$$genes{$$gn}->[1]"; \
			foreach my $$nm ( sort @names ) {                              \
				$$res = 0;                                                 \
				if ( $$mtx{$$nm}{"acm"} < 1e-6 ) {                         \
					print "\t-1";                                          \
				}                                                          \
				elsif ( ($$mtx{$$nm}{$$gn} / $$mtx{$$nm}{"acm"}) < 1e-4 ) { \
					print "\t0";                                           \
				}                                                          \
				else {                                                     \
					$$res = $$mtx{$$nm}{$$gn} / $$mtx{$$nm}{"acm"};        \
					printf("\t%06.4f", $$res);                             \
				}                                                          \
			}                                                              \
			print "\n";                                                    \
		}                                                                  \
	}
endef


define quantify
	perl -ane '                                                                \
		BEGIN { $$ck = 0; }                                                    \
		$$act = $$ARGV, $$ck++ unless $$ck;                                    \
		                                                                       \
		if ( $$act eq $$ARGV ) {                                               \
			next if /^Name/;                                                   \
			next if /^_/;                                                      \
			push @{ $$c{$$F[0]} } => $$F[1];                                   \
		}                                                                      \
		else {                                                                 \
			if ( /^GENE_ID/ ) {                                                \
				$$pos = 0;                                                     \
				for ( 4..$$#F ) {                                              \
					$$pos = $$_, last if $$act eq $$F[$$_];                    \
				}                                                              \
			}                                                                  \
			next if /^#/ or (! $$pos);                                         \
			next unless exists $$c{$$F[0]};                                    \
			                                                                   \
			push @{ $$c{$$F[0]} } => @F[0, 3, 3, $$pos];                       \
		}                                                                      \
		                                                                       \
		END {                                                                  \
			print "Name\tLength\tEffectiveLength\tTPM\tNumReads\n";            \
			print join("\t", @{ $$c{$$_} }[1..4, 0]), "\n"                     \
				foreach sort keys %c;                                          \
		}'
endef



############################## MAIN TARGETS ####################################

biotools_help:
	@$(ECHO) "\nUnder development. Arguments: '$(args)'." >&2

biotools_version:
	@$(ECHO) "\nUnder development. Arguments: '$(args)'." >&2

biotools_test:
	@$(ECHO) "\nA simple test for Biotools module. Arguments: '$(args)'." >&2

biotools_rec: $(manifest)
	@$(ECHO) "\nRecursively calling SK8 with commands '$(args)'." >&2
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) $(args)
	@$(ECHO) "Done." >&2



############################## COMMONS #########################################

manifest: $(manifest_type) # 'tcga_manifest' or '1KG_manifest'. 
	@$(ECHO) "\nManifest complete: $^." >&2


# TCGA files manifest.
tcga_manifest: $(tcga_manifest)

$(tcga_manifest): $(tcga_precursor)
	@$(ECHO) "\nCreating manifest file '$@' from precursor file '$<' ..." >&2
	$(manifest_gen) $< > $@ 2> $(tcga_associations)
	[ -s "$(tcga_associations)" ] || ( echo "ERROR: Creation failed for file '$@'." >&2; exit 7 )
	@$(ECHO) "Done." >&2


tcga_precursor: $(tcga_precursor)

$(tcga_precursor): $(tcga_payload) $(internet_get)
	@$(ECHO) "\nCreating precursor file '$@' from GDC payload file '$<' ..." >&2
	$(call downloader_cmd,$(dwlr),query,$(tcga_endpoint_files),$@,$<)
	[ -s "$@" ] || ( echo "ERROR: Download of precursor file '$@' failed." >&2; exit 7 )
	@$(ECHO) "Done." >&2


tcga_payload: $(tcga_payload)

.SECONDEXPANSION:
$(tcga_payload): $(manifest_FORCE)
	@$(ECHO) "\nCreating GDC payload file for data type '$(data_type)' and tumor type '$(data_sample)' ..." >&2
	cat <<< '$(payload_gen)' > $@
	@$(ECHO) "Done." >&2

tcga_stt:
	@$(ECHO) "\nTCGA server status." >&2
	$(call downloader_cmd,$(dwlr),status,$(tcga_endpoint_status))
	@$(ECHO) "Done" >&2


# UCSC files manifest (1K genomes).
1KG_manifest: $(1KG_manifest)
	@$(ECHO) "\nUnsupported manifest type." >&2
	exit 7


manifest_FORCE:
	@$(ECHO) "\nForcing payload file '$(tcga_payload)' recriation." >&2
	rm -f $(tcga_payload)
	@$(ECHO) "Done." >&2



############################## CONTINUOUS MODE #################################
ifeq ($(batch),)


ifneq ($(MAKELEVEL),0)
ifneq ($(manifest),)
      dwl := $(addprefix $(outputsdir)/, \
		$(shell perl -F'\t' -lane 'next if /^CASE_ID/; print $$F[4]' "$(manifest)" 2> /dev/null))
endif

$(if $(dwl),,$(error ERROR: Either manifest file and input list are empty.))
endif


extr                           := $(basename $(dwl))

ifneq ($(filter %.bam, $(dwl)),)
dln                            := $(dwl:.bam=_dealigned_R1.fastq)
aln                            := $(dln:_dealigned_R1.fastq=_realigned.bam)
srt                            := $(if $(realign), $(aln:.bam=_sorted.bam), $(dwl:.bam=_sorted.bam))
endif

ifneq ($(filter %.fastq, $(dwl)),)
aln                            := $(filter %_aligned.bam, $(dwl:1.fastq=_aligned.bam))
srt                            := $(aln:.bam=_sorted.bam)
endif

ifneq ($(filter %.counts.gz, $(dwl)),)
quant                          := $(extr:.counts=.sf)
endif


analyze: $(manifest)
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) anlz

anlz: $(anlz)
	@$(ECHO) "\nAll analysis complete of files '$^'." >&2

$(anlz): $($(precursors)) | $(samtools_status)
	@$(ECHO) "\nAnalyzing file '$@' from '$^' ..." >&2
	@$(ECHO) "Done." >&2


sort: $(manifest)
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) srt


srt: $(srt)
	@$(ECHO) "\nAll sortings complete of files '$^'." >&2

ifneq ($(filter %.bam, $(dwl)),)
$(srt): %_sorted.bam: %.bam | $(samtools_status)
	@$(ECHO) "\nSorting file '$@' from '$^' ..." >&2
	if [ "$(sort_bam)" == "never" ]; then \
		echo "BAM file '$<' wont be sort."; \
		ln -s $< $@; \
	elif [ "$(sort_bam)" == "queryname" ]; then \
		samtools sort -n -O BAM -m 1G -@4 $< -o $@; \
	elif [ -n "$(sort_bam)" ]; then \
		samtools sort -O BAM -m 1G -@4 $< -o $@; \
	elif [ -n "$$(samtools view -H $< 2> /dev/null | perl -le '$$a = <>; print 1 if $$a =~ /SO:coordinate/')" ]; then \
		echo "BAM File '$<' was already sorted by coordinate."; \
		ln -s $< $@; \
	else \
		samtools sort -O BAM -m 1G -@4 $< -o $@; \
	fi
	@$(ECHO) "Done." >&2

else
$(srt): %_sorted.bam: %_aligned.bam | $(samtools_status)
	@$(ECHO) "\nSorting file '$@' from '$^' ..." >&2
	if [ "$(sort_bam)" == "never" ]; then \
		echo "BAM file '$<' wont be sort."; \
		ln -s $< $@; \
	elif [ "$(sort_bam)" == "name" ]; then \
		samtools sort -n -O BAM -m 1G -@4 $< -o $@; \
	elif [ -n "$(sort_bam)" ]; then \
		samtools sort -O BAM -m 1G -@4 $< -o $@; \
	elif [ -n "$$(samtools view -H $< 2> /dev/null | perl -le '$$a = <>; print 1 if $$a =~ /SO:coordinate/')" ]; then \
		echo "BAM File '$<' was already sorted by coordinate."; \
		ln -s $< $@; \
	else \
		samtools sort -O BAM -m 1G -@4 $< -o $@; \
	fi
	@$(ECHO) "Done." >&2

endif


align: $(manifest)
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) aln

aln: $(aln)
	@$(ECHO) "\nAll alignments complete for files '$^'." >&2

ifneq ($(filter %.bam, $(dwl)),)
$(aln): %_realigned.bam: $(genome_ref) %_dealigned_R1.fastq %_dealigned_R2.fastq | $(bwa_status) $(samtools_status)
	@$(ECHO) "\nAligning file '$@' from '$^' ..." >&2
	if [ "$(aligner)" == "bwa" ]; then \
		bwa mem -t$(jobs) $^ | samtools sort -O BAM -m 1G -@4 -o $@; \
	else \
		star -t$(jobs) $^ | samtools view -b -o $@; \
	fi
	[ -s "$@" ] || ( echo "ERROR: Alignment failed for file '$@'." >&2; exit 7 )
	@$(ECHO) "Done." >&2

else
$(aln): %_aligned.bam: $(genome_ref) %1.fastq %2.fastq | $(bwa_status) $(samtools_status)
	@$(ECHO) "\nAligning file '$@' from '$^' ..." >&2
	if [ "$(aligner)" == "bwa" ]; then \
		bwa mem -t$(jobs) $^ | samtools sort -O BAM -m 1G -@4 -o $@ -; \
	else \
		star -t$(jobs) $^ | samtools view -b -o $@ -; \
	fi
	[ -s "$@" ] || ( echo "ERROR: Alignment failed for file '$@'." >&2; exit 7 )
	@$(ECHO) "Done." >&2

endif


dealign: $(manifest)
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) dln

dln: $(dln)
	@$(ECHO) "\nAll dealignments complete for files '$^'." >&2

$(dln): %_dealigned_R1.fastq: %.bam | $(samtools_status)
	@$(ECHO) "\nExtracting file '$@' from '$<' ..." >&2
	if [ -n "$$(samtools view -H $< 2> /dev/null | perl -le '$$a = <>; print 1 if $$a =~ /SO:queryname/')" ]; then \
		$(samtools) fastq -1 $@ -2 $(@:_R1.fastq=_R2.fastq) $< > $@; \
	else \
		$(samtools) sort -n -O BAM -m 1G -@4 $< | $(samtools) fastq -1 $@ -2 $(@:1.fastq=2.fastq) > $@; \
	fi
	[ -s "$@" ] || ( echo "ERROR: Dealignment failed for file '$@'." >&2; exit 7 )
	@$(ECHO) "Done." >&2


list: $(manifest)
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) lst

lst: $(lst)
	@$(ECHO) "\nAll lists complete for files '$^'." >&2

$(lst): $(if $(list_choice), $($(list_choice)), $(dwl))
	@$(ECHO) "\nCreating list file '$@' from files '$^' ..." >&2
	[ -n "$^" ] || ( echo "ERROR: No file specified for '$@'." >&2; exit 7 )
	for i in $^; do echo "$$i"; done > $@
	@$(ECHO) "Done." >&2


extract: $(manifest)
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) extr

extr: $(extr)
	@$(ECHO) "\nAll extractions complete for files: '$^'." >&2

$(extr): %: %.gz
	@$(ECHO) "\nExtracting file '$@' from gzipped file '$<' ..." >&2
	gunzip -fk $<
	[ -s "$@" ] || ( rm -f $<; echo "ERROR: Extraction produced an empty file '$@'." >&2; exit 7 )
	@$(ECHO) "Done." >&2


downloads: $(manifest)
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) dwl

dwl: $(dwl)
	@$(ECHO) "\nAll downloads complete for files: '$^'." >&2

ifneq ($(manifest),)
$(dwl): $(manifest) | $(token) $(outputsdir)
	@$(ECHO) "\nDownloading file '$@' from manifest file '$<' ..." >&2
	[ -s "$<" ] || ( echo "ERROR: Manifest file '$<' is empty." >&2; exit 7 )
	id=$$(grep `basename $@` $< | cut -f4); \
	$(call downloader_cmd,$(dwlr),download_token,$(tcga_endpoint_data)/$${id},$@)
	[ -s "$@" ] || ( echo "ERROR: Downloaded file '$@' is empty" >&2; exit 7 )
	touch $@
	@$(ECHO) "Done." >&2


else
$(dwl): | $(inputfl) $(outputsdir)
	@$(ECHO) "\nLinking file '$@' from input list '$(inputfl)' ..." >&2
	inst=$(filter %$(notdir $@), $(inputfl)); \
	if [ "$@" != "$${inst}" ]; then \
		ln -fs `readlink -f $${inst}` $@; \
	fi
	#touch $@
	@$(ECHO) "Done." >&2

endif



############################## SQuIRE ##########################################

# Needs the activation of the SQuIRE conda environment.
#include $(sk8_rootdir)/modules/squire.mk

squire_fetch                   := fetch.log
squire_clean                   := clean.log
squire_map                     := $(dln:1.fastq=_map.log)
squire_count                   := $(dln:1.fastq=_count.log)
squire_call                    := call.log

squire_fetchflags := -f -c -r -g -x
squire_fetchdir := fetch
squire_cleandir := clean
squire_mapdir := map
squire_countdir := count
squire_calldir := call
squire_blddir := hg38


squire_class := LINE
squire_family := L1
squire_subfamily := L1HS


squire_all: $(manifest)
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) squire_cl


squire_fetch: $(squire_fetch)

$(squire_fetch): | $(squire_status)
	@$(ECHO) "\nFetching file requisites for 'SQuIRE' ..." >&2
	squire Fetch \
		-b $(squire_blddir) \
		-o $(squire_fetchdir) \
		$(squire_fetchflags) \
		-p 4 -v 2> $@
	@$(ECHO) "Done." >&2


squire_clean: $(squire_clean)

$(squire_clean): $(squire_fetch) | $(squire_status)
	@$(ECHO) "\nCleaning files for 'SQuIRE' ..." >&2
	squire Clean \
		-b $(squire_blddir) \
		-o $(squire_cleandir) \
		-c $(squire_class) \
		-f $(squire_family) \
		-s $(squire_subfamily) \
		-r $(squire_fetchdir)/$(squire_blddir)_rmsk.txt \
		-p 4 -v 2> $@
	@$(ECHO) "Done." >&2


squire_map: $(squire_map)

$(squire_map): %_map.log: %1.fastq $(squire_clean) | $(squire_status)
	@$(ECHO) "\nMapping files '$<' and '$(<:1.fastq=2.fastq)' ..." >&2
	r2="-2 $(<:1.fastq=2.fastq)"; \
	[ -s "$(<:1.fastq=2.fastq)" ] || r2=""; \
	squire Map \
		-b $(squire_blddir) \
		-o $(squire_mapdir) \
		-f $(squire_fetchdir) \
		-g $(annotation_ref) \
		-1 $< $${r2} \
		-n $(*F) \
		-r $(shell perl -lane 'print(length($$_)), die if $$. == 2' $< 2> /dev/null) \
		-p 4 -v 2> $@
	@$(ECHO) "Done." >&2


squire_count: $(manifest)
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) squire_ct

squire_ct: $(squire_count)

$(squire_count): %_count.log: %_map.log | $(squire_status)
	@$(ECHO) "\nCounting file '$<' ..." >&2
	squire Count \
		-b $(squire_blddir) \
		-o $(squire_countdir) \
		-f $(squire_fetchdir) \
		-c $(squire_cleandir) \
		-m $(squire_mapdir) \
		-t $(squire_countdir) \
		-n $(*F) \
		-r $(shell perl -lane 'print(length($$_)), die if $$. == 2' $(dln) 2> /dev/null) \
		-p 4 -v 2> $@
	@$(ECHO) "Done." >&2


squire_call: $(manifest)
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) squire_cl

squire_cl: $(squire_call)

$(squire_call): $(squire_count) | $(squire_status)
	@$(ECHO) "\nCalling TEs from files '$<' ..." >&2
	g1="$(^:_count.log=)"; \
	g2=""; \
	squire Call \
		-o $(squire_calldir) \
		-1 $${g1} \
		-2 $${g2} \
		-A treated \
		-B control \
		-N ponga \
		-f pdf \
		-p 4 -v 2> $@
	@$(ECHO) "Done." >&2


squire_create_env: | $(conda_status)
	@$(ECHO) "\nQuantifying transcripts expression in files '$^' ..." >&2
	if [ -n "$$(conda list -n squire 2> /dev/null)" ]; then \
		echo "Conda environment already exists."; \
	else \
		conda create -y --name squire --override-channels \
			-c iuc -c bioconda -c conda-forge -c defaults -c r \
			python=2.7.13 \
			bioconductor-deseq2=1.16.1 \
			r-base=3.4.1 \
			r-pheatmap \
			bioconductor-vsn \
			bioconductor-biocparallel=1.12.0 \
			r-ggrepel \
			star=2.5.3a \
			bedtools=2.25.0 \
			samtools=1.1 \
			stringtie=1.3.3 \
			igvtools=2.3.93 \
			ucsc-genepredtobed \
			ucsc-gtftogenepred \
			ucsc-genepredtogtf \
			ucsc-bedgraphtobigwig \
			r-hexbin; \
		git clone https://github.com/wyang17/SQuIRE.git; \
		cd SQuIRE; pip install -e .; \
		perl -i -pe 's#"/bin/sh"#"/bin/bash"#g' squire/Count.py; \
	fi
	@$(ECHO) "MESSAGE: User must activate the environment before use it:" >&2
	@$(ECHO) "    \$$ conda activate squire\n" >&2
	@$(ECHO) "Done." >&2



############################## BATCH MODE ######################################
else

num_lines                      := $(words $(inputfl) ponga)
ifneq ($(MAKELEVEL),0)
ifneq ($(manifest),)
      num_lines                := $(shell wc -l $(manifest) | awk '{ print $$1 }')
endif
endif

sfx_end                        := $(shell perl -e '$$a = $(num_lines) - 1; $$b = $(step); $$k = $$a / $$b; $$k = int($$a/$$b) + 1 if $$a % $$b; print $$k;')
sfx_list                       := $(shell seq -f "%04g" 1 $(sfx_end))
dwl1                           := $(addprefix $(outputsdir)/, $(sfx_list))
dwl2                           := $(addsuffix /, $(dwl1))
dwl3                           := $(join $(dwl2), $(sfx_list))
dwl                            := $(addsuffix _$(radical).dwl, $(dwl3))

lst                            := $(dwl:.dwl=.lst)
dln                            := $(dwl:.dwl=.dln)
db                             := $(dwl:.dwl=.db)
vcf                            := $(dwl:.dwl=.vcf)

sider_tsv                      := $(radical).tsv


dealign: $(manifest)
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) dln batch=1

dln: $(dln)

$(dln): %.dln: %.lst
	@$(ECHO) "\nDealigning files from list '$<' ..."
	parallel samtools sort -n -O BAM -m1G {} '|' \
		samtools fastq -1 {.}_dealigned_R1.fastq -2 {.}_dealigned_R2.fastq '>' {.}_dealigned_R1.fastq :::: $<; \
	[ $? -eq 0 ] || ( echo "ERROR: Dealignment failed for file '$@'." >&2; exit 7; )
	touch $@
	@$(ECHO) "Done." >&2
	

downloads list: $(manifest)
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) lst batch=1

lst: $(lst)

ifneq ($(manifest),)
$(lst): %.lst: %.dwl | $(gdc_token) $(parallel_status)
	@$(ECHO) "\nDownloading files by ID from '$<' ..." >&2
	tk=$$(< $(gdc_token)); \
	mn="$(abspath $(manifest))"; \
	cd $(*D); \
	if [[ "$(dwlr)" =~ curl ]]; then \
		parallel -q -j $(jobs) curl --remote-name --remote-header-name --header "X-Auth-Token: $${tk}" "$(tcga_endpoint_data)/{}" :::: $<; \
		[ $$? -eq 0 ] || ( echo "ERROR: Download fail in parallel." >&2; exit 7; ); \
	else \
		parallel -q -j $(jobs) wget -c --header "X-Auth-Token: $${tk}" "$(tcga_endpoint_data)/{}" -O {}.k :::: $<; \
		[ $$? -eq 0 ] || ( echo "ERROR: Download fail in parallel." >&2; exit 7; ); \
		for fl in $(*D)/*.k; do \
			new=$$(grep `basename $${fl%.*}` $${mn} | cut -f5); \
			mv $${fl} $(*D)/$${new}; \
		done \
	fi
	sfx=`tail -n1 $(manifest) | cut -f5`; \
	sfx=$${sfx//*.}; \
	[ -n "$$(find $(*D) -mindepth 1 -maxdepth 1 -type f -name "*.$${sfx}" -size -1M)" ] && ( echo "ERROR: File '$@' was corrupted." >&2; exit 7 )
	find $(*D) -mindepth 1 -maxdepth 1 -type f -name "*.$${sfx}" > $@
	[ -s "$@" ] || ( echo "ERROR: Empty list file '$@'." >&2; exit 7 )
	@$(ECHO) "Done." >&2


else
slice = $(shell perl -lane \
	'$$F[0] =~ s/.*(\d{4}).*/$$1/g; print "@F[($(step)*(int($$F[0]) - 1) + 1)..$(step)*int($$F[0])]"' <<< "$(1) $(2)")

.SECONDEXPANSION:
$(lst): %.lst: $$(call slice, $$*, $(inputfl))
	@$(ECHO) "\nCreating list file '$@' from inputs '$^'..." >&2
	mkdir -p $(*D);
	for i in $^; do echo "$$i"; done > $@
	@$(ECHO) "Done." >&2

endif


pre_download: $(manifest)
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) dwl batch=1

dwl: $(dwl)
	@$(ECHO) "\nAll pre downloads complete: $^"

$(dwl): %.dwl: $(manifest)
	@$(ECHO) "\nCreating download file '$@' from manifest file '$<' ..." >&2
	instance=$$(sed -r -n -e 's|.*/([0-9]{4})_$(radical).dwl|\1|p' <<< "$@"); \
	[ -z "$${instance}" ] && echo "ERROR: Instance parsing error." && exit 1; \
	inst=$$(perl -ne '$$k = $$_; $$k = substr($$_,1) if ($$_ < 1000 and $$_ >= 100); $$k = substr($$_,2) if ($$_ < 100 and $$_ >= 10); $$k = substr($$_,3) if $$_ < 10; print $$k;' <<< "$${instance}"); \
	[ -z "$${inst}" ] && echo "ERROR: Instance parsing error." && exit 1; \
	mkdir -p $(*D); \
	if [ "$${inst}" -eq "$(sfx_end)" ]; then \
		if (( ($(num_lines)-1) % $(step) )); then \
			tail -n $$((($(num_lines)-1) % $(step))) $< | cut -f4 > $@; \
		else \
			tail $< | cut -f4 > $@; \
		fi \
	else \
		head -n $$(($${inst}*$(step) + 1)) $< | tail -n $(step) | cut -f4 > $@; \
	fi
	@$(ECHO) "Done." >&2


endif # Batch mode.



############################## 1K GENOMES ######################################

test_data_genomes:

1KG_endpoint                   := http://ftp.1000genomes.ebi.ac.uk/vol1/ftp/phase1/data/
hg00096_u                      := $(1KG_endpoint)/HG00096/alignment/HG00096.chrom20.ILLUMINA.bwa.GBR.low_coverage.20101123.bam
hg00097_u                      := $(1KG_endpoint)/HG00096/alignment/HG00096.chrom20.ILLUMINA.bwa.GBR.low_coverage.20101123.bam
hg00099_u                      := $(1KG_endpoint)/HG00096/alignment/HG00096.chrom20.ILLUMINA.bwa.GBR.low_coverage.20101123.bam
hg00100_u                      := $(1KG_endpoint)/HG00096/alignment/HG00096.chrom20.ILLUMINA.bwa.GBR.low_coverage.20101123.bam
hg00101_u                      := $(1KG_endpoint)/HG00096/alignment/HG00096.chrom20.ILLUMINA.bwa.GBR.low_coverage.20101123.bam



############################## UCSC ############################################

ucsc_get_latest: $(ucsc_latest)
ucsc_get_hg38: $(ucsc_hg38)
ucsc_get_hg19: $(ucsc_hg19)
ucsc_get_all: $(ucsc_all)

WGET := wget

samtools_index: $(fasta)

bwa_index := $(fasta:.fa.gz=.fai)
bwa_index: $(bwa_index)

$(bwa_index): %.fai: $(fasta) | $(bwa_status)
	@$(ECHO) "\nIndexing fasta file '$<' with bwa ..." >&2
	[ -s "$<" ] || (echo "ERROR: Fasta file '$<' can not be empty." >&2; exit 1)
	bwa index -p $* $<
	@$(ECHO) "Done." >&2
	
	

$(ucsc_all): %.fa.gz: | $(internet_status) $(ucsc_dir)
	@$(ECHO) "\nDownloading UCSC genome file '$(notdir $@)' from '$(filter %$(notdir $@), $(ucsc_urls))' ..." >&2
	$(WGET) $(filter %$(notdir $@), $(ucsc_urls)) -P $(dir $@)
	@$(ECHO) "Done." >&2

$(ucsc_dir):
	mkdir -p $@

gene_universe: $(gene_universe)

$(gene_universe): $(annotation_ref)
	@$(ECHO) "\nCreating Gene Universe file '$@' from annotation file '$<' ..." >&2
	perl -F"\t" -lane \
		'next if /^#/ or $$F[2] ne "gene"; $$id = $$1 if $$F[8] =~ /gene_id "(.*?)";/; $$nm = $$1 if $$F[8] =~ /gene_name "(.*?)";/; $$tp = $$1 if $$F[8] =~ /gene_type "(.*?)";/; print "$$id\t$$nm\t$$tp"' \
		<(zcat $<) > $@
	@$(ECHO) "Done." >&2





############################## SALMON ##########################################

ifneq ($(filter salmon_%, $(MAKECMDGOALS)),)

ifeq ($(inputfl),)
      salmon_inputs            := $(shell perl -lane 'next if /^CASE/; print $$F[4]' $(manifest) 2> /dev/null)
else
      salmon_inputs            := $(if $(ctrl), $(dln), $(inputfl))
endif


salmon_inputs1                 += $(filter %1.fastq, $(salmon_inputs))
salmon_inputs2                 += $(filter %2.fastq, $(salmon_inputs))

#salmon_inputs1                 += $(wildcard *1.fastq.gz, $(salmon_inputs))
#salmon_inputs2                 += $(wildcard $(patsubst %1.fastq.gz, %2.fastq.gz, $(salmon_inputs1)))

ifneq ($(words (salmon_inputs1)),$(words $(salmon_inputs2)))
      $(error ERROR: No matching number of FASTq files.)
endif

salmon_quant                   := $(addprefix $(outputsdir)/, $(patsubst %.fastq,%,$(notdir $(salmon_inputs1))))
salmon_idx                     ?= $(gencode_dir)/Hs_salmon_idx


salmon_quant: $(salmon_quant)

$(salmon_quant): %: %1.fastq %2.fastq | $(salmon_idx)
	@$(ECHO) "\nQuantifying transcripts expression in files '$^' ..." >&2
	[ -n "$^" ] || ( echo "ERROR: Argument list is empty." >&2; exit 7 )
	salmon quant \
		-i $(salmon_idx) \
		-l A \
		-p $(jobs) \
		--validateMappings \
		-1 $< \
		-2 $(<:1.fastq=2.fastq) \
		-o $@
	[ -d "$@" ] || ( echo "ERROR: Nonexistent directory '$@'." >&2; exit 7 )
	@$(ECHO) "Done." >&2


salmon_index: $(salmon_idx)

$(salmon_idx): $(transcriptome_ref) | $(salmon_status)
	@$(ECHO) "\nCreating transcripts index from file '$<' ..." >&2
	salmon index -t $< -i $@
	@$(ECHO) "Done." >&2


ensembl_transcriptome: $(transcriptome_ref)

$(transcriptome_ref): | $(internet_status)
	@$(ECHO) "\nDownloading reference transcriptome file '$@' ..." >&2
	curl $(ensembl_transcriptome_v103_u) -o $@
	@$(ECHO) "Done." >&2

endif



############################## GETx ############################################

gtex_table                     := gtex_table.tsv
gtex_BoE                       := gtex_BoE.tsv
tau_up                         := 0.75
tau_down                       := 0.4
tpm_cutoff                     := 0.05


define perl_medians_by_tissue
	BEGIN { $$pos = 0; }                                                       \
	next if /^#/ or scalar @F < 3;                                             \
	                                                                           \
	if ( /^Name/ ) {                                                           \
		foreach ( 2..$$#F ) {                                                  \
			$$pos = $$_, last if $$F[$$_] eq "$(args)";                        \
		}                                                                      \
		die "Nonexistent tissue <$(args)>." unless $$pos;                      \
	}                                                                          \
	                                                                           \
	print join("\t", @F[0, 1, $$pos]), "\n";
endef

define perl_protein_coding_medians_by_tissue
	BEGIN { $$ck = 0; $$pos = 0; }                                             \
	$$act = $$ARGV, $$ck++ unless $$ck;                                        \
	next if /^#/ or scalar @F < 3;                                             \
	                                                                           \
	if ( $$act eq $$ARGV ) {                                                   \
		next unless $$F[2] eq "gene";                                          \
		next unless $$F[8] =~ /protein_coding/;                                \
		$$name = $$1 if $$F[8] =~ /gene_name "(.*?)";/;                        \
		next unless $$1;                                                       \
		$$gn{$$name} = 0;                                                      \
	}                                                                          \
	else {                                                                     \
		if ( /^Name/ ) {                                                       \
			foreach ( 2..$$#F ) {                                              \
				$$pos = $$_, last if $$F[$$_] eq "$(args)";                    \
			}                                                                  \
			die "Nonexistent tissue <$(args)>." unless $$pos;                  \
			print join("\t", @F[0, 1, $$pos]), "\n";                           \
			next;                                                              \
		}                                                                      \
		print join("\t", @F[0, 1, $$pos]), "\n" if exists $$gn{$$F[1]} and $$gn{$$F[1]} == 0; \
		$$gn{$$F[1]}++ if exists $$gn{$$F[1]};                                 \
	}
endef

define perl_boe
	next if /^#/ or scalar @F < 3; \
	@names = @F, next if /^Name/; \
	chomp $$F[-1]; \
	\
	@idx = sort { $$F[$$b] <=> $$F[$$a] } 2..$$#F; \
	$$cut = $(tpm_cutoff); \
	$$mx = max @F[2..$$#F]; \
	$$boe = -1; \
	$$boe = sum(map { $$_ >= $$cut ? 1 - ($$_/$$mx) : 1 } @F[2..$$#F])/(scalar @F - 3) if $$mx >= $$cut; \
	\
	$$add = ""; \
	$$tp = "missing"; \
	if ( $$boe > $(tau_up) ) { \
		$$tp = "specific"; \
		$$add = join "|",  @names[ grep { $$F[$$_] >= $$cut } @idx ]; \
	} \
	elsif ( $$boe < $(tau_down) and $$boe != -1 ) { \
		$$tp = "housekeeping"; \
		$$add = join "|",  @names[ grep { $$F[$$_] == 0 } @idx ]; \
	} \
	elsif ( $$boe != -1 ) { \
		$$tp = "middle_range"; \
		$$add = join "|", @names[ @idx[0..9] ]; \
	} \
	else {} \
	$$gn{$$F[0]} = [ $$F[0], $$F[1], $$boe, $$tp, $$add ]; \
	\
	END { \
		print join("\t", @{ $$gn{$$_} }) \
			for sort { $$gn{$$b}->[2] cmp $$gn{$$a}->[2] } keys %gn; \
	}
endef


gtex_get_all: $(gtex_attributes) $(gtex_reads) $(gtex_tpm) $(gtex_medians)


gtex_breadth_of_expression: $(gtex_BoE)

$(gtex_BoE): $(gtex_medians)
	@$(ECHO) "\nCalculating Breadth of Expression for genes in GTEx data: '$<' ..." >&2
	perl -F"\t" -MList::Util=sum,max -lane '$(perl_boe)' <(zcat $<) > $@
	@$(ECHO) "Done." >&2


gtex_calibrate: $(gtex_table)

$(gtex_table): $(gencode_v26) $(gtex_reads)
	@$(ECHO) "\nCalibrating TPM algorithm for GTEx data ..."
	perl -F"\t" -MList::Util=max -ane '$(tpm_gen)' $< <(zcat $(gtex_reads)) > $@
	@$(ECHO) "Done."


gtex_protein_coding_medians_by_tissue: $(gencode_v26) $(gtex_medians)
	@$(ECHO) "\nTaking GTEx protein coding genes expression medians for tissues: '$(args)' ..." >&2
	[ -z "$(args)" ] \
		&& echo "ERROR: Empty variable: 'args' for tissue name." >&2 && exit 1
	perl -F"\t" -ane '$(perl_protein_coding_medians_by_tissue)' $< <(zcat $(gtex_medians))
	@$(ECHO) "Done." >&2


gtex_medians_by_tissue: $(gtex_medians)
	@$(ECHO) "\nTaking GTEx gene expression medians for tissues: '$(args)' ..." >&2
	[ -z "$(args)" ] \
		&& echo "ERROR: Empty variable: 'args' for tissue name." >&2 && exit 1
	zcat $< | perl -F"\t" -ane '$(perl_medians_by_tissue)'
	@$(ECHO) "Done." >&2


gtex_tissues: $(gtex_medians)
	@$(ECHO) "\nTaking GTEx tissues names from '$<' ..." >&2
	zcat $< | perl -F"\t" -ane 'print join("\n", @F[2..$$#F]) if /^Name/'
	@$(ECHO) "Done." >&2

$(gtex_attributes) $(gtex_reads) $(gtex_tpm) $(gtex_medians):
	@$(ECHO) "\nDownloading GTEx data '$@' ..."
	wget $(filter %$(notdir $@), $(gtex_urls)) -P $(gtex_dir)
	touch $@
	@$(ECHO) "Done."


gencode_get_all: $(gencode_v22) $(gencode_v26_collab) $(gencode_latest)

$(gencode_v22) $(gencode_v26_collab) $(gencode_latest):
	@$(ECHO) "\nDownloading Gencode annotation data '$@' ..."
	wget $(filter %$(notdir $@), $(gencode_urls)) -P $(gencode_dir)
	touch $@
	@$(ECHO) "Done."



############################## MISCELLANEOUS ###################################

manifest_of_power              := tcga_power.manifest
manifest_wgs_all               := tcga_wgs_all.manifest
manifest_wgs_all_p             := tcga_wgs_all_p.manifest
manifest_wxs_all               := tcga_wxs_all.manifest
manifest_wxs_all_p             := tcga_wxs_all_p.manifest
manifest_wts_all               := tcga_wts_all.manifest
manifest_wts_all_p             := tcga_wts_all_p.manifest
manifest_tp_all                := tcga_tp_all.manifest
manifest_tp_all_p              := tcga_tp_all_p.manifest


rtc_table                      := rtc_table.tsv
rtc_combined_manifest          := rtc_combined.manifest
rtc_match                      := rtc_expression_match.manifest
rtc_expression                 := rtc_expression.tsv
rtc_expression_soma            := rtc_expression_soma.tsv

rtc_parentals                  := rtc_parentals.tsv
rtc_hosts                      := rtc_hosts.tsv
rtc_patterns                   := rtc_patterns.tsv
rtc_targets                    := rtc_targets.tsv
rtc_models                     := rtc_models.Rdata
mretro                         := $(radical)_mretro.tsv


define R_header
	options(tidyverse.quiet = T); \
	options(DESeq2.quiet = T); \
	library(tidyverse); \
	args <- commandArgs(trailingOnly = T);
endef

define R_import
	tbl <- read.table(args[1], header = T, sep = "\t");
endef

define R_device
	pdf($1, height = 21*0.3, width = 29.7*0.5); \
	print(g1); \
	dev.off();
endef

define R_gsea
	$(R_header) \
	library(clusterProfiler); \
	 \
	print(paste("Importing files:", args)); \
	res <- read.table(args[1], header = T, sep = "\t"); \
	tbl <- read.table(args[2], header = T, sep = "\t"); \
	tbl <- tbl[!tbl$$INFO %% 2, ]; \
	tbl <- tbl[!tbl$$INDIV == ".", ]; \
	str(tbl); \
	 \
	ids <- sub("(.*?)\\.\\d*", "\\1", rownames(res)); \
	geneList <- res$$log2FoldChange; \
	names(geneList) <- ids; \
	geneList <- sort(geneList, decreasing = TRUE); \
	 \
	eg = bitr(ids, fromType = "ENSEMBL", toType = c("ENTREZID", "SYMBOL"), OrgDb = "org.Hs.eg.db"); \
	geneList <- geneList[names(geneList) %in% unique(eg$$ENSEMBL)]; \
	k <- bitr(names(geneList), fromType = "ENSEMBL", toType = "ENTREZID", OrgDb = "org.Hs.eg.db"); \
	 \
	x <- NA; \
	nm <- names(geneList); \
	for (i in nm) \
		x <- c(x, which(k$$ENSEMBL == i)[1]); \
	 \
	names(geneList) <- k[x[!is.na(x)], 2]; \
	 \
	wpgmtfile <- system.file("extdata/wikipathways-20180810-gmt-Homo_sapiens.gmt", package = "clusterProfiler"); \
	wp2gene <- read.gmt(wpgmtfile); \
	str(wp2gene); \
	wp2gene <- wp2gene %>% tidyr::separate(term, c("name","version","wpid","org"), "%"); \
	wpid2gene <- wp2gene %>% dplyr::select(wpid, gene); \
	wpid2name <- wp2gene %>% dplyr::select(wpid, name); \
	 \
	ewp <- enricher(unique(names(geneList)), TERM2GENE = wpid2gene, TERM2NAME = wpid2name); \
	ewp <- setReadable(ewp, org.Hs.eg.db, keyType = "ENTREZID"); \
	write.table(as.data.frame(ewp), file = "$(gsea)", quote = F, sep = "\t");
endef
	
define R_DEG
	options(DESeq2.quiet = T); \
	library("DESeq2"); \
	args <- commandArgs(trailingOnly = T); \
	print(paste("Arguments:", args)); \
	\
	tbl <- read.table(args[1], header = T, sep = "\t"); \
	tbl <- tbl[order(tbl$$FILE_NAME), ]; \
	tumor <- sub("TCGA-(.*?)$$", "\\1", tbl$$TUMOR_TYPE[1]); \
	\
	genes <- read.table(args[2], sep = "\t"); \
	names(genes) <- c("ID", "NAME", "TYPE"); \
	\
	directory <- args[3]; \
	sampleFiles <- sort(grep(".counts$$", list.files(directory), value = T)); \
	\
	\
	if ( length(tbl$$ID) != length(sampleFiles) ) \
		exit(); \
	\
	sampleCondition <- sub("(.*treated).*","\\1",sampleFiles); \
	sampleTable <- data.frame(sampleName = sampleFiles, \
					fileName = sampleFiles, \
					condition = factor(tbl$$SAMPLE_TYPE, levels = c("Solid Tissue Normal", "Primary Tumor")) \
					); \
	\
	dds <- DESeqDataSetFromHTSeqCount(sampleTable = sampleTable, directory = directory, design = ~ condition); \
	keep <- rowSums(counts(dds)) > 10; \
	dds <- dds[keep,]; \
	\
	dds <- DESeq(dds); \
	res <- results(dds, alpha = 0.05); \
	\
	pv <- !is.na(res$$pvalue); \
	pdj <- !is.na(res$$padj); \
	relev <- !is.na(res$$log2FoldChange); \
	res <- res[pdj & relev & pv, ]; \
	\
	resOrd <- res[order(res$$log2FoldChange), ]; \
	write.table(as.data.frame(resOrd), file = paste0(tumor, "_deg.tsv"), quote = F, sep = "\t");
endef


define R_plot
	options(tidyverse.quiet = T);
	library(tidyverse);
	args <- commandArgs(trailingOnly = TRUE);
	
	man <- read.table(args[1], header = T, sep = "\t");
	expr <- read_tsv(args[2], comment = "#");
	a <- strsplit(args[3], split = " ")[[1]];
	
	nm <- names(expr)[-(1:4)];
	ord <- order(nm);
	guide <- data.frame( type = ifelse(man$$SAMPLE_TYPE == "Solid Tissue Normal", "Normal", "Tumor"),
		indiv = man$$FILE_NAME
		);
	guide <- guide[guide$$indiv %in% paste0(nm, ".gz"), ];
	guide <- guide[order(guide$$indiv), ];
	
	gene_ord <- order(a);
	ptn <- paste("^", a[gene_ord], "$$", sep = "", collapse = "|");
	mtx <- expr %>% filter(str_detect(GENE_NAME, ptn));
	expr <- NULL;
	
	genes <- factor(mtx$$GENE_NAME);
	cl <- length(genes);
	mtx <- t(mtx[, -(1:4)]);
	mtx <- mtx[ord, ];
	mtx <- as.data.frame(matrix(as.numeric(mtx), ncol = 1));
	mtx <- cbind(mtx, as.factor(rep(genes, each = length(ord))));
	mtx <- cbind(mtx, as.factor(rep(guide$$type, cl)));
	mtx <- cbind(mtx, rep(guide$$indiv, cl));
	colnames(mtx) <- c("DATA", "GENE", "TYPE", "INDIV");
	
	pdf("$(data_sample).pdf", height = 21*0.3, width = 29.7*0.5);
	ggplot(mtx, aes(x = TYPE, y = DATA, fill = TYPE)) + geom_boxplot() + labs(x = "$(data_sample)", y = "TPM", fill = "Tissue") + facet_wrap(~ GENE, nrow = 1, scales = "free");
	dev.off();
endef


define R_volcano
	options(tidyverse.quiet = T);
	library(tidyverse);
	require("ggrepel");
	args <- commandArgs(trailingOnly = TRUE);
	
	genes <- read.table(args[1], header = T, sep = "\t");
	genes$$Significant <- ifelse(genes$$padj < 0.05, "FDR < 0.05", "Not Sig");
	
	g <- ggplot(genes, aes(x = log2FoldChange, y = -log10(pvalue))) +
	geom_point(aes(color = Significant)) +
	scale_color_manual(values = c("red", "grey")) +
	theme_bw(base_size = 12) + theme(legend.position = "bottom")
	
	pdf("$(data_sample).pdf", height = 21*0.3, width = 29.7*0.5);
	print(g);
	dev.off();
endef


define perl_most_expressed
	$$act = $$ARGV, $$ck++ unless $$ck; \
	next if /^#/; \
	\
	if ( $$act eq $$ARGV ) { \
		next if /^CASE_ID/; \
		$$k = $$1 if $$F[4] =~ /(.*?)\.gz/; \
		$$n{$$k}++ if $$F[2] eq "Solid Tissue Normal"; \
		$$t{$$k}++ if $$F[2] eq "Primary Tumor"; \
	} \
	else { \
		@names = @F, next if /^GENE_ID/; \
		next if $$F[2] ne "protein_coding"; \
		@nacm = (); \
		@tacm = (); \
		foreach ( 4..$$#F ) { \
			push @nacm => $$F[$$_] if exists $$n{$$names[$$_]}; \
			push @tacm => $$F[$$_] if exists $$t{$$names[$$_]}; \
		} \
		$$nc{$$F[1]} = median(@nacm); \
		$$tc{$$F[1]} = median(@tacm); \
	} \
	\
	END { \
		print join("\n", map { "$$_\t$$nc{$$_}" } sort { $$nc{$$b} <=> $$nc{$$a} } keys %nc), "\n"; \
		print STDERR join("\n", map { "$$_\t$$tc{$$_}" } sort { $$tc{$$b} <=> $$tc{$$a} } keys %tc), "\n"; \
	}
endef

define perl_somatics
	next if /^#/ or $F[10] & 1; \
	next unless $$F[10] & 64; \
	\
	$$n = "Normal"; \
	$$n = "Tumor" if sum(@F[23, 24]); \
	$$a{$$F[15]} = $F[16]; \
	$$b{$$F[5]} = $F[16]; \
	\
	END { \
		print "$$_\t$a{$$_}\t$$n" for sort keys %a; \
		delete $$b{"none"}; \
		print STDERR "$$_\t$b{$$_}\t$$n" for sort keys %b; \
	}
endef

define perl_individuals
	$$act = $$ARGV, $$ck++ unless $$ck; \
	next if /^#/; \
	\
	if ( $$act eq $$ARGV ) { \
		next unless $$F[2] eq "gene"; \
		\
		$$id = $$1 if $$F[8] =~ /gene_id "(.*?)";/; \
		$$name = $$1 if $$F[8] =~ /gene_name "(.*?)";/; \
		$$type = $$1 if $$F[8] =~ /gene_type "(.*?)";/; \
		$$genes{$$id} = [ $$name, $$type ]; \
	} \
	else { \
		next if $$F[10] & 1 or $$F[31] eq "."; \
		\
		$$tst = "$(with_polymorphics)"; \
		$$mk = 0; \
		$$mk++ if $$F[10] & 64; \
		$$rtc{$$F[0]} = [ $$F[5], $$F[15], $$F[16], $$mk ] if $$mk or $$tst; \
		push @{ $$ind{$$_} } => $$F[0] for split /\|/, $$F[31]; \
	} \
	\
	END { \
		@pc = sort \
			uniq map { $$genes{$$_}->[0] } \
			grep { $$genes{$$_}->[1] eq "protein_coding" } keys %genes; \
		push @zr => 0 for 0..$$#pc; \
		print "INDIV\t", join("\t", @pc); \
		print STDERR "INDIV\t", join("\t", @pc); \
		foreach my $$i ( sort keys %ind ) { \
			@z_par = @zr; \
			@z_host = @zr; \
			foreach my $$pos ( 0..$$#pc ) { \
				$$z_par[$$pos]++ if grep { $$rtc{$$_}->[1] eq $$pc[$$pos] } @{ $$ind{$$i} }; \
				$$z_host[$$pos]++ if grep { $$rtc{$$_}->[0] eq $$pc[$$pos] } @{ $$ind{$$i} }; \
			} \
			print "$$i\t", join("\t", @z_par); \
			print STDERR "$$i\t", join("\t", @z_host); \
		} \
	}
endef

define perl_expression_match
	$$act = $$ARGV, $$ck++ unless $$ck;                                        \
	next if /^#/ or /^\s*$$/ or /^ID/ or /^CASE/;                              \
	chomp;                                                                     \
	                                                                           \
	if ( $$act eq $$ARGV ) {                                                   \
		next if $$F[10] & 1 or $$F[-1] eq "." or $$F[9] < 10;                  \
		next unless $$F[10] & 64;                                              \
		push @{ $$ind{$$F[-1]} } => [ @F ];                                    \
	}                                                                          \
	elsif ( $$ARGV =~ /wxs_all_p/ ) {                                          \
		push @{ $$cases{$$F[0]} } => [ @F ] if exists $$ind{$$F[4]};           \
	}                                                                          \
	else {                                                                     \
		$$prt{$$F[4]} = [ @F ] if exists $$cases{$$F[0]};                      \
	}                                                                          \
	                                                                           \
	END {                                                                      \
		print join("\t", @{ $$prt{$$_} }) for sort { $$prt{$$a}->[0] cmp $$prt{$$b}->[0] } keys %prt; \
	}
endef

define perl_expression_soma
	$$act = $$ARGV, $$ck++ unless $$ck;                                        \
	next if /^#/ or /^\s*$$/ or /^ID/ or /^CASE/;                              \
	                                                                           \
	                                                                           \
	if ( $$act eq $$ARGV ) {                                                   \
		next if $$F[10] & 1 or $$F[-1] eq "." or $$F[9] < 10;                  \
		next unless $$F[10] & 64 and ( $$F[4] eq "intronic" or $$F[4] eq "exonic" ); \
		                                                                       \
		$$tumors{$$F[16]} = "";                                                \
		push @{ $$ind{$$F[-1]} } => [ @F ];                                    \
	}                                                                          \
	elsif ( $$ARGV =~ /wxs_all_p/ ) {                                          \
		push @{ $$cases{$$F[0]} } => [ @F ] if exists $$ind{$$F[4]};           \
	}                                                                          \
	elsif ( $$ARGV =~ /tp_all/ ) {                                             \
		my $$type = $$F[1] =~ /TCGA-(.*?)$$/ ? $$1 : ".";                      \
		push @{ $$types{$$type}{$$F[2]} } => $$F[4] if exists $$tumors{$$type}; \
		next if $$F[2] =~ /Normal/;                                            \
		push @{ $$expr{$$F[4]} } => [ @F ] if exists $$cases{$$F[0]};          \
	}                                                                          \
	elsif ( $$ARGV =~ /gtex/ ) {                                               \
		$$boe{$$F[1]} = [ @F ] unless defined $$boe{$$F[1]};                   \
	}                                                                          \
	elsif ( $$ARGV =~ /degao/ ) {                                              \
		my $$mt = "";                                                          \
		$$mt = (grep { $$F[1] eq $$boe{$$_}->[0] } keys %boe)[0] if exists $$tumors{$$F[0]}; \
		$$deg{$$F[0]}{$$mt} = [ @F ] if $$mt;                                  \
	}                                                                          \
	else {                                                                     \
		if ( /^GENE_ID/ ) {                                                    \
			@hd = @F[0..3];                                                    \
			push @hd => (split(m#/#, $$_))[-1] . ".gz" for @F[4..$$#F];        \
			next;                                                              \
		}                                                                      \
		                                                                       \
		@x = ();                                                               \
		foreach my $$i ( keys %ind ) {                                         \
			push @x => map { $$_->[15] } grep { $$F[1] eq "$$_->[5]" } @{ $$ind{$$i} }; \
		}                                                                      \
		if ( @x ) {                                                            \
			$$all{$$_}++ for ( $$F[1], @x );                                   \
		}                                                                      \
		                                                                       \
		                                                                       \
		foreach my $$tp ( keys %types ) {                                      \
			next if $$tp eq ".";                                               \
			if ( ! $$ct ) {                                                    \
				for my $$a ( @{ $$types{$$tp}{"Primary Tumor"} } ) {           \
					push @{ $$pos{$$tp} } => grep {$$_ > 0 } map { $$a =~ /$$hd[$$_]/ ? $$_: 0 } 4..$$#hd; \
				}                                                              \
			}                                                                  \
			                                                                   \
			my @ch = @{ $$pos{$$tp} };                                         \
			$$genes{$$tp}{$$F[1]}{"tumor"} = {                                 \
				"n"       => scalar(@ch),                                      \
				"median"  => median(@F[@ch]),                                  \
				"mean"    => mean(@F[@ch]),                                    \
				"std"     => stddev(@F[@ch]),                                  \
				"norm"    => 0,                                                \
				"pos"     => [ @ch ],                                          \
				"val"     => [ @F ],                                           \
			};                                                                 \
		}                                                                      \
		$$ct++;                                                                \
	}                                                                          \
	                                                                           \
	END {                                                                      \
		delete $$types{"."};                                                   \
		$$ex = (keys %genes)[0];                                               \
		print STDERR "IND ", scalar keys %ind, "."; \
		print STDERR "CASES ", scalar keys %cases, "."; \
		print STDERR "EXPR ", scalar keys %expr, "."; \
		print STDERR "BOE ", scalar keys %boe, "."; \
		print STDERR "ALL ", scalar keys %all, "."; \
		print STDERR "GENES $$ct."; \
		print STDERR "GENES hash ", scalar keys %{ $$genes{$$ex} }, "."; \
		foreach my $$g ( keys %{ $$genes{$$ex} } ) {                           \
			unless ( exists $$all{$$g} ) {                                     \
				delete $$genes{$$_}{$$g} for keys %genes;                      \
			}                                                                  \
		}                                                                      \
		print STDERR "Ah la2 ", scalar keys %{ $$genes{$$ex} }, "."; \
	                                                                           \
		                                                                       \
		print "CHROM\tPOS\tPOL\tREG\tHOST\tCASE\tFILE\tSAMPLE\tSUBSAMPLE\tINFO\tN\tMEDIAN\tMEAN\tSTD\tNORMALITY\tTAU\tTISSUE\tDIFF_EXPR\tVAL\tZ\tPARENTAL\tMEDIANp\tMEANp\tSTDp\tNORMALITYp\tTAUp\tTISSUEp\tDIFF_EXPRp\tVALp\tZp\tCORR\tREL";                                                                \
		my @out;                                                               \
		foreach my $$i ( keys %ind ) {                                         \
			foreach my $$ins ( @{ $$ind{$$i} } ) {                             \
				my @vct = @{ $$ins };                                          \
				my $$host = $$vct[5];                                          \
				my $$tumor = $$vct[16];                                        \
				die "ERROR: Unexistent gene \"$$host\"." unless exists $$genes{$$tumor}{$$host}; \
				                                                               \
				my $$reg = $$vct[4];                                           \
				my $$par = $$vct[15];                                          \
				my $$tissue = "Tumor";                                         \
				$$tissue = "Normal" if sum(@vct[21, 22]);                      \
				my $$rel = ".";                                                \
				                                                               \
				my $$cs = "none";                                              \
				for my $$k ( keys %cases ) {                                   \
					$$cs = (map { $$_->[0] } grep { $$i eq $$_->[4] } @{ $$cases{$$k} })[0]; \
					last if length($$cs) > 4;                                  \
				}                                                              \
				die "ERROR: File \"$$i\" must have a case ID." if $$cs eq "none"; \
				                                                               \
				my @exp = ();                                                  \
				for my $$k ( keys %expr ) {                                    \
					next if $$expr{$$k}->[0][2] !~ /$$tissue/;                 \
					push @exp => $$k if grep { $$cs eq $$_->[0] } @{ $$expr{$$k} }; \
				}                                                              \
				next unless @exp;                                              \
				                                                               \
				my @pos = ();                                                  \
				for my $$p ( @exp ) {                                          \
					push @pos => (grep { $$_ > 0 } map { $$p =~ /$$hd[$$_]/ ? $$_ : 0 } 4..$$#hd)[0]; \
				}                                                              \
				                                                               \
				$$boe_tau_h = exists $$boe{$$host} ? $$boe{$$host}->[2] : -1;  \
				$$boe_spec_h = exists $$boe{$$host} ? (grep { $$_ !~ /Cells/ } split(/\|/, $$boe{$$host}->[4]))[0] . "." : "."; \
				$$boe_tau_p = exists $$boe{$$par} ? $$boe{$$par}->[2] : -1;    \
				$$boe_spec_h = exists $$boe{$$par} ? (grep { $$_ !~ /Cells/ } split(/\|/, $$boe{$$par}->[4]))[0] . "." : "."; \
				                                                               \
				                                                               \
				$$valh = @{ $$genes{$$tumor}{$$host}{"tumor"}->{"val"} }[$$pos[0]]; \
				$$meanh = $$genes{$$tumor}{$$host}{"tumor"}->{mean};           \
				$$stdh = $$genes{$$tumor}{$$host}{"tumor"}->{std};             \
				$$zh = $$stdh > 0 ? ($$valh - $$meanh)/$$stdh : 0;             \
				$$diffh = exists $$deg{$$tumor}{$$host} ? $$deg{$$tumor}{$$host}->[-1] : -1; \
				                                                               \
				$$valp = @{ $$genes{$$tumor}{$$par}{"tumor"}->{"val"} }[$$pos[0]]; \
				$$meanp = $$genes{$$tumor}{$$par}{"tumor"}->{mean};            \
				$$stdp = $$genes{$$tumor}{$$par}{"tumor"}->{std};              \
				$$zp = $$stdp > 0 ? ($$valp - $$meanp)/$$stdp : 0;             \
				$$diffp = exists $$deg{$$tumor}{$$par} ? $$deg{$$tumor}{$$par}->[-1] : -1; \
				                                                               \
				@po = @{ $$genes{$$tumor}{$$host}{"tumor"}->{pos} };           \
				@c1 = @{ $$genes{$$tumor}{$$host}{"tumor"}->{val} }[@po];      \
				@c2 = @{ $$genes{$$tumor}{$$par}{"tumor"}->{val} }[@po];       \
				$$corr = correlation( \@c1, \@c2 );                            \
				$$corr = $$corr eq "n/a" ? 0 : $$corr;                         \
				                                                               \
				my $$dt = [                                                    \
					$$genes{$$tumor}{$$host}{"tumor"}->{n},                    \
					$$genes{$$tumor}{$$host}{"tumor"}->{median},               \
					$$genes{$$tumor}{$$host}{"tumor"}->{mean},                 \
					$$genes{$$tumor}{$$host}{"tumor"}->{std},                  \
					$$genes{$$tumor}{$$host}{"tumor"}->{norm},                 \
					$$boe_tau_h,                                               \
					$$boe_spec_h,                                              \
					$$diffh,                                                   \
					$$valh,                                                    \
					$$zh,                                                      \
					$$par,                                                     \
					$$genes{$$tumor}{$$par}{"tumor"}->{median},                \
					$$genes{$$tumor}{$$par}{"tumor"}->{mean},                  \
					$$genes{$$tumor}{$$par}{"tumor"}->{std},                   \
					$$genes{$$tumor}{$$par}{"tumor"}->{norm},                  \
					$$boe_tau_p,                                               \
					$$boe_spec_p,                                              \
					$$diffp,                                                   \
					$$valp,                                                    \
					$$zp,                                                      \
					$$corr,                                                    \
					join("|", @exp),                                           \
				];                                                             \
				push @out => [ @vct[1..5], $$cs, $$i, $$tumor, $$tissue, $$vct[10], @{ $$dt } ]; \
			}                                                                  \
		}                                                                      \
		                                                                       \
		print join("\t", @{ $$_ }) for sort { $$a->[0] cmp $$b->[0] || $$a->[1] cmp $$b->[1] || $$a->[5] cmp $$b->[5] } @out; \
	}
endef


define perl_targets
	$$act = $$ARGV, $$ck++ unless $$ck; \
	next if /^#/; \
	\
	if ( $$act eq $$ARGV ) { \
		next unless $$F[2] eq "gene"; \
		next unless $$F[8] =~ /protein_coding/; \
		\
		$$id = $$1 if $$F[8] =~ /gene_id "(.*?)";/; \
		$$name = $$1 if $$F[8] =~ /gene_name "(.*?)";/; \
		$$type = $$1 if $$F[8] =~ /gene_type "(.*?)";/; \
		$$genes{$$id} = [ $$name, $$type ]; \
		$$ck = -1;
	} \
	elsif ( $$ARGV =~ /GTEx.*Attributes/ ) { \
		next if /^SAMPID/; \
		$$sample{$$F[0]}++ if $$F[6] eq "Whole Blood"; \
	} \
	elsif ( $$ARGV =~ /\/dev\/fd\/.*/ and $$ck < 0 ) { \
		next if scalar @F < 3; \
		\
		if ( /^Name/ ) { \
			@names = @F; \
			push @pos => 0 for 0..$$#F; \
			$$pos[$$_] = exists $$sample{$$F[$$_]} ? $$_ : 0 for 0..$$#F; \
		} \
		\
		next unless grep { $$F[1] eq $$genes{$$_}->[0] } keys %genes; \
		$$tpm{$$F[1]} = [ @F[@pos] ]; \
	} \
	elsif ( $$ARGV =~ /rtc_parentals/ ) { \
		next if /^INDIV/; \
		push @ord_ptn => $$F[0]; \
	} \
	elsif ( $$ARGV =~ /rtc_expression.assoc/ and $$ck < 0 ) { \
		$$fl = join("|", @F[2..$$#F]); \
		push @ord_tgt => $$F[1] if grep { $$_ =~ // } @ord_ptn; \
	} \
	else { last; } \
	END { print scalar keys %tpm, " -- ", scalar keys %expr; }
endef

define perl_combine_manifest
	die "Variable args is empty." unless "$(args)";                            \
	                                                                           \
	$$act = $$ARGV, $$ct++ unless $$ct;                                        \
	next if /^#/ or /^ID/ or /^CASE/;                                          \
	my $$arg = "$(args)";                                                      \
	                                                                           \
	if ( $$act eq $$ARGV ) {                                                   \
		next if $$F[-1] eq "." or $$F[10] & 1 or $$F[9] < 10;                  \
		                                                                       \
		if ( $$F[16] eq $$arg or $$arg eq "all" ) {                            \
			my @id = split /\|/, $$F[-1];                                      \
			push @{ $$ind{$$_} } => [ 0, 0, 0, @F[1, 2, 15] ] for @id;         \
			                                                                   \
			if ( $$F[10] & 64 ) {                                              \
				$$ind{$$_}->[-1][0]++ for @id;                                 \
			}                                                                  \
			elsif ( $$F[10] & 32 ) {                                           \
				$$ind{$$_}->[-1][1]++ for @id;                                 \
			}                                                                  \
			else {                                                             \
				$$ind{$$_}->[-1][2]++ for @id;                                 \
			}                                                                  \
		}                                                                      \
	}                                                                          \
	elsif ( $$ARGV =~ /wxs_all_p/ ) {                                          \
		my $$type = $$F[1] =~ /^TCGA-(\w{2,4})/ ? $$1 : ".";                   \
		next if $$type eq "." or ( $$arg ne $$type and $$arg ne "all" );       \
		                                                                       \
		push @{ $$cases{$$F[0]} } => [ @F ] if exists $$ind{$$F[4]};           \
	}                                                                          \
	else {                                                                     \
		$$res{$$F[4]} = [ @F ] if exists $$cases{$$F[0]};                      \
	}                                                                          \
	                                                                           \
	END {           	                                                       \
		my @out;                                                               \
		foreach my $$i ( keys %res ) {                                         \
			my $$tissue = $$res{$$i}->[2];                                     \
			my $$cs = "";                                                      \
			foreach my $$c ( keys %cases ) {                                   \
				$$cs = $$c, last if grep { $$_->[0] eq $$res{$$i}->[0] } @{ $$cases{$$c} }; \
			}                                                                  \
			next unless $$cs;                                                  \
			                                                                   \
			my @id = map { $$_->[4] } grep { $$_->[2] eq $$tissue } @{ $$cases{$$cs} }; \
			next unless @id;                                                   \
			                                                                   \
			my $$soma = scalar grep { $$_->[0] > 0 } @{ $$ind{$$id[0]} };      \
			my $$germ = scalar grep { $$_->[1] > 0 } @{ $$ind{$$id[0]} };      \
			my $$poly = scalar grep { $$_->[2] > 0 } @{ $$ind{$$id[0]} };      \
			my @ins;                                                           \
			$$ins[0] = join("|", map { join ":", @{ $$_ }[3..5] } grep { $$_->[0] > 0 } @{ $$ind{$$id[0]} }); \
			$$ins[1] = join("|", map { join ":", @{ $$_ }[3..5] } grep { $$_->[1] > 0 } @{ $$ind{$$id[0]} }); \
			$$ins[2] = join("|", map { join ":", @{ $$_ }[3..5] } grep { $$_->[2] > 0 } @{ $$ind{$$id[0]} }); \
			print STDERR "Exomes related to transcriptome $$i (case_id $$cs) => ", join("|", @id), "."; \
			                                                                   \
			push @out => [ @{ $$res{$$i} }[0..$$#{ $$res{$$i} }-1], $$id[0], $$soma, $$germ, $$poly, join("@", @ins) ]; \
			@{ $$cases{$$cs} } = grep { $$_->[4] ne $$id[0] } @{ $$cases{$$cs} }; \
		}                                                                      \
		                                                                       \
		my $$sm = 0;                                                           \
		$$sm += scalar grep { $$_->[0] > 0 } @{ $$ind{$$_} } for keys %ind;    \
		print STDERR "Counts for \"$$arg\": Files = ", scalar keys %ind, "; Cases = ", scalar keys %cases, "; Somatic Insertions = ", $$sm, "; Transcriptomes assoc = ", scalar @out, "."; \
		print join("\t", @{ $$_ }) for sort { $$a->[1] cmp $$b->[1] || $$b->[6] <=> $$a->[6] || $$b->[7] <=> $$a->[7] || $$b->[8] <=> $$a->[8] } @out; \
	}
endef

define perl_mretro
	next if /^#/ or /^ID/ or $$F[-1] eq "." or $$F[10] & 1 or $$F[9] < 10;     \
	                                                                           \
	next unless sum(@F[23..24]);                                               \
	$$ch = length("$(args)") > 0 ? $$F[5] : $$F[15];                           \
	next if $$ch eq "none";                                                    \
	                                                                           \
	$$rtc{$$ch}{$$F[16]} = [ 0, 0, 0 ] unless exists $$rtc{$$ch}{$$F[16]};     \
	                                                                           \
	if ( $$F[10] & 64 ) {                                                      \
		$$rtc{$$ch}{$$F[16]}->[0]++;                                           \
	}                                                                          \
	if ( $$F[10] & 32 ) {                                                      \
		$$rtc{$$ch}{$$F[16]}->[1]++;                                           \
	}                                                                          \
	else {                                                                     \
		$$rtc{$$ch}{$$F[16]}->[2] += sum(@F[23..24]);                          \
	}                                                                          \
	                                                                           \
																			   \
	END {                                                                      \
		print "GENE\tTUMOR\tSOMATICS\tGERMLINES\tPOLYMORPHICS";                \
		foreach my $$r ( sort keys %rtc ) {                                    \
			print join("\t", ( $$r, $$_, @{ $$rtc{$$r}{$$_} } )) for sort keys %{ $$rtc{$$r} }; \
		}                                                                      \
	}
endef

define perl_stats
	next if /^#/;                                                              \
	                                                                           \
	if ( $$ARGV =~ /manifest/ ) {                                              \
		$$ind{$$F[4]} = [ @F ];                                                \
		                                                                       \
		$$group = $$F[1] =~ /TCGA-(.*?)/ ? $$1 : "none";                       \
		next if $$group eq "none";                                             \
		$$groups{$$group}++;                                                   \
	}                                                                          \
	else {                                                                     \
		die "ERROR: Incorrect file type.\n" unless /^GENE_ID/;                 \
		@hd = @F, next if /^GENE_ID/;                                          \
		                                                                       \
		if ( defined %groups ) {                                               \
			foreach my $$gp ( keys %groups ) {                                 \
				$$idx{$$gp} grep {} @evwr;                                     \
			}                                                                  \
		}                                                                      \
		else {                                                                 \
		}                                                                      \
	}                                                                          \
	                                                                           \
	END {                                                                      \
		print "## Expression statistics for 1 group in TPM.\n";                \
		print "GENE_ID\tGENE_SYMBOL\tGENE_TYPE\tGENE_LENGTH\tGENE_BoE\tN\tMEDIAN\tMEAN\tSTD\tNORMALITY\n"; \
		print join("\t", @{ $$genes{$$_} }), "\n" for sort keys %genes;        \
	}
endef


transpose: $(args)
	@$(ECHO) "\nTransposing files '$(args)'." >&2
	perl -F"\t" -lane '\
		$$num = @F if $$k ne $$ARGV; \
		$$k = $$ARGV if $$k ne $$ARGV;
		$$ck++, die "ERROR: Inconsistent file '$$ARGV'.\n" unless $$num == @F; \
		push @{ $$files{$$ARGV} } => [ @F ]; \
		\
		END { \
			die if $$ck; \
			for my $$fl ( keys %files ) { \
				$$num = $$#{ $$files{$$fl} }; \
				open my $$fh, '>', "$$fl.trans" or die "ERROR!\n"; \
				for my $$i ( 0..$$num ) { \
					for my $$j ( 0..$$#{ $$files{$$fl}->[$$i] } ); \
						print $$fh "$$files{$$fl}->[$$j][$$i]\t"; \
					} \
					print $$fh "\n"; \
				} \
				close $$fh
			} \
		}' $(args)
	@$(ECHO) "Done." >&2


renderize: $(renderfile)
	@$(ECHO) "\nRendering document from file '$(renderfile)' with arguments '$(args)'." >&2
	[ -n "$(args)" ] || ( echo "ERROR: Variable 'args' is empty." && exit 7 )
	R --vanilla --slave -e 'library(rmarkdown); render("$<");' --args $(args)
	@$(ECHO) "Done." >&2


rtc_model: $(rtc_model)

$(rtc_model): $(rtc_ind) $(rct_targets)


rtc_targets: $(rtc_targets)

$(rtc_targets): $(annotation_ref) $(gtex_attributes) $(gtex_tpm) $(rtc_parentals) $(manifest_rtc_aux) $(rtc_expression)
	@$(ECHO) "\nGenerating Targets and Patterns table of RTC insertions by individual from '$(rtc_table)' file." >&2
	perl -F"\t" -MList::Util=sum -lane '$(perl_targets)' \
		<(zcat $<) $(gtex_attributes) <(zcat $(gtex_tpm)) \
		$(rtc_parentals) $(manifest_rtc_aux) $(rtc_expression)
	@$(ECHO) "Done." >&2


rtc_individuals: $(rtc_parentals)

$(rtc_parentals): $(annotation_ref) $(rtc_table)
	@$(ECHO) "\nGenerating table of RTC insertions by individual from '$(rtc_table)' file." >&2
	perl -F"\t" -MList::Util=sum -MList::MoreUtils=uniq -lane \
		'$(perl_individuals)' <(zcat $<) $(rtc_table) > $@ 2> $(rtc_hosts)
	@$(ECHO) "Done." >&2


rtc_expression_somatics: $(rtc_expression_soma)

$(rtc_expression_soma): $(rtc_table) $(rtc_expression) $(gtex_BoE) $(degao)
	@$(ECHO) "\nGenerating table of expression of hosts apporting somatics RTCs from file '$<'." >&2
	perl -F"\t" \
		-MStatistics::Basic=median,mean,stddev,correlation \
		-MStatistics::Normality=shapiro_wilk_test \
		-MList::Util=sum \
		-lane \
		'$(perl_expression_soma)' \
			$< \
			$(gtex_BoE) \
			$(degao) \
			$(datadir)/tcga_wxs_all_p.manifest \
			$(datadir)/tcga_tp_all.manifest \
			$(rtc_expression) > $@
	@$(ECHO) "Done." >&2


rtc_expression_match: $(rtc_expression)

$(rtc_expression): $(rtc_match)
	@$(ECHO) "\nCalculate expression table for RTC manifest file: '$<'." >&2
	[ -s "$<" ] || ( echo "ERROR: Variable 'rtc_match' is empty." >&2; exit 7 )
	#cp $< $(datadir)/tmp.manifest
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) \
		-j$(jobs) \
		manifest=$(datadir)/tmp.manifest \
		expr
	mv all_tpm.tsv $@
	perl -i -lne 'print if /^GENE/ or /protein_coding/' $@
	@$(ECHO) "Done." >&2

define olalaoh
		$$act = $$ARGV, $$ct++ unless $$ct; \
		next if /^#/ or /^ID/ or /^CASE/; \
		\
		if ( $$act eq $$ARGV ) { \
			$$tumors{$$F[16]}++; \
		} \
		else { \
			$$tp = $$F[1] =~ /TCGA-(.*?)$$/ ? $$1 : "."; \
			$$tp = "." unless $$tp; \
			print join("\t", @F) if exists $$tumors{$$tp}; \
		}
endef

rtc_expression_manifest: $(rtc_match)

$(rtc_match): $(rtc_table) trick $(manifest)
	@$(ECHO) "\nGenerating expression manifest that match cases in '$<' file." >&2
	[ -n "$(rtc_table)" ] || ( echo "ERROR: Variable 'rtc_table' is empty." >&2; exit 7 )
	cp $(manifest) $(datadir)/tcga_tp_all.manifest
	rm -f $(manifest:.manifest=.payl)
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) \
		module=$(sk8_rootdir)/modules/biotools.mk \
		data_sample=all \
		data_type=wxs \
		data_control="Blood Derived Normal" \
		paired=yes \
		manifest
	[ -s "$(manifest)" ] || ( echo "ERROR: Manifest generation failed for '$@'." >&2; exit 7 )
	cp $(manifest) $(datadir)/tcga_wxs_all_p.manifest
	rm -f $(manifest:.manifest=.payl)
	#perl -F"\t" -lane '$(perl_expression_match)' \
	#	$< \
	#	$(datadir)/tcga_wxs_all_p.manifest \
	#	$(datadir)/tcga_tp_all.manifest > $@
	perl -F"\t" -lane '$(olalaoh)' $< $(datadir)/tcga_tp_all.manifest > $@
	@$(ECHO) "Done." >&2


rtc_retro_higher: mretro
	#$(MAKE) -f $(firstword $(MAKEFILE_LIST)) mretro

mretro: $(mretro)

$(mretro): $(rtc_table)
	@$(ECHO) "\nCalculating list of the most retrocopied parental genes in file: '$<'." >&2
	perl -F"\t" -MList::Util=sum -lane '$(perl_mretro)' $< > $@
	@$(ECHO) "Done." >&2


rtc_expression_higher: $(manifest)
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) rtc_mexpr

rtc_mexpr: $(rtc_mexpr)

$(rtc_mexpr): %_normals.tsv: $(manifest) $(rtc_table)
	@$(ECHO) "\nCreating lists of most expressed coding genes for tumors in file: '$<'." >&2
	#[ -n "$(args)" ] || ( echo "ERROR: Variable 'args' is empty." >&2; exit 7 )
	perl -F"\t" -MStatistics::Basic=median -ane '$(perl_most_expressed)' $^ > $@ 2> $(@:_normals.tsv=_tumors.tsv)
	@$(ECHO) "Done." >&2


rtc_combined_manifest: $(rtc_combined_manifest)

$(rtc_combined_manifest): $(rtc_table) $(manifest_wxs_all_p) $(manifest) # This last manifest determines the combination.
	@$(ECHO) "\nGenerating expression manifest that match cases in '$<' file." >&2
	[ -n "$(args)" ] || ( echo "ERROR: Variable 'args' is empty." >&2; exit 7 )
	perl -F"\t" -lane '$(perl_combine_manifest)' $^ > $@
	@$(ECHO) "Done." >&2


$(manifest_wxs_all_p):
	@$(ECHO) "\nGenerating manifest file for all paired WXS data." >&2
	#cp $(manifest) $@
	@$(ECHO) "Done." >&2

$(manifest_wts_all):
	@$(ECHO) "\nGenerating manifest file for all WTS data." >&2
	#cp $(manifest) $@
	@$(ECHO) "Done." >&2


trick:
	@$(ECHO) "\nSome cleanings ..." >&2
	rm -f $(manifest:.manifest=.payl)
	@$(ECHO) "Done." >&2

$(rtc_table):
	@$(ECHO) "\nUser MUST provide a TSV file in variable 'rtc_table'." >&2
	exit 7


tcga_volcano: $(degao)
	@$(ECHO) "\nPloting a Volcano plot for tumor types: '$(data_sample)'." >&2
	R --vanilla --slave --args $< <<< '$(R_volcano)'
	@$(ECHO) "Done." >&2
	

tcga_expression_boxplot: $(manifest)
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) plt

plt: $(plt)

$(plt): %.png: $(manifest) %_tpm.tsv
	@$(ECHO) "\nGenerating TCGA gene expression boxplot for genes: '$(args)'." >&2
	[ -z "$(args)" ] \
		&& echo "ERROR: Empty variable: 'args' for gene names." >&2 && exit 1
	R --vanilla --slave --args $< $(expr) "$(args)" <<< '$(R_plot)'
	@$(ECHO) "Done." >&2


tcga_gsea: $(gsea)

$(gsea): $(deg) $(rtc_table) | $(R_status)# Pq rtc_table?
	@$(ECHO) "\nCalculating Gene Set Enrichment for RTCs of tumor types: '$(data_sample)'." >&2
	R --vanilla --slave --args $^ <<< '$(R_gsea)'
	@$(ECHO) "Done." >&2


tcga_deg: extract
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) deg

deg: $(deg)

$(deg): $(manifest) $(gene_universe) $(extr)
	@$(ECHO) "\nCreating lists of Differential Expressed Genes for tumor types: '$(data_sample)'." >&2
	R --vanilla --slave --args $< $(gene_universe) $(dir $(firstword $(extr))) <<< '$(R_DEG)'
	@$(ECHO) "Done." >&2


tcga_expression_stats: $(stats)

$(stats): $(manifest) $(expr)
	@$(ECHO) "\nCalculating statistics for expression table and manifest file: '$^' ..." >&2
	perl -F"\t" -MStatistics::Basic -ane '$(perl_stats)' $^ > $@
	@$(ECHO) "Done." >&2


tcga_expression: $(manifest)
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) -j$(jobs) expr

expr: $(expr)

$(expr): $(gencode_v22) $(extr)
	@$(ECHO) "\nCalculating expression table: '$@' ..." >&2
	perl -F"\t" -MList::Util=max -ane '$(tpm_gen)' <(zcat $<) $(extr) > $@
	@$(ECHO) "Done." >&2


quantification: $(manifest)
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) quant

quant: $(quant)

$(quant): %.sf: %.counts $(expr)
	@$(ECHO) "\nCreating quantification file '$@' from '$(expr)' TPM table ..."
	$(quantify) $< $(expr) > $@
	@$(ECHO) "Done."



############################## sideRETRO #######################################

ifneq ($(filter sider_%, $(MAKECMDGOALS)),)

include $(sk8_rootdir)/modules/sider.mk


sider_db                       := $(db)
sider_vcf                      := $(vcf)

sider_ps_opts                  := -a $(annotation_ref) -c 2000000 -Q 20 -F 0.9 -t $(jobs)
sider_mc_opts                  := -B $(blacklist) -c 2000000 -x 1000000 -g 5 -I
sider_vcf_opts                 := -r $(genome_ref)


sider_all: sider_final
	@$(ECHO) "\nAll done for sider." >&2



sider_results: $(manifest)
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) sider_vcf batch=1

sider_vcf: $(sider_vcf)

$(sider_vcf): %.vcf: %.db
	@$(ECHO) "\nCreating VCF file '$@' from database '$<' ..."
	cd $(*D); \
	$(sider) vcf $(sider_vcf_opts) -p $(*F) $<
	@$(ECHO) "Done."


sider_databases: $(manifest)
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) sider_db batch=1

sider_db: $(sider_db)
	@$(ECHO) "\nAll databases complete: $^"

$(sider_db): %.db: %.lst | $(sider_status) $(blacklist) $(annotation_ref)
	@$(ECHO) "\nCreating database '$@' from list file '$<' ..."
	cd $(*D); \
	$(sider) ps $(sider_ps_opts) -p $(*F) -i $<
	$(sider) mc $(sider_mc_opts) $@
	[ -n "$$(find $(*D) -type f -name '*.db' -size +10M)" ] || exit 7
	[ -n "$(keep_files)" ] || rm -rf $(*D)/*.bam
	@$(ECHO) "Done."

endif



############################## MELT ############################################

ifneq ($(filter melt_%, $(MAKECMDGOALS)),)

include $(sk8_rootdir)/modules/melt.mk


OUTPUTS_DIR := $(outputsdir)/MELT_results

# Some commands.
JAR                            := $(stageddir)/bin/MELT.jar
PREPROCESS                     := java -Xmx2G -jar $(JAR) Preprocess
INDIV_ANALYSIS                 := java -Xmx16G -jar $(JAR) IndivAnalysis
GROUP_ANALYSIS                 := java -Xmx32G -jar $(JAR) GroupAnalysis
GENOTYPE                       := java -Xmx32G -jar $(JAR) Genotype
MAKE_VCF                       := java -Xmx2G -jar $(JAR) MakeVCF
CALU                           := java -Xmx2G -jar $(JAR) CALU
LINEU                          := java -Xmx2G -jar $(JAR) LINEU

BASE_DISCOVERY_DIR             := $(OUTPUTS_DIR)/results
HERVK_DISCOVERY_DIR            := $(BASE_DISCOVERY_DIR)/HERVK
LINE1_DISCOVERY_DIR            := $(BASE_DISCOVERY_DIR)/LINE1
ALU_DISCOVERY_DIR              := $(BASE_DISCOVERY_DIR)/ALU
SVA_DISCOVERY_DIR              := $(BASE_DISCOVERY_DIR)/SVA

MELT_PLACE                     := $(tarballsdir)/MELT-2.2.2
MEI_PLACE                      := $(MELT_PLACE)/me_refs/Hg38
HERVK_ZIP                      := $(MEI_PLACE)/HERVK_MELT.zip
LINE1_ZIP                      := $(MEI_PLACE)/LINE1_MELT.zip
ALU_ZIP                        := $(MEI_PLACE)/ALU_MELT.zip
SVA_ZIP                        := $(MEI_PLACE)/SVA_MELT.zip


# Folder for timestamps.
TMSTP                          := $(OUTPUTS_DIR)/tmstp
REFERENCE_BED                  := $(MELT_PLACE)/add_bed_files/Hg38/Hg38.genes.bed
REFERENCE_GTF                  := $(annotation_ref)
REFERENCE_GENOME               := $(genome_ref)
REFERENCE_GENOME_FASTA         := $(genome_ref)
BAM_COVERAGE                   := 40



INPUTS_l := $(inputfl)
INPUTS_FILENAME_l := $(notdir $(INPUTS_l))
INPUTS_BASENAME_l := $(basename $(notdir $(INPUTS_l)))
INPUTS_DIR_l := $(dir $(INPUT_l))
#INPUTS_DIR_BASENAME_l :=


#OUTPUTS_l :=
#OUTPUTS_FILENAME_l :=
#OUTPUTS_BASENAME_l :=
#OUTOUTS_DIR_l :=
OUTPUTS_DIR_BASENAME_l := $(addprefix $(OUTPUTS_DIR)/, $(INPUTS_BASENAME_l))
OUTPUTS_l := $(join $(OUTPUTS_DIR_BASENAME_l), $(addprefix /, $(INPUTS_FILENAME_l)))
OUTPUTS_l := $(srt)


# Derivated lists: '.bai', '.abnormal', '.disc', '.disc.bai' and others.
OUTPUTS_BAI_l := $(addsuffix .bai, $(OUTPUTS_l))
OUTPUTS_DISC_l := $(addsuffix .disc, $(OUTPUTS_l))
OUTPUTS_DISC_BAI_l := $(addsuffix .disc.bai, $(OUTPUTS_l))
OUTPUTS_DISC_FQ_l:= $(addsuffix .disc.fq, $(OUTPUTS_l))
OUTPUTS_ABNORMAL_l := $(addsuffix .abnormal, $(OUTPUTS_l))

# Timestamp lists, terminated in '_tml'.
# Lists for individual analysis.
OUTPUTS_HERVK_INDIV_tml := $(addsuffix .hervk.indiv.tmstp, $(OUTPUTS_l))
OUTPUTS_LINE1_INDIV_tml := $(addsuffix .line1.indiv.tmstp, $(OUTPUTS_l))
OUTPUTS_ALU_INDIV_tml := $(addsuffix .alu.indiv.tmstp, $(OUTPUTS_l))
OUTPUTS_SVA_INDIV_tml := $(addsuffix .sva.indiv.tmstp, $(OUTPUTS_l))

# Simple timestamps files for group analysis.
OUTPUTS_HERVK_GRP := $(TMSTP)/common.bam.hervk.group.tmstp
OUTPUTS_LINE1_GRP := $(TMSTP)/common.bam.line1.group.tmstp
OUTPUTS_ALU_GRP := $(TMSTP)/common.bam.alu.group.tmstp
OUTPUTS_SVA_GRP := $(TMSTP)/common.bam.sva.group.tmstp

# Lists for genotype analysis.
OUTPUTS_HERVK_GEN_tml := $(addsuffix .hervk.gen.tmstp, $(OUTPUTS_l))
OUTPUTS_LINE1_GEN_tml := $(addsuffix .line1.gen.tmstp, $(OUTPUTS_l))
OUTPUTS_ALU_GEN_tml := $(addsuffix .alu.gen.tmstp, $(OUTPUTS_l))
OUTPUTS_SVA_GEN_tml := $(addsuffix .sva.gen.tmstp, $(OUTPUTS_l))

# Simple VCF files.
OUTPUTS_HERVK_VCF := $(HERVK_DISCOVERY_DIR)/HERVK.final_comp.vcf
OUTPUTS_LINE1_VCF := $(LINE1_DISCOVERY_DIR)/LINE1.final_comp.vcf
OUTPUTS_ALU_VCF := $(ALU_DISCOVERY_DIR)/ALU.final_comp.vcf
OUTPUTS_SVA_VCF := $(SVA_DISCOVERY_DIR)/SVA.final_comp.vcf


melt_all: melt_vcf



############################## MAKE VCF ########################################

# Note: VCF are made for all BAMs together, so it can't run in parallel.

# Token target:
melt_vcf: hervk_vcf line1_vcf alu_vcf sva_vcf
hervk_vcf: $(OUTPUTS_HERVK_VCF)
line1_vcf: $(OUTPUTS_LINE1_VCF)
alu_vcf: $(OUTPUTS_ALU_VCF)
sva_vcf: $(OUTPUTS_SVA_VCF)

$(OUTPUTS_HERVK_VCF): %HERVK.final_comp.vcf: $(OUTPUTS_HERVK_GEN_tml)
	$(info )
	$(info $(CALL) Create VCF for HERVK from files $^.)
	$(MAKE_VCF) \
		-genotypingdir $(HERVK_DISCOVERY_DIR) \
		-h $(REFERENCE_GENOME) \
		-t $(HERVK_ZIP) \
		-w $(HERVK_DISCOVERY_DIR) \
		-p $(HERVK_DISCOVERY_DIR) \
		-o $(@D)

$(OUTPUTS_LINE1_VCF): %LINE1.final_comp.vcf: $(OUTPUTS_LINE1_GEN_tml)
	$(info )
	$(info $(CALL) Create VCF for LINE1 from files $^.)
	$(MAKE_VCF) \
		-genotypingdir $(LINE1_DISCOVERY_DIR) \
		-h $(REFERENCE_GENOME) \
		-t $(LINE1_ZIP) \
		-w $(LINE1_DISCOVERY_DIR) \
		-p $(LINE1_DISCOVERY_DIR) \
		-o $(@D)

$(OUTPUTS_ALU_VCF): %ALU.final_comp.vcf: $(OUTPUTS_ALU_GEN_tml)
	$(info )
	$(info $(CALL) Create VCF for ALU from files $^.)
	$(MAKE_VCF) \
		-genotypingdir $(ALU_DISCOVERY_DIR) \
		-h $(REFERENCE_GENOME) \
		-t $(ALU_ZIP) \
		-w $(ALU_DISCOVERY_DIR) \
		-p $(ALU_DISCOVERY_DIR) \
		-o $(@D)

$(OUTPUTS_SVA_VCF): %SVA.final_comp.vcf: $(OUTPUTS_SVA_GEN_tml)
	$(info )
	$(info $(CALL) Create VCF for SVA from files $^.)
	$(MAKE_VCF) \
		-genotypingdir $(SVA_DISCOVERY_DIR) \
		-h $(REFERENCE_GENOME) \
		-t $(SVA_ZIP) \
		-w $(SVA_DISCOVERY_DIR) \
		-p $(SVA_DISCOVERY_DIR) \
		-o $(@D)



############################## GENOTYPING ######################################

# Token target:
melt_gen: hervk_gen line1_gen alu_gen sva_gen
hervk_gen: $(OUTPUTS_HERVK_GEN_tml)
line1_gen: $(OUTPUTS_LINE1_GEN_tml)
alu_gen: $(OUTPUTS_ALU_GEN_tml)
sva_gen: $(OUTPUTS_SVA_GEN_tml)

$(OUTPUTS_HERVK_GEN_tml): %.bam.hervk.gen.tmstp: %.bam $(OUTPUTS_HERVK_GRP)
	$(info )
	$(info $(CALL) Genotyping HERVK in file $<.)
	$(GENOTYPE) \
		-bamfile $< \
		-h $(REFERENCE_GENOME) \
		-t $(HERVK_ZIP) \
		-w $(HERVK_DISCOVERY_DIR) \
		-p $(HERVK_DISCOVERY_DIR)
	touch $@

$(OUTPUTS_LINE1_GEN_tml): %.bam.line1.gen.tmstp: %.bam $(OUTPUTS_LINE1_GRP)
	$(info )
	$(info $(CALL) Genotyping LINE1 in file $<.)
	$(GENOTYPE) \
		-bamfile $< \
		-h $(REFERENCE_GENOME) \
		-t $(LINE1_ZIP) \
		-w $(LINE1_DISCOVERY_DIR) \
		-p $(LINE1_DISCOVERY_DIR)
	touch $@

$(OUTPUTS_ALU_GEN_tml): %.bam.alu.gen.tmstp: %.bam $(OUTPUTS_ALU_GRP)
	$(info )
	$(info $(CALL) Genotyping ALU in file $<.)
	$(GENOTYPE) \
		-bamfile $< \
		-h $(REFERENCE_GENOME) \
		-t $(ALU_ZIP) \
		-w $(ALU_DISCOVERY_DIR) \
		-p $(ALU_DISCOVERY_DIR)
	touch $@

$(OUTPUTS_SVA_GEN_tml): %.bam.sva.gen.tmstp: %.bam $(OUTPUTS_SVA_GRP)
	$(info )
	$(info $(CALL) Genotyping SVA in file $<.)
	$(GENOTYPE) \
		-bamfile $< \
		-h $(REFERENCE_GENOME) \
		-t $(SVA_ZIP) \
		-w $(SVA_DISCOVERY_DIR) \
		-p $(SVA_DISCOVERY_DIR)
	touch $@



############################## GROUP ANALYSES ##################################

# Note: Group analysis are made for all together, so it can't run in parallel.

# Token target:
melt_group: hervk_group line1_group alu_group sva_group
hervk_group: $(OUTPUTS_HERVK_GRP)
line1_group: $(OUTPUTS_LINE1_GRP)
alu_group: $(OUTPUTS_ALU_GRP)
sva_group: $(OUTPUTS_SVA_GRP)

$(OUTPUTS_HERVK_GRP): $(OUTPUTS_HERVK_INDIV_tml) | $(TMSTP)
	$(info )
	$(info $(CALL) Group analysis of HERVK on files $^.)
	$(GROUP_ANALYSIS) \
		-discoverydir $(HERVK_DISCOVERY_DIR) \
		-h $(REFERENCE_GENOME) \
		-t $(HERVK_ZIP) \
		-w $(HERVK_DISCOVERY_DIR) \
		-n $(REFERENCE_BED)
	touch $@

$(OUTPUTS_LINE1_GRP): $(OUTPUTS_LINE1_INDIV_tml) | $(TMSTP)
	$(info )
	$(info $(CALL) Group analysis of LINE1 on files $^.)
	$(GROUP_ANALYSIS) \
		-discoverydir $(LINE1_DISCOVERY_DIR) \
		-h $(REFERENCE_GENOME) \
		-t $(LINE1_ZIP) \
		-w $(LINE1_DISCOVERY_DIR) \
		-n $(REFERENCE_BED)
	touch $@

$(OUTPUTS_ALU_GRP): $(OUTPUTS_ALU_INDIV_tml) | $(TMSTP)
	$(info )
	$(info $(CALL) Group analysis of ALU on files $^.)
	$(GROUP_ANALYSIS) \
		-discoverydir $(ALU_DISCOVERY_DIR) \
		-h $(REFERENCE_GENOME) \
		-t $(ALU_ZIP) \
		-w $(ALU_DISCOVERY_DIR) \
		-n $(REFERENCE_BED)
	touch $@

$(OUTPUTS_SVA_GRP): $(OUTPUTS_SVA_INDIV_tml) | $(TMSTP)
	$(info )
	$(info $(CALL) Group analysis of SVA on files $^.)
	$(GROUP_ANALYSIS) \
		-discoverydir $(SVA_DISCOVERY_DIR) \
		-h $(REFERENCE_GENOME) \
		-t $(SVA_ZIP) \
		-w $(SVA_DISCOVERY_DIR) \
		-n $(REFERENCE_BED)
	touch $@



############################## INDIVIDUAL ANALYSES #############################

melt_indiv: hervk_indiv line1_indiv alu_indiv sva_indiv
hervk_indiv: $(OUTPUTS_HERVK_INDIV_tml)
line1_indiv: $(OUTPUTS_LINE1_INDIV_tml)
alu_indiv: $(OUTPUTS_ALU_INDIV_tml)
sva_indiv: $(OUTPUTS_SVA_INDIV_tml)

$(OUTPUTS_HERVK_INDIV_tml): %.bam.hervk.indiv.tmstp: %.bam %.bam.disc
	$(info )
	$(info $(CALL) Individual analysis of HERVK in file $<.)
	$(INDIV_ANALYSIS) \
		-bamfile $< \
		-h $(REFERENCE_GENOME) \
		-t $(HERVK_ZIP) \
		-w $(HERVK_DISCOVERY_DIR) \
		-c $(BAM_COVERAGE)
	touch $@

$(OUTPUTS_LINE1_INDIV_tml): %.bam.line1.indiv.tmstp: %.bam %.bam.disc
	$(info )
	$(info $(CALL) Individual analysis of LINE1 in file $<.)
	$(INDIV_ANALYSIS) \
		-bamfile $< \
		-h $(REFERENCE_GENOME) \
		-t $(LINE1_ZIP) \
		-w $(LINE1_DISCOVERY_DIR) \
		-c $(BAM_COVERAGE)
	touch $@

$(OUTPUTS_ALU_INDIV_tml): %.bam.alu.indiv.tmstp: %.bam %.bam.disc
	$(info )
	$(info $(CALL) Individual analysis of ALU in file $<.)
	$(INDIV_ANALYSIS) \
		-bamfile $< \
		-h $(REFERENCE_GENOME) \
		-t $(ALU_ZIP) \
		-w $(ALU_DISCOVERY_DIR) \
		-c $(BAM_COVERAGE)
	touch $@

$(OUTPUTS_SVA_INDIV_tml): %.bam.sva.indiv.tmstp: %.bam %.bam.disc
	$(info )
	$(info $(CALL) Individual analysis of SVA in file $<.)
	$(INDIV_ANALYSIS) \
		-bamfile $< \
		-h $(REFERENCE_GENOME) \
		-t $(SVA_ZIP) \
		-w $(SVA_DISCOVERY_DIR) \
		-c $(BAM_COVERAGE)
	touch $@


############################## PREPROCESSING ###################################

melt_preprocess: $(OUTPUTS_DISC_l)

# Preprocessing BAMs.
$(OUTPUTS_DISC_l): %.bam.disc: %.bam %.bam.bai | $(REFERENCE_GENOME_FASTA) $(melt_status)
	@$(ECHO) "\nCreating preprocess file '$@' from file '$<' ..." >&2
	$(PREPROCESS) -bamfile $< -h $(REFERENCE_GENOME_FASTA)
	@$(ECHO) "Done." >&2


melt_common: $(OUTPUTS_BAI_l) 

$(OUTPUTS_BAI_l): %.bam.bai: %.bam
	@$(ECHO) "\nCreating index file '$@' for BAM file '$< ' ..." >&2
	if [ -s "$$(readlink -f $<).bai" ]; then \
		ln -sf "$$(readlink -f $<).bai" $@; \
	else \
		samtools index -b -@4 $< $@; \
	fi
	@$(ECHO) "Done." >&2


melt_rec = $(filter %$(*F).bam, $(inputfl))

melt_links: $(OUTPUTS_l)

ifeq (fdgsfd,)
.SECONDEXPANSION:
$(OUTPUTS_l): %.bam: $$(melt_rec) | $(OUTPUT_DIR) $(samtools_status)
	@$(ECHO) "\nCreating link '$@' for input file '$<' ..." >&2
	mkdir -p $(*D)
	if [ "$$(samtools view -H $< 2> /dev/null | head -n1 | cut -f3)" = "SO:coordinate" ]; then \
		ln -sf "$$(readlink -f $<)" $@; \
	elif [ -s "$$(find $$(dirname $$(readlink -f $<)) -type f -name '*$(*F)*.sorted.bam')" ]; then \
		ln -sf "$$(find $$(dirname $$(readlink -f $<)) -type f -name '*$(*F)*.sorted.bam')" $@; \
	else \
		samtools sort -O BAM -m 1G -@4 $< -o $@; \
	fi
	@$(ECHO) "Done." >&2

endif


$(OUTPUTS_DIR) $(HERVK_DIR) $(LINE1_DIR) $(ALU_DIR) $(SVA_DIR) $(TMSTP):
	@$(ECHO) "\nCreating directory '$@' ..." >&2
	mkdir -p $@
	@$(ECHO) "Done." >&2

endif # melt



############################## DEBUG ###########################################

ifeq ($(dbg_biotools),yes)
$(info )
$(info ############################## BIOTOOLS DEBUG ##################################)
$(info General paths.)
$(info CURDIR:                        $(CURDIR).)
$(info PWD:                           $(PWD).)
$(info ./:                            $(abspath .).)
$(info workdir:                       $(workdir).)
$(info sk8_rootdir:                   $(sk8_rootdir).)
$(info sk8_modules:                   $(MAKEFILE_LIST).)
$(info LIBRARY_PATH:                  $(LIBRARY_PATH))
$(info CPATH:                         $(CPATH))
$(info PATH:                          $(PATH))
$(info )

$(info Build paths.)
$(info DESTDIR:                       $(DESTDIR).)
$(info prefix:                        $(prefix).)
$(info installdir:                    $(installdir).)
$(info stageddir:                     $(stageddir).)
$(info rootdir:                       $(rootdir).)
$(info srcdir:                        $(srcdir).)
$(info builddir:                      $(builddir).)
$(info datadir:                       $(datadir).)
$(info )

$(info Environment paths.)
$(info switch:                        $(switch).)
$(info capsule:                       $(capsule).)
$(info basedir:                       $(basedir).)
$(info configdir:                     $(configdir).)
$(info inputsdir:                     $(inputsdir).)
$(info outputsdir:                    $(outputsdir).)
$(info datadir:                       $(datadir).)
$(info referencedir:                  $(referencedir).)
$(info annotationdir:                 $(annotationdir).)
$(info extradir:                      $(extradir).)
$(info tmpdir:                        $(tmpdir).)
$(info infrastructure:                $(infrastructure).)
$(info )

$(info Runtime data.)
$(info default goal:                  $(DEFAULT_GOAL))
$(info runmode:                       $(runmode))
$(info data_sample:                   $(data_sample).)
$(info data_type:                     $(data_type).)
$(info data_mode:                     $(datamode).)
$(info data_control:                  $(data_control).)
$(info data_treated:                  $(data_treated).)
$(info pairing:                       $(pairing).)
$(info radical:                       $(radical).)
$(info manifest:                      $(manifest).)
$(info manifest_type:                 $(manifest_type).)
$(info Command Line:                  $(CMD).)
$(info )

$(info Configuration files.)
$(info configfile: $(configfile).)
$(info dft_config: $(dft_config).)
$(info cfg: $(cfg).)
$(info )

$(info Input data.)
$(info suffix:                        $(sfx).)
$(info inputfl:                       $(inputfl).)
$(info args:                          $(args).)
$(info )

$(info Misc data.)
$(info hg19:                          $(hg19).)
$(info hg38:                          $(hg38).)
$(info genome_ref:                    $(genome_ref).)
$(info annotation_ref:                $(annotation_ref).)
$(info Genes blacklist:               $(blacklist).)
$(info GDC_token:                     $(gdc_token).)
$(info tcga_status:                   $(tcga_status).)
$(info sider:                         $(sider).)
$(info sider_ps_opts:                 $(sider_ps_opts).)
$(info sider_mc_opts:                 $(sider_mc_opts).)
$(info sider_vcf_opts:                $(sider_vcf_opts).)
$(info )

$(info More misc data.)
$(info dwl:                           $(dwl).)
$(info lst:                           $(lst).)
$(info db:                            $(db).)
$(info vcf:                           $(vcf).)
$(info tsv:                           $(tsv).)
$(info pdf:                           $(pdf).)
$(info ################################################################################)
endif
