# Info to download, configure, build, install and use 'java' package.
java_src                    := https://javadl.oracle.com/webapps/download/AutoDL?BundleId=244058_89d678f2be164786b292527658ca1605
java_ext                    := .tar.gz

java_package                := java
java_version                := 1.8.0
java_id                     := $(java_package)-xxxxxx
java_distname               := $(java_package)-$(java_version)
java_license                := GPLv4

# Potential installation data.
java_repository             := $(java_src)
java_tarball                := $(tarballsdir)/$(java_distname)$(java_ext)
java_builddir               := $(tarballsdir)/$(java_distname)
java_configurefile          := $(java_builddir)/configure
java_buildfile              := $(java_builddir)/Makefile
java_execfile               := $(java_builddir)/bin/$(java_package)
java_installfile            := $(installdir)/$(java_package)


# Actual installation data.
java_has                    := $(shell which $(java_package) 2> /dev/null)
java_get                    := $(if $(java_has),,java_install)
java                        := $(if $(java_has),$(java_has),$(java_installfile))


# FORCE data.
java_force                  := $(if $(FORCE),java_force,)
java_status                 := $(if $(FORCE),java_install,$(java_get))
java                        := $(if $(FORCE),$(java_installfile),$(java))


# Include required modules.
#include $(addprefix $(sk8_rootdir)/modules/, gcc.mk)
java_deps                   := $(if $(recursive),java_deps,)
java_depscheck              := $(gcc_status) $(internet_status) $(java_deps)


# Verification.
java_status: $(java_status) # 'java_install' or none.
	@$(ECHO) "\nSystem has '$(java_package)' up and running." >&2


# Install.
java_install: $(java_installfile) # $(installdir)/$(java_package)
	@$(ECHO) "\nSystem has '$(java_package)' in PATH: '$<'." >&2

$(java_installfile): $(java_execfile) | $(installdir) # just copy the executable file to $(installdir).
	@$(ECHO) "\nInstalling '$(java_package)' on directory '$(installdir)' ..." >&2
	mkdir -p $(dir $(installdir))/lib
	install -t $(installdir) -m 755 $(dir $<)/*
	cp -r $(java_builddir)/lib/amd64/ $(dir $(installdir))/lib
	cp $(java_builddir)/lib/*.jar $(dir $(installdir))/lib
	cp $(java_builddir)/lib/tzdb.dat $(java_builddir)/lib/currency.data $(dir $(installdir))/lib
	if [ -z "$$(perl -ne 'print if s#.*$(installdir).*#AA#g' <<< $${PATH})" ]; then \
		echo -e "export PATH=$(installdir):\$$PATH" >> ~/.bashrc; \
		source ~/.bashrc; \
		echo -e "WARNING: You may need to run 'source ~/.bashrc' to release the new \$$PATH."; \
	elif [ "$(java)" != "$(java_installfile)" ]; then \
		echo "WARNING: Previuos installation of '$(java_package)' may be shadowing the new one in the \$$PATH."; \
	fi
	touch $@
	@$(ECHO) "Done." >&2


# Build.
java_build: $(java_execfile) # $(java_builddir)/$(java_package)

$(java_execfile): $(java_buildfile) | $(gcc_status) # a Makefile generates the executable file.
	@$(ECHO) "\nBuilding '$(java_package)' on directory '$(java_builddir)' ..." >&2
	#$(MAKE) -C $(java_builddir) -j4
	touch $@
	@$(ECHO) "Done." >&2


# Configure.
java_configure: $(java_buildfile) # $(java_builddir)/Makefile

$(java_buildfile): $(java_configurefile)
	@$(ECHO) "\nConfiguring '$(java_package)' on directory '$(java_builddir)' ..." >&2
	#cd $(java_builddir); \
	#./configure --prefix=$(dir $(installdir)) $(java_args)
	touch $@
	@$(ECHO) "Done." >&2

$(java_configurefile): $(java_tarball)
	@$(ECHO) "\nExtracting '$(java_package)' tarball to directory '$(java_builddir)' ..." >&2
	mkdir -p $(java_builddir)
	if [ "$(java_ext)" == ".tar.gz" ]; then \
		tar -xzvf $< -C $(java_builddir) --strip-components=1; \
	else \
		tar -xvf $< -C $(java_builddir) --strip-components=1; \
	fi
	touch $@
	@$(ECHO) "Done." >&2


# Download.
java_tarball: $(java_tarball) # $(tarballsdir)/$(java_tarball).

$(java_tarball): $(internet_status) $(java_force) | $(tarballsdir) $(java_depscheck)
	@$(ECHO) "\nDownloading '$(java_package)' tarball from '$(java_repository)' ..." >&2
	wget -c $(java_repository) -P $(tarballsdir) -O $@
	touch $@
	@$(ECHO) "Done." >&2

java_deps:
	@$(ECHO) "\nRecursively installing dependecies for '$(java_distname)' ..." >&2
	[ -n "$(args)" ] || ( echo "ERROR: Variable 'args' can not be empty." >&2; exit 1 )
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) recursive= $(args)
	@$(ECHO) "Done." >&2

java_force:
	@$(ECHO) "\nForce reinstallation of '$(java_package)' on directory '$(installdir)'." >&2
	rm -f $(java_tarball)
	@$(ECHO) "Done." >&2


java_refresh: java_uninstall
	if [ -z "$(usermode)" ]; then \
		$(MAKE) -f $(firstword $(MAKEFILE_LIST)) DESTDIR=$(DESTDIR) prefix=$(prefix) java_install; \
	else \
		$(MAKE) -f $(firstword $(MAKEFILE_LIST)) DESTDIR=$(DESTDIR) prefix=$(prefix) java_install usermode=1; \
	fi


# Uninstall and clean.
java_purge: java_uninstall java_clean

java_uninstall:
	@$(ECHO) "\nUninstalling '$(java_package)' from directory '$(installdir)' ..." >&2
	[ -d "$(installdir)" ] || exit 1;
	if [ -f "$(java_buildfile)_no" ]; then \
		$(MAKE) -C $(java_builddir) uninstall DESTDIR=$(DESTDIR) prefix=$(dir $(installdir)) -j4; \
	else \
		cd $(installdir); \
		rm -f ControlPanel  java  javaws  jcontrol  jjs  keytool  orbd  pack200  policytool  rmid  rmiregistry  servertool  tnameserv  unpack200; \
		cd -; \
		rm -rf $(dir $(installdir))/lib/amd64; \
		rm -f $(dir $(installdir))/lib/*.jar; \
		rm -f $(dir $(installdir))/lib/tzdb.dat $(dir $(installdir))/lib/currency.data; \
	fi
	@$(ECHO) "User must resolve \$$PATH in '~/.bashrc' file." >&2
	@$(ECHO) "Done." >&2

java_clean:
	@$(ECHO) "\nCleaning '$(java_package)' from staged directory '$(stageddir)' ..." >&2
	rm -rf $(tarballsdir)/$(java_package)*
	@$(ECHO) "Done." >&2


# Tests.
java_test:
	@$(ECHO) "\nTesting '$(java_package)' ..." >&2
	if [ -z "$$($(java_package) --help)" ]; then \
		echo "Package '$(java_package)' is not properly running." >&2; \
		exit 1; \
	else \
		echo "PASS!"; \
	fi
	@$(ECHO) "Done." >&2

java_test_build: args := make -f SK8/SK8.mk module=SK8/modules/java.mk usermode=1 \
	java_src=$(java_src) java_ext=$(java_ext) java_package=$(java_package) java_version=$(java_version) recursive=$(recursive) \
	java_install java_test
java_test_build: docker_test_build
	@$(ECHO) "\nFinished Docker build test for '$(java_package)' module." >&2
