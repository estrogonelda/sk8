############################## MESON PACKAGE ###################################

# Info to download, configure, build, install and use 'meson' package.
meson_src                      := https://github.com/mesonbuild/meson/releases/download/0.55.0/meson-0.55.0.tar.gz
meson_ext                      := .tar.gz

meson_package                  := $(shell perl -ne 'print "@{[ (split(/-/, $$_))[0] ]}"' <<< "$(notdir $(meson_src))")
meson_version                  := $(shell perl -pe 's/^$(meson_package)-(.*?)\$(meson_ext)/$$1/g' <<< "$(notdir $(meson_src))")
meson_id                       := $(meson_package)-xxxxxx
meson_distname                 := $(meson_package)-$(meson_version)
meson_license                  := GPLv4

# Potential installation data.
meson_dependencies             := python
meson_repository               := $(meson_src)
meson_tarball                  := $(tarballsdir)/$(meson_distname)$(meson_ext)
meson_builddir                 := $(tarballsdir)/$(meson_distname)
meson_configurefile            := $(meson_builddir)/configure.fk
meson_buildfile                := $(meson_builddir)/Makefile.fk
meson_execfile                 := $(meson_builddir)/$(meson_package).py
meson_installfile              := $(installdir)/$(meson_package)


# Actual installation data.
meson_has                      := $(shell which $(meson_package) 2> /dev/null)
meson_get                      := $(if $(meson_has),,meson_install)
meson                          := $(if $(meson_has),$(meson_has),$(meson_installfile))


# FORCE data.
meson_force                    := $(if $(FORCE),meson_force,)
meson_status                   := $(if $(FORCE),meson_install,$(meson_get))
meson                          := $(if $(FORCE),$(meson_installfile),$(meson))


# Include required modules.
sider_depscheck                := $(conda_status)


# Verification.
meson_status: $(meson_status) # 'meson_install' or none.
	@$(ECHO) "\nSystem has '$(meson_package)' up and running."


# Install.
meson_install: $(meson_installfile) # $(installdir)/$(meson_package)
	@$(ECHO) "\nSystem has '$(meson_package)' in PATH: '$<'."

$(meson_installfile): $(meson_execfile) # just copy the executable file to $(installdir).
	@$(ECHO) "\nInstalling '$(meson_package)' on directory '$(installdir)' ..."
	install -d $(installdir)
	install -m 755 $< $(installdir)
	mv $(meson_installfile).py $(meson_installfile)
	cp -r $(meson_builddir)/mesonbuild $(installdir)
	#$(MAKE) -C $(meson_builddir) install DESTDIR=$(DESTDIR) prefix=$(prefix) -j
	if [ -z "$$(perl -ne 'print if s#.*$(installdir).*#AA#g' <<< $${PATH})" ]; then \
		echo -e "export PATH=$(installdir):\$$PATH" >> ~/.bashrc; \
		source ~/.bashrc; \
		echo -e "WARNING: You may need to run 'source ~/.bashrc' to release the new \$$PATH."; \
	elif [ "$(meson)" != "$(meson_installfile)" ]; then \
		echo "WARNING: Previuos installation of '$(meson_package)' may be shadowing the new one in the \$$PATH."; \
	fi
	touch $@
	@$(ECHO) "Done."


# Build.
meson_build: $(meson_execfile) # $(meson_builddir)/$(meson_package)

$(meson_execfile): $(meson_buildfile) # a Makefile generates the executable file.
	@$(ECHO) "\nBuilding '$(meson_package)' on directory '$(meson_builddir)' ..."
	#$(MAKE) -C $(meson_builddir) -j
	touch $@
	@$(ECHO) "Done."


# Configure.
meson_configure: $(meson_buildfile) # $(meson_builddir)/Makefile

$(meson_buildfile): $(meson_configurefile)
	@$(ECHO) "\nConfiguring '$(meson_package)' on directory '$(meson_builddir)' ..."
	#cd $(meson_builddir); \
	#./configure
	touch $@
	@$(ECHO) "Done."

$(meson_configurefile): $(meson_tarball)
	@$(ECHO) "\nExtracting '$(meson_package)' tarball to directory '$(meson_builddir)' ..."
	mkdir -p $(meson_builddir)
	tar -xzvf $< -C $(meson_builddir) --strip-components=1
	touch $@
	@$(ECHO) "Done."


# Download.
meson_tarball: $(meson_tarball) # $(tarballsdir)/$(meson_tarball).

$(meson_tarball): $(internet_get) $(meson_force) | $(tarballsdir)
	@$(ECHO) "\nDownloading '$(meson_package)' tarball from '$(meson_repository)' ..."
	wget -c $(meson_repository) -P $(tarballsdir) -O $@
	touch $@
	@$(ECHO) "Done."


meson_refresh: meson_uninstall
	if [ -z "$(usermode)" ]; then \
		$(MAKE) -f $(firstword $(MAKEFILE_LIST)) DESTDIR=$(DESTDIR) prefix=$(prefix) meson_install; \
	else \
		$(MAKE) -f $(firstword $(MAKEFILE_LIST)) DESTDIR=$(DESTDIR) prefix=$(prefix) meson_install usermode=1; \
	fi


# Uninstall and clean.
meson_purge: meson_uninstall meson_clean

meson_uninstall:
	@$(ECHO) "\nUninstalling '$(meson_package)' from directory '$(installdir)' ...";
	[ -d "$(installdir)" ] || exit 1;
	if [ -f "$(meson_buildfile)_no" ]; then \
		$(MAKE) -C $(meson_builddir) uninstall DESTDIR=$(DESTDIR) prefix=$(dir $(installdir)) -j4; \
	else \
		rm -rf $(meson_installfile)*; \
	fi
	echo "User may resolve \$$PATH in '~/.bashrc' file." >&2 
	@$(ECHO) "Done."

meson_clean:
	@$(ECHO) "\nCleaning '$(meson_package)' from staged directory '$(stageddir)' ...";
	rm -rf $(tarballsdir)/$(meson_package)*
	@$(ECHO) "Done."


# FORCE!
meson_force:
	@$(ECHO) "\nForce installation of '$(meson_package)' on directory '$(installdir)'."
	-rm $(meson_tarball)
	@$(ECHO) "Done."


# Test.
meson_test:
	@$(ECHO) "\nTesting package '$(meson_package)' ...";
	if [ -z "$$($(meson_package) --version 2> /dev/null)" ]; then \
		echo "Package '$(meson_package)' is not properly running."; \
		exit 1; \
	else \
		echo "PASS!"; \
	fi
	@$(ECHO) "Done."


# Include required modules.
include  $(addsuffix .mk, $(addprefix $(sk8_rootdir)/modules/,$(meson_dependencies)))



############################## DEBUG CODE ######################################

ifeq ($(dbg_meson),yes)
$(info ****************************** DEBUG GENERIC PACK ******************************)
$(info Precursor variables for a 'meson' package source.)
$(info workdir:                       $(workdir).)
$(info stageddir:                     $(stageddir).)
$(info tarballsdir:                   $(tarballsdir).)
$(info DESTDIR:                       $(DESTDIR).)
$(info installdir:                    $(installdir).)
$(info usermode:                      $(usermode).)
$(info src:                           $(src).)
$(info ext:                           $(ext).)
$(info )

$(info Info to download, configure, build, install and use 'meson' package.)
$(info meson_source:                $(meson_source).)
$(info meson_ext:                   $(meson_ext).)
$(info )

$(info meson_package:               $(meson_package).)
$(info meson_version:               $(meson_version).)
$(info meson_id:                    $(meson_id).)
$(info meson_distname:              $(meson_distname).)
$(info meson_license:               $(meson_license).)
$(info )

$(info Potential installation data.)
$(info meson_dependecies:           $(meson_dependecies).)
$(info meson_repository:            $(meson_repository).)
$(info meson_tarball:               $(meson_tarball).)
$(info meson_builddir:              $(meson_builddir).)
$(info meson_configurefile:         $(meson_configurefile).)
$(info meson_buildfile:             $(meson_buildfile).)
$(info meson_execfile:              $(meson_execfile).)
$(info meson_installfile:           $(meson_installfile).)


$(info Actual installation data.)
$(info meson_has:                   $(meson_has).)
$(info meson_get:                   $(meson_get).)
$(info meson:                       $(meson).)
$(info )


$(info Status and FORCE data.)
$(info meson_force:                 $(meson_force).)
$(info meson_status:                $(meson_status).)
$(info ********************************************************************************)
endif
