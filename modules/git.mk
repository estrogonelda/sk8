############################## HEADER ##########################################
#
# SK8 Module installation for 'Git' version '2.26.2'.
#
################################################################################



############################## PREAMBLE ########################################
# Verify initialization variables.



############################## DEFINITIONS #####################################

# Info to download, configure, build, install and use 'git' package.
git_package                    := git
git_version                    := 2.26.2
git_id                         := $(git_package)-xxxxxx
git_distname                   := $(git_package)-$(git_version)
git_license                    := GPLv4

# Potential installation data.
git_dependecies                := bash make gcc
git_tarball                    := $(git_distname).tar.gz
git_repository                 := https://mirrors.edge.kernel.org/pub/software/scm/git/$(git_tarball)
git_tarball                    := $(tarballsdir)/$(git_tarball)
git_builddir                   := $(tarballsdir)/$(git_distname)
git_configurefile              := $(git_builddir)/configure
git_buildfile                  := $(git_builddir)/Makefile
git_execfile                   := $(git_builddir)/$(git_package)
git_installfile                := $(installdir)/$(git_package)


# Actual installation data.
git_has                        := which $(git_package) 2> /dev/null
git_get                        := $(if $(shell $(git_has)),,git_install)
git                            := $(if $(shell $(git_has)),$(shell $(git_has)),$(git_installfile))


# FORCE data.
git_force                      := $(if $(FORCE),git_force,)
git_status                     := $(if $(FORCE),git_install,$(git_get))
git                            := $(if $(FORCE),$(git_installfile),$(git))


# Verification.
git_status: $(git_status) # 'git_install' or none.
	@$(ECHO) "\nSystem has '$(git_package)' up and running."


# Install.
git_install: $(git_installfile) # $(installdir)/$(package)
	@$(ECHO) "\nSystem has '$(git_package)' in PATH: '$<'."
	@$(ECHO) "Please, run '. ~/.bashrc' or reload your bash for the changes take place."

$(git_installfile): $(git_execfile) # just copy the executable file to $(installdir).
	@$(ECHO) "\nInstalling '$(git_package)' on directory '$(installdir)' ..."
	$(MAKE) -C $(git_builddir) install DESTDIR=$(DESTDIR) prefix=$(prefix) -j
	if [ -n "$(DESTDIR)" ]; then \
		if [ -z "$$(perl -F: -ane 'print if s#.*($(installdir)).*#$$1#g' <<< $$PATH)" ]; then \
			echo -e "export PATH=$(installdir):\$$PATH" >> ~/.bashrc; \
			. ~/.bashrc; \
			echo -e "WARNING: You may need to run 'source ~/.bashrc' to release the new \$$PATH." >&2; \
		fi \
	elif [ "$(git)" != "$(git_installfile)" ]; then \
		echo "WARNING: Previuos installation of '$(git_package)' may be shadowing the new one in the \$$PATH." >&2; \
	fi
	touch $@
	@$(ECHO) "Done."


# Build.
git_build: $(execfile) # $(git_builddir)/$(git_package)

$(execfile): $(git_buildfile) # a Makefile generates the executable file.
	@$(ECHO) "\nBuilding '$(git_package)' on directory '$(git_builddir)' ..."
	$(MAKE) -C $(git_builddir) all -j
	touch $@
	@$(ECHO) "Done."


# Configure.
git_configure: $(git_buildfile) # $(git_builddir)/Makefile

$(git_buildfile): $(configurefile)
	@$(ECHO) "\nConfiguring '$(git_package)' on directory '$(git_builddir)' ..."
	cd $(git_builddir); \
	$(MAKE) configure; \
	./configure
	touch $@
	@$(ECHO) "Done."

$(git_configurefile): $(git_tarball)
	@$(ECHO) "\nExtracting '$(git_package)' tarball to directory '$(git_builddir)' ..."
	tar -xzvf $< -C $(tarballsdir)
	touch $@
	@$(ECHO) "Done."


# Download.
git_tarball: $(git_tarball) # $(tarballsdir)/$(git_tarball).

$(git_tarball): $(internet_get) $(git_force) | $(tarballsdir)
	@$(ECHO) "\nDownloading '$(git_package)' tarball from '$(git_repository)' ..."
	wget -c $(git_repository) -P $(tarballsdir) -O $@
	touch $@
	@$(ECHO) "Done."


git_refresh: git_uninstall git_install


# Uninstall and clean.
git_uninstall:
	@$(ECHO) "\nUninstalling '$(git_package)' from directory '$(installdir)' ...";
	[ -d "$(installdir)" ] || exit 1;
	$(MAKE) -C $(git_builddir) uninstall DESTDIR=$(DESTDIR) prefix=$(prefix) -j
	echo "WARNING: User must check \$$PATH variable in '~/.bashrc' file." >&2
	@$(ECHO) "Done."

git_clean:
	@$(ECHO) "\nCleaning '$(git_package)' from staged directory '$(stageddir)' ...";
	rm -rf $(tarballsdir)/$(git_package)*
	@$(ECHO) "Done."

git_purge: git_uninstall git_clean


# FORCE!
git_force:
	@$(ECHO) "\nForce reinstallation of '$(git_package)' on directory '$(installdir)'."
	rm $(git_tarball)
	@$(ECHO) "Done."


# Test.
git_test:
	@$(ECHO) "\nTesting '$(git_package)' ...";
	if [ -z "$$(git --version 2> /dev/null)" ]; then \
		echo "Package '$(git_package)' is not properly running." >&2; \
		exit 1; \
	else \
		echo "PASS!" \
	fi
	@$(ECHO) "Done."


# Extra.
$(git_builddir):
	@$(ECHO) "\nCreating directory '$@' ..."
	mkdir -p $@
	@$(ECHO) "Done."
