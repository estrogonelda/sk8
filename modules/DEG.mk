#!/bin/env make
############################## HEADER ##########################################
#
# DEG - Differential Expressed Genes module for SK8 framework.
#
################################################################################



############################## PREAMBLE ########################################

# Module metadata.
deg_self                       := DEG.mk
deg_version                    := 0.1.0


# Predefined commands.
SHELL                          := /bin/bash
ECHO                           := echo -e


# Verify module hierarchy.
sk8_rootdir                    := $(dir $(abspath $(strip $(filter %SK8.mk, $(firstword $(MAKEFILE_LIST))))))
$(if $(sk8_rootdir),,$(error ERROR: Module 'DEG.mk' cannot run appart from SK8 framework))


# Verify arguments.
ifeq ($(args),)
      $(error ERROR: Variable 'args' cannot be empty)
endif


# Default goal: 'help'.
deg_help:
	@$(ECHO) "Usage: sdfadsfa"

deg_version:
	@$(ECHO) "$(deg_self) v$(deg_version) (2020)"

deg_test:
	@$(ECHO) "Test for SK8 module $(deg_self), arguments: $(args)."



############################## MAIN ############################################

# Global scope variables.
tumor_type                     := $(firstword $(args))
manifest_type                  := WTS


deg_source                     := $(if $(inputfl),$(inputfl),$(tumor_type).json)
deg_radical                    := $(basename $(notdir $(deg_source)))
deg_payload                    := $(deg_radical).payl
deg_precursor                  := $(deg_radical).prec
deg_manifest                   := $(deg_radical).manifest
deg_assoc                      := $(deg_radical).assoc
deg_tsv                        := $(deg_radical).tsv


payload_gen                    := $(sk8_rootdir)/scripts/payload_gen.sh
precursor_gen                  := $(sk8_rootdir)/scripts/precursor_gen.sh
manifest_gen                   := $(sk8_rootdir)/scripts/manifest_gen.sh

gdc_token                      := /home/venus/mar/pedrogalante/data/gdc/gdc_token.txt



# DEG procedural targets.
deg_all: deg_analyze


deg_analyze: $(deg_analyze)

$(deg_analyze): $(deg_count)


deg_count: $(deg_count)

$(deg_count): $(deg_dealign)

deg_dealign: $(deg_dealign)

$(deg_dealign): $(deg_decompress)


deg_decompress: $(deg_decompress)

$(deg_decompress): $(deg_downloads)


deg_lists: $(deg_manifest)
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) depth=1 deg_lst

deg_lst: $(deg_lst)
	@$(ECHO) "\nAll input lists complete: $^"

$(deg_lst): %.lst: %.dwl $(gdc_token)
	@$(ECHO) "\nDownloading BAMs from download file '$<' ..."
	tk=$$(< $(gdc_token)); \
	cd $(*D); \
	parallel --tmpdir $(gbgdir) -q -j $(jobs) curl --remote-name --remote-header-name --header "X-Auth-Token: $${tk}" "https://api.gdc.cancer.gov/data/{}" :::: $<
	find $(*D) -mindepth 1 -maxdepth 1 -type f -name '*bam' > $@
	@$(ECHO) "Done."

.SECONDEXPANSION:
$(deg_bam): %.bam: %.dwl $(gdc_token)
	@$(ECHO) "\nDownloading BAMs from manifest file '$<' ..."
	#$(gdc_client) download -t $(gdc_token) -d $(*D) -m $<
	@$(ECHO) "Done."


deg_downloads: $(deg_manifest)
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) depth=1 deg_dwl

deg_dwl: $(deg_dwl)
	@$(ECHO) "\nAll downloads complete: $^"

$(deg_dwl): %.dwl: $(deg_manifest)
	@$(ECHO) "\nCreating download file '$@' from manifest file '$<' ..."
	instance=$$(sed -r -n -e 's|.*/([0-9]{4})_$(deg_radical).dwl|\1|p' <<< "$@"); \
	inst=$$(perl -ne '$$k = $$_; $$k = substr($$_,1) if ($$_ < 1000 and $$_ >= 100); $$k = substr($$_,2) if ($$_ < 100 and $$_ >= 10); $$k = substr($$_,3) if $$_ < 10; print $$k;' <<< "$${instance}"); \
	mkdir -p $(*D); \
	if [ "$${inst}" -eq "$(sfx_end)" ]; then \
		if ((($(num_lines)-1) % $(step))); then \
			tail -n $$((($(num_lines)-1) % $(step))) $< | cut -f3 >> $@; \
		else \
			tail $< | cut -f3 >> $@; \
		fi \
	else \
		head -n $$(($${inst}*$(step) + 1)) $< | tail | cut -f3 >> $@; \
	fi
	@$(ECHO) "Done."


deg_manifest: $(deg_manifest)

$(deg_manifest): $(deg_precursor)
	@$(ECHO) "\nCreating manifest file: '$@'."
	$(manifest_gen) $< > $@ 2> /dev/null
	@$(ECHO) "Done."


deg_precursor: $(deg_precursor)

$(deg_precursor): $(deg_payload)
	@$(ECHO) "\nCreating precursor manifest file: '$@'."
	curl --request POST --header "Content-Type: application/json" --data @$< 'https://api.gdc.cancer.gov/files' > $@
	@$(ECHO) "Done."


deg_payload: $(deg_payload)

$(deg_payload): $(deg_source)
	@$(ECHO) "\nCreating payload file for manifest generation of '$(tumor_type)' tumor type."
	$(payload_gen) $(tumor_type) $(manifest_type) > $@ 2> /dev/null
	@$(ECHO) "Done."



$(deg_source): $(deg_requisites)
	touch $@
