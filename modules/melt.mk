# Non-standalone run verification.
this                           := melt.mk
ifeq ($(notdir $(firstword $(MAKEFILE_LIST))),$(this))
      $(error ERROR: Module '$(this)' cannot run in standalone mode, \
      it needs to be loaded by 'SK8')
endif


# Info to download, configure, build, install and use 'melt' package.
melt_src                    := MELT-2.2.2.tar.gz
melt_ext                    := .tar.gz

melt_package                := $(shell perl -ne 'print "@{[ (split(/-/, $$_))[0] ]}"' <<< "$(notdir $(melt_src))")
melt_version                := $(shell perl -pe 's/^$(melt_package)-(.*?)\$(melt_ext)/$$1/g' <<< "$(notdir $(melt_src))")
melt_id                     := $(melt_package)-xxxxxx
melt_distname               := $(melt_package)-$(melt_version)
melt_license                := GPLv4

# Potential installation data.
melt_repository             := $(melt_src)
melt_tarball                := $(tarballsdir)/$(melt_distname)$(melt_ext)
melt_builddir               := $(tarballsdir)/$(melt_distname)
melt_configurefile          := $(melt_builddir)/configure
melt_buildfile              := $(melt_builddir)/Makefile
melt_execfile               := $(melt_builddir)/$(melt_package).jar
melt_installfile            := $(installdir)/$(melt_package).jar


# Actual installation data.
melt_has                    := $(shell which $(melt_package) 2> /dev/null)
melt_get                    := $(if $(melt_has),,melt_install)
melt                        := $(if $(melt_has),$(melt_has),$(melt_installfile))


# FORCE data.
melt_force                  := $(if $(FORCE),melt_force,)
melt_status                 := $(if $(FORCE),melt_install,$(melt_get))
melt                        := $(if $(FORCE),$(melt_installfile),$(melt))


# Include required modules.
include $(addprefix $(sk8_rootdir)/modules/, java.mk bowtie2.mk)
melt_deps                   := $(if $(recursive),melt_deps,)
melt_depscheck              := $(gcc_status) $(java_status) $(bowtie2_status) $(melt_deps)


# Verification.
melt_status: $(melt_status) # 'melt_install' or none.
	@$(ECHO) "\nSystem has '$(melt_package)' up and running." >&2


# Install.
melt_install: $(melt_installfile) # $(installdir)/$(melt_package)
	@$(ECHO) "\nSystem has '$(melt_package)' in PATH: '$<'." >&2

$(melt_installfile): $(melt_execfile) # just copy the executable file to $(installdir).
	@$(ECHO) "\nInstalling '$(melt_package)' on directory '$(installdir)' ..." >&2
	#$(MAKE) -C $(melt_builddir) install DESTDIR=$(DESTDIR) prefix=$(dir $(installdir)) -j4
	install -d $(installdir)
	install -t $(installdir) -m 644 $<
	mkdir -p $(datadir)/melt
	cp -r $(melt_builddir)/add_bed_files $(datadir)/melt
	cp -r $(melt_builddir)/prior_files $(datadir)/melt
	cp -r $(melt_builddir)/me_refs $(datadir)/melt
	if [ -z "$$(perl -ne 'print if s#.*$(installdir).*#AA#g' <<< $${PATH})" ]; then \
		echo -e "export PATH=$(installdir):\$$PATH" >> ~/.bashrc; \
		source ~/.bashrc; \
		echo -e "WARNING: You may need to run 'source ~/.bashrc' to release the new \$$PATH."; \
	elif [ "$(melt)" != "$(melt_installfile)" ]; then \
		echo "WARNING: Previuos installation of '$(melt_package)' may be shadowing the new one in the \$$PATH."; \
	fi
	echo -e "alias melt=\"java -Xmx1G -jar $@\"" >> ~/.bashrc
	touch $@
	@$(ECHO) "Done." >&2


# Build.
melt_build: $(melt_execfile) # $(melt_builddir)/$(melt_package)

$(melt_execfile): $(melt_buildfile) | $(gcc_status) # a Makefile generates the executable file.
	@$(ECHO) "\nBuilding '$(melt_package)' on directory '$(melt_builddir)' ..." >&2
	#$(MAKE) -C $(melt_builddir) -j4
	touch $@
	@$(ECHO) "Done." >&2


# Configure.
melt_configure: $(melt_buildfile) # $(melt_builddir)/Makefile

$(melt_buildfile): $(melt_configurefile)
	@$(ECHO) "\nConfiguring '$(melt_package)' on directory '$(melt_builddir)' ..." >&2
	#cd $(melt_builddir); \
	#./configure --prefix=$(dir $(installdir)) $(melt_args)
	touch $@
	@$(ECHO) "Done." >&2

$(melt_configurefile): $(melt_tarball)
	@$(ECHO) "\nExtracting '$(melt_package)' tarball to directory '$(melt_builddir)' ..." >&2
	mkdir -p $(melt_builddir)
	if [ "$(melt_ext)" == ".tar.gz" ]; then \
		tar -xzvf $< -C $(melt_builddir) --strip-components=1; \
	else \
		tar -xvf $< -C $(melt_builddir) --strip-components=1; \
	fi
	touch $@
	@$(ECHO) "Done." >&2


# Download.
melt_tarball: $(melt_tarball) # $(tarballsdir)/$(melt_tarball).

$(melt_tarball): $(melt_force) | $(tarballsdir) $(internet_status) $(melt_depscheck)
	@$(ECHO) "\nDownloading '$(melt_package)' tarball from '$(melt_repository)' ..." >&2
	#wget -c $(melt_repository) -P $(tarballsdir) -O $@
	cp $(args) $@
	[ -s "$@" ] || ( echo "ERROR: Tarball is missing. Provide it with 'args' variable." >&2; exit 7 )
	touch $@
	@$(ECHO) "Done." >&2

melt_deps:
	@$(ECHO) "\nRecursively installing dependecies for '$(melt_distname)' ..." >&2
	[ -n "$(args)" ] || ( echo "ERROR: Variable 'args' can not be empty." >&2; exit 1 )
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) recursive= $(args)
	@$(ECHO) "Done." >&2

melt_force:
	@$(ECHO) "\nForce reinstallation of '$(melt_package)' on directory '$(installdir)'." >&2
	rm -f $(melt_tarball)
	@$(ECHO) "Done." >&2


melt_refresh: melt_uninstall
	if [ -z "$(usermode)" ]; then \
		$(MAKE) -f $(firstword $(MAKEFILE_LIST)) DESTDIR=$(DESTDIR) prefix=$(prefix) melt_install; \
	else \
		$(MAKE) -f $(firstword $(MAKEFILE_LIST)) DESTDIR=$(DESTDIR) prefix=$(prefix) melt_install usermode=1; \
	fi


# Uninstall and clean.
melt_purge: melt_uninstall melt_clean

melt_uninstall:
	@$(ECHO) "\nUninstalling '$(melt_package)' from directory '$(installdir)' ..." >&2
	[ -d "$(installdir)" ] || exit 1;
	if [ -f "$(melt_buildfile)_no" ]; then \
		$(MAKE) -C $(melt_builddir) uninstall DESTDIR=$(DESTDIR) prefix=$(dir $(installdir)) -j4; \
	else \
		rm -rf $(melt_installfile)*; \
	fi
	@$(ECHO) "User must resolve \$$PATH in '~/.bashrc' file." >&2
	@$(ECHO) "Done." >&2

melt_clean:
	@$(ECHO) "\nCleaning '$(melt_package)' from staged directory '$(stageddir)' ..." >&2
	rm -rf $(tarballsdir)/$(melt_package)*
	@$(ECHO) "Done." >&2


# Tests.
melt_test:
	@$(ECHO) "\nTesting '$(melt_package)' ..." >&2
	if [ -z "$$($(melt_package) --version 2> /dev/null)" ]; then \
		echo "Package '$(melt_package)' is not properly running." >&2; \
		exit 1; \
	else \
		echo "PASS!" \
	fi
	@$(ECHO) "Done." >&2

melt_test_build: args := make -f SK8/SK8.mk module=SK8/modules/melt.mk usermode=1 \
	melt_src=$(melt_src) melt_ext=$(melt_ext) melt_package=$(melt_package) melt_version=$(melt_version) recursive=$(recursive) \
	melt_install melt_test
melt_test_build: docker_test_build
	@$(ECHO) "\nFinished Docker build test for '$(melt_package)' module." >&2
