# Non-standalone run verification.
this                           := glib.mk
ifeq ($(notdir $(firstword $(MAKEFILE_LIST))),$(this))
      $(error ERROR: Module '$(this)' cannot run in standalone mode, \
      it needs to be loaded by 'SK8')
endif

# Info to download, configure, build, install and use 'glib' package.
glib_src                    := https://download.gnome.org/sources/glib/2.66/glib-2.66.7.tar.xz
glib_ext                    := .tar.xz

glib_package                := $(shell perl -ne 'print "@{[ (split(/-/, $$_))[0] ]}"' <<< "$(notdir $(glib_src))")
glib_version                := $(shell perl -pe 's/^$(glib_package)-(.*?)\$(glib_ext)/$$1/g' <<< "$(notdir $(glib_src))")
glib_id                     := $(glib_package)-xxxxxx
glib_distname               := $(glib_package)-$(glib_version)
glib_license                := GPLv4

# Potential installation data.
glib_repository             := $(glib_src)
glib_tarball                := $(tarballsdir)/$(glib_distname)$(glib_ext)
glib_builddir               := $(tarballsdir)/$(glib_distname)
glib_configurefile          := $(glib_builddir)/configure
glib_buildfile              := $(glib_builddir)/Makefile
glib_execfile               := $(glib_builddir)/$(glib_package)
glib_installfile            := $(installdir)/$(glib_package)


# Actual installation data.
glib_has                    := $(shell which $(glib_package) 2> /dev/null)
glib_get                    := $(if $(glib_has),,glib_install)
glib                        := $(if $(glib_has),$(glib_has),$(glib_installfile))


# FORCE data.
glib_force                  := $(if $(FORCE),glib_force,)
glib_status                 := $(if $(FORCE),glib_install,$(glib_get))
glib                        := $(if $(FORCE),$(glib_installfile),$(glib))


# Include required modules.
include $(addprefix $(sk8_rootdir)/modules/, meson.mk ninja.mk)
glib_deps                   := $(if $(recursive),glib_deps,)
glib_depscheck              := $(gcc_status) $(meson_status) $(ninja_status) glib_deps


# Usage.
glib_help:
	@$(ECHO) "\n\
	NAME\n\
		\n\
		glib_package - $(glib_package), version $(glib_version)\n\
		\n\
	SYNOPSIS\n\
		\n\
		$$ sk8 [make_option ...] [variable=value ...] <glib_target ...>\n\
		\n\
	DESCRIPTION\n\
		\n\
		This is an engine to download, configure, build, install and test packages\n\
		in a glib way. So, the package must be compliant with the GNU Coding\n\
		Standards in order to be handled by SK8.\n\
		Now, the package in question is '$(glib_distname)'.\n\
		\n\
	OPTIONS\n\
		\n\
		Targets:\n\
		\n\
		glib_help                   Print this help text.\n\
		glib_version                Print '$(glib_package)' package info.\n\
		glib_status                 Verifies '$(glib_package)' current installation status.\n\
		glib_run                    Run '$(glib_package)' with the arguments given in 'args' variable.\n\
		glib_install                Install '$(glib_package)' binary in '$$(prefix)/bin' directory.\n\
		glib_build                  Compile the '$(glib_package)' binary in the build directory.\n\
		glib_configure              Configure '$(glib_package)' build files for this system.\n\
		glib_tarball                Download the '$(glib_package)' tarball from its repository: $(glib_src)\n\
		glib_uninstall              Uninstall '$(glib_package)' from the '$$(prefix)/bin' directory.\n\
		glib_clear                  Remove '$(glib_package)' tarball from tarball''s directory.\n\
		glib_purge                  Uninstall and clear '$(glib_package)' from the system.\n\
		glib_refresh                Uninstall and then re-install '$(glib_package)' again.\n\
		glib_deps                   Install a '$(glib_package)' dependency recursivelly as passed in 'args' variable.\n\
		glib_test                   Test '$(glib_package)' installation.\n\
		glib_test_build             Test '$(glib_package)' build process, natively.\n\
		glib_test_build_conda       Test '$(glib_package)' build process in a new Conda environment.\n\
		glib_test_build_docker      Test '$(glib_package)' build process inside a vanilla Docker container.\n\
		glib_test_build_aws         Test '$(glib_package)' build process inside an AWS instance. NOT WORKING!!\n\
		glib_create_img             Create a Docker image for '$(glib_package)', based on '$(image)' image.\n\
		glib_create_dockerfile      Create a Dockerfile to build an image for '$(glib_package)'.\n\
		glib_create_module          Create a pre-filled SK8 module for '$(glib_package)' package.\n\
		\n\
		conda_run                      Run '$(glib_package)' in a new Conda environment. See 'conda_help' for more info.\n\
		docker_run                     Run '$(glib_package)' in a vanilla Docker image. See 'docker_help' for more info.\n\
		aws_run                        Run '$(glib_package)' in an AWS instance. See 'aws_help' for more info. NOT WORKING!!\n\
		\n\
		Variables:\n\
		\n\
		args                           String of arguments passed to '$(glib_package)' when running.               [ '' ]\n\
		prefix                         Path to the directory where to put the built '$(glib_packages)' binaries.   [ /usr/local ]\n\
		DESTDIR                        Path to an alternative staged installation directory.                          [ '' ]\n\
		workdir                        The current directory.                                                         [ . ]\n\
		stageddir                      Path to a staged directory for tarballs and package builds.                    [ ./.local ]\n\
		usermode                       Boolean to switch between local or system-wide installations paths.            [ '' ]\n\
		recursive                      Boolean to enable recursive dependency installations.                          [ '' ]\n\
		FORCE                          Boolean to force a fresh installation over an existent one.                    [ '' ]\n\
		\n\
		src                            URL of the '$(glib_package)' repository.                                    [ '$(glib_src)' ]\n\
		ext                            The tarball extension. Only for extensions other than '.tar.gz'.               [ '.tar.gz' ]\n\
		glib_package                The name of the package, derived from its tarball''s name.                     [ '$(glib_package)' ]\n\
		glib_version                The version of the package, derived from its tarball''s name.                  [ '$(glib_version)' ]\n\
		\n\
		Make options:\n\
		\n\
		\n\
		NOTE: Some packages cannot build glibally. So, user must test the chosen target first.\n\
		For a list of known glibally installable packages, pleas read the\n\
		'$(sk8_rootdir)/modules/modules_urls.txt' file.\n\
		\n\
		\n\
	COPYRIGHT\n\
		\n\
		$(sk8_package), version $(sk8_version) for GNU/Linux.\n\
		Copyright (C) Bards Agrotechnologies, 2011-2021.\n\
		\n\
	SEE ALSO\n\
		\n\
		Search for 'help', 'docker_help', 'conda_help' and 'miscellaneus'\n\
		for other sources of command line help or visit <$(basename $(sk8_url))>\n\
		for full documentation."

glib_version:
	@$(ECHO) "\n\
	Package:    $(glib_package)\n\
	Version:    $(glib_version)\n\
	Id:         $(glib_id)\n\
	License:    $(glib_license)\n\
	\n\
	Status:     $(glib_status)\n\
	Source:     $(glib_src)\n\
	Installdir: $(installdir)\n\
	\n\
	Runned with $(sk8_package), version $(sk8_version) for GNU/Linux.\n\
	Copyright (C) Bards Agrotechnologies, 2011-2021."

glib_run: $(glib_status)
	@$(ECHO) "\nRunning '$(glib)' with arguments '$(args)' ..." >&2
	$(glib) $(args)
	@$(ECHO) "Done." >&2


# Verification.
glib_status: $(glib_status) # 'glib_install' or none.
	@$(ECHO) "\nSystem has '$(glib_package)' up and running." >&2


# Install.
glib_install: $(glib_installfile) # $(installdir)/$(glib_package)
	@$(ECHO) "\nSystem has '$(glib_package)' in PATH: '$<'." >&2

$(glib_installfile): $(glib_execfile) | $(installdir) # just copy the executable file to $(installdir).
	@$(ECHO) "\nInstalling '$(glib_package)' on directory '$(installdir)' ..." >&2
	$(MAKE) -C $(glib_builddir) install DESTDIR=$(DESTDIR) prefix=$(dir $(installdir)) -j4
	if [ -z "$$(perl -ne 'print if s#.*$(installdir).*#AA#g' <<< $${PATH})" ]; then \
		echo -e "export PATH=$(installdir):\$$PATH" >> ~/.bashrc; \
		source ~/.bashrc; \
		echo -e "WARNING: You may need to run 'source ~/.bashrc' to release the new \$$PATH."; \
	elif [ "$(glib)" != "$(glib_installfile)" ]; then \
		echo "WARNING: Previuos installation of '$(glib_package)' may be shadowing the new one in the \$$PATH."; \
	fi
	touch $@
	@$(ECHO) "Done." >&2


# Build.
glib_build: $(glib_execfile) # $(glib_builddir)/$(glib_package)

$(glib_execfile): $(glib_buildfile) | $(gcc_status) # a Makefile generates the executable file.
	@$(ECHO) "\nBuilding '$(glib_package)' on directory '$(glib_builddir)' ..." >&2
	#$(MAKE) -C $(glib_builddir) -j4
	ninja -C $(glib_builddir)/_build
	touch $@
	@$(ECHO) "Done." >&2


# Configure.
glib_configure: $(glib_buildfile) # $(glib_builddir)/Makefile

$(glib_buildfile): $(glib_configurefile)
	@$(ECHO) "\nConfiguring '$(glib_package)' on directory '$(glib_builddir)' ..." >&2
	cd $(glib_builddir); \
	meson _build $(glib_args)
	touch $@
	@$(ECHO) "Done." >&2

$(glib_configurefile): $(glib_tarball) | $(glib_builddir)
	@$(ECHO) "\nExtracting '$(glib_package)' tarball to directory '$(glib_builddir)' ..." >&2
	if [ "$(glib_ext)" == ".tar.gz" ]; then \
		tar -xzvf $< -C $(glib_builddir) --strip-components=1; \
	elif [ "$(glib_ext)" == ".tar.xz" ]; then \
		tar -xJvf $< -C $(glib_builddir) --strip-components=1; \
	else \
		tar -xvf $< -C $(glib_builddir) --strip-components=1; \
	fi
	touch $@
	@$(ECHO) "Done." >&2

$(glib_builddir):
	@$(ECHO) "\nCreating directory '$(glib_builddir)' ..." >&2
	mkdir -p $(glib_builddir)
	@$(ECHO) "Done." >&2


# Download.
glib_tarball: $(glib_tarball) # $(tarballsdir)/$(glib_tarball).

$(glib_tarball): $(glib_force) | $(tarballsdir) $(internet_status) $(glib_depscheck)
	@$(ECHO) "\nDownloading '$(glib_package)' tarball from '$(glib_repository)' ..." >&2
	wget -c $(glib_repository) -P $(tarballsdir) -O $@
	touch $@
	@$(ECHO) "Done." >&2

glib_deps: args := usermode=$(usermode) \
	generic_src=https://downloads.sourceforge.net/project/lzmautils/xz-5.2.5.tar.gz \
	generic_ext=.tar.gz \
	generic_package=xz \
	generic_version=5.2.5 \
	generic_intall
glib_deps:
	@$(ECHO) "\nRecursively installing dependecies for '$(glib_distname)' ..." >&2
	[ -n "$(args)" ] || ( echo "ERROR: Variable 'args' can not be empty." >&2; exit 1 )
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) recursive= $(args)
	@$(ECHO) "Done." >&2

glib_force:
	@$(ECHO) "\nForce reinstallation of '$(glib_package)' on directory '$(installdir)'." >&2
	rm -f $(glib_tarball)
	@$(ECHO) "Done." >&2


glib_refresh: glib_uninstall
	if [ -z "$(usermode)" ]; then \
		$(MAKE) -f $(firstword $(MAKEFILE_LIST)) DESTDIR=$(DESTDIR) prefix=$(prefix) glib_install; \
	else \
		$(MAKE) -f $(firstword $(MAKEFILE_LIST)) DESTDIR=$(DESTDIR) prefix=$(prefix) glib_install usermode=1; \
	fi


# Uninstall and clean.
glib_purge: glib_uninstall glib_clean

glib_uninstall:
	@$(ECHO) "\nUninstalling '$(glib_package)' from directory '$(installdir)' ..." >&2
	[ -d "$(installdir)" ] || exit 1;
	if [ -f "$(glib_buildfile)" ]; then \
		$(MAKE) -C $(glib_builddir) uninstall DESTDIR=$(DESTDIR) prefix=$(dir $(installdir)) -j4; \
	else \
		rm -rf $(glib_installfile)*; \
	fi
	@$(ECHO) "User must resolve \$$PATH in '~/.bashrc' file." >&2
	@$(ECHO) "Done." >&2

glib_clean:
	@$(ECHO) "\nCleaning '$(glib_package)' from staged directory '$(stageddir)' ..." >&2
	rm -rf $(tarballsdir)/$(glib_package)*
	@$(ECHO) "Done." >&2


# Tests.
glib_test:
	@$(ECHO) "\nTesting '$(glib_package)' ..." >&2
	if [ -n "$(is_lib)" ]; then \
		if [ ! -s "$$(find $(dir $(installdir))/lib -type f -name '$(glib_package)*')" ]; then \
			echo "Library '$(glib_package)' is not properly installed."; \
			exit 7; \
		fi \
	else \
		if [ -z "$$($(glib_package) --version 2> /dev/null)" ]; then \
			echo "Package '$(glib_package)' is not properly running."; \
			exit 7; \
		fi \
	fi
	@$(ECHO) "Package '$(glib_package)' test status: PASS!"
	@$(ECHO) "Done." >&2


glib_test_build_native: _install := $(basedir)/_install_$(glib_distname)
glib_test_build_native:
	@$(ECHO) "\nTesting native build of '$(glib_package)' module." >&2
	[ -n "$(_install)" ] || ( echo "ERROR: Variable '_install' is empty."; exit 7 )
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) \
		stageddir=$(_install) \
		usermode=1 \
		glib_src=$(glib_src) \
		glib_ext=$(glib_ext) \
		glib_package=$(glib_package) \
		glib_version=$(glib_version) \
		glib_args=$(glib_args) \
		recursive=$(recursive) args="$(args)" \
		glib_install glib_test
	[ -n "$(keep_install)" ] || rm -rf $(_install)
	@$(ECHO) "Done." >&2
	@$(ECHO) "\nFinished native build test for '$(glib_package)' module." >&2

glib_test_build_conda: conda_test_build
	@$(ECHO) "\nFinished Conda build test for '$(glib_package)' module." >&2

glib_test_build_docker: override args := make -f SK8/SK8.mk \
	module=$(addprefix SK8/modules/, $(notdir $(module))) \
	usermode=1 \
	glib_src=$(glib_src) \
	glib_ext=$(glib_ext) \
	glib_package=$(glib_package) \
	glib_version=$(glib_version) \
	glib_args=$(glib_args) \
	recursive=$(recursive) args="$(args)" \
	glib_install glib_test
glib_test_build_docker: docker_test_build
	@$(ECHO) "\nFinished Docker build test for '$(glib_package)' module." >&2

