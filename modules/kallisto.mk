############################## HEADER ##########################################
# SK8 module for kallisto package.
################################################################################



############################## PREAMBLE ########################################
# Non-standalone run verification.
this                           := kallisto.mk
ifeq ($(notdir $(firstword $(MAKEFILE_LIST))),$(this))
      $(error ERROR: Module '$(this)' cannot run in standalone mode, \
      it needs to be loaded by 'SK8')
endif

# Info to download, configure, build, install and use 'kallisto' package.
kallisto_src                    ?= https://github.com/pachterlab/kallisto/releases/download/v0.46.1/kallisto_linux-v0.46.1.tar.gz
kallisto_ext                    ?= .tar.gz

kallisto_package                ?= kallisto
kallisto_version                ?= 0.46.1
kallisto_id                     ?= $(kallisto_package)-$(shell echo $$RANDOM)
kallisto_license                ?= GPLv4
kallisto_distname               ?= $(kallisto_package)-$(kallisto_version)

# Potential installation data.
kallisto_repository             ?= $(kallisto_src)
kallisto_tarball                ?= $(tarballsdir)/$(kallisto_distname)$(kallisto_ext)
kallisto_builddir               ?= $(tarballsdir)/$(kallisto_distname)
kallisto_configurefile          ?= $(kallisto_builddir)/configure
kallisto_buildfile              ?= $(kallisto_builddir)/Makefile
kallisto_execfile               ?= $(kallisto_builddir)/$(kallisto_package)
kallisto_installfile            ?= $(installdir)/$(kallisto_package)


# Actual installation data.
kallisto_check                  ?= $(shell which $(kallisto_package) 2> /dev/null)
kallisto_get                    ?= $(if $(kallisto_check),,kallisto_install)
kallisto                        ?= $(or $(kallisto_check), $(kallisto_installfile))


# FORCE data.
kallisto_force                  ?= $(if $(FORCE),kallisto_force,)
kallisto_status                 ?= $(if $(FORCE),kallisto_install,$(kallisto_get))
kallisto                        ?= $(if $(FORCE),$(kallisto_installfile),$(kallisto))


# Include required modules.
#include $(addprefix $(sk8_rootdir)/modules/, gcc.mk)
kallisto_deps_check             ?= $(if $(recursive),kallisto_deps,)
kallisto_deps_status            ?= $(gcc_status) $(kallisto_deps_check)



############################## MAIN ############################################
# Usage.
kallisto_help:
	@$(ECHO) "\n\
	NAME\n\
		\n\
		kallisto_package - $(kallisto_package), version $(kallisto_version)\n\
		\n\
	SYNOPSIS\n\
		\n\
		$$ sk8 [make_option ...] [variable=value ...] <kallisto_target ...>\n\
		\n\
	DESCRIPTION\n\
		\n\
		This is an engine to download, configure, build, install and test packages\n\
		in a kallisto way. So, the package must be compliant with the GNU Coding\n\
		Standards in order to be handled by SK8.\n\
		Now, the package in question is '$(kallisto_distname)'.\n\
		\n\
	OPTIONS\n\
		\n\
		Targets:\n\
		\n\
		kallisto_help                   Print this help text.\n\
		kallisto_version                Print '$(kallisto_package)' package version.\n\
		kallisto_status                 Verifies '$(kallisto_package)' current installation status.\n\
		kallisto_run                    Run '$(kallisto_package)' with the arguments given in the 'args' variable.\n\
		kallisto_install                Install '$(kallisto_package)' binary in '\$$(prefix)/bin' directory.\n\
		kallisto_build                  Compile the '$(kallisto_package)' binary in the build directory.\n\
		kallisto_configure              Configure '$(kallisto_package)' build files for this system.\n\
		kallisto_tarball                Download the '$(kallisto_package)' tarball from its repository: $(kallisto_src)\n\
		kallisto_uninstall              Uninstall '$(kallisto_package)' from the '\$$(prefix)/bin' directory.\n\
		kallisto_clear                  Remove '$(kallisto_package)' tarball from tarball''s directory.\n\
		kallisto_purge                  Uninstall and clear '$(kallisto_package)' from the system.\n\
		kallisto_refresh                Uninstall and then re-install '$(kallisto_package)' again.\n\
		kallisto_deps                   Install a '$(kallisto_package)' dependency recursivelly as passed by the 'args' variable.\n\
		kallisto_test                   Test '$(kallisto_package)' installation.\n\
		kallisto_test_build_native      Test '$(kallisto_package)' build process, natively.\n\
		kallisto_test_build_conda       Test '$(kallisto_package)' build process in a new Conda environment.\n\
		kallisto_test_build_docker      Test '$(kallisto_package)' build process inside a vanilla Docker container.\n\
		kallisto_test_build_aws         Test '$(kallisto_package)' build process inside an AWS instance. NOT WORKING!!\n\
		kallisto_create_img             Create a Docker image for '$(kallisto_package)', based on '$(image)' image.\n\
		kallisto_create_dockerfile      Create a Dockerfile to build an image for '$(kallisto_package)'.\n\
		kallisto_create_module          Create a pre-filled SK8 module for '$(kallisto_package)' package.\n\
		\n\
		conda_run                      Run '$(kallisto_package)' in a new Conda environment. See 'conda_help' for more info.\n\
		docker_run                     Run '$(kallisto_package)' in a vanilla Docker image. See 'docker_help' for more info.\n\
		aws_run                        Run '$(kallisto_package)' in an AWS instance. See 'aws_help' for more info. NOT WORKING!!\n\
		\n\
		Variables:\n\
		\n\
		args                           String of arguments passed to '$(kallisto_package)' when running.               [ '' ]\n\
		prefix                         Path to the directory where to put the built '$(kallisto_packages)' binaries.   [ /usr/local ]\n\
		DESTDIR                        Path to an alternative staged installation directory.                          [ '' ]\n\
		workdir                        The current directory.                                                         [ . ]\n\
		stageddir                      Path to a staged directory for tarballs and package builds.                    [ ./.local ]\n\
		usermode                       Boolean to switch between local or system-wide installations paths.            [ '' ]\n\
		recursive                      Boolean to enable recursive dependency installations.                          [ '' ]\n\
		FORCE                          Boolean to force a fresh installation over an existent one.                    [ '' ]\n\
		\n\
		src                            URL of the '$(kallisto_package)' repository.                                    [ '$(kallisto_src)' ]\n\
		ext                            The tarball extension. Only for extensions other than '.tar.gz'.               [ '.tar.gz' ]\n\
		kallisto_package                The name of the package, derived from its tarball''s name.                     [ '$(kallisto_package)' ]\n\
		kallisto_version                The version of the package, derived from its tarball''s name.                  [ '$(kallisto_version)' ]\n\
		\n\
		Make options:\n\
		\n\
		\n\
		NOTE: Some packages cannot build kallistoally. So, user must test the chosen target first.\n\
		For a list of known kallistoally installable packages, please read the\n\
		'$(sk8_rootdir)/modules/modules_urls.txt' file.\n\
		\n\
		\n\
	COPYRIGHT\n\
		\n\
		$(sk8_package), version $(sk8_version) for GNU/Linux.\n\
		Copyright (C) Bards Agrotechnologies, 2011-2021.\n\
		\n\
	SEE ALSO\n\
		\n\
		Search for 'help', 'docker_help', 'conda_help' and 'miscellaneus'\n\
		for other sources of command line help or visit <$(basename $(sk8_source))>\n\
		for full documentation."

kallisto_manual:
	@$(ECHO) "\n$(kallisto_package) v$(kallisto_version) for GNU/Linux.\n\
	Please, see <$(kallisto_src)> for full documentation." >&2

kallisto_version:
	@$(ECHO) "\n\
	Package:    $(kallisto_package)\n\
	Version:    $(kallisto_version)\n\
	Id:         $(kallisto_id)\n\
	License:    $(kallisto_license)\n\
	\n\
	Status:     $(kallisto_status)\n\
	Source:     $(kallisto_src)\n\
	Installdir: $(installdir)\n\
	\n\
	Runned with $(sk8_package), version $(sk8_version) for GNU/Linux.\n\
	Copyright (C) Bards Agrotechnologies, 2011-2021."


kallisto_run: $(kallisto_status)
	@$(ECHO) "\nRunning '$(kallisto)' with arguments '$(args)' ..." >&2
	$(kallisto) $(args)
	@$(ECHO) "Done." >&2


# Verification.
kallisto_status: $(kallisto_status) # 'kallisto_install' or none.
	@$(ECHO) "\nSystem has '$(kallisto_package)' up and running." >&2


# Install.
kallisto_install: $(kallisto_installfile) # $(installdir)/$(kallisto_package)
	@$(ECHO) "\nSystem has '$(kallisto_package)' in PATH: '$<'." >&2

$(kallisto_installfile): $(kallisto_execfile) | $(installdir) # just copy the executable file to $(installdir).
	@$(ECHO) "\nInstalling '$(kallisto_package)' on directory '$(installdir)' ..." >&2
	#$(MAKE) -C $(kallisto_builddir) install DESTDIR=$(DESTDIR) prefix=$(dir $(installdir)) -j4
	install -d $(installdir)
	install -t $(installdir) -m 755 $<
	if [ -z "$$(perl -ne 'print if s#.*$(installdir).*#AA#g' <<< $${PATH})" ]; then \
		echo -e "export PATH=$(installdir):\$$PATH" >> ~/.bashrc; \
		source ~/.bashrc; \
		echo -e "WARNING: You may need to run 'source ~/.bashrc' to release the new \$$PATH."; \
	elif [ "$(kallisto)" != "$(kallisto_installfile)" ]; then \
		echo "WARNING: Previuos installation of '$(kallisto_package)' may be shadowing the new one in the \$$PATH."; \
	fi
	touch $@
	@$(ECHO) "Done." >&2


# Build.
kallisto_build: $(kallisto_execfile) # $(kallisto_builddir)/$(kallisto_package)

$(kallisto_execfile): $(kallisto_buildfile) # a Makefile generates the executable file.
	@$(ECHO) "\nBuilding '$(kallisto_package)' on directory '$(kallisto_builddir)' ..." >&2
	#$(MAKE) -C $(kallisto_builddir) -j4
	touch $@
	@$(ECHO) "Done." >&2


# Configure.
kallisto_configure: $(kallisto_buildfile) # $(kallisto_builddir)/Makefile

$(kallisto_buildfile): kallisto_configure_cmd := ./configure --prefix=$(dir $(installdir)) $(kallisto_args)
$(kallisto_buildfile): $(kallisto_configurefile) | $(kallisto_builddir)
	@$(ECHO) "\nConfiguring '$(kallisto_package)' on directory '$(kallisto_builddir)' ..." >&2
	#cd $(kallisto_builddir) && $(kallisto_configure_cmd)
	touch $@
	@$(ECHO) "Done." >&2

$(kallisto_builddir):
	$(MKDIR) $<


$(kallisto_configurefile): $(kallisto_tarball) | $(kallisto_builddir)
	@$(ECHO) "\nExtracting '$(kallisto_package)' tarball to directory '$(kallisto_builddir)' ..." >&2
	if [ "$(kallisto_ext)" == ".tar.gz" ]; then \
		tar -xzvf $< -C $(kallisto_builddir) --strip-components=1; \
	elif [ "$(kallisto_ext)" == ".tar.xz" ]; then \
		tar -xJvf $< -C $(kallisto_builddir) --strip-components=1; \
	else \
		tar -xvf $< -C $(kallisto_builddir) --strip-components=1; \
	fi
	touch $@
	@$(ECHO) "Done." >&2

$(kallisto_builddir):
	@$(ECHO) "\nCreating directory '$(kallisto_builddir)' ..." >&2
	mkdir -p $(kallisto_builddir)
	@$(ECHO) "Done." >&2


# Download.
kallisto_tarball: $(kallisto_tarball) # $(tarballsdir)/$(kallisto_tarball).

$(kallisto_tarball): $(kallisto_force) | $(tarballsdir) $(internet_status) $(kallisto_deps_status)
	@$(ECHO) "\nDownloading '$(kallisto_package)' tarball from '$(kallisto_repository)' ..." >&2
	if [ "$(dwlr)" == "wget" ]; then \
		$(MAKE) -f $(firstword $(MAKEFILE_LIST)) $(mode)_run \
			args="$(dwlr) -c $(kallisto_repository) -O $@"; \
	elif [ "$(dwlr)" == "curl" ]; then \
		$(MAKE) -f $(firstword $(MAKEFILE_LIST)) $(mode)_run \
			args="$(dwlr) -L $(kallisto_repository) -o $@"; \
	elif [ "$(dwlr)" == "git" ]; then \
		$(MAKE) -f $(firstword $(MAKEFILE_LIST)) $(mode)_run \
			args="$(dwlr) clone $(kallisto_repository) $(kallisto_builddir)"; \
	else \
		echo "ERROR: Invalid mode '$(mode)'." >&2; \
		exit 7; \
	fi
	touch $@
	@$(ECHO) "Done." >&2

kallisto_deps:
	@$(ECHO) "\nRecursively installing dependecies for '$(kallisto_distname)' ..." >&2
	[ -n "$(args)" ] || ( echo "ERROR: Variable 'args' can not be empty." >&2; exit 7 )
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) recursive= $(args)
	@$(ECHO) "Done." >&2

kallisto_force:
	@$(ECHO) "\nForce reinstallation of '$(kallisto_package)' on directory '$(installdir)'." >&2
	rm -f $(kallisto_tarball)
	@$(ECHO) "Done." >&2


kallisto_refresh: kallisto_uninstall
	if [ -z "$(usermode)" ]; then \
		$(MAKE) -f $(firstword $(MAKEFILE_LIST)) DESTDIR=$(DESTDIR) prefix=$(prefix) kallisto_install; \
	else \
		$(MAKE) -f $(firstword $(MAKEFILE_LIST)) DESTDIR=$(DESTDIR) prefix=$(prefix) kallisto_install usermode=1; \
	fi


# Uninstall and clean.
kallisto_purge: kallisto_uninstall kallisto_clean

kallisto_uninstall:
	@$(ECHO) "\nUninstalling '$(kallisto_package)' from directory '$(installdir)' ..." >&2
	[ -d "$(installdir)" ] || exit 1;
	if [ -f "$(kallisto_buildfile)" ]; then \
		$(MAKE) -C $(kallisto_builddir) uninstall DESTDIR=$(DESTDIR) prefix=$(dir $(installdir)) -j4; \
	else \
		rm -rf $(kallisto_installfile)*; \
	fi
	@$(ECHO) "User must resolve \$$PATH in '~/.bashrc' file." >&2
	@$(ECHO) "Done." >&2

kallisto_clean:
	@$(ECHO) "\nCleaning '$(kallisto_package)' from staged directory '$(stageddir)' ..." >&2
	rm -rf $(tarballsdir)/$(kallisto_package)*
	@$(ECHO) "Done." >&2


# Unit Tests.
kallisto_inputs                := $(abspath $(strip $(wildcard $(kallisto_builddir)/test/*.fastq.gz)))
kallisto_transcripts_ref       := $(abspath $(strip $(wildcard $(kallisto_builddir)/test/*.fasta.gz)))
kallisto_transcripts_ref_url   := ftp://ftp.ensembl.org/pub/release-94/fasta/homo_sapiens/cdna/Homo_sapiens.GRCh38.cdna.all.fa.gz
kallisto_outputsdir            := $(outputsdir)/kallisto_outputs
kallisto_idx                   := $(kallisto_outputsdir)/transcripts.idx
kallisto_result                := $(kallisto_outputsdir)/abundance.tsv


kallisto_test:


kallisto_test_run: $(kallisto_result) | $(kallisto_status)

$(kallisto_result): $(kallisto_idx) $(kallisto_inputs)
	$(ECHO) "\nRunning '$(kallisto_package)' on inputs '' ..." >&2
	[ -n "$(kallisto_inputs)" ] || ( echo "ERROR: Nonexistent input files." >&2; exit 7; )
	$(kallisto) quant \
		-i $< \
		-b 100 \
		-o $(kallisto_outputsdir) \
		$(kallisto_args) \
		$(kallisto_inputs)
	touch $@
	$(ECHO) "Done." >&2

$(kallisto_idx): $(kallisto_trascripts_ref)
	$(ECHO) "\nIndexing trasncriptome '$<' ..." >&2
	$(kallisto) index \
		-i $@ \
		$<
	touch $@
	$(ECHO) "Done." >&2

$(kallisto_trascripts_ref):
	$(ECHO) "\nIndexing trasncriptome '$<' ..." >&2
	curl -O $(kallisto_trascripts_ref_url) -o $@
	touch $@
	$(ECHO) "Done." >&2


kallisto_test_install: args ?= cite
kallisto_test_install:
	$(ECHO) "\nTesting package installation ...\n\
		Package:        $(kallisto_package)\n\
		Version:        $(kallisto_version)\n\
		Install Path:   $(kallisto)\n\
		Arguments:      '$(args)'\n\
		Runtime log:" >&2
	if [ ! -s "$(kallisto_installfile)" ]; then \
		echo "ERROR: Binary file for $(kallisto_package) isn't in $(installdir)."; \
		exit 7; \
	fi
	if $(kallisto) $(args); then                                               \
		echo "Package '$(kallisto_package)' PASSED the test.";                 \
	else                                                                       \
		err=$$?;                                                               \
		echo "ERROR: Package '$(kallisto_package)' wasn't properly installed."; \
		echo -e "Exit code: $$err.";                                           \
		exit 7;                                                                \
	fi >&2
	$(ECHO) "Done." >&2


kallisto_test_build_native: _install := $(basedir)/_install_$(kallisto_distname)
kallisto_test_build_native:
	@$(ECHO) "\nTesting native build of '$(kallisto_package)' module." >&2
	[ -n "$(_install)" ] || ( echo "ERROR: Variable '_install' is empty."; exit 7 )
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) \
		stageddir=$(_install) \
		usermode=1 \
		kallisto_src=$(kallisto_src) \
		kallisto_ext=$(kallisto_ext) \
		kallisto_package=$(kallisto_package) \
		kallisto_version=$(kallisto_version) \
		kallisto_args=$(kallisto_args) \
		recursive=$(recursive) args="$(args)" \
		kallisto_install kallisto_test
	[ -n "$(keep_install)" ] || rm -rf $(_install)
	@$(ECHO) "Done." >&2
	@$(ECHO) "\nFinished native build test for '$(kallisto_package)' module." >&2

kallisto_test_build_conda: conda_test_build
	@$(ECHO) "\nFinished Conda build test for '$(kallisto_package)' module." >&2

kallisto_test_build_docker: override args := make -f SK8/SK8.mk \
	module=$(addprefix SK8/modules/, $(notdir $(module))) \
	usermode=1 \
	kallisto_src=$(kallisto_src) \
	kallisto_ext=$(kallisto_ext) \
	kallisto_package=$(kallisto_package) \
	kallisto_version=$(kallisto_version) \
	kallisto_args=$(kallisto_args) \
	recursive=$(recursive) args="$(args)" \
	kallisto_install kallisto_test
kallisto_test_build_docker: docker_test_build
	@$(ECHO) "\nFinished Docker build test for '$(kallisto_package)' module." >&2


.PHONY: kallisto_help kallisto_manual kallisto_version kallisto_status



############################## DOCUMENTATION ###################################
# Documentation was already written in the targets above.
