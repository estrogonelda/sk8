############################## HEADER ##########################################
# SK8 module for 'SQuIRE' package.
################################################################################



############################## PREAMBLE ########################################
# Non-standalone run verification.
this                           := squire.mk
ifeq ($(notdir $(firstword $(MAKEFILE_LIST))),$(this))
      $(error ERROR: Module '$(this)' cannot run in standalone mode, \
      it needs to be loaded by 'SK8')
endif

# Info to download, configure, build, install and use 'squire' package.
squire_src                    ?= $(src)
squire_ext                    ?= $(ext)

squire_package                ?= $(shell perl -ne 'print "@{[ (split(/-/, $$_))[0] ]}"' <<< "$(notdir $(squire_src))")
squire_version                ?= $(shell perl -pe 's/^$(squire_package)-(.*?)\$(squire_ext)/$$1/g' <<< "$(notdir $(squire_src))")
squire_id                     ?= $(squire_package)-$(shell echo $$RANDOM)
squire_license                ?= GPLv4
squire_distname               ?= $(squire_package)-$(squire_version)

# Potential installation data.
squire_repository             ?= $(squire_src)
squire_tarball                ?= $(tarballsdir)/$(squire_distname)$(squire_ext)
squire_builddir               ?= $(tarballsdir)/$(squire_distname)
squire_configurefile          ?= $(squire_builddir)/configure
squire_buildfile              ?= $(squire_builddir)/Makefile
squire_execfile               ?= $(squire_builddir)/$(squire_package)
squire_installfile            ?= $(installdir)/$(squire_package)


# Actual installation data.
squire_check                  ?= $(shell which $(squire_package) 2> /dev/null)
squire_get                    ?= $(if $(squire_check),,squire_install)
squire                        ?= $(or $(squire_check), $(squire_installfile))


# FORCE data.
squire_force                  ?= $(if $(FORCE),squire_force,)
squire_status                 ?= $(if $(FORCE),squire_install,$(squire_get))
squire                        ?= $(if $(FORCE),$(squire_installfile),$(squire))


# Include required modules.
#include $(addprefix $(sk8_rootdir)/modules/, gcc.mk)
squire_deps_check             ?= $(if $(recursive),squire_deps,)
squire_deps_status            ?= $(gcc_status) $(squire_deps_check)



############################## MAIN ############################################
# Usage.
squire_help:
	@$(ECHO) "\n\
	NAME\n\
		\n\
		squire_package - $(squire_package), version $(squire_version)\n\
		\n\
	SYNOPSIS\n\
		\n\
		$$ sk8 [make_option ...] [variable=value ...] <squire_target ...>\n\
		\n\
	DESCRIPTION\n\
		\n\
		This is an engine to download, configure, build, install and test packages\n\
		in a squire way. So, the package must be compliant with the GNU Coding\n\
		Standards in order to be handled by SK8.\n\
		Now, the package in question is '$(squire_distname)'.\n\
		\n\
	OPTIONS\n\
		\n\
		Targets:\n\
		\n\
		squire_help                   Print this help text.\n\
		squire_version                Print '$(squire_package)' package version.\n\
		squire_status                 Verifies '$(squire_package)' current installation status.\n\
		squire_run                    Run '$(squire_package)' with the arguments given in the 'args' variable.\n\
		squire_install                Install '$(squire_package)' binary in '\$$(prefix)/bin' directory.\n\
		squire_build                  Compile the '$(squire_package)' binary in the build directory.\n\
		squire_configure              Configure '$(squire_package)' build files for this system.\n\
		squire_tarball                Download the '$(squire_package)' tarball from its repository: $(squire_src)\n\
		squire_uninstall              Uninstall '$(squire_package)' from the '\$$(prefix)/bin' directory.\n\
		squire_clear                  Remove '$(squire_package)' tarball from tarball''s directory.\n\
		squire_purge                  Uninstall and clear '$(squire_package)' from the system.\n\
		squire_refresh                Uninstall and then re-install '$(squire_package)' again.\n\
		squire_deps                   Install a '$(squire_package)' dependency recursivelly as passed by the 'args' variable.\n\
		squire_test                   Test '$(squire_package)' installation.\n\
		squire_test_build_native      Test '$(squire_package)' build process, natively.\n\
		squire_test_build_conda       Test '$(squire_package)' build process in a new Conda environment.\n\
		squire_test_build_docker      Test '$(squire_package)' build process inside a vanilla Docker container.\n\
		squire_test_build_aws         Test '$(squire_package)' build process inside an AWS instance. NOT WORKING!!\n\
		squire_create_img             Create a Docker image for '$(squire_package)', based on '$(image)' image.\n\
		squire_create_dockerfile      Create a Dockerfile to build an image for '$(squire_package)'.\n\
		squire_create_module          Create a pre-filled SK8 module for '$(squire_package)' package.\n\
		\n\
		conda_run                      Run '$(squire_package)' in a new Conda environment. See 'conda_help' for more info.\n\
		docker_run                     Run '$(squire_package)' in a vanilla Docker image. See 'docker_help' for more info.\n\
		aws_run                        Run '$(squire_package)' in an AWS instance. See 'aws_help' for more info. NOT WORKING!!\n\
		\n\
		Variables:\n\
		\n\
		args                           String of arguments passed to '$(squire_package)' when running.               [ '' ]\n\
		prefix                         Path to the directory where to put the built '$(squire_packages)' binaries.   [ /usr/local ]\n\
		DESTDIR                        Path to an alternative staged installation directory.                          [ '' ]\n\
		workdir                        The current directory.                                                         [ . ]\n\
		stageddir                      Path to a staged directory for tarballs and package builds.                    [ ./.local ]\n\
		usermode                       Boolean to switch between local or system-wide installations paths.            [ '' ]\n\
		recursive                      Boolean to enable recursive dependency installations.                          [ '' ]\n\
		FORCE                          Boolean to force a fresh installation over an existent one.                    [ '' ]\n\
		\n\
		src                            URL of the '$(squire_package)' repository.                                    [ '$(squire_src)' ]\n\
		ext                            The tarball extension. Only for extensions other than '.tar.gz'.               [ '.tar.gz' ]\n\
		squire_package                The name of the package, derived from its tarball''s name.                     [ '$(squire_package)' ]\n\
		squire_version                The version of the package, derived from its tarball''s name.                  [ '$(squire_version)' ]\n\
		\n\
		Make options:\n\
		\n\
		\n\
		NOTE: Some packages cannot build squireally. So, user must test the chosen target first.\n\
		For a list of known squireally installable packages, please read the\n\
		'$(sk8_rootdir)/modules/modules_urls.txt' file.\n\
		\n\
		\n\
	COPYRIGHT\n\
		\n\
		$(sk8_package), version $(sk8_version) for GNU/Linux.\n\
		Copyright (C) Bards Agrotechnologies, 2011-2021.\n\
		\n\
	SEE ALSO\n\
		\n\
		Search for 'help', 'docker_help', 'conda_help' and 'miscellaneus'\n\
		for other sources of command line help or visit <$(basename $(sk8_source))>\n\
		for full documentation."

squire_manual:
	@$(ECHO) "\n$(squire_package) v$(squire_version) for GNU/Linux.\n\
	Please, see <$(squire_src)> for full documentation." >&2

squire_version:
	@$(ECHO) "\n\
	Package:    $(squire_package)\n\
	Version:    $(squire_version)\n\
	Id:         $(squire_id)\n\
	License:    $(squire_license)\n\
	\n\
	Status:     $(squire_status)\n\
	Source:     $(squire_src)\n\
	Installdir: $(installdir)\n\
	\n\
	Runned with $(sk8_package), version $(sk8_version) for GNU/Linux.\n\
	Copyright (C) Bards Agrotechnologies, 2011-2021."


squire_run: $(squire_status)
	@$(ECHO) "\nRunning '$(squire)' with arguments '$(args)' ..." >&2
	$(squire) $(args)
	@$(ECHO) "Done." >&2


# Verification.
squire_status: $(squire_status) # 'squire_install' or none.
	@$(ECHO) "\nSystem has '$(squire_package)' up and running." >&2


# Install.
squire_install: $(squire_installfile) # $(installdir)/$(squire_package)
	@$(ECHO) "\nSystem has '$(squire_package)' in PATH: '$<'." >&2

$(squire_installfile): $(squire_execfile) | $(installdir) # just copy the executable file to $(installdir).
	@$(ECHO) "\nInstalling '$(squire_package)' on directory '$(installdir)' ..." >&2
	$(MAKE) -C $(squire_builddir) install DESTDIR=$(DESTDIR) prefix=$(dir $(installdir)) -j4
	if [ -z "$$(perl -ne 'print if s#.*$(installdir).*#AA#g' <<< $${PATH})" ]; then \
		echo -e "export PATH=$(installdir):\$$PATH" >> ~/.bashrc; \
		source ~/.bashrc; \
		echo -e "WARNING: You may need to run 'source ~/.bashrc' to release the new \$$PATH."; \
	elif [ "$(squire)" != "$(squire_installfile)" ]; then \
		echo "WARNING: Previuos installation of '$(squire_package)' may be shadowing the new one in the \$$PATH."; \
	fi
	touch $@
	@$(ECHO) "Done." >&2


# Build.
squire_build: $(squire_execfile) # $(squire_builddir)/$(squire_package)

$(squire_execfile): $(squire_buildfile) | $(gcc_status) # a Makefile generates the executable file.
	@$(ECHO) "\nBuilding '$(squire_package)' on directory '$(squire_builddir)' ..." >&2
	$(MAKE) -C $(squire_builddir) -j4
	touch $@
	@$(ECHO) "Done." >&2


# Configure.
squire_configure: $(squire_buildfile) # $(squire_builddir)/Makefile

$(squire_buildfile): squire_configure_cmd := ./configure --prefix=$(dir $(installdir)) $(squire_args)
$(squire_buildfile): $(squire_configurefile)
	@$(ECHO) "\nConfiguring '$(squire_package)' on directory '$(squire_builddir)' ..." >&2
	cd $(squire_builddir); \
	$(squire_configure_cmd)
	touch $@
	@$(ECHO) "Done." >&2

$(squire_configurefile): $(squire_tarball) | $(squire_builddir)
	@$(ECHO) "\nExtracting '$(squire_package)' tarball to directory '$(squire_builddir)' ..." >&2
	if [ "$(squire_ext)" == ".tar.gz" ]; then \
		tar -xzvf $< -C $(squire_builddir) --strip-components=1; \
	elif [ "$(squire_ext)" == ".tar.xz" ]; then \
		tar -xJvf $< -C $(squire_builddir) --strip-components=1; \
	else \
		tar -xvf $< -C $(squire_builddir) --strip-components=1; \
	fi
	touch $@
	@$(ECHO) "Done." >&2

$(squire_builddir):
	@$(ECHO) "\nCreating directory '$(squire_builddir)' ..." >&2
	mkdir -p $(squire_builddir)
	@$(ECHO) "Done." >&2


# Download.
squire_tarball: $(squire_tarball) # $(tarballsdir)/$(squire_tarball).

$(squire_tarball): $(squire_force) | $(tarballsdir) $(internet_status) $(squire_deps_status)
	@$(ECHO) "\nDownloading '$(squire_package)' tarball from '$(squire_repository)' ..." >&2
	if [ "$(dwlr)" == "wget" ]; then \
		$(MAKE) -f $(firstword $(MAKEFILE_LIST)) $(mode)_run \
			args="$(dwlr) -c $(squire_repository) -O $@"; \
	elif [ "$(dwlr)" == "curl" ]; then \
		$(MAKE) -f $(firstword $(MAKEFILE_LIST)) $(mode)_run \
			args="$(dwlr) -L $(squire_repository) -o $@"; \
	elif [ "$(dwlr)" == "git" ]; then \
		$(MAKE) -f $(firstword $(MAKEFILE_LIST)) $(mode)_run \
			args="$(dwlr) clone $(squire_repository) $(squire_builddir)"; \
	else \
		echo "ERROR: Invalid mode '$(mode)'." >&2; \
		exit 7; \
	fi
	touch $@
	@$(ECHO) "Done." >&2

squire_deps:
	@$(ECHO) "\nRecursively installing dependecies for '$(squire_distname)' ..." >&2
	[ -n "$(args)" ] || ( echo "ERROR: Variable 'args' can not be empty." >&2; exit 7 )
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) recursive= $(args)
	@$(ECHO) "Done." >&2

squire_force:
	@$(ECHO) "\nForce reinstallation of '$(squire_package)' on directory '$(installdir)'." >&2
	rm -f $(squire_tarball)
	@$(ECHO) "Done." >&2


squire_refresh: squire_uninstall
	if [ -z "$(usermode)" ]; then \
		$(MAKE) -f $(firstword $(MAKEFILE_LIST)) DESTDIR=$(DESTDIR) prefix=$(prefix) squire_install; \
	else \
		$(MAKE) -f $(firstword $(MAKEFILE_LIST)) DESTDIR=$(DESTDIR) prefix=$(prefix) squire_install usermode=1; \
	fi


# Uninstall and clean.
squire_purge: squire_uninstall squire_clean

squire_uninstall:
	@$(ECHO) "\nUninstalling '$(squire_package)' from directory '$(installdir)' ..." >&2
	[ -d "$(installdir)" ] || exit 1;
	if [ -f "$(squire_buildfile)" ]; then \
		$(MAKE) -C $(squire_builddir) uninstall DESTDIR=$(DESTDIR) prefix=$(dir $(installdir)) -j4; \
	else \
		rm -rf $(squire_installfile)*; \
	fi
	@$(ECHO) "User must resolve \$$PATH in '~/.bashrc' file." >&2
	@$(ECHO) "Done." >&2

squire_clean:
	@$(ECHO) "\nCleaning '$(squire_package)' from staged directory '$(stageddir)' ..." >&2
	rm -rf $(tarballsdir)/$(squire_package)*
	@$(ECHO) "Done." >&2


# Unit Test.
squire_test: args ?= --version
squire_test:
	$(ECHO) "\nTesting package installation ...\n\
		Package:        $(squire_package)\n\
		Version:        $(squire_version)\n\
		Install Path:   $(squire)\n\
		Arguments:      '$(args)'\n\
		Runtime log:" >&2
	if $(squire) $(args); then                                                \
		echo "Package '$(squire_package)' PASSED the test.";                  \
	else                                                                      \
		err=$$?;                                                              \
		echo "ERROR: Package '$(squire_package)' wasn't properly installed."; \
		echo -e "Exit code: $$err.";                                          \
		exit 7;                                                               \
	fi >&2
	$(ECHO) "Done." >&2


squire_test_build_native: _install := $(basedir)/_install_$(squire_distname)
squire_test_build_native:
	@$(ECHO) "\nTesting native build of '$(squire_package)' module." >&2
	[ -n "$(_install)" ] || ( echo "ERROR: Variable '_install' is empty."; exit 7 )
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) \
		stageddir=$(_install) \
		usermode=1 \
		squire_src=$(squire_src) \
		squire_ext=$(squire_ext) \
		squire_package=$(squire_package) \
		squire_version=$(squire_version) \
		squire_args=$(squire_args) \
		recursive=$(recursive) args="$(args)" \
		squire_install squire_test
	[ -n "$(keep_install)" ] || rm -rf $(_install)
	@$(ECHO) "Done." >&2
	@$(ECHO) "\nFinished native build test for '$(squire_package)' module." >&2

squire_test_build_conda: conda_test_build
	@$(ECHO) "\nFinished Conda build test for '$(squire_package)' module." >&2

squire_test_build_docker: override args := make -f SK8/SK8.mk \
	module=$(addprefix SK8/modules/, $(notdir $(module))) \
	usermode=1 \
	squire_src=$(squire_src) \
	squire_ext=$(squire_ext) \
	squire_package=$(squire_package) \
	squire_version=$(squire_version) \
	squire_args=$(squire_args) \
	recursive=$(recursive) args="$(args)" \
	squire_install squire_test
squire_test_build_docker: docker_test_build
	@$(ECHO) "\nFinished Docker build test for '$(squire_package)' module." >&2


.PHONY: squire_help squire_manual squire_version squire_status


############################## OPERATIONAL #####################################

squire_fetch                   := fetch.log
squire_clean                   := clean.log
squire_map                     := $(dln:1.fastq=_map.log)
squire_count                   := $(dln:1.fastq=_count.log)
squire_call                    := call.log

squire_fetchflags := -f -c -r -g -x
squire_fetchdir := fetch
squire_cleandir := clean
squire_mapdir := map
squire_countdir := count
squire_calldir := call
squire_blddir := hg38


squire_class := LINE
squire_family := L1
squire_subfamily := L1HS


squire_all: $(manifest)
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) squire_cl


squire_fetch: $(squire_fetch)

$(squire_fetch): | $(squire_status)
	@$(ECHO) "\nFetching file requisites for 'SQuIRE' ..." >&2
	squire Fetch \
		-b $(squire_blddir) \
		-o $(squire_fetchdir) \
		$(squire_fetchflags) \
		-p 4 -v 2> $@
	@$(ECHO) "Done." >&2


squire_clean: $(squire_clean)

$(squire_clean): $(squire_fetch) | $(squire_status)
	@$(ECHO) "\nCleaning files for 'SQuIRE' ..." >&2
	squire Clean \
		-b $(squire_blddir) \
		-o $(squire_cleandir) \
		-c $(squire_class) \
		-f $(squire_family) \
		-s $(squire_subfamily) \
		-r $(squire_fetchdir)/$(squire_blddir)_rmsk.txt \
		-p 4 -v 2> $@
	@$(ECHO) "Done." >&2


squire_map: $(squire_map)

$(squire_map): %_map.log: %1.fastq $(squire_clean) | $(squire_status)
	@$(ECHO) "\nMapping files '$<' and '$(<:1.fastq=2.fastq)' ..." >&2
	r2="-2 $(<:1.fastq=2.fastq)"; \
	[ -s "$(<:1.fastq=2.fastq)" ] || r2=""; \
	squire Map \
		-b $(squire_blddir) \
		-o $(squire_mapdir) \
		-f $(squire_fetchdir) \
		-g $(annotation_ref) \
		-1 $< $${r2} \
		-n $(*F) \
		-r $(shell perl -lane 'print(length($$_)), die if $$. == 2' $< 2> /dev/null) \
		-p 4 -v 2> $@
	@$(ECHO) "Done." >&2


squire_count: $(manifest)
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) squire_ct

squire_ct: $(squire_count)

$(squire_count): %_count.log: %_map.log | $(squire_status)
	@$(ECHO) "\nCounting file '$<' ..." >&2
	squire Count \
		-b $(squire_blddir) \
		-o $(squire_countdir) \
		-f $(squire_fetchdir) \
		-c $(squire_cleandir) \
		-m $(squire_mapdir) \
		-t $(squire_countdir) \
		-n $(*F) \
		-r $(shell perl -lane 'print(length($$_)), die if $$. == 2' $(dln) 2> /dev/null) \
		-p 4 -v 2> $@
	@$(ECHO) "Done." >&2


squire_call: $(manifest)
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) squire_cl

squire_cl: $(squire_call)

$(squire_call): $(squire_count) | $(squire_status)
	@$(ECHO) "\nCalling TEs from files '$<' ..." >&2
	g1="$(^:_count.log=)"; \
	g2=""; \
	squire Call \
		-o $(squire_calldir) \
		-1 $${g1} \
		-2 $${g2} \
		-A treated \
		-B control \
		-N ponga \
		-f pdf \
		-p 4 -v 2> $@
	@$(ECHO) "Done." >&2


squire_create_env: | $(conda_status)
	@$(ECHO) "\nQuantifying transcripts expression in files '$^' ..." >&2
	if [ -n "$$(conda list -n squire 2> /dev/null)" ]; then \
		echo "Conda environment already exists."; \
	else \
		conda create -y --name squire --override-channels \
			-c iuc -c bioconda -c conda-forge -c defaults -c r \
			python=2.7.13 \
			bioconductor-deseq2=1.16.1 \
			r-base=3.4.1 \
			r-pheatmap \
			bioconductor-vsn \
			bioconductor-biocparallel=1.12.0 \
			r-ggrepel \
			star=2.5.3a \
			bedtools=2.25.0 \
			samtools=1.1 \
			stringtie=1.3.3 \
			igvtools=2.3.93 \
			ucsc-genepredtobed \
			ucsc-gtftogenepred \
			ucsc-genepredtogtf \
			ucsc-bedgraphtobigwig \
			r-hexbin; \
		git clone https://github.com/wyang17/SQuIRE.git; \
		cd SQuIRE; conda run -n squire pip install -e .; \
		perl -i -pe 's#/bin/sh#/bin/bash#g' squire/Count.py; \
	fi
	@$(ECHO) "MESSAGE: User must activate the environment before use it:" >&2
	@$(ECHO) "    \$$ conda activate squire\n" >&2
	@$(ECHO) "Done." >&2



############################## DOCUMENTATION ###################################
# Documentation was already written in the targets above.
