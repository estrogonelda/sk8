#!/usr/bin/make
############################## HEADER ##########################################
#
# A Makefile for Docker utilities.
#
#
# Metadata:
#
#	Project:		upipe
#	Module:			core
#	File:			dck2.mk
#	Default Goal:	help
#	
#	Package:		dck2mk
#	Version:		0.1.0
#	Id:				undef
#	Distname:		upipe-0.1.0
#	License:		GPLv4
#	Source:			undef
#	
#	Title:			{}
#	Subtitle:		{}
#	Authors:		Jose L. L. Buzzo
#	Organization:	Galantelab
#	Local:			Sao Paulo
#	Year:			2020
#	Publisher:		{}
#	DOI:			{}
#	URL:			www.bioinfo.mochsl.org.br
#
# Requisites:
#
#	bash, make, wget, perl
#
# Usage:
#
#	$ make -f dck2mk.mk <targets ...> [options]
#	
# Options:
#
#	none
#
################################################################################





############################## PREAMBLE ########################################

# Generic package main variables.
package					:= dck2mk
version					:= 0.1.0
id						:= xxxxxx
distname				:= $(package)-$(version)
license					:= GPLv4 
source					:= https://raw.githubusercontent.com/jlbuzzo/B3/master/Makefile


# GNU Standard Path variables.
rootdir					:= $(CURDIR)
srcdir					:= $(CURDIR)
testdir					:= $(srcdir)/test
builddir				:= $(srcdir)/build
distdir					:= $(srcdir)/$(distname)

prefix					:= /usr/local
oldincludedir			:= /usr/include
includedir				:= $(prefix)/include
localstatedir			:= $(prefix)/var
sysconfdir				:= $(prefix)/etc
sharedstatedir			:= $(prefix)/com
datarootdir				:= $(prefix)/share
exec_prefix				:= $(prefix)

bindir					:= $(exec_prefix)/bin
sbindir					:= $(exec_prefix)/sbin
libdir					:= $(exec_prefix)/lib
libexecdir				:= $(exec_prefix)/libexec
scriptsdir				:= $(exec_prefix)/scripts

datadir					:= $(datarootdir)
mandir					:= $(datarootdir)/man
infodir					:= $(datarootdir)/info
localedir				:= $(datarootdir)/locale
lispdir					:= $(datarootdir)/emacs/site-lisp
docdir					:= $(datarootdir)/doc/$(package)
htmldir					:= $(docdir)
dvidir					:= $(docdir)
pdfdir					:= $(docdir)
psdir					:= $(docdir)


# Common command macros.
SHELL					:= /bin/bash
USER					:= $(shell whoami)
DATE					:= $(shell date --utc)
ECHO					:= echo -e
MKDIR					:= mkdir -p
PING					:= ping -c1


# Common color macros.
GREEN					:= "\033[0;32m"
YELLOW					:= "\033[0;33m"
RED						:= "\033[0;31m"
WINE					:= "\033[0;35m"
RESET					:= "\033[0;m"


# Verify prefix existence.
ifeq ($(abspath $(strip $(wildcard $(prefix)))),)
      $(error ERROR: Invalid value "$(prefix)" for 'prefix' variable)
endif



############################## DOCKER INFO ####################################

# Info for configuration and installation: docker-19.03.5.
docker_package				:= docker
docker_version				:= 19.03.5
docker_id					:= $(docker_package)-xxxxxx
docker_distname				:= $(docker_package)-$(docker_version)
docker_license				:=

docker_configfile			:=
docker_builderize			:=
docker_builder				:=
docker_buildfile			:=
docker_tarball				:= $(docker_distname).tgz
docker_buildrepository		:= https://download.docker.com/linux/static/stable/x86_64/$(docker_tarball)
docker_conteinerize			:=
docker_container			:=
docker_containerfile		:=
docker_containerhub			:=
docker_image				:=
docker_clouderize			:=
docker_clouder				:=
docker_cloudfile			:=
docker_cloud				:=
docker_bucket				:=
docker_subprojects			:=
docker_dependecies			:= wget

docker_has					:= [[ -n "$$(which docker 2> /dev/null)" ]] && echo "1"
docker_get					:= $(if $(shell $(docker_has)),,docker_install)

# App specific tests.
use_mode					:= $(if $(containerize),app_docker_run,app_native_run)

dockerfile_has				:= [[ -f "$(containerfile)" ]] && echo "1"
dockerfile_get				:= $(if $(shell $(dockerfile_has)),,dockerfile_gen)

image_get_mode				:= $(if $(shell $(dockerfile_has)),image_build,image_pull)

image_has					:= echo -n "$$(sed -n '\|^$(image) |{p}' <<< "$$(docker images 2> /dev/null)" 2> /dev/null)" | grep -w 'latest'
image_get					:= $(if $(shell $(image_has)),,$(image_get_mode))

tarball_has					:= [[ -f "$(tarball)" ]] && echo "1"
tarball_get					:= $(if $(shell $(tarball_has)),,$(tarball))

app							:= $(package)
app_has						:= [[ -n "$$(which $(app) 2> /dev/null)" ]] && echo "1"
app_has_mode				:= $(if $(containerize),$(image_has),$(app_has))
app_get_mode				:= $(if $(containerize),$(image_get),app_install)
app_get						:= $(if $(shell $(app_has_mode)),,$(app_get_mode))

# General system tests.
internet_has				:= read <<< "$$($(PING) www.google.com 2> /dev/null)" && echo -n $$REPLY
internet_get				:= $(if $(shell $(internet_has)),,internet_configure)

wget_has					:= [[ -n "$$(which wget 2> /dev/null)" ]] && echo "1"
wget_has					:= $(if $(shell $(wget_has)),,wget_install)



############################## GENERAL TARGETS #################################

help:
	@$(ECHO) "\nUsage:\n"
	@$(ECHO) "\t$ make -f SK.mk [options] [targets] [var1=\"xxx\" var2=\"yyy\" ...]"

test:
	@$(ECHO) "\nThis is a simple test."
	@if [[ -n '$(args)' ]]; then \
		$(ECHO) "\n\tARGS: \"$(args)\"."; \
	fi



############################## DOCKER TARGETS ##################################

image_run: inputs | docker_has image_has infrastructure
	@$(ECHO) "Running docker image '$(image)' ..."
	#$(DOCKER_RUN) $(DOCKER_ARGS) $(IMAGE) $(ARGS)
	@$(ECHO) "Done.\n"

image_has: $(image_get) # 'image_build' or 'image_pull'.
	@$(ECHO) "This system has docker image '$(image)'.\n"

image_build: docker_has dockerfile_has internet_has
	@$(ECHO) "Building image '$(image)' from dockerfile '$(dockerfile)' ..."
	#$(docker_build) -t $(image) - < $(dockerfile)
	@$(ECHO) "Done.\n"

dockerfile_has: $(dockerfile_get) # dockerfile_gen
	@$(ECHO) "This system has the dockerfile for image '$(IMAGE)'.\n"

dockerfile_gen:
	@$(ECHO) "Processing Dockerfile for $(app).\n"
	@$(ECHO) "Done.\n"

image_pull: docker_has internet_has
	@$(ECHO) "Pulling Docker image: $(image) from DockerHub ..."
	docker pull $(image)
	@$(ECHO) "Done.\n"



docker_has: $(docker_get) # 'docker_install' or none.
	@$(ECHO) "This system has 'Docker' installed.\n"

docker_install: docker_build
	@$(ECHO) "Installing 'Docker' ..."
	sudo cp $(subst .tgz,,$(docker_tarball))/* $(DESTDIR)$(prefix)/bin
	sudo dockerd &
	@$(ECHO) "Done.\n"

docker_build: docker_tarball_has
	@$(ECHO) "Building Docker ..."
	#cd $(DOCKER_TARBALL)
	#./configure && $(MAKE) && $(MAKE) install
	@$(ECHO) "Done.\n"

docker_tarball_has: $(docker_tarball_get) # '$(docker_tarball)' or none.
	@$(ECHO) "This system has the tarball for 'Docker'.\n"

$(docker_tarball): wget_has internet_has
	@$(ECHO) "Downloading and unpacking 'Docker' tarball ..."
	wget -c $(docker_buildrepository) -P $(tarballsdir)
	tar xzvf $@ -C $(temp)
	$(ECHO) "Done.\n"


wget_has: $(wget_get) # 'wget_install' or none.
	@$(ECHO) "This system has 'wget' installed.\n"

wget_install:


$(info Ah la $(ppp).)
