# Info to download, configure, build, install and use 'sandy' package.
sandy_src                    := https://github.com/galantelab/sandy/archive/v0.23.tar.gz
sandy_ext                    := .tar.gz

sandy_package                := sandy
sandy_version                := 0.23
sandy_id                     := $(sandy_package)-xxxxxx
sandy_distname               := $(sandy_package)-$(sandy_version)
sandy_license                := GPLv4

# Potential installation data.
sandy_repository             := $(sandy_src)
sandy_tarball                := $(tarballsdir)/$(sandy_distname)$(sandy_ext)
sandy_builddir               := $(tarballsdir)/$(sandy_distname)
sandy_configurefile          := $(sandy_builddir)/configure
sandy_buildfile              := $(sandy_builddir)/Makefile
sandy_execfile               := $(sandy_builddir)/$(sandy_package)
sandy_installfile            := ~/perl5/bin/$(sandy_package)


# Actual installation data.
sandy_has                    := $(shell which $(sandy_package) 2> /dev/null)
sandy_get                    := $(if $(sandy_has),,sandy_install)
sandy                        := $(if $(sandy_has),$(sandy_has),$(sandy_installfile))


# FORCE data.
sandy_force                  := $(if $(FORCE),sandy_force,)
sandy_status                 := $(if $(FORCE),sandy_install,$(sandy_get))
sandy                        := $(if $(FORCE),$(sandy_installfile),$(sandy))

cpanm_status                 := $(if $(shell which cpanm 2> /dev/null),,cpanm_install)
zlib_status                  := $(if $(shell [ -f "$(installdir)/../lib/libz.a" ] && echo "1"),,sandy_zlib)
perl_status                  := $(if $(shell perl -MPerlIO -le 'print "Ahh"' 2> /dev/null),,sandy_perl)

zlib_src2                    := https://zlib.net/zlib-1.2.11.tar.gz
perl_src2                    := https://www.cpan.org/src/5.0/perl-5.32.0.tar.gz


# Include required modules.
sandy_depscheck              := $(internet_status) $(gcc_status) $(zlib_status) $(perl_status) $(cpanm_status)


# Verification.
sandy_status: $(sandy_status) # 'sandy_install' or none.
	@$(ECHO) "\nSystem has '$(sandy_package)' up and running." >&2


# Install.
sandy_install: $(sandy_installfile) # $(installdir)/$(sandy_package)
	@$(ECHO) "\nSystem has '$(sandy_package)' in PATH: '$<'." >&2

$(sandy_installfile): | $(sandy_depscheck) # just copy the executable file to $(installdir).
	@$(ECHO) "\nInstalling '$(sandy_package)' on directory '$(dir $(installfile))' ..." >&2
	cpanm -L ~/perl5 --force File::ShareDir
	cpanm -L ~/perl5 --force App::Sandy
	if [ -z "$$(perl -ne 'print if s#.*~/perl5/bin.*#AA#g' <<< $${PATH})" ]; then \
		echo -e "export PATH=~/perl5/bin:\$$PATH" >> ~/.bashrc; \
		echo -e "export PERL5LIB=~/perl5/lib/perl5:\$$PERL5LIB" >> ~/.bashrc; \
		echo -e "export PERL5_LOCAL_LIB_ROOT=~/perl5:\$$PERL5_LOCAL_LIB_ROOT" >> ~/.bashrc; \
		export PATH=~/perl5/bin:$$PATH; \
		export PERL5LIB=~/perl5/lib/perl5:$$PERL5LIB; \
		export PERL5_LOCAL_LIB_ROOT=~/perl5:$$PERL5_LOCAL_LIB_ROOT; \
		source ~/.bashrc; \
		echo -e "WARNING: You may need to run 'source ~/.bashrc' to release the new \$$PATH."; \
	elif [ "$(sandy)" != "$(sandy_installfile)" ]; then \
		echo "WARNING: Previuos installation of '$(sandy_package)' may be shadowing the new one in the \$$PATH."; \
	fi
	touch $@
	@$(ECHO) "Done." >&2


# cpanm.
cpanm_install:
	@$(ECHO) "\nInstalling 'cpanm' on directory '$(installdir)' ..." >&2
	curl -L https://cpanmin.us | perl - -L ~/perl5 App::cpanminus
	#cpanm -L ~/perl5 --force PerlIO::gzip
	@$(ECHO) "Done." >&2


sandy_perl:
	@$(ECHO) "\nRecursively installing dependecies for '$(sandy_distname)' ..." >&2
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) module=$(sk8_rootdir)/modules/perl.mk \
		usermode=$(usermode) perl_install
	@$(ECHO) "Done." >&2

sandy_zlib:
	@$(ECHO) "\nRecursively installing dependecies for '$(sandy_distname)' ..." >&2
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) src=$(zlib_src2) \
		usermode=$(usermode) generic_install
	@$(ECHO) "Done." >&2


# Uninstall and clean.
sandy_purge: sandy_uninstall

sandy_uninstall:
	@$(ECHO) "\nUninstalling '$(sandy_package)' from directory '$(installdir)' ..." >&2
	[ -d "$(installdir)" ] || exit 1;
	cpanm -L ~/perl5 --uninstall --force App::Sandy
	@$(ECHO) "User must resolve \$$PATH in '~/.bashrc' file." >&2
	@$(ECHO) "Done." >&2


# Tests.
sandy_test:
	@$(ECHO) "\nTesting '$(sandy_package)' ..." >&2
	source ~/.bashrc; \
	if [ -z "$$($(sandy_package) --version 2> /dev/null)" ]; then \
		echo "Package '$(sandy_package)' is not properly running."; \
		exit 1; \
	else \
		echo "PASS!"; \
	fi
	@$(ECHO) "Done." >&2

sandy_test_build: docker_more_opts := -e PATH=$(c_basedir)/perl5/bin:./.local/bin:$(c_basedir)/.local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
sandy_test_build: docker_more_opts += -e LIBRARY_PATH=$(c_basedir)/.local/lib
sandy_test_build: docker_more_opts += -e CPATH=$(c_basedir)/.local/include
sandy_test_build: args := make -f SK8/SK8.mk module=SK8/modules/sandy.mk usermode=1 sandy_install sandy_test args="ponga=0"
sandy_test_build: docker_test_build
	@$(ECHO) "\nFinished Docker build test for '$(generic_package)' module." >&2
