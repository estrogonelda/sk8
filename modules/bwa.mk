# Info to download, configure, build, install and use 'bwa' package.
bwa_src                    := https://github.com/lh3/bwa/archive/0.7.9.tar.gz
bwa_ext                    := .tar.gz

bwa_package                := bwa
bwa_version                := 0.7.9
bwa_id                     := $(bwa_package)-xxxxxx
bwa_distname               := $(bwa_package)-$(bwa_version)
bwa_license                := GPLv4

# Potential installation data.
bwa_repository             := $(bwa_src)
bwa_tarball                := $(tarballsdir)/$(bwa_distname)$(bwa_ext)
bwa_builddir               := $(tarballsdir)/$(bwa_distname)
bwa_configurefile          := $(bwa_builddir)/configure
bwa_buildfile              := $(bwa_builddir)/Makefile
bwa_execfile               := $(bwa_builddir)/$(bwa_package)
bwa_installfile            := $(installdir)/$(bwa_package)


# Actual installation data.
bwa_has                    := $(shell which $(bwa_package) 2> /dev/null)
bwa_get                    := $(if $(bwa_has),,bwa_install)
bwa                        := $(if $(bwa_has),$(bwa_has),$(bwa_installfile))


# FORCE data.
bwa_force                  := $(if $(FORCE),bwa_force,)
bwa_status                 := $(if $(FORCE),bwa_install,$(bwa_get))
bwa                        := $(if $(FORCE),$(bwa_installfile),$(bwa))


# Include required modules.
#include $(addprefix $(sk8_rootdir)/modules/, gcc.mk)
bwa_deps                   := $(if $(recursive),bwa_deps,)
bwa_depscheck              := $(gcc_status) $(bwa_deps)


# Verification.
bwa_status: $(bwa_status) # 'bwa_install' or none.
	@$(ECHO) "\nSystem has '$(bwa_package)' up and running." >&2


# Install.
bwa_install: $(bwa_installfile) # $(installdir)/$(bwa_package)
	@$(ECHO) "\nSystem has '$(bwa_package)' in PATH: '$<'." >&2

$(bwa_installfile): $(bwa_execfile) # just copy the executable file to $(installdir).
	@$(ECHO) "\nInstalling '$(bwa_package)' on directory '$(installdir)' ..." >&2
	#$(MAKE) -C $(bwa_builddir) install DESTDIR=$(DESTDIR) prefix=$(dir $(installdir)) -j4
	install -t $(installdir) -m 755 $<
	if [ -z "$$(perl -ne 'print if s#.*$(installdir).*#AA#g' <<< $${PATH})" ]; then \
		echo -e "export PATH=$(installdir):\$$PATH" >> ~/.bashrc; \
		source ~/.bashrc; \
		echo -e "WARNING: You may need to run 'source ~/.bashrc' to release the new \$$PATH."; \
	elif [ "$(bwa)" != "$(bwa_installfile)" ]; then \
		echo "WARNING: Previuos installation of '$(bwa_package)' may be shadowing the new one in the \$$PATH."; \
	fi
	touch $@
	@$(ECHO) "Done." >&2


# Build.
bwa_build: $(bwa_execfile) # $(bwa_builddir)/$(bwa_package)

$(bwa_execfile): $(bwa_buildfile) | $(gcc_status) # a Makefile generates the executable file.
	@$(ECHO) "\nBuilding '$(bwa_package)' on directory '$(bwa_builddir)' ..." >&2
	$(MAKE) -C $(bwa_builddir) -j4
	touch $@
	@$(ECHO) "Done." >&2


# Configure.
bwa_configure: $(bwa_buildfile) # $(bwa_builddir)/Makefile

$(bwa_buildfile): $(bwa_configurefile)
	@$(ECHO) "\nConfiguring '$(bwa_package)' on directory '$(bwa_builddir)' ..." >&2
	#cd $(bwa_builddir); \
	#./configure --prefix=$(dir $(installdir)) $(bwa_args)
	touch $@
	@$(ECHO) "Done." >&2

$(bwa_configurefile): $(bwa_tarball)
	@$(ECHO) "\nExtracting '$(bwa_package)' tarball to directory '$(bwa_builddir)' ..." >&2
	mkdir -p $(bwa_builddir)
	if [ "$(bwa_ext)" == ".tar.gz" ]; then \
		tar -xzvf $< -C $(bwa_builddir) --strip-components=1; \
	else \
		tar -xvf $< -C $(bwa_builddir) --strip-components=1; \
	fi
	touch $@
	@$(ECHO) "Done." >&2


# Download.
bwa_tarball: $(bwa_tarball) # $(tarballsdir)/$(bwa_tarball).

$(bwa_tarball): $(internet_status) $(bwa_force) | $(tarballsdir) $(bwa_depscheck)
	@$(ECHO) "\nDownloading '$(bwa_package)' tarball from '$(bwa_repository)' ..." >&2
	wget -c $(bwa_repository) -P $(tarballsdir) -O $@
	touch $@
	@$(ECHO) "Done." >&2

bwa_deps:
	@$(ECHO) "\nRecursively installing dependecies for '$(bwa_distname)' ..." >&2
	[ -n "$(args)" ] || ( echo "ERROR: Variable 'args' can not be empty." >&2; exit 1 )
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) recursive= $(args)
	@$(ECHO) "Done." >&2

bwa_force:
	@$(ECHO) "\nForce reinstallation of '$(bwa_package)' on directory '$(installdir)'." >&2
	rm -f $(bwa_tarball)
	@$(ECHO) "Done." >&2


bwa_refresh: bwa_uninstall
	if [ -z "$(usermode)" ]; then \
		$(MAKE) -f $(firstword $(MAKEFILE_LIST)) DESTDIR=$(DESTDIR) prefix=$(prefix) bwa_install; \
	else \
		$(MAKE) -f $(firstword $(MAKEFILE_LIST)) DESTDIR=$(DESTDIR) prefix=$(prefix) bwa_install usermode=1; \
	fi


# Uninstall and clean.
bwa_purge: bwa_uninstall bwa_clean

bwa_uninstall:
	@$(ECHO) "\nUninstalling '$(bwa_package)' from directory '$(installdir)' ..." >&2
	[ -d "$(installdir)" ] || exit 1;
	if [ -f "$(bwa_buildfile)_no" ]; then \
		$(MAKE) -C $(bwa_builddir) uninstall DESTDIR=$(DESTDIR) prefix=$(dir $(installdir)) -j4; \
	else \
		rm -rf $(bwa_installfile)*; \
	fi
	@$(ECHO) "User must resolve \$$PATH in '~/.bashrc' file." >&2
	@$(ECHO) "Done." >&2

bwa_clean:
	@$(ECHO) "\nCleaning '$(bwa_package)' from staged directory '$(stageddir)' ..." >&2
	rm -rf $(tarballsdir)/$(bwa_package)*
	@$(ECHO) "Done." >&2


# Test.
bwa_test:
	@$(ECHO) "\nTesting '$(bwa_package)' ..." >&2
	if [ -z "$$(which $(bwa_package) 2> /dev/null)" ]; then \
		echo "Package '$(bwa_package)' is not properly running."; \
		exit 1; \
	else \
		echo "PASS!"; \
	fi
	@$(ECHO) "Done." >&2

bwa_test_build_docker: override args := make -f SK8/SK8.mk module=$(addprefix SK8/modules/, $(notdir $(module))) usermode=1 \
	bwa_src=$(bwa_src) bwa_ext=$(bwa_ext) bwa_package=$(bwa_package) bwa_version=$(bwa_version) bwa_args=$(bwa_args) \
	recursive=$(recursive) args="$(args)" \
	bwa_install bwa_test
bwa_test_build_docker: docker_test_build
	@$(ECHO) "\nFinished Docker build test for '$(bwa_package)' module." >&2

