############################## PYTHON3 #########################################

# Info to download, configure, build, install and use 'generic' package.
python_src                     := https://www.python.org/ftp/python/3.9.1/Python-3.9.1.tar.xz
python_ext                     := .tar.xz

python_package                 := $(shell perl -ne 'print "@{[ (split(/-/, $$_))[0] ]}"' <<< "$(notdir $(python_src))")
python_version                 := $(shell perl -pe 's/^$(python_package)-(.*?)\$(python_ext)/$$1/g' <<< "$(notdir $(python_src))")
python_id                      := $(python_package)-xxxxxx
python_distname                := $(python_package)-$(python_version)
python_license                 :=

# Potential installation data.
python_dependecies             := gcc
python_repository              := $(python_src)
python_tarball                 := $(tarballsdir)/$(notdir $(python_src))
python_builddir                := $(python_tarball:.tar.xz=)
python_configurefile           := $(python_builddir)/configure
python_buildfile               := $(python_builddir)/Makefile
python_execfile                := $(python_builddir)/python
python_installfile             := $(installdir)/python

# Actual installation data.
python_has                     := $(shell which python3 2> /dev/null)
python_get                     := $(if $(python_has),,python_install)
python                         := $(if $(python_has),$(python_has),$(python_installfile))

# FORCE data.
python_force                   := $(if $(FORCE),python_force,)
python_status                  := $(if $(FORCE),python_install,$(python_get))
python                         := $(if $(FORCE),$(python_installfile),$(python))


python_status: $(python_status) # 'python_install' or none.
	@$(ECHO) "\nSystem has 'Python' up and running."


# Installation process.
python_install: $(python_installfile) # $(installdir)/python3
	@$(ECHO) "\nSystem has 'Python' in PATH: '$<'."

$(python_installfile): $(python_execfile) # just copy the executable file to $(installdir).
	@$(ECHO) "\nInstalling 'Python' on directory '$(installdir)' ..."
	$(MAKE) -f $(python_buildfile) -C $(python_builddir) install DESTDIR=$(DESTDIR) prefix=$(prefix) -j 4
	ln -sf python3 $@
	if [ -z "$$(perl -ne 'print if s#.*$(installdir).*#AA#g' <<< $${PATH})" ]; then \
		echo -e "export PATH=$(installdir):\$$PATH" >> ~/.bashrc; \
		source ~/.bashrc; \
		echo -e "WARNING: You may need to run 'source ~/.bashrc' to release the new \$$PATH."; \
	elif [ "$(python)" != "$(python_installfile)" ]; then \
		echo "WARNING: Previuos installation of '$(python_package)' may be shadowing the new one in the \$$PATH."; \
	fi
	@$(ECHO) "Done."


# Build process.
python_build: $(python_execfile) # $(python_builddir)/python3

$(python_execfile): $(python_buildfile) # a Makefile generates the executable file.
	@$(ECHO) "\nBuilding 'Python' on directory '$(python_builddir)' ..."
	$(MAKE) -f $< -C $(python_builddir) -j 4
	#touch $@
	@$(ECHO) "Done."


# Configuration process:
python_configure: $(python_buildfile) # $(python_builddir)/configure

$(python_buildfile): $(python_configurefile)
	@$(ECHO) "\nConfiguring 'Python' on directory '$(python_builddir)' ..."
	cd $(python_builddir) && $(abspath $<) --enable-optimizations
	#touch $@
	@$(ECHO) "Done."

$(python_configurefile): $(python_tarball)
	@$(ECHO) "\nExtracting 'Python' tarball to directory '$(python_builddir)' ..."
	mkdir -p $(python_builddir)
	tar -xJvf $< -C $(python_builddir) --strip-components=1
	touch $@
	@$(ECHO) "Done."


# Tarball download.
python_tarball: $(python_tarball) # $(tarballsdir)/$(python_distname).tar.xz.
	@$(ECHO) "\nSystem has 'Python' tarball '$<'."

$(python_tarball): $(internet_get) $(python_force) | $(tarballsdir)
	@$(ECHO) "\nDownloading 'Python' tarball from '$(python_repository)' ..."
	wget -c $(python_repository) -P $(tarballsdir) -O $@
	touch $@
	@$(ECHO) "Done."


python_force:
	@$(ECHO) "\nForce reinstallation for 'Python' on directory '$(installdir)'."
	rm $(python_tarball)
	@$(ECHO) "Done."


python_refresh: python_uninstall
	if [ -z "$(usermode)" ]; then \
		$(MAKE) -f $(firstword $(MAKEFILE_LIST)) DESTDIR=$(DESTDIR) prefix=$(prefix) python_install; \
	else \
		$(MAKE) -f $(firstword $(MAKEFILE_LIST)) DESTDIR=$(DESTDIR) prefix=$(prefix) python_install usermode=1; \
	fi

python_purge: python_uninstall python_clean

python_uninstall:
	@$(ECHO) "\nUninstalling 'Python' from directory '$(installdir)' ...";
	[ -d "$(installdir)" ] || exit 1;
	if [ -f "$(python_buildfile)_no" ]; then \
		$(MAKE) -f $(python_buildfile) -C $(python_builddir) uninstall DESTDIR=$(DESTDIR) prefix=$(prefix) -j; \
	else \
		rm -rf $(installdir)/python*; \
		rm -rf $(installdir)/pydoc*; \
		rm -rf $(installdir)/pip*; \
		rm -rf $(installdir)/idle*; \
		rm -rf $(installdir)/2to3*; \
		rm -rf $(installdir)/easy_install*; \
		rm -rf $(installdir)/../lib/python*; \
		rm -rf $(installdir)/../lib/libpython*; \
		rm -rf $(installdir)/../include/python*; \
		rm -rf $(installdir)/../share/man/man1/python*; \
	fi
	@$(ECHO) "User must resolve \$$PATH in '~/.bashrc' file." >&2
	@$(ECHO) "Done."

python_clean:
	@$(ECHO) "\nCleaning '$(python_package)' from staged directory '$(stageddir)' ..." >&2
	rm -rf $(tarballsdir)/$(python_package)*
	@$(ECHO) "Done." >&2


python_upgrade:


python_check:
	@$(ECHO) "\nChecking 'Python' installation ..."
	@$(ECHO) "Actual path:                                                                '$(python)'."
	@$(ECHO) "Result for 'python --version':                                              '$(shell python --version)'."
	@$(ECHO) "Potential installation directory:                                           '$(installdir)'."
	@if [ -f "$(python_installfile)" ]; then \
		$(ECHO) "Presence of executable file in potential installation directory:            yes."; \
	else \
		$(ECHO) "Presence of executable file in potential installation directory:            no."; \
	fi
	@if [ -n "$$(perl -F: -ane 'print if s#.*($(installdir)).*#$$1#g' ~/.bashrc)" ]; then \
		$(ECHO) "Presence of potential installation directory on \$$PATH in '~/.bashrc' file:  yes."; \
	else \
		$(ECHO) "Presence of potential installation directory on \$$PATH in '~/.bashrc' file:  no."; \
	fi
	@$(ECHO) "Done."
