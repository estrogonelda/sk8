############################## HEADER ##########################################
# SK8 module for zip package.
################################################################################



############################## PREAMBLE ########################################
# Non-standalone run verification.
this                           := zip.mk
ifeq ($(notdir $(firstword $(MAKEFILE_LIST))),$(this))
      $(error ERROR: Module '$(this)' cannot run in standalone mode, \
      it needs to be loaded by 'SK8')
endif

# Info to download, configure, build, install and use 'zip' package.
zip_src                    ?= https://downloads.sourceforge.net/infozip/zip30.tar.gz
zip_ext                    ?= .tar.gz

zip_package                ?= zip
zip_version                ?= 3.0
zip_id                     ?= $(zip_package)-$(shell echo $$RANDOM)
zip_license                ?= GPLv4
zip_distname               ?= $(zip_package)-$(zip_version)

# Potential installation data.
zip_repository             ?= $(zip_src)
zip_tarball                ?= $(tarballsdir)/$(zip_distname)$(zip_ext)
zip_builddir               ?= $(tarballsdir)/$(zip_distname)
zip_configurefile          ?= $(zip_builddir)/configure
zip_buildfile              ?= $(zip_builddir)/Makefile
zip_execfile               ?= $(zip_builddir)/$(zip_package)

zip_prefix                 ?= $(if $(supprefix),$(dir $(installdir))/$(zip_package),$(dir $(installdir)))
zip_install_bin            ?= $(zip_prefix)/bin/$(zip_package)
zip_install_lib            ?= $(zip_prefix)/lib/lib$(zip_package).a
zip_install_include        ?= $(zip_prefix)/include/$(zip_package).h
zip_install_share          ?= $(zip_prefix)/share/man

zip_bins                   = $(wildcard $(zip_builddir)/bin/*)
zip_libs                   = $(wildcard $(zip_builddir)/lib/lib*)
zip_includes               = $(wildcard $(zip_builddir)/include/*.h)


# Actual installation data.
ifeq ($(islib),)
zip_installfile            ?= $(zip_install_bin)
zip_check                  ?= $(shell which $(zip_package) 2> /dev/null)
else
zip_installfile            ?= $(zip_install_lib)
zip_check                  ?= $(abspath $(strip $(wildcard $(basename $(zip_installfile)).*)))
endif

zip_get                    ?= $(if $(zip_check),,zip_install)
zip                        ?= $(or $(zip_check), $(zip_installfile))


# FORCE data.
zip_force                  ?= $(if $(FORCE),zip_force,)
zip_status                 ?= $(if $(FORCE),zip_install,$(zip_get))
zip                        ?= $(if $(FORCE),$(zip_installfile),$(zip))


# Include required modules.
#include $(addprefix $(sk8_rootdir)/modules/, gcc.mk)

zip_deps_status             ?= $(if $(recursive),zip_deps,)
zip_deps                    ?= $(gcc_status)



############################## MAIN ############################################
# Usage.
zip_help:
	@$(ECHO) "\n\
	NAME\n\
		\n\
		zip_package - $(zip_package), version $(zip_version)\n\
		\n\
	SYNOPSIS\n\
		\n\
		$$ sk9 [make_option ...] [variable=value ...] <zip_target ...>\n\
		\n\
	DESCRIPTION\n\
		\n\
		This is an engine to download, configure, build, install and test packages\n\
		in a zip way. So, the package must be compliant with the GNU Coding\n\
		Standards in order to be handled by SK8.\n\
		Now, the package in question is '$(zip_distname)'.\n\
		\n\
	OPTIONS\n\
		\n\
		Targets:\n\
		\n\
		zip_help                   Print this help text.\n\
		zip_version                Print '$(zip_package)' package version.\n\
		zip_status                 Verifies '$(zip_package)' current installation status.\n\
		zip_run                    Run '$(zip_package)' with the arguments given in the 'args' variable.\n\
		zip_install                Install '$(zip_package)' binary in '\$$(prefix)/bin' directory.\n\
		zip_build                  Compile the '$(zip_package)' binary in the build directory.\n\
		zip_configure              Configure '$(zip_package)' build files for this system.\n\
		zip_tarball                Download the '$(zip_package)' tarball from its repository: $(zip_src)\n\
		zip_uninstall              Uninstall '$(zip_package)' from the '\$$(prefix)/bin' directory.\n\
		zip_clear                  Remove '$(zip_package)' tarball from tarball''s directory.\n\
		zip_purge                  Uninstall and clear '$(zip_package)' from the system.\n\
		zip_refresh                Uninstall and then re-install '$(zip_package)' again.\n\
		zip_deps                   Install a '$(zip_package)' dependency recursivelly as passed by the 'args' variable.\n\
		zip_test                   Test '$(zip_package)' installation.\n\
		zip_test_build_native      Test '$(zip_package)' build process, natively.\n\
		zip_test_build_conda       Test '$(zip_package)' build process in a new Conda environment.\n\
		zip_test_build_docker      Test '$(zip_package)' build process inside a vanilla Docker container.\n\
		zip_test_build_aws         Test '$(zip_package)' build process inside an AWS instance. NOT WORKING!!\n\
		zip_create_img             Create a Docker image for '$(zip_package)', based on '$(image)' image.\n\
		zip_create_dockerfile      Create a Dockerfile to build an image for '$(zip_package)'.\n\
		zip_create_module          Create a pre-filled SK8 module for '$(zip_package)' package.\n\
		\n\
		conda_run                      Run '$(zip_package)' in a new Conda environment. See 'conda_help' for more info.\n\
		docker_run                     Run '$(zip_package)' in a vanilla Docker image. See 'docker_help' for more info.\n\
		aws_run                        Run '$(zip_package)' in an AWS instance. See 'aws_help' for more info. NOT WORKING!!\n\
		\n\
		Variables:\n\
		\n\
		args                           String of arguments passed to '$(zip_package)' when running.               [ '' ]\n\
		prefix                         Path to the directory where to put the built '$(zip_packages)' binaries.   [ /usr/local ]\n\
		DESTDIR                        Path to an alternative staged installation directory.                          [ '' ]\n\
		workdir                        The current directory.                                                         [ . ]\n\
		stageddir                      Path to a staged directory for tarballs and package builds.                    [ ./.local ]\n\
		usermode                       Boolean to switch between local or system-wide installations paths.            [ '' ]\n\
		recursive                      Boolean to enable recursive dependency installations.                          [ '' ]\n\
		FORCE                          Boolean to force a fresh installation over an existent one.                    [ '' ]\n\
		\n\
		src                            URL of the '$(zip_package)' repository.                                    [ '$(zip_src)' ]\n\
		ext                            The tarball extension. Only for extensions other than '.tar.gz'.               [ '.tar.gz' ]\n\
		zip_package                The name of the package, derived from its tarball''s name.                     [ '$(zip_package)' ]\n\
		zip_version                The version of the package, derived from its tarball''s name.                  [ '$(zip_version)' ]\n\
		\n\
		Make options:\n\
		\n\
		\n\
		NOTE: Some packages cannot build zipally. So, user must test the chosen target first.\n\
		For a list of known zipally installable packages, please read the\n\
		'$(sk8_rootdir)/modules/modules_urls.txt' file.\n\
		\n\
		\n\
	COPYRIGHT\n\
		\n\
		$(sk8_package), version $(sk8_version) for GNU/Linux.\n\
		Copyright (C) Bards Agrotechnologies, 2011-2021.\n\
		\n\
	SEE ALSO\n\
		\n\
		Search for 'help', 'docker_help', 'conda_help' and 'miscellaneus'\n\
		for other sources of command line help or visit <$(basename $(sk8_source))>\n\
		for full documentation."

zip_manual:
	@$(ECHO) "\n$(zip_package) v$(zip_version) for GNU/Linux.\n\
	Please, see <$(zip_src)> for full documentation." >&2

zip_version:
	@$(ECHO) "\n\
	Package:    $(zip_package)\n\
	Version:    $(zip_version)\n\
	Id:         $(zip_id)\n\
	License:    $(zip_license)\n\
	\n\
	Status:     $(zip_status)\n\
	Source:     $(zip_src)\n\
	Installdir: $(installdir)\n\
	\n\
	Runned with $(sk8_package), version $(sk8_version) for GNU/Linux.\n\
	Copyright (C) Bards Agrotechnologies, 2011-2021."


zip_run: $(zip_status)
	@$(ECHO) "\nRunning '$(zip)' with arguments '$(args)' ..." >&2
	$(zip) $(args)
	@$(ECHO) "Done." >&2


# Verification.
zip_status: $(zip_status) # 'zip_install' or none.
	@$(ECHO) "\nSystem has '$(zip_package)' up and running." >&2


# Install.
zip_install: $(zip_installfile) # $(installdir)/$(zip_package)
	@$(ECHO) "\nSystem has '$(zip_package)' in PATH: '$<'." >&2

$(zip_installfile): $(zip_execfile) | $(installdir) # just copy the executable file to $(installdir).
	@$(ECHO) "\nInstalling '$(zip_package)' on directory '$(zip_prefix)' ..." >&2
	$(MAKE) -C $(zip_builddir) install DESTDIR=$(DESTDIR) prefix=$(zip_prefix) -f $(zip_builddir)/unix/Makefile -j4
	if [ -z "$$(perl -ne 'print if s#.*$(installdir).*#AA#g' <<< $${PATH})" ]; then \
		echo -e "export PATH=$(installdir):\$$PATH" >> ~/.bashrc; \
		source ~/.bashrc; \
		echo -e "WARNING: You may need to run 'source ~/.bashrc' to release the new \$$PATH."; \
	elif [ "$(zip)" != "$(zip_installfile)" ]; then \
		echo "WARNING: Previuos installation of '$(zip_package)' may be shadowing the new one in the \$$PATH."; \
	fi
	touch $@
	@$(ECHO) "Done." >&2


# Build.
zip_build: $(zip_execfile) # $(zip_builddir)/$(zip_package)

$(zip_execfile): $(zip_buildfile) | $(gcc_status) # a Makefile generates the executable file.
	@$(ECHO) "\nBuilding '$(zip_package)' on directory '$(zip_builddir)' ..." >&2
	$(MAKE) -C $(zip_builddir) -f $(zip_builddir)/unix/Makefile generic -j4
	touch $@
	@$(ECHO) "Done." >&2


# Configure.
zip_configure: $(zip_buildfile) # $(zip_builddir)/Makefile

$(zip_buildfile): zip_configure_cmd := ./configure --prefix=$(dir $(installdir)) $(zip_args)
$(zip_buildfile): $(zip_configurefile)
	@$(ECHO) "\nConfiguring '$(zip_package)' on directory '$(zip_builddir)' ..." >&2
	#cd $(zip_builddir); \
	#$(zip_configure_cmd)
	touch $@
	@$(ECHO) "Done." >&2

$(zip_configurefile): $(zip_tarball) | $(zip_builddir)
	@$(ECHO) "\nExtracting '$(zip_package)' tarball to directory '$(zip_builddir)' ..." >&2
	if [ "$(zip_ext)" == ".tar.gz" ]; then \
		tar -xzvf $< -C $(zip_builddir) --strip-components=1; \
	elif [ "$(zip_ext)" == ".tar.xz" ]; then \
		tar -xJvf $< -C $(zip_builddir) --strip-components=1; \
	else \
		tar -xvf $< -C $(zip_builddir) --strip-components=1; \
	fi
	#[ -s "$@" ] || { echo "ERROR: No file '$@' was extracted." >&2; exit 8; }
	touch $@
	@$(ECHO) "Done." >&2

$(zip_builddir):
	@$(ECHO) "\nCreating directory '$(zip_builddir)' ..." >&2
	mkdir -p $(zip_builddir)
	@$(ECHO) "Done." >&2


# Download.
zip_tarball: $(zip_tarball) # $(tarballsdir)/$(zip_tarball).

$(zip_tarball): $(zip_force) | $(tarballsdir) $(internet_status) $(zip_deps_status) # none or 'zip_deps'
	@$(ECHO) "\nDownloading '$(zip_package)' tarball from '$(zip_repository)' ..." >&2
	$(call downloader_cmd,$(dwlr),download,$(zip_repository),$@,)
	touch $@
	@$(ECHO) "Done." >&2

zip_deps: $(zip_deps)
	@$(ECHO) "\nRecursively installing dependecies for '$(zip_distname)' ..." >&2
	if [ -n "$(args)" ]; then \
		$(MAKE) -f $(firstword $(MAKEFILE_LIST)) recursive= $(args); \
	fi
	@$(ECHO) "Done." >&2

zip_force:
	@$(ECHO) "\nForce reinstallation of '$(zip_package)' on directory '$(installdir)'." >&2
	rm -f $(zip_tarball)
	@$(ECHO) "Done." >&2


zip_refresh: zip_uninstall
	if [ -z "$(usermode)" ]; then \
		$(MAKE) -f $(firstword $(MAKEFILE_LIST)) DESTDIR=$(DESTDIR) prefix=$(prefix) zip_install; \
	else \
		$(MAKE) -f $(firstword $(MAKEFILE_LIST)) DESTDIR=$(DESTDIR) prefix=$(prefix) zip_install usermode=1; \
	fi


# Uninstall and clean.
zip_purge: zip_uninstall zip_clean

zip_uninstall:
	@$(ECHO) "\nUninstalling '$(zip_package)' from directory '$(installdir)' ..." >&2
	[ -d "$(installdir)" ] || exit 1;
	if [ -f "$(zip_buildfile)" ]; then \
		$(MAKE) -C $(zip_builddir) uninstall DESTDIR=$(DESTDIR) prefix=$(dir $(installdir)) -j4; \
	else \
		rm -f $(zip_install_bin); \
		rm -f $(zip_install_include); \
		rm -f $(dir $(zip_install_lib))/pkgconfig/$(zip_package).pc; \
		rm -rf $(basename $(zip_install_lib)).*; \
		rm -rf $(zip_installfile)*; \
	fi
	@$(ECHO) "User must resolve \$$PATH in '~/.bashrc' file." >&2
	@$(ECHO) "Done." >&2

zip_clean:
	@$(ECHO) "\nCleaning '$(zip_package)' from staged directory '$(stageddir)' ..." >&2
	rm -rf $(tarballsdir)/$(zip_package)*
	@$(ECHO) "Done." >&2


# Unit Test.
zip_test: alt_name := $(or $(zip_alt_name),$(zip_package))
zip_test: $(zip_status)
	$(ECHO) "\nTesting installation of '$(zip_distname)' package ..." >&2
	if [ -f "$(zip_buildfile)_no" ]; then \
		if [ -n "$$(grep 'installcheck' $(zip_buildfile))" ]; then \
			$(MAKE) -C $(zip_builddir) installcheck; \
		else \
			$(MAKE) -C $(zip_builddir) check; \
		fi \
	else \
		if [ -n "$(islib)" ]; then \
			echo "int main() {}" | cc -xc - -L$(zip_installfile) -l$(alt_name) -o /dev/null; \
		else \
			$(zip_installfile) --version; \
		fi \
	fi >&2
	#$(call test_lib,$(alt_name),$(zip_prefix),$(args))
	$(ECHO) "Done." >&2


# Build tests.
args2 = $(MAKE) -f $(c_basedir)/SK8/$(notdir $(firstword $(MAKEFILE_LIST))) \
	usermode=1 \
	module=/home/lion/SK8/modules/$(this) \
	zip_src=$(zip_src) \
	zip_ext=$(zip_ext) \
	zip_package=$(zip_package) \
	zip_version=$(zip_version) \
	zip_alt_name=$(zip_alt_name) \
	zip_args='$(zip_args)' \
	recursive=$(recursive) \
	islib=$(islib) \
	args='$(or $(args),--version)' \
	zip_test


zip_test_native: override args := $(args2)
zip_test_native: native_test_build | $(srctestsdir)
	$(ECHO) "\nFinished native build test for '$(zip_package)' package." >&2


zip_test_conda: override args := $(args2)
zip_test_conda: conda_test_build | $(srctestsdir)
	$(ECHO) "\nFinished Conda build test for '$(zip_package)' package." >&2


zip_test_docker: override args := $(args2)
zip_test_docker: docker_test_build | $(srctestsdir)
	$(ECHO) "\nFinished Docker build test for '$(zip_package)' package." >&2


zip_test_all: zip_test_docker zip_test_conda zip_test_native


.PHONY: zip_help zip_version zip_manual zip_force



############################## DOCUMENTATION ###################################
# Documentation was already written in the targets above.
