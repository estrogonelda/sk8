# Info to download, configure, build, install and use 'kallisto' package.
kallisto_src                    := https://github.com/pachterlab/kallisto/releases/download/v0.46.1/kallisto_linux-v0.46.1.tar.gz
kallisto_ext                    := .tar.gz

kallisto_package                := kallisto
kallisto_version                := 0.46.1
kallisto_id                     := $(kallisto_package)-xxxxxx
kallisto_distname               := $(kallisto_package)-$(kallisto_version)
kallisto_license                := GPLv4

# Potential installation data.
kallisto_repository             := $(kallisto_src)
kallisto_tarball                := $(tarballsdir)/$(kallisto_distname)$(kallisto_ext)
kallisto_builddir               := $(tarballsdir)/$(kallisto_distname)
kallisto_configurefile          := $(kallisto_builddir)/configure
kallisto_buildfile              := $(kallisto_builddir)/Makefile
kallisto_execfile               := $(kallisto_builddir)/$(kallisto_package)
kallisto_installfile            := $(installdir)/$(kallisto_package)


# Actual installation data.
kallisto_has                    := $(shell which $(kallisto_package) 2> /dev/null)
kallisto_get                    := $(if $(kallisto_has),,kallisto_install)
kallisto                        := $(if $(kallisto_has),$(kallisto_has),$(kallisto_installfile))


# FORCE data.
kallisto_force                  := $(if $(FORCE),kallisto_force,)
kallisto_status                 := $(if $(FORCE),kallisto_install,$(kallisto_get))
kallisto                        := $(if $(FORCE),$(kallisto_installfile),$(kallisto))


# Include required modules.
#include $(addprefix $(sk8_rootdir)/modules/, gcc.mk)
kallisto_deps                   := $(if $(recursive),kallisto_deps,)
kallisto_depscheck              := $(gcc_status) $(kallisto_deps)


# Verification.
kallisto_status: $(kallisto_status) # 'kallisto_install' or none.
	@$(ECHO) "\nSystem has '$(kallisto_package)' up and running." >&2


# Install.
kallisto_install: $(kallisto_installfile) # $(installdir)/$(kallisto_package)
	@$(ECHO) "\nSystem has '$(kallisto_package)' in PATH: '$<'." >&2

$(kallisto_installfile): $(kallisto_execfile) # just copy the executable file to $(installdir).
	@$(ECHO) "\nInstalling '$(kallisto_package)' on directory '$(installdir)' ..." >&2
	#$(MAKE) -C $(kallisto_builddir) install DESTDIR=$(DESTDIR) prefix=$(dir $(installdir)) -j4
	install -t $(installdir) -m 755 $<
	if [ -z "$$(perl -ne 'print if s#.*$(installdir).*#AA#g' <<< $${PATH})" ]; then \
		echo -e "export PATH=$(installdir):\$$PATH" >> ~/.bashrc; \
		source ~/.bashrc; \
		echo -e "WARNING: You may need to run 'source ~/.bashrc' to release the new \$$PATH."; \
	elif [ "$(kallisto)" != "$(kallisto_installfile)" ]; then \
		echo "WARNING: Previuos installation of '$(kallisto_package)' may be shadowing the new one in the \$$PATH."; \
	fi
	touch $@
	@$(ECHO) "Done." >&2


# Build.
kallisto_build: $(kallisto_execfile) # $(kallisto_builddir)/$(kallisto_package)

$(kallisto_execfile): $(kallisto_buildfile) | $(gcc_status) # a Makefile generates the executable file.
	@$(ECHO) "\nBuilding '$(kallisto_package)' on directory '$(kallisto_builddir)' ..." >&2
	#$(MAKE) -C $(kallisto_builddir) -j4
	touch $@
	@$(ECHO) "Done." >&2


# Configure.
kallisto_configure: $(kallisto_buildfile) # $(kallisto_builddir)/Makefile

$(kallisto_buildfile): $(kallisto_configurefile)
	@$(ECHO) "\nConfiguring '$(kallisto_package)' on directory '$(kallisto_builddir)' ..." >&2
	touch $@
	@$(ECHO) "Done." >&2

$(kallisto_configurefile): $(kallisto_tarball)
	@$(ECHO) "\nExtracting '$(kallisto_package)' tarball to directory '$(kallisto_builddir)' ..." >&2
	mkdir -p $(kallisto_builddir)
	if [ "$(kallisto_ext)" == ".tar.gz" ]; then \
		tar -xzvf $< -C $(kallisto_builddir) --strip-components=1; \
	else \
		tar -xvf $< -C $(kallisto_builddir) --strip-components=1; \
	fi
	touch $@
	@$(ECHO) "Done." >&2


# Download.
kallisto_tarball: $(kallisto_tarball) # $(tarballsdir)/$(kallisto_tarball).

$(kallisto_tarball): $(internet_status) $(kallisto_force) | $(tarballsdir) $(kallisto_depscheck)
	@$(ECHO) "\nDownloading '$(kallisto_package)' tarball from '$(kallisto_repository)' ..." >&2
	wget -c $(kallisto_repository) -P $(tarballsdir) -O $@
	touch $@
	@$(ECHO) "Done." >&2

kallisto_deps:
	@$(ECHO) "\nRecursively installing dependecies for '$(kallisto_distname)' ..." >&2
	[ -n "$(args)" ] || ( echo "ERROR: Variable 'args' can not be empty." >&2; exit 1 )
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) recursive= $(args)
	@$(ECHO) "Done." >&2

kallisto_force:
	@$(ECHO) "\nForce reinstallation of '$(kallisto_package)' on directory '$(installdir)'." >&2
	rm -f $(kallisto_tarball)
	@$(ECHO) "Done." >&2


kallisto_refresh: kallisto_uninstall
	if [ -z "$(usermode)" ]; then \
		$(MAKE) -f $(firstword $(MAKEFILE_LIST)) DESTDIR=$(DESTDIR) prefix=$(prefix) kallisto_install; \
	else \
		$(MAKE) -f $(firstword $(MAKEFILE_LIST)) DESTDIR=$(DESTDIR) prefix=$(prefix) kallisto_install usermode=1; \
	fi


# Uninstall and clean.
kallisto_purge: kallisto_uninstall kallisto_clean

kallisto_uninstall:
	@$(ECHO) "\nUninstalling '$(kallisto_package)' from directory '$(installdir)' ..." >&2
	[ -d "$(installdir)" ] || exit 1;
	if [ -f "$(kallisto_buildfile)_no" ]; then \
		$(MAKE) -C $(kallisto_builddir) uninstall DESTDIR=$(DESTDIR) prefix=$(dir $(installdir)) -j4; \
	else \
		rm -rf $(kallisto_installfile)*; \
	fi
	@$(ECHO) "User must resolve \$$PATH in '~/.bashrc' file." >&2
	@$(ECHO) "Done." >&2

kallisto_clean:
	@$(ECHO) "\nCleaning '$(kallisto_package)' from staged directory '$(stageddir)' ..." >&2
	rm -rf $(tarballsdir)/$(kallisto_package)*
	@$(ECHO) "Done." >&2


# Test.
kallisto_test:
	@$(ECHO) "\nTesting '$(kallisto_package)' ..." >&2
	if [ -z "$$($(kallisto_package) --version 2> /dev/null)" ]; then \
		echo "Package '$(kallisto_package)' is not properly running." >&2; \
		exit 1; \
	else \
		echo "PASS!" \
	fi
	@$(ECHO) "Done." >&2

