############################## HEADER ##########################################
# SK8 module for melt package.
################################################################################



############################## PREAMBLE ########################################
# Non-standalone run verification.
this                           := melt.mk
ifeq ($(notdir $(firstword $(MAKEFILE_LIST))),$(this))
      $(error ERROR: Module '$(this)' cannot run in standalone mode, \
      it needs to be loaded by 'SK8')
endif

# Info to download, configure, build, install and use 'melt' package.
melt_src                    ?= $(src)
melt_ext                    ?= $(ext)

melt_package                ?= $(shell perl -ne 'print "@{[ (split(/-/, $$_))[0] ]}"' <<< "$(notdir $(melt_src))")
melt_version                ?= $(shell perl -pe 's/^$(melt_package)-(.*?)\$(melt_ext)/$$1/g' <<< "$(notdir $(melt_src))")
melt_id                     ?= $(melt_package)-$(shell echo $$RANDOM)
melt_license                ?= GPLv4
melt_distname               ?= $(melt_package)-$(melt_version)

# Potential installation data.
melt_repository             ?= $(melt_src)
melt_tarball                ?= $(tarballsdir)/$(melt_distname)$(melt_ext)
melt_builddir               ?= $(tarballsdir)/$(melt_distname)
melt_configurefile          ?= $(melt_builddir)/configure
melt_buildfile              ?= $(melt_builddir)/Makefile
melt_execfile               ?= $(melt_builddir)/$(melt_package)
melt_installfile            ?= $(installdir)/$(melt_package)


# Actual installation data.
melt_check                  ?= $(shell which $(melt_package) 2> /dev/null)
melt_get                    ?= $(if $(melt_check),,melt_install)
melt                        ?= $(or $(melt_check), $(melt_installfile))


# FORCE data.
melt_force                  ?= $(if $(FORCE),melt_force,)
melt_status                 ?= $(if $(FORCE),melt_install,$(melt_get))
melt                        ?= $(if $(FORCE),$(melt_installfile),$(melt))


# Include required modules.
#include $(addprefix $(sk8_rootdir)/modules/, gcc.mk)
melt_deps_check             ?= $(if $(recursive),melt_deps,)
melt_deps_status            ?= $(gcc_status) $(melt_deps_check)



############################## MAIN ############################################
# Usage.
melt_help:
	@$(ECHO) "\n\
	NAME\n\
		\n\
		melt_package - $(melt_package), version $(melt_version)\n\
		\n\
	SYNOPSIS\n\
		\n\
		$$ sk8 [make_option ...] [variable=value ...] <melt_target ...>\n\
		\n\
	DESCRIPTION\n\
		\n\
		This is an engine to download, configure, build, install and test packages\n\
		in a melt way. So, the package must be compliant with the GNU Coding\n\
		Standards in order to be handled by SK8.\n\
		Now, the package in question is '$(melt_distname)'.\n\
		\n\
	OPTIONS\n\
		\n\
		Targets:\n\
		\n\
		melt_help                   Print this help text.\n\
		melt_version                Print '$(melt_package)' package version.\n\
		melt_status                 Verifies '$(melt_package)' current installation status.\n\
		melt_run                    Run '$(melt_package)' with the arguments given in the 'args' variable.\n\
		melt_install                Install '$(melt_package)' binary in '\$$(prefix)/bin' directory.\n\
		melt_build                  Compile the '$(melt_package)' binary in the build directory.\n\
		melt_configure              Configure '$(melt_package)' build files for this system.\n\
		melt_tarball                Download the '$(melt_package)' tarball from its repository: $(melt_src)\n\
		melt_uninstall              Uninstall '$(melt_package)' from the '\$$(prefix)/bin' directory.\n\
		melt_clear                  Remove '$(melt_package)' tarball from tarball''s directory.\n\
		melt_purge                  Uninstall and clear '$(melt_package)' from the system.\n\
		melt_refresh                Uninstall and then re-install '$(melt_package)' again.\n\
		melt_deps                   Install a '$(melt_package)' dependency recursivelly as passed by the 'args' variable.\n\
		melt_test                   Test '$(melt_package)' installation.\n\
		melt_test_build_native      Test '$(melt_package)' build process, natively.\n\
		melt_test_build_conda       Test '$(melt_package)' build process in a new Conda environment.\n\
		melt_test_build_docker      Test '$(melt_package)' build process inside a vanilla Docker container.\n\
		melt_test_build_aws         Test '$(melt_package)' build process inside an AWS instance. NOT WORKING!!\n\
		melt_create_img             Create a Docker image for '$(melt_package)', based on '$(image)' image.\n\
		melt_create_dockerfile      Create a Dockerfile to build an image for '$(melt_package)'.\n\
		melt_create_module          Create a pre-filled SK8 module for '$(melt_package)' package.\n\
		\n\
		conda_run                      Run '$(melt_package)' in a new Conda environment. See 'conda_help' for more info.\n\
		docker_run                     Run '$(melt_package)' in a vanilla Docker image. See 'docker_help' for more info.\n\
		aws_run                        Run '$(melt_package)' in an AWS instance. See 'aws_help' for more info. NOT WORKING!!\n\
		\n\
		Variables:\n\
		\n\
		args                           String of arguments passed to '$(melt_package)' when running.               [ '' ]\n\
		prefix                         Path to the directory where to put the built '$(melt_packages)' binaries.   [ /usr/local ]\n\
		DESTDIR                        Path to an alternative staged installation directory.                          [ '' ]\n\
		workdir                        The current directory.                                                         [ . ]\n\
		stageddir                      Path to a staged directory for tarballs and package builds.                    [ ./.local ]\n\
		usermode                       Boolean to switch between local or system-wide installations paths.            [ '' ]\n\
		recursive                      Boolean to enable recursive dependency installations.                          [ '' ]\n\
		FORCE                          Boolean to force a fresh installation over an existent one.                    [ '' ]\n\
		\n\
		src                            URL of the '$(melt_package)' repository.                                    [ '$(melt_src)' ]\n\
		ext                            The tarball extension. Only for extensions other than '.tar.gz'.               [ '.tar.gz' ]\n\
		melt_package                The name of the package, derived from its tarball''s name.                     [ '$(melt_package)' ]\n\
		melt_version                The version of the package, derived from its tarball''s name.                  [ '$(melt_version)' ]\n\
		\n\
		Make options:\n\
		\n\
		\n\
		NOTE: Some packages cannot build meltally. So, user must test the chosen target first.\n\
		For a list of known meltally installable packages, please read the\n\
		'$(sk8_rootdir)/modules/modules_urls.txt' file.\n\
		\n\
		\n\
	COPYRIGHT\n\
		\n\
		$(sk8_package), version $(sk8_version) for GNU/Linux.\n\
		Copyright (C) Bards Agrotechnologies, 2011-2021.\n\
		\n\
	SEE ALSO\n\
		\n\
		Search for 'help', 'docker_help', 'conda_help' and 'miscellaneus'\n\
		for other sources of command line help or visit <$(basename $(sk8_source))>\n\
		for full documentation."

melt_manual:
	@$(ECHO) "\n$(melt_package) v$(melt_version) for GNU/Linux.\n\
	Please, see <$(melt_src)> for full documentation." >&2

melt_version:
	@$(ECHO) "\n\
	Package:    $(melt_package)\n\
	Version:    $(melt_version)\n\
	Id:         $(melt_id)\n\
	License:    $(melt_license)\n\
	\n\
	Status:     $(melt_status)\n\
	Source:     $(melt_src)\n\
	Installdir: $(installdir)\n\
	\n\
	Runned with $(sk8_package), version $(sk8_version) for GNU/Linux.\n\
	Copyright (C) Bards Agrotechnologies, 2011-2021."


melt_run: $(melt_status)
	@$(ECHO) "\nRunning '$(melt)' with arguments '$(args)' ..." >&2
	$(melt) $(args)
	@$(ECHO) "Done." >&2


# Verification.
melt_status: $(melt_status) # 'melt_install' or none.
	@$(ECHO) "\nSystem has '$(melt_package)' up and running." >&2


# Install.
melt_install: $(melt_installfile) # $(installdir)/$(melt_package)
	@$(ECHO) "\nSystem has '$(melt_package)' in PATH: '$<'." >&2

$(melt_installfile): $(melt_execfile) | $(installdir) # just copy the executable file to $(installdir).
	@$(ECHO) "\nInstalling '$(melt_package)' on directory '$(installdir)' ..." >&2
	$(MAKE) -C $(melt_builddir) install DESTDIR=$(DESTDIR) prefix=$(dir $(installdir)) -j4
	if [ -z "$$(perl -ne 'print if s#.*$(installdir).*#AA#g' <<< $${PATH})" ]; then \
		echo -e "export PATH=$(installdir):\$$PATH" >> ~/.bashrc; \
		source ~/.bashrc; \
		echo -e "WARNING: You may need to run 'source ~/.bashrc' to release the new \$$PATH."; \
	elif [ "$(melt)" != "$(melt_installfile)" ]; then \
		echo "WARNING: Previuos installation of '$(melt_package)' may be shadowing the new one in the \$$PATH."; \
	fi
	touch $@
	@$(ECHO) "Done." >&2


# Build.
melt_build: $(melt_execfile) # $(melt_builddir)/$(melt_package)

$(melt_execfile): $(melt_buildfile) | $(gcc_status) # a Makefile generates the executable file.
	@$(ECHO) "\nBuilding '$(melt_package)' on directory '$(melt_builddir)' ..." >&2
	$(MAKE) -C $(melt_builddir) -j4
	touch $@
	@$(ECHO) "Done." >&2


# Configure.
melt_configure: $(melt_buildfile) # $(melt_builddir)/Makefile

$(melt_buildfile): melt_configure_cmd := ./configure --prefix=$(dir $(installdir)) $(melt_args)
$(melt_buildfile): $(melt_configurefile)
	@$(ECHO) "\nConfiguring '$(melt_package)' on directory '$(melt_builddir)' ..." >&2
	cd $(melt_builddir); \
	$(melt_configure_cmd)
	touch $@
	@$(ECHO) "Done." >&2

$(melt_configurefile): $(melt_tarball) | $(melt_builddir)
	@$(ECHO) "\nExtracting '$(melt_package)' tarball to directory '$(melt_builddir)' ..." >&2
	if [ "$(melt_ext)" == ".tar.gz" ]; then \
		tar -xzvf $< -C $(melt_builddir) --strip-components=1; \
	elif [ "$(melt_ext)" == ".tar.xz" ]; then \
		tar -xJvf $< -C $(melt_builddir) --strip-components=1; \
	else \
		tar -xvf $< -C $(melt_builddir) --strip-components=1; \
	fi
	touch $@
	@$(ECHO) "Done." >&2

$(melt_builddir):
	@$(ECHO) "\nCreating directory '$(melt_builddir)' ..." >&2
	mkdir -p $(melt_builddir)
	@$(ECHO) "Done." >&2


# Download.
melt_tarball: $(melt_tarball) # $(tarballsdir)/$(melt_tarball).

$(melt_tarball): $(melt_force) | $(tarballsdir) $(internet_status) $(melt_deps_status)
	@$(ECHO) "\nDownloading '$(melt_package)' tarball from '$(melt_repository)' ..." >&2
	if [ "$(dwlr)" == "wget" ]; then \
		$(MAKE) -f $(firstword $(MAKEFILE_LIST)) $(mode)_run \
			args="$(dwlr) -c $(melt_repository) -O $@"; \
	elif [ "$(dwlr)" == "curl" ]; then \
		$(MAKE) -f $(firstword $(MAKEFILE_LIST)) $(mode)_run \
			args="$(dwlr) -L $(melt_repository) -o $@"; \
	elif [ "$(dwlr)" == "git" ]; then \
		$(MAKE) -f $(firstword $(MAKEFILE_LIST)) $(mode)_run \
			args="$(dwlr) clone $(melt_repository) $(melt_builddir)"; \
	else \
		echo "ERROR: Invalid mode '$(mode)'." >&2; \
		exit 7; \
	fi
	touch $@
	@$(ECHO) "Done." >&2

melt_deps:
	@$(ECHO) "\nRecursively installing dependecies for '$(melt_distname)' ..." >&2
	[ -n "$(args)" ] || ( echo "ERROR: Variable 'args' can not be empty." >&2; exit 7 )
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) recursive= $(args)
	@$(ECHO) "Done." >&2

melt_force:
	@$(ECHO) "\nForce reinstallation of '$(melt_package)' on directory '$(installdir)'." >&2
	rm -f $(melt_tarball)
	@$(ECHO) "Done." >&2


melt_refresh: melt_uninstall
	if [ -z "$(usermode)" ]; then \
		$(MAKE) -f $(firstword $(MAKEFILE_LIST)) DESTDIR=$(DESTDIR) prefix=$(prefix) melt_install; \
	else \
		$(MAKE) -f $(firstword $(MAKEFILE_LIST)) DESTDIR=$(DESTDIR) prefix=$(prefix) melt_install usermode=1; \
	fi


# Uninstall and clean.
melt_purge: melt_uninstall melt_clean

melt_uninstall:
	@$(ECHO) "\nUninstalling '$(melt_package)' from directory '$(installdir)' ..." >&2
	[ -d "$(installdir)" ] || exit 1;
	if [ -f "$(melt_buildfile)" ]; then \
		$(MAKE) -C $(melt_builddir) uninstall DESTDIR=$(DESTDIR) prefix=$(dir $(installdir)) -j4; \
	else \
		rm -rf $(melt_installfile)*; \
	fi
	@$(ECHO) "User must resolve \$$PATH in '~/.bashrc' file." >&2
	@$(ECHO) "Done." >&2

melt_clean:
	@$(ECHO) "\nCleaning '$(melt_package)' from staged directory '$(stageddir)' ..." >&2
	rm -rf $(tarballsdir)/$(melt_package)*
	@$(ECHO) "Done." >&2


# Unit Test.
melt_test: args ?= --version
melt_test:
	$(ECHO) "\nTesting package installation ...\n\
		Package:        $(melt_package)\n\
		Version:        $(melt_version)\n\
		Install Path:   $(melt)\n\
		Arguments:      '$(args)'\n\
		Runtime log:" >&2
	if $(melt) $(args); then                                                \
		echo "Package '$(melt_package)' PASSED the test.";                  \
	else                                                                       \
		err=$$?;                                                               \
		echo "ERROR: Package '$(melt_package)' wasn't properly installed."; \
		echo -e "Exit code: $$err.";                                           \
		exit 7;                                                                \
	fi >&2
	$(ECHO) "Done." >&2


melt_test_build_native: _install := $(basedir)/_install_$(melt_distname)
melt_test_build_native:
	@$(ECHO) "\nTesting native build of '$(melt_package)' module." >&2
	[ -n "$(_install)" ] || ( echo "ERROR: Variable '_install' is empty."; exit 7 )
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) \
		stageddir=$(_install) \
		usermode=1 \
		melt_src=$(melt_src) \
		melt_ext=$(melt_ext) \
		melt_package=$(melt_package) \
		melt_version=$(melt_version) \
		melt_args=$(melt_args) \
		recursive=$(recursive) args="$(args)" \
		melt_install melt_test
	[ -n "$(keep_install)" ] || rm -rf $(_install)
	@$(ECHO) "Done." >&2
	@$(ECHO) "\nFinished native build test for '$(melt_package)' module." >&2

melt_test_build_conda: conda_test_build
	@$(ECHO) "\nFinished Conda build test for '$(melt_package)' module." >&2

melt_test_build_docker: override args := make -f SK8/SK8.mk \
	module=$(addprefix SK8/modules/, $(notdir $(module))) \
	usermode=1 \
	melt_src=$(melt_src) \
	melt_ext=$(melt_ext) \
	melt_package=$(melt_package) \
	melt_version=$(melt_version) \
	melt_args=$(melt_args) \
	recursive=$(recursive) args="$(args)" \
	melt_install melt_test
melt_test_build_docker: docker_test_build
	@$(ECHO) "\nFinished Docker build test for '$(melt_package)' module." >&2


.PHONY: melt_help melt_manual melt_version melt_status


############################## DOCUMENTATION ###################################


OUTPUTS_DIR := $(outputsdir)/MELT_results

# Some commands.
JAR                            := $(stageddir)/bin/MELT.jar
PREPROCESS                     := java -Xmx2G -jar $(JAR) Preprocess
INDIV_ANALYSIS                 := java -Xmx16G -jar $(JAR) IndivAnalysis
GROUP_ANALYSIS                 := java -Xmx32G -jar $(JAR) GroupAnalysis
GENOTYPE                       := java -Xmx32G -jar $(JAR) Genotype
MAKE_VCF                       := java -Xmx2G -jar $(JAR) MakeVCF
CALU                           := java -Xmx2G -jar $(JAR) CALU
LINEU                          := java -Xmx2G -jar $(JAR) LINEU

BASE_DISCOVERY_DIR             := $(OUTPUTS_DIR)/results
HERVK_DISCOVERY_DIR            := $(BASE_DISCOVERY_DIR)/HERVK
LINE1_DISCOVERY_DIR            := $(BASE_DISCOVERY_DIR)/LINE1
ALU_DISCOVERY_DIR              := $(BASE_DISCOVERY_DIR)/ALU
SVA_DISCOVERY_DIR              := $(BASE_DISCOVERY_DIR)/SVA

MELT_PLACE                     := $(tarballsdir)/MELT-2.2.2
MEI_PLACE                      := $(MELT_PLACE)/me_refs/Hg38
HERVK_ZIP                      := $(MEI_PLACE)/HERVK_MELT.zip
LINE1_ZIP                      := $(MEI_PLACE)/LINE1_MELT.zip
ALU_ZIP                        := $(MEI_PLACE)/ALU_MELT.zip
SVA_ZIP                        := $(MEI_PLACE)/SVA_MELT.zip


# Folder for timestamps.
TMSTP                          := $(OUTPUTS_DIR)/tmstp
REFERENCE_BED                  := $(MELT_PLACE)/add_bed_files/Hg38/Hg38.genes.bed
REFERENCE_GTF                  := $(annotation_ref)
REFERENCE_GENOME               := $(genome_ref)
REFERENCE_GENOME_FASTA         := $(genome_ref)
BAM_COVERAGE                   := 40



INPUTS_l := $(inputfl)
INPUTS_FILENAME_l := $(notdir $(INPUTS_l))
INPUTS_BASENAME_l := $(basename $(notdir $(INPUTS_l)))
INPUTS_DIR_l := $(dir $(INPUT_l))
#INPUTS_DIR_BASENAME_l :=


#OUTPUTS_l :=
#OUTPUTS_FILENAME_l :=
#OUTPUTS_BASENAME_l :=
#OUTOUTS_DIR_l :=
OUTPUTS_DIR_BASENAME_l := $(addprefix $(OUTPUTS_DIR)/, $(INPUTS_BASENAME_l))
OUTPUTS_l := $(join $(OUTPUTS_DIR_BASENAME_l), $(addprefix /, $(INPUTS_FILENAME_l)))
OUTPUTS_l := $(srt)


# Derivated lists: '.bai', '.abnormal', '.disc', '.disc.bai' and others.
OUTPUTS_BAI_l := $(addsuffix .bai, $(OUTPUTS_l))
OUTPUTS_DISC_l := $(addsuffix .disc, $(OUTPUTS_l))
OUTPUTS_DISC_BAI_l := $(addsuffix .disc.bai, $(OUTPUTS_l))
OUTPUTS_DISC_FQ_l:= $(addsuffix .disc.fq, $(OUTPUTS_l))
OUTPUTS_ABNORMAL_l := $(addsuffix .abnormal, $(OUTPUTS_l))

# Timestamp lists, terminated in '_tml'.
# Lists for individual analysis.
OUTPUTS_HERVK_INDIV_tml := $(addsuffix .hervk.indiv.tmstp, $(OUTPUTS_l))
OUTPUTS_LINE1_INDIV_tml := $(addsuffix .line1.indiv.tmstp, $(OUTPUTS_l))
OUTPUTS_ALU_INDIV_tml := $(addsuffix .alu.indiv.tmstp, $(OUTPUTS_l))
OUTPUTS_SVA_INDIV_tml := $(addsuffix .sva.indiv.tmstp, $(OUTPUTS_l))

# Simple timestamps files for group analysis.
OUTPUTS_HERVK_GRP := $(TMSTP)/common.bam.hervk.group.tmstp
OUTPUTS_LINE1_GRP := $(TMSTP)/common.bam.line1.group.tmstp
OUTPUTS_ALU_GRP := $(TMSTP)/common.bam.alu.group.tmstp
OUTPUTS_SVA_GRP := $(TMSTP)/common.bam.sva.group.tmstp

# Lists for genotype analysis.
OUTPUTS_HERVK_GEN_tml := $(addsuffix .hervk.gen.tmstp, $(OUTPUTS_l))
OUTPUTS_LINE1_GEN_tml := $(addsuffix .line1.gen.tmstp, $(OUTPUTS_l))
OUTPUTS_ALU_GEN_tml := $(addsuffix .alu.gen.tmstp, $(OUTPUTS_l))
OUTPUTS_SVA_GEN_tml := $(addsuffix .sva.gen.tmstp, $(OUTPUTS_l))

# Simple VCF files.
OUTPUTS_HERVK_VCF := $(HERVK_DISCOVERY_DIR)/HERVK.final_comp.vcf
OUTPUTS_LINE1_VCF := $(LINE1_DISCOVERY_DIR)/LINE1.final_comp.vcf
OUTPUTS_ALU_VCF := $(ALU_DISCOVERY_DIR)/ALU.final_comp.vcf
OUTPUTS_SVA_VCF := $(SVA_DISCOVERY_DIR)/SVA.final_comp.vcf


melt_all: melt_vcf



############################## MAKE VCF ########################################

# Note: VCF are made for all BAMs together, so it can't run in parallel.

# Token target:
melt_vcf: hervk_vcf line1_vcf alu_vcf sva_vcf
hervk_vcf: $(OUTPUTS_HERVK_VCF)
line1_vcf: $(OUTPUTS_LINE1_VCF)
alu_vcf: $(OUTPUTS_ALU_VCF)
sva_vcf: $(OUTPUTS_SVA_VCF)

$(OUTPUTS_HERVK_VCF): %HERVK.final_comp.vcf: $(OUTPUTS_HERVK_GEN_tml)
	$(info )
	$(info $(CALL) Create VCF for HERVK from files $^.)
	$(MAKE_VCF) \
		-genotypingdir $(HERVK_DISCOVERY_DIR) \
		-h $(REFERENCE_GENOME) \
		-t $(HERVK_ZIP) \
		-w $(HERVK_DISCOVERY_DIR) \
		-p $(HERVK_DISCOVERY_DIR) \
		-o $(@D)

$(OUTPUTS_LINE1_VCF): %LINE1.final_comp.vcf: $(OUTPUTS_LINE1_GEN_tml)
	$(info )
	$(info $(CALL) Create VCF for LINE1 from files $^.)
	$(MAKE_VCF) \
		-genotypingdir $(LINE1_DISCOVERY_DIR) \
		-h $(REFERENCE_GENOME) \
		-t $(LINE1_ZIP) \
		-w $(LINE1_DISCOVERY_DIR) \
		-p $(LINE1_DISCOVERY_DIR) \
		-o $(@D)

$(OUTPUTS_ALU_VCF): %ALU.final_comp.vcf: $(OUTPUTS_ALU_GEN_tml)
	$(info )
	$(info $(CALL) Create VCF for ALU from files $^.)
	$(MAKE_VCF) \
		-genotypingdir $(ALU_DISCOVERY_DIR) \
		-h $(REFERENCE_GENOME) \
		-t $(ALU_ZIP) \
		-w $(ALU_DISCOVERY_DIR) \
		-p $(ALU_DISCOVERY_DIR) \
		-o $(@D)

$(OUTPUTS_SVA_VCF): %SVA.final_comp.vcf: $(OUTPUTS_SVA_GEN_tml)
	$(info )
	$(info $(CALL) Create VCF for SVA from files $^.)
	$(MAKE_VCF) \
		-genotypingdir $(SVA_DISCOVERY_DIR) \
		-h $(REFERENCE_GENOME) \
		-t $(SVA_ZIP) \
		-w $(SVA_DISCOVERY_DIR) \
		-p $(SVA_DISCOVERY_DIR) \
		-o $(@D)



############################## GENOTYPING ######################################

# Token target:
melt_gen: hervk_gen line1_gen alu_gen sva_gen
hervk_gen: $(OUTPUTS_HERVK_GEN_tml)
line1_gen: $(OUTPUTS_LINE1_GEN_tml)
alu_gen: $(OUTPUTS_ALU_GEN_tml)
sva_gen: $(OUTPUTS_SVA_GEN_tml)

$(OUTPUTS_HERVK_GEN_tml): %.bam.hervk.gen.tmstp: %.bam $(OUTPUTS_HERVK_GRP)
	$(info )
	$(info $(CALL) Genotyping HERVK in file $<.)
	$(GENOTYPE) \
		-bamfile $< \
		-h $(REFERENCE_GENOME) \
		-t $(HERVK_ZIP) \
		-w $(HERVK_DISCOVERY_DIR) \
		-p $(HERVK_DISCOVERY_DIR)
	touch $@

$(OUTPUTS_LINE1_GEN_tml): %.bam.line1.gen.tmstp: %.bam $(OUTPUTS_LINE1_GRP)
	$(info )
	$(info $(CALL) Genotyping LINE1 in file $<.)
	$(GENOTYPE) \
		-bamfile $< \
		-h $(REFERENCE_GENOME) \
		-t $(LINE1_ZIP) \
		-w $(LINE1_DISCOVERY_DIR) \
		-p $(LINE1_DISCOVERY_DIR)
	touch $@

$(OUTPUTS_ALU_GEN_tml): %.bam.alu.gen.tmstp: %.bam $(OUTPUTS_ALU_GRP)
	$(info )
	$(info $(CALL) Genotyping ALU in file $<.)
	$(GENOTYPE) \
		-bamfile $< \
		-h $(REFERENCE_GENOME) \
		-t $(ALU_ZIP) \
		-w $(ALU_DISCOVERY_DIR) \
		-p $(ALU_DISCOVERY_DIR)
	touch $@

$(OUTPUTS_SVA_GEN_tml): %.bam.sva.gen.tmstp: %.bam $(OUTPUTS_SVA_GRP)
	$(info )
	$(info $(CALL) Genotyping SVA in file $<.)
	$(GENOTYPE) \
		-bamfile $< \
		-h $(REFERENCE_GENOME) \
		-t $(SVA_ZIP) \
		-w $(SVA_DISCOVERY_DIR) \
		-p $(SVA_DISCOVERY_DIR)
	touch $@



############################## GROUP ANALYSES ##################################

# Note: Group analysis are made for all together, so it can't run in parallel.

# Token target:
melt_group: hervk_group line1_group alu_group sva_group
hervk_group: $(OUTPUTS_HERVK_GRP)
line1_group: $(OUTPUTS_LINE1_GRP)
alu_group: $(OUTPUTS_ALU_GRP)
sva_group: $(OUTPUTS_SVA_GRP)

$(OUTPUTS_HERVK_GRP): $(OUTPUTS_HERVK_INDIV_tml) | $(TMSTP)
	$(info )
	$(info $(CALL) Group analysis of HERVK on files $^.)
	$(GROUP_ANALYSIS) \
		-discoverydir $(HERVK_DISCOVERY_DIR) \
		-h $(REFERENCE_GENOME) \
		-t $(HERVK_ZIP) \
		-w $(HERVK_DISCOVERY_DIR) \
		-n $(REFERENCE_BED)
	touch $@

$(OUTPUTS_LINE1_GRP): $(OUTPUTS_LINE1_INDIV_tml) | $(TMSTP)
	$(info )
	$(info $(CALL) Group analysis of LINE1 on files $^.)
	$(GROUP_ANALYSIS) \
		-discoverydir $(LINE1_DISCOVERY_DIR) \
		-h $(REFERENCE_GENOME) \
		-t $(LINE1_ZIP) \
		-w $(LINE1_DISCOVERY_DIR) \
		-n $(REFERENCE_BED)
	touch $@

$(OUTPUTS_ALU_GRP): $(OUTPUTS_ALU_INDIV_tml) | $(TMSTP)
	$(info )
	$(info $(CALL) Group analysis of ALU on files $^.)
	$(GROUP_ANALYSIS) \
		-discoverydir $(ALU_DISCOVERY_DIR) \
		-h $(REFERENCE_GENOME) \
		-t $(ALU_ZIP) \
		-w $(ALU_DISCOVERY_DIR) \
		-n $(REFERENCE_BED)
	touch $@

$(OUTPUTS_SVA_GRP): $(OUTPUTS_SVA_INDIV_tml) | $(TMSTP)
	$(info )
	$(info $(CALL) Group analysis of SVA on files $^.)
	$(GROUP_ANALYSIS) \
		-discoverydir $(SVA_DISCOVERY_DIR) \
		-h $(REFERENCE_GENOME) \
		-t $(SVA_ZIP) \
		-w $(SVA_DISCOVERY_DIR) \
		-n $(REFERENCE_BED)
	touch $@



############################## INDIVIDUAL ANALYSES #############################

melt_indiv: hervk_indiv line1_indiv alu_indiv sva_indiv
hervk_indiv: $(OUTPUTS_HERVK_INDIV_tml)
line1_indiv: $(OUTPUTS_LINE1_INDIV_tml)
alu_indiv: $(OUTPUTS_ALU_INDIV_tml)
sva_indiv: $(OUTPUTS_SVA_INDIV_tml)

$(OUTPUTS_HERVK_INDIV_tml): %.bam.hervk.indiv.tmstp: %.bam %.bam.disc
	$(info )
	$(info $(CALL) Individual analysis of HERVK in file $<.)
	$(INDIV_ANALYSIS) \
		-bamfile $< \
		-h $(REFERENCE_GENOME) \
		-t $(HERVK_ZIP) \
		-w $(HERVK_DISCOVERY_DIR) \
		-c $(BAM_COVERAGE)
	touch $@

$(OUTPUTS_LINE1_INDIV_tml): %.bam.line1.indiv.tmstp: %.bam %.bam.disc
	$(info )
	$(info $(CALL) Individual analysis of LINE1 in file $<.)
	$(INDIV_ANALYSIS) \
		-bamfile $< \
		-h $(REFERENCE_GENOME) \
		-t $(LINE1_ZIP) \
		-w $(LINE1_DISCOVERY_DIR) \
		-c $(BAM_COVERAGE)
	touch $@

$(OUTPUTS_ALU_INDIV_tml): %.bam.alu.indiv.tmstp: %.bam %.bam.disc
	$(info )
	$(info $(CALL) Individual analysis of ALU in file $<.)
	$(INDIV_ANALYSIS) \
		-bamfile $< \
		-h $(REFERENCE_GENOME) \
		-t $(ALU_ZIP) \
		-w $(ALU_DISCOVERY_DIR) \
		-c $(BAM_COVERAGE)
	touch $@

$(OUTPUTS_SVA_INDIV_tml): %.bam.sva.indiv.tmstp: %.bam %.bam.disc
	$(info )
	$(info $(CALL) Individual analysis of SVA in file $<.)
	$(INDIV_ANALYSIS) \
		-bamfile $< \
		-h $(REFERENCE_GENOME) \
		-t $(SVA_ZIP) \
		-w $(SVA_DISCOVERY_DIR) \
		-c $(BAM_COVERAGE)
	touch $@


############################## PREPROCESSING ###################################

melt_preprocess: $(OUTPUTS_DISC_l)

# Preprocessing BAMs.
$(OUTPUTS_DISC_l): %.bam.disc: %.bam %.bam.bai | $(REFERENCE_GENOME_FASTA) $(melt_status)
	@$(ECHO) "\nCreating preprocess file '$@' from file '$<' ..." >&2
	$(PREPROCESS) -bamfile $< -h $(REFERENCE_GENOME_FASTA)
	@$(ECHO) "Done." >&2


melt_common: $(OUTPUTS_BAI_l) 

$(OUTPUTS_BAI_l): %.bam.bai: %.bam
	@$(ECHO) "\nCreating index file '$@' for BAM file '$< ' ..." >&2
	if [ -s "$$(readlink -f $<).bai" ]; then \
		ln -sf "$$(readlink -f $<).bai" $@; \
	else \
		samtools index -b -@4 $< $@; \
	fi
	@$(ECHO) "Done." >&2


melt_rec = $(filter %$(*F).bam, $(inputfl))

melt_links: $(OUTPUTS_l)

ifeq (fdgsfd,)
.SECONDEXPANSION:
$(OUTPUTS_l): %.bam: $$(melt_rec) | $(OUTPUT_DIR) $(samtools_status)
	@$(ECHO) "\nCreating link '$@' for input file '$<' ..." >&2
	mkdir -p $(*D)
	if [ "$$(samtools view -H $< 2> /dev/null | head -n1 | cut -f3)" = "SO:coordinate" ]; then \
		ln -sf "$$(readlink -f $<)" $@; \
	elif [ -s "$$(find $$(dirname $$(readlink -f $<)) -type f -name '*$(*F)*.sorted.bam')" ]; then \
		ln -sf "$$(find $$(dirname $$(readlink -f $<)) -type f -name '*$(*F)*.sorted.bam')" $@; \
	else \
		samtools sort -O BAM -m 1G -@4 $< -o $@; \
	fi
	@$(ECHO) "Done." >&2

endif


$(OUTPUTS_DIR) $(HERVK_DIR) $(LINE1_DIR) $(ALU_DIR) $(SVA_DIR) $(TMSTP):
	@$(ECHO) "\nCreating directory '$@' ..." >&2
	mkdir -p $@
	@$(ECHO) "Done." >&2



############################## DOCUMENTATION ###################################
# Documentation was already written in the targets above.
