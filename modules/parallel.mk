# Non-standalone run verification.
this                           := parallel.mk
ifeq ($(notdir $(firstword $(MAKEFILE_LIST))),$(this))
      $(error ERROR: Module '$(this)' cannot run in standalone mode, \
      it needs to be loaded by 'SK8')
endif

# Info to download, configure, build, install and use 'parallel' package.
parallel_src                    := https://ftp.gnu.org/gnu/parallel/parallel-20201022.tar.bz2
parallel_ext                    := .tar.bz2

parallel_package                := $(shell perl -ne 'print "@{[ (split(/-/, $$_))[0] ]}"' <<< "$(notdir $(parallel_src))")
parallel_version                := $(shell perl -pe 's/^$(parallel_package)-(.*?)\$(parallel_ext)/$$1/g' <<< "$(notdir $(parallel_src))")
parallel_id                     := $(parallel_package)-xxxxxx
parallel_distname               := $(parallel_package)-$(parallel_version)
parallel_license                := GPLv4

# Potential installation data.
parallel_repository             := $(parallel_src)
parallel_tarball                := $(tarballsdir)/$(parallel_distname)$(parallel_ext)
parallel_builddir               := $(tarballsdir)/$(parallel_distname)
parallel_configurefile          := $(parallel_builddir)/configure
parallel_buildfile              := $(parallel_builddir)/Makefile
parallel_execfile               := $(parallel_builddir)/$(parallel_package)
parallel_installfile            := $(installdir)/$(parallel_package)


# Actual installation data.
parallel_has                    := $(shell which $(parallel_package) 2> /dev/null)
parallel_get                    := $(if $(parallel_has),,parallel_install)
parallel                        := $(if $(parallel_has),$(parallel_has),$(parallel_installfile))


# FORCE data.
parallel_force                  := $(if $(FORCE),parallel_force,)
parallel_status                 := $(if $(FORCE),parallel_install,$(parallel_get))
parallel                        := $(if $(FORCE),$(parallel_installfile),$(parallel))


# Include required modules.
#include $(addprefix $(sk8_rootdir)/modules/, gcc.mk)
parallel_deps                   := $(if $(recursive),parallel_deps,)
parallel_depscheck              := $(gcc_status) $(parallel_deps)


# Usage.
parallel_run: $(parallel_status)
	@$(ECHO) "\nRunning '$(parallel)' with arguments '$(args)' ..." >&2
	$(parallel) $(args)
	@$(ECHO) "Done." >&2


# Verification.
parallel_status: $(parallel_status) # 'parallel_install' or none.
	@$(ECHO) "\nSystem has '$(parallel_package)' up and running." >&2


# Install.
parallel_install: $(parallel_installfile) # $(installdir)/$(parallel_package)
	@$(ECHO) "\nSystem has '$(parallel_package)' in PATH: '$<'." >&2

$(parallel_installfile): $(parallel_execfile) | $(installdir) # just copy the executable file to $(installdir).
	@$(ECHO) "\nInstalling '$(parallel_package)' on directory '$(installdir)' ..." >&2
	$(MAKE) -C $(parallel_builddir) install DESTDIR=$(DESTDIR) prefix=$(dir $(installdir)) -j4
	if [ -z "$$(perl -ne 'print if s#.*$(installdir).*#AA#g' <<< $${PATH})" ]; then \
		echo -e "export PATH=$(installdir):\$$PATH" >> ~/.bashrc; \
		source ~/.bashrc; \
		echo -e "WARNING: You may need to run 'source ~/.bashrc' to release the new \$$PATH."; \
	elif [ "$(parallel)" != "$(parallel_installfile)" ]; then \
		echo "WARNING: Previuos installation of '$(parallel_package)' may be shadowing the new one in the \$$PATH."; \
	fi
	touch $@
	@$(ECHO) "Done." >&2


# Build.
parallel_build: $(parallel_execfile) # $(parallel_builddir)/$(parallel_package)

$(parallel_execfile): $(parallel_buildfile) | $(gcc_status) # a Makefile generates the executable file.
	@$(ECHO) "\nBuilding '$(parallel_package)' on directory '$(parallel_builddir)' ..." >&2
	$(MAKE) -C $(parallel_builddir) -j4
	touch $@
	@$(ECHO) "Done." >&2


# Configure.
parallel_configure: $(parallel_buildfile) # $(parallel_builddir)/Makefile

$(parallel_buildfile): $(parallel_configurefile)
	@$(ECHO) "\nConfiguring '$(parallel_package)' on directory '$(parallel_builddir)' ..." >&2
	cd $(parallel_builddir); \
	./configure --prefix=$(dir $(installdir)) $(parallel_args)
	touch $@
	@$(ECHO) "Done." >&2

$(parallel_configurefile): $(parallel_tarball) | $(parallel_builddir)
	@$(ECHO) "\nExtracting '$(parallel_package)' tarball to directory '$(parallel_builddir)' ..." >&2
	if [ "$(parallel_ext)" == ".tar.gz" ]; then \
		tar -xzvf $< -C $(parallel_builddir) --strip-components=1; \
	else \
		tar -xvf $< -C $(parallel_builddir) --strip-components=1; \
	fi
	touch $@
	@$(ECHO) "Done." >&2

$(parallel_builddir):
	@$(ECHO) "\nCreating directory '$(parallel_builddir)' ..." >&2
	mkdir -p $(parallel_builddir)
	@$(ECHO) "Done." >&2


# Download.
parallel_tarball: $(parallel_tarball) # $(tarballsdir)/$(parallel_tarball).

$(parallel_tarball): $(parallel_force) | $(tarballsdir) $(internet_status) $(parallel_depscheck)
	@$(ECHO) "\nDownloading '$(parallel_package)' tarball from '$(parallel_repository)' ..." >&2
	wget -c $(parallel_repository) -P $(tarballsdir) -O $@
	touch $@
	@$(ECHO) "Done." >&2

parallel_deps:
	@$(ECHO) "\nRecursively installing dependecies for '$(parallel_distname)' ..." >&2
	[ -n "$(args)" ] || ( echo "ERROR: Variable 'args' can not be empty." >&2; exit 1 )
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) recursive= $(args)
	@$(ECHO) "Done." >&2

parallel_force:
	@$(ECHO) "\nForce reinstallation of '$(parallel_package)' on directory '$(installdir)'." >&2
	rm -f $(parallel_tarball)
	@$(ECHO) "Done." >&2


parallel_refresh: parallel_uninstall
	if [ -z "$(usermode)" ]; then \
		$(MAKE) -f $(firstword $(MAKEFILE_LIST)) DESTDIR=$(DESTDIR) prefix=$(prefix) parallel_install; \
	else \
		$(MAKE) -f $(firstword $(MAKEFILE_LIST)) DESTDIR=$(DESTDIR) prefix=$(prefix) parallel_install usermode=1; \
	fi


# Uninstall and clean.
parallel_purge: parallel_uninstall parallel_clean

parallel_uninstall:
	@$(ECHO) "\nUninstalling '$(parallel_package)' from directory '$(installdir)' ..." >&2
	[ -d "$(installdir)" ] || exit 1;
	if [ -f "$(parallel_buildfile)" ]; then \
		$(MAKE) -C $(parallel_builddir) uninstall DESTDIR=$(DESTDIR) prefix=$(dir $(installdir)) -j4; \
	else \
		rm -rf $(parallel_installfile)*; \
	fi
	@$(ECHO) "User must resolve \$$PATH in '~/.bashrc' file." >&2
	@$(ECHO) "Done." >&2

parallel_clean:
	@$(ECHO) "\nCleaning '$(parallel_package)' from staged directory '$(stageddir)' ..." >&2
	rm -rf $(tarballsdir)/$(parallel_package)*
	@$(ECHO) "Done." >&2


# Tests.
parallel_test:
	@$(ECHO) "\nTesting '$(parallel_package)' ..." >&2
	if [ -z "$$($(parallel_package) --version 2> /dev/null)" ]; then \
		echo "Package '$(parallel_package)' is not properly running."; \
		exit 1; \
	else \
		echo "PASS!"; \
	fi
	@$(ECHO) "Done." >&2

parallel_test_build_docker: override args := make -f SK8/SK8.mk module=$(addprefix SK8/modules/, $(notdir $(module))) usermode=1 \
	parallel_src=$(parallel_src) parallel_ext=$(parallel_ext) parallel_package=$(parallel_package) parallel_version=$(parallel_version) parallel_args=$(parallel_args) \
	recursive=$(recursive) args="$(args)" \
	parallel_install parallel_test
parallel_test_build_docker: docker_test_build
	@$(ECHO) "\nFinished Docker build test for '$(parallel_package)' module." >&2

