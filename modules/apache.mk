############################## APACHE PACKAGE ##################################

# Info to download, configure, build, install and use 'httpd' package.
httpd_source                 := https://downloads.apache.org//httpd/httpd-2.4.46.tar.gz
httpd_ext                    := .tar.gz

# Info to download, configure, build, install and use 'httpd' package.
httpd_package                := $(shell perl -lne '$$a = (split(/-/, $$_))[0]; print $$a' <<< "$(notdir $(httpd_source))")
httpd_version                := $(shell perl -MFile::Basename -lne '($$fl, $$pth, $$ext) = fileparse($$_, "$(httpd_ext)"); $$vr = (split(/-/, $$fl))[-1]; print $$vr' <<< "$(httpd_source)")
httpd_id                     := $(httpd_package)-xxxxxx
httpd_distname               := $(httpd_package)-$(httpd_version)
httpd_license                := GPLv4

# Potential installation data.
installdir                   := $(dir $(installdir))/apache/bin

httpd_dependecies            := bash make gcc
httpd_repository             := $(httpd_source)
httpd_tarball                := $(tarballsdir)/$(httpd_distname)$(httpd_ext)
httpd_builddir               := $(tarballsdir)/$(httpd_distname)
httpd_configurefile          := $(httpd_builddir)/configure
httpd_buildfile              := $(httpd_builddir)/Makefile
httpd_execfile               := $(httpd_builddir)/$(httpd_package)
httpd_installfile            := $(installdir)/$(httpd_package)


# Actual installation data.
httpd_has                    := which $(httpd_package) 2> /dev/null
httpd_get                    := $(if $(shell $(httpd_has)),,httpd_install)
httpd                        := $(if $(shell $(httpd_has)),$(shell $(httpd_has)),$(httpd_installfile))


# FORCE data.
httpd_force                  := $(if $(FORCE),httpd_force,)
httpd_status                 := $(if $(FORCE),httpd_install,$(httpd_get))
httpd                        := $(if $(FORCE),$(httpd_installfile),$(httpd))


# Verification.
httpd_status: $(httpd_status) # 'httpd_install' or none.
	@$(ECHO) "\nSystem has '$(httpd_package)' up and running."


# Install.
httpd_install: $(httpd_installfile) # $(installdir)/$(httpd_package)
	@$(ECHO) "\nSystem has '$(httpd_package)' in PATH: '$<'."

$(httpd_installfile): prefix := $(if $(DESTDIR),$(prefix),$(dir $(installdir)))
$(httpd_installfile): $(httpd_execfile) # just copy the executable file to $(installdir).
	@$(ECHO) "\nInstalling '$(httpd_package)' on directory '$(installdir)' ..."
	$(MAKE) -C $(httpd_builddir) install DESTDIR=$(DESTDIR) prefix=$(prefix) -j
	if [ -n "$(DESTDIR)" -o -n "$(usermode)" ]; then \
		if [ -z "$$(perl -F: -ane 'print if s#.*($(installdir)).*#$$1#g' ~/.bashrc)" ]; then \
			echo -e "export PATH=$(installdir):\$$PATH" >> ~/.bashrc; \
			. ~/.bashrc; \
			echo -e "WARNING: You may need to run 'source ~/.bashrc' to release the new \$$PATH." >&2; \
		fi \
	elif [ "$(httpd)" != "$(httpd_installfile)" ]; then \
		echo "WARNING: Previuos installation of '$(httpd_package)' may be shadowing the new one in the \$$PATH." >&2; \
	fi
	touch $@
	@$(ECHO) "Done."


# Build.
httpd_build: $(httpd_execfile) # $(httpd_builddir)/$(httpd_package)

$(httpd_execfile): $(httpd_buildfile) # a Makefile generates the executable file.
	@$(ECHO) "\nBuilding '$(httpd_package)' on directory '$(httpd_builddir)' ..."
	$(MAKE) -C $(httpd_builddir) -j
	touch $@
	@$(ECHO) "Done."


# Configure.
httpd_configure: $(httpd_buildfile) # $(httpd_builddir)/Makefile

$(httpd_buildfile): prefix := $(if $(DESTDIR),$(prefix),$(dir $(installdir)))
$(httpd_buildfile): $(httpd_configurefile)
	@$(ECHO) "\nConfiguring '$(httpd_package)' on directory '$(httpd_builddir)' ..."
	cd $(httpd_builddir); \
	./configure --prefix=$(prefix) --enable-so
	touch $@
	@$(ECHO) "Done."

$(httpd_configurefile): $(httpd_tarball)
	@$(ECHO) "\nExtracting '$(httpd_package)' tarball to directory '$(httpd_builddir)' ..."
	tar -xzvf $< -C $(tarballsdir)
	touch $@
	@$(ECHO) "Done."


# Download.
httpd_tarball: $(httpd_tarball) # $(tarballsdir)/$(httpd_tarball).

$(httpd_tarball): $(internet_get) $(httpd_force) | $(tarballsdir)
	@$(ECHO) "\nDownloading '$(httpd_package)' tarball from '$(httpd_repository)' ..."
	wget -c $(httpd_repository) -P $(tarballsdir) -O $@
	touch $@
	@$(ECHO) "Done."


httpd_refresh: httpd_uninstall httpd_install


# Uninstall and clean.
httpd_uninstall:
	@$(ECHO) "\nUninstalling '$(httpd_package)' from directory '$(installdir)' ...";
	[ -d "$(installdir)" ] || exit 1;
	#$(MAKE) -C $(httpd_builddir) uninstall DESTDIR=$(DESTDIR) prefix=$(prefix) -j
	# NOTE: Directory '$(isntalldir)' must be something '.../apache/bin'.
	find "$(installdir)/../.." -type d -name '*apache*' -exec rm -rf '{}' ';'
	echo "User may resolve \$$PATH in '~/.bashrc' file." >&2 
	@$(ECHO) "Done."

httpd_clean:
	@$(ECHO) "\nCleaning '$(httpd_package)' from staged directory '$(stageddir)' ...";
	rm -rf $(tarballsdir)/$(httpd_package)*
	@$(ECHO) "Done."

httpd_purge: httpd_uninstall httpd_clean


# FORCE!
httpd_force:
	@$(ECHO) "\nForce installation of '$(httpd_package)' on directory '$(installdir)'."
	-rm $(httpd_tarball)
	@$(ECHO) "Done."


# Test.
httpd_test:
	@$(ECHO) "\nTesting package '$(httpd_package)' ...";
	if [ -z "$$($(httpd_package) --version 2> /dev/null)" ]; then \
		echo "Package '$(httpd_package)' is not properly running." >&2; \
		exit 1; \
	else \
		echo "PASS!"; \
	fi
	@$(ECHO) "Done."



############################## DEBUG CODE ######################################

ifeq ($(dbg_httpd),yes)
$(info ****************************** DEBUG GENERIC PACK ******************************)
$(info Precursor variables for a 'httpd' package source.)
$(info workdir:                       $(workdir).)
$(info stageddir:                     $(stageddir).)
$(info tarballsdir:                   $(tarballsdir).)
$(info DESTDIR:                       $(DESTDIR).)
$(info installdir:                    $(installdir).)
$(info usermode:                      $(usermode).)
$(info src:                           $(src).)
$(info ext:                           $(ext).)
$(info )

$(info Info to download, configure, build, install and use 'httpd' package.)
$(info httpd_source:                $(httpd_source).)
$(info httpd_ext:                   $(httpd_ext).)
$(info )

$(info httpd_package:               $(httpd_package).)
$(info httpd_version:               $(httpd_version).)
$(info httpd_id:                    $(httpd_id).)
$(info httpd_distname:              $(httpd_distname).)
$(info httpd_license:               $(httpd_license).)
$(info )

$(info Potential installation data.)
$(info httpd_dependecies:           $(httpd_dependecies).)
$(info httpd_repository:            $(httpd_repository).)
$(info httpd_tarball:               $(httpd_tarball).)
$(info httpd_builddir:              $(httpd_builddir).)
$(info httpd_configurefile:         $(httpd_configurefile).)
$(info httpd_buildfile:             $(httpd_buildfile).)
$(info httpd_execfile:              $(httpd_execfile).)
$(info httpd_installfile:           $(httpd_installfile).)


$(info Actual installation data.)
$(info httpd_has:                   $(httpd_has).)
$(info httpd_get:                   $(httpd_get).)
$(info httpd:                       $(httpd).)
$(info )


$(info Status and FORCE data.)
$(info httpd_force:                 $(httpd_force).)
$(info httpd_status:                $(httpd_status).)
$(info ********************************************************************************)
endif
