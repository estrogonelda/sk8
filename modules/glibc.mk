# Non-standalone run verification.
this                           := glibc.mk
ifeq ($(notdir $(firstword $(MAKEFILE_LIST))),$(this))
      $(error ERROR: Module '$(this)' cannot run in standalone mode, \
      it needs to be loaded by 'SK8')
endif

# Info to download, configure, build, install and use 'glibc' package.
glibc_src                    := http://ftp.gnu.org/gnu/libc/glibc-2.33.tar.gz
glibc_ext                    := .tar.gz

glibc_package                := $(shell perl -ne 'print "@{[ (split(/-/, $$_))[0] ]}"' <<< "$(notdir $(glibc_src))")
glibc_version                := $(shell perl -pe 's/^$(glibc_package)-(.*?)\$(glibc_ext)/$$1/g' <<< "$(notdir $(glibc_src))")
glibc_id                     := $(glibc_package)-xxxxxx
glibc_distname               := $(glibc_package)-$(glibc_version)
glibc_license                := GPLv4

# Potential installation data.
glibc_repository             := $(glibc_src)
glibc_tarball                := $(tarballsdir)/$(glibc_distname)$(glibc_ext)
glibc_builddir               := $(tarballsdir)/$(glibc_distname)
glibc_configurefile          := $(glibc_builddir)/configure
glibc_buildfile              := $(glibc_builddir)/Makefile
glibc_execfile               := $(glibc_builddir)/$(glibc_package)
glibc_installfile            := $(installdir)/$(glibc_package)


# Actual installation data.
glibc_has                    := $(shell which $(glibc_package) 2> /dev/null)
glibc_get                    := $(if $(glibc_has),,glibc_install)
glibc                        := $(if $(glibc_has),$(glibc_has),$(glibc_installfile))


# FORCE data.
glibc_force                  := $(if $(FORCE),glibc_force,)
glibc_status                 := $(if $(FORCE),glibc_install,$(glibc_get))
glibc                        := $(if $(FORCE),$(glibc_installfile),$(glibc))


# Include required modules.
#include $(addprefix $(sk8_rootdir)/modules/, gcc.mk)
glibc_deps                   := $(if $(recursive),glibc_deps,)
glibc_depscheck              := $(gcc_status) $(glibc_deps)


# Usage.
glibc_help:
	@$(ECHO) "\n\
	NAME\n\
		\n\
		glibc_package - $(glibc_package), version $(glibc_version)\n\
		\n\
	SYNOPSIS\n\
		\n\
		$$ sk8 [make_option ...] [variable=value ...] <glibc_target ...>\n\
		\n\
	DESCRIPTION\n\
		\n\
		This is an engine to download, configure, build, install and test packages\n\
		in a glibc way. So, the package must be compliant with the GNU Coding\n\
		Standards in order to be handled by SK8.\n\
		Now, the package in question is '$(glibc_distname)'.\n\
		\n\
	OPTIONS\n\
		\n\
		Targets:\n\
		\n\
		glibc_help                   Print this help text.\n\
		glibc_version                Print '$(glibc_package)' package info.\n\
		glibc_status                 Verifies '$(glibc_package)' current installation status.\n\
		glibc_run                    Run '$(glibc_package)' with the arguments given in 'args' variable.\n\
		glibc_install                Install '$(glibc_package)' binary in '$$(prefix)/bin' directory.\n\
		glibc_build                  Compile the '$(glibc_package)' binary in the build directory.\n\
		glibc_configure              Configure '$(glibc_package)' build files for this system.\n\
		glibc_tarball                Download the '$(glibc_package)' tarball from its repository: $(glibc_src)\n\
		glibc_uninstall              Uninstall '$(glibc_package)' from the '$$(prefix)/bin' directory.\n\
		glibc_clear                  Remove '$(glibc_package)' tarball from tarball''s directory.\n\
		glibc_purge                  Uninstall and clear '$(glibc_package)' from the system.\n\
		glibc_refresh                Uninstall and then re-install '$(glibc_package)' again.\n\
		glibc_deps                   Install a '$(glibc_package)' dependency recursivelly as passed in 'args' variable.\n\
		glibc_test                   Test '$(glibc_package)' installation.\n\
		glibc_test_build             Test '$(glibc_package)' build process, natively.\n\
		glibc_test_build_conda       Test '$(glibc_package)' build process in a new Conda environment.\n\
		glibc_test_build_docker      Test '$(glibc_package)' build process inside a vanilla Docker container.\n\
		glibc_test_build_aws         Test '$(glibc_package)' build process inside an AWS instance. NOT WORKING!!\n\
		glibc_create_img             Create a Docker image for '$(glibc_package)', based on '$(image)' image.\n\
		glibc_create_dockerfile      Create a Dockerfile to build an image for '$(glibc_package)'.\n\
		glibc_create_module          Create a pre-filled SK8 module for '$(glibc_package)' package.\n\
		\n\
		conda_run                      Run '$(glibc_package)' in a new Conda environment. See 'conda_help' for more info.\n\
		docker_run                     Run '$(glibc_package)' in a vanilla Docker image. See 'docker_help' for more info.\n\
		aws_run                        Run '$(glibc_package)' in an AWS instance. See 'aws_help' for more info. NOT WORKING!!\n\
		\n\
		Variables:\n\
		\n\
		args                           String of arguments passed to '$(glibc_package)' when running.               [ '' ]\n\
		prefix                         Path to the directory where to put the built '$(glibc_packages)' binaries.   [ /usr/local ]\n\
		DESTDIR                        Path to an alternative staged installation directory.                          [ '' ]\n\
		workdir                        The current directory.                                                         [ . ]\n\
		stageddir                      Path to a staged directory for tarballs and package builds.                    [ ./.local ]\n\
		usermode                       Boolean to switch between local or system-wide installations paths.            [ '' ]\n\
		recursive                      Boolean to enable recursive dependency installations.                          [ '' ]\n\
		FORCE                          Boolean to force a fresh installation over an existent one.                    [ '' ]\n\
		\n\
		src                            URL of the '$(glibc_package)' repository.                                    [ '$(glibc_src)' ]\n\
		ext                            The tarball extension. Only for extensions other than '.tar.gz'.               [ '.tar.gz' ]\n\
		glibc_package                The name of the package, derived from its tarball''s name.                     [ '$(glibc_package)' ]\n\
		glibc_version                The version of the package, derived from its tarball''s name.                  [ '$(glibc_version)' ]\n\
		\n\
		Make options:\n\
		\n\
		\n\
		NOTE: Some packages cannot build glibcally. So, user must test the chosen target first.\n\
		For a list of known glibcally installable packages, pleas read the\n\
		'$(sk8_rootdir)/modules/modules_urls.txt' file.\n\
		\n\
		\n\
	COPYRIGHT\n\
		\n\
		$(sk8_package), version $(sk8_version) for GNU/Linux.\n\
		Copyright (C) Bards Agrotechnologies, 2011-2021.\n\
		\n\
	SEE ALSO\n\
		\n\
		Search for 'help', 'docker_help', 'conda_help' and 'miscellaneus'\n\
		for other sources of command line help or visit <$(basename $(sk8_url))>\n\
		for full documentation."

glibc_version:
	@$(ECHO) "\n\
	Package:    $(glibc_package)\n\
	Version:    $(glibc_version)\n\
	Id:         $(glibc_id)\n\
	License:    $(glibc_license)\n\
	\n\
	Status:     $(glibc_status)\n\
	Source:     $(glibc_src)\n\
	Installdir: $(installdir)\n\
	\n\
	Runned with $(sk8_package), version $(sk8_version) for GNU/Linux.\n\
	Copyright (C) Bards Agrotechnologies, 2011-2021."

glibc_run: $(glibc_status)
	@$(ECHO) "\nRunning '$(glibc)' with arguments '$(args)' ..." >&2
	$(glibc) $(args)
	@$(ECHO) "Done." >&2


# Verification.
glibc_status: $(glibc_status) # 'glibc_install' or none.
	@$(ECHO) "\nSystem has '$(glibc_package)' up and running." >&2


# Install.
glibc_install: $(glibc_installfile) # $(installdir)/$(glibc_package)
	@$(ECHO) "\nSystem has '$(glibc_package)' in PATH: '$<'." >&2

$(glibc_installfile): $(glibc_execfile) | $(installdir) # just copy the executable file to $(installdir).
	@$(ECHO) "\nInstalling '$(glibc_package)' on directory '$(installdir)' ..." >&2
	$(MAKE) -C $(glibc_builddir) install DESTDIR=$(DESTDIR) prefix=$(dir $(installdir)) -j4
	if [ -z "$$(perl -ne 'print if s#.*$(installdir).*#AA#g' <<< $${PATH})" ]; then \
		echo -e "export PATH=$(installdir):\$$PATH" >> ~/.bashrc; \
		source ~/.bashrc; \
		echo -e "WARNING: You may need to run 'source ~/.bashrc' to release the new \$$PATH."; \
	elif [ "$(glibc)" != "$(glibc_installfile)" ]; then \
		echo "WARNING: Previuos installation of '$(glibc_package)' may be shadowing the new one in the \$$PATH."; \
	fi
	touch $@
	@$(ECHO) "Done." >&2


# Build.
glibc_build: $(glibc_execfile) # $(glibc_builddir)/$(glibc_package)

$(glibc_execfile): $(glibc_buildfile) | $(gcc_status) # a Makefile generates the executable file.
	@$(ECHO) "\nBuilding '$(glibc_package)' on directory '$(glibc_builddir)' ..." >&2
	$(MAKE) -C $(glibc_builddir) -j4
	touch $@
	@$(ECHO) "Done." >&2


# Configure.
glibc_configure: $(glibc_buildfile) # $(glibc_builddir)/Makefile

$(glibc_buildfile): $(glibc_configurefile)
	@$(ECHO) "\nConfiguring '$(glibc_package)' on directory '$(glibc_builddir)' ..." >&2
	$< --prefix=$(dir $(installdir)) $(glibc_args)
	touch $@
	@$(ECHO) "Done." >&2

$(glibc_configurefile): $(glibc_tarball) | $(glibc_builddir)
	@$(ECHO) "\nExtracting '$(glibc_package)' tarball to directory '$(glibc_builddir)' ..." >&2
	if [ "$(glibc_ext)" == ".tar.gz" ]; then \
		tar -xzvf $< -C $(glibc_builddir) --strip-components=1; \
	elif [ "$(glibc_ext)" == ".tar.xz" ]; then \
		tar -xJvf $< -C $(glibc_builddir) --strip-components=1; \
	else \
		tar -xvf $< -C $(glibc_builddir) --strip-components=1; \
	fi
	touch $@
	@$(ECHO) "Done." >&2

$(glibc_builddir):
	@$(ECHO) "\nCreating directory '$(glibc_builddir)' ..." >&2
	mkdir -p $(glibc_builddir)
	@$(ECHO) "Done." >&2


# Download.
glibc_tarball: $(glibc_tarball) # $(tarballsdir)/$(glibc_tarball).

$(glibc_tarball): $(glibc_force) | $(tarballsdir) $(internet_status) $(glibc_depscheck)
	@$(ECHO) "\nDownloading '$(glibc_package)' tarball from '$(glibc_repository)' ..." >&2
	wget -c $(glibc_repository) -P $(tarballsdir) -O $@
	touch $@
	@$(ECHO) "Done." >&2

glibc_deps:
	@$(ECHO) "\nRecursively installing dependecies for '$(glibc_distname)' ..." >&2
	[ -n "$(args)" ] || ( echo "ERROR: Variable 'args' can not be empty." >&2; exit 1 )
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) recursive= $(args)
	@$(ECHO) "Done." >&2

glibc_force:
	@$(ECHO) "\nForce reinstallation of '$(glibc_package)' on directory '$(installdir)'." >&2
	rm -f $(glibc_tarball)
	@$(ECHO) "Done." >&2


glibc_refresh: glibc_uninstall
	if [ -z "$(usermode)" ]; then \
		$(MAKE) -f $(firstword $(MAKEFILE_LIST)) DESTDIR=$(DESTDIR) prefix=$(prefix) glibc_install; \
	else \
		$(MAKE) -f $(firstword $(MAKEFILE_LIST)) DESTDIR=$(DESTDIR) prefix=$(prefix) glibc_install usermode=1; \
	fi


# Uninstall and clean.
glibc_purge: glibc_uninstall glibc_clean

glibc_uninstall:
	@$(ECHO) "\nUninstalling '$(glibc_package)' from directory '$(installdir)' ..." >&2
	[ -d "$(installdir)" ] || exit 1;
	if [ -f "$(glibc_buildfile)" ]; then \
		$(MAKE) -C $(glibc_builddir) uninstall DESTDIR=$(DESTDIR) prefix=$(dir $(installdir)) -j4; \
	else \
		rm -rf $(glibc_installfile)*; \
	fi
	@$(ECHO) "User must resolve \$$PATH in '~/.bashrc' file." >&2
	@$(ECHO) "Done." >&2

glibc_clean:
	@$(ECHO) "\nCleaning '$(glibc_package)' from staged directory '$(stageddir)' ..." >&2
	rm -rf $(tarballsdir)/$(glibc_package)*
	@$(ECHO) "Done." >&2


# Tests.
glibc_test:
	@$(ECHO) "\nTesting '$(glibc_package)' ..." >&2
	if [ -n "$(is_lib)" ]; then \
		if [ ! -s "$$(find $(dir $(installdir))/lib -type f -name '$(glibc_package)*')" ]; then \
			echo "Library '$(glibc_package)' is not properly installed."; \
			exit 7; \
		fi \
	else \
		if [ -z "$$($(glibc_package) --version 2> /dev/null)" ]; then \
			echo "Package '$(glibc_package)' is not properly running."; \
			exit 7; \
		fi \
	fi
	@$(ECHO) "Package '$(glibc_package)' test status: PASS!"
	@$(ECHO) "Done." >&2


glibc_test_build_native: _install := $(basedir)/_install_$(glibc_distname)
glibc_test_build_native:
	@$(ECHO) "\nTesting native build of '$(glibc_package)' module." >&2
	[ -n "$(_install)" ] || ( echo "ERROR: Variable '_install' is empty."; exit 7 )
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) \
		stageddir=$(_install) \
		usermode=1 \
		glibc_src=$(glibc_src) \
		glibc_ext=$(glibc_ext) \
		glibc_package=$(glibc_package) \
		glibc_version=$(glibc_version) \
		glibc_args=$(glibc_args) \
		recursive=$(recursive) args="$(args)" \
		glibc_install glibc_test
	[ -n "$(keep_install)" ] || rm -rf $(_install)
	@$(ECHO) "Done." >&2
	@$(ECHO) "\nFinished native build test for '$(glibc_package)' module." >&2

glibc_test_build_conda: conda_test_build
	@$(ECHO) "\nFinished Conda build test for '$(glibc_package)' module." >&2

glibc_test_build_docker: override args := make -f SK8/SK8.mk \
	module=$(addprefix SK8/modules/, $(notdir $(module))) \
	usermode=1 \
	glibc_src=$(glibc_src) \
	glibc_ext=$(glibc_ext) \
	glibc_package=$(glibc_package) \
	glibc_version=$(glibc_version) \
	glibc_args=$(glibc_args) \
	recursive=$(recursive) args="$(args)" \
	glibc_install glibc_test
glibc_test_build_docker: docker_test_build
	@$(ECHO) "\nFinished Docker build test for '$(glibc_package)' module." >&2

