# Non-standalone run verification.
this                           := bowtie2.mk
ifeq ($(notdir $(firstword $(MAKEFILE_LIST))),$(this))
      $(error ERROR: Module '$(this)' cannot run in standalone mode, \
      it needs to be loaded by 'SK8')
endif

# Info to download, configure, build, install and use 'bowtie2' package.
bowtie2_src                    := https://downloads.sourceforge.net/project/bowtie-bio/bowtie2/2.4.2/bowtie2-2.4.2-source.zip
bowtie2_ext                    := -source.zip

bowtie2_package                := $(shell perl -ne 'print "@{[ (split(/-/, $$_))[0] ]}"' <<< "$(notdir $(bowtie2_src))")
bowtie2_version                := $(shell perl -pe 's/^$(bowtie2_package)-(.*?)\$(bowtie2_ext)/$$1/g' <<< "$(notdir $(bowtie2_src))")
bowtie2_id                     := $(bowtie2_package)-xxxxxx
bowtie2_distname               := $(bowtie2_package)-$(bowtie2_version)
bowtie2_license                := GPLv4

# Potential installation data.
bowtie2_repository             := $(bowtie2_src)
bowtie2_tarball                := $(tarballsdir)/$(bowtie2_distname)$(bowtie2_ext)
bowtie2_builddir               := $(tarballsdir)/$(bowtie2_distname)
bowtie2_configurefile          := $(bowtie2_builddir)/configure
bowtie2_buildfile              := $(bowtie2_builddir)/Makefile
bowtie2_execfile               := $(bowtie2_builddir)/$(bowtie2_package)
bowtie2_installfile            := $(installdir)/$(bowtie2_package)


# Actual installation data.
bowtie2_has                    := $(shell which $(bowtie2_package) 2> /dev/null)
bowtie2_get                    := $(if $(bowtie2_has),,bowtie2_install)
bowtie2                        := $(if $(bowtie2_has),$(bowtie2_has),$(bowtie2_installfile))


# FORCE data.
bowtie2_force                  := $(if $(FORCE),bowtie2_force,)
bowtie2_status                 := $(if $(FORCE),bowtie2_install,$(bowtie2_get))
bowtie2                        := $(if $(FORCE),$(bowtie2_installfile),$(bowtie2))


# Include required modules.
#include $(addprefix $(sk8_rootdir)/modules/, gcc.mk)
bowtie2_deps                   := $(if $(recursive),bowtie2_deps,)
bowtie2_depscheck              := $(gcc_status) $(bowtie2_deps)


# Usage.
bowtie2_run: | $(bowtie2_status)
	@$(ECHO) "\nRunning '$(bowtie2)' with arguments '$(args)' ..." >&2
	$(bowtie2) $(args)
	@$(ECHO) "Done." >&2


# Verification.
bowtie2_status: $(bowtie2_status) # 'bowtie2_install' or none.
	@$(ECHO) "\nSystem has '$(bowtie2_package)' up and running." >&2


# Install.
bowtie2_install: $(bowtie2_installfile) # $(installdir)/$(bowtie2_package)
	@$(ECHO) "\nSystem has '$(bowtie2_package)' in PATH: '$<'." >&2

$(bowtie2_installfile): $(bowtie2_execfile) | $(installdir) # just copy the executable file to $(installdir).
	@$(ECHO) "\nInstalling '$(bowtie2_package)' on directory '$(installdir)' ..." >&2
	#$(MAKE) -C $(bowtie2_builddir) install DESTDIR=$(DESTDIR) prefix=$(dir $(installdir)) -j4
	cd $(bowtie2_builddir); \
	install -t $(installdir) -m 755 $< \
		bowtie2-build-s bowtie2-build-l bowtie2-align-s bowtie2-align-l \
		bowtie2-inspect-s bowtie2-inspect-l bowtie2-inspect bowtie2-build
	mkdir -p $(dir $(installdir))/lib $(dir $(installdir))/include
	cp -r $(bowtie2_builddir)/.tmp/lib/* $(dir $(installdir))/lib
	cp -r $(bowtie2_builddir)/.tmp/include/* $(dir $(installdir))/include
	if [ -z "$$(perl -ne 'print if s#.*$(installdir).*#AA#g' <<< $${PATH})" ]; then \
		echo -e "export PATH=$(installdir):\$$PATH" >> ~/.bashrc; \
		source ~/.bashrc; \
		echo -e "WARNING: You may need to run 'source ~/.bashrc' to release the new \$$PATH."; \
	elif [ "$(bowtie2)" != "$(bowtie2_installfile)" ]; then \
		echo "WARNING: Previuos installation of '$(bowtie2_package)' may be shadowing the new one in the \$$PATH."; \
	fi
	touch $@
	@$(ECHO) "Done." >&2


# Build.
bowtie2_build: $(bowtie2_execfile) # $(bowtie2_builddir)/$(bowtie2_package)

$(bowtie2_execfile): $(bowtie2_buildfile) | $(gcc_status) # a Makefile generates the executable file.
	@$(ECHO) "\nBuilding '$(bowtie2_package)' on directory '$(bowtie2_builddir)' ..." >&2
	$(MAKE) -C $(bowtie2_builddir) static-libs -j4
	$(MAKE) -C $(bowtie2_builddir) STATIC_BUILD=1 -j4
	touch $@
	@$(ECHO) "Done." >&2


# Configure.
bowtie2_configure: $(bowtie2_buildfile) # $(bowtie2_builddir)/Makefile

$(bowtie2_buildfile): $(bowtie2_configurefile)
	@$(ECHO) "\nConfiguring '$(bowtie2_package)' on directory '$(bowtie2_builddir)' ..." >&2
	#cd $(bowtie2_builddir); \
	#./configure --prefix=$(dir $(installdir)) $(bowtie2_args)
	touch $@
	@$(ECHO) "Done." >&2

$(bowtie2_configurefile): $(bowtie2_tarball)
	@$(ECHO) "\nExtracting '$(bowtie2_package)' tarball to directory '$(bowtie2_builddir)' ..." >&2
	unzip -d $(tarballsdir) $<
	touch $@
	@$(ECHO) "Done." >&2

$(bowtie2_builddir):
	mkdir -p $(bowtie2_builddir)


# Download.
bowtie2_tarball: $(bowtie2_tarball) # $(tarballsdir)/$(bowtie2_tarball).

$(bowtie2_tarball): $(internet_status) $(bowtie2_force) | $(tarballsdir) $(bowtie2_depscheck)
	@$(ECHO) "\nDownloading '$(bowtie2_package)' tarball from '$(bowtie2_repository)' ..." >&2
	wget -c $(bowtie2_repository) -P $(tarballsdir) -O $@
	touch $@
	@$(ECHO) "Done." >&2

bowtie2_deps:
	@$(ECHO) "\nRecursively installing dependecies for '$(bowtie2_distname)' ..." >&2
	[ -n "$(args)" ] || ( echo "ERROR: Variable 'args' can not be empty." >&2; exit 1 )
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) recursive= $(args)
	@$(ECHO) "Done." >&2

bowtie2_force:
	@$(ECHO) "\nForce reinstallation of '$(bowtie2_package)' on directory '$(installdir)'." >&2
	rm -f $(bowtie2_tarball)
	@$(ECHO) "Done." >&2


bowtie2_refresh: bowtie2_uninstall
	if [ -z "$(usermode)" ]; then \
		$(MAKE) -f $(firstword $(MAKEFILE_LIST)) DESTDIR=$(DESTDIR) prefix=$(prefix) bowtie2_install; \
	else \
		$(MAKE) -f $(firstword $(MAKEFILE_LIST)) DESTDIR=$(DESTDIR) prefix=$(prefix) bowtie2_install usermode=1; \
	fi


# Uninstall and clean.
bowtie2_purge: bowtie2_uninstall bowtie2_clean

bowtie2_uninstall:
	@$(ECHO) "\nUninstalling '$(bowtie2_package)' from directory '$(installdir)' ..." >&2
	[ -d "$(installdir)" ] || exit 1;
	if [ -f "$(bowtie2_buildfile)_no" ]; then \
		$(MAKE) -C $(bowtie2_builddir) uninstall DESTDIR=$(DESTDIR) prefix=$(dir $(installdir)) -j4; \
	else \
		cd $(installdir); \
		rm -rf bowtie2 \
		bowtie2-build-s bowtie2-build-l bowtie2-align-s bowtie2-align-l \
		bowtie2-inspect-s bowtie2-inspect-l bowtie2-inspect bowtie2-build; \
	fi
	@$(ECHO) "User must resolve \$$PATH in '~/.bashrc' file." >&2
	@$(ECHO) "Done." >&2

bowtie2_clean:
	@$(ECHO) "\nCleaning '$(bowtie2_package)' from staged directory '$(stageddir)' ..." >&2
	rm -rf $(tarballsdir)/$(bowtie2_package)*
	@$(ECHO) "Done." >&2


# Tests.
bowtie2_test:
	@$(ECHO) "\nTesting '$(bowtie2_package)' ..." >&2
	if [ -z "$$($(bowtie2_package) --version 2> /dev/null)" ]; then \
		echo "Package '$(bowtie2_package)' is not properly running."; \
		exit 1; \
	else \
		echo "PASS!"; \
	fi
	@$(ECHO) "Done." >&2


bowtie2_test_native_build: usermode := 1
bowtie2_test_native_build: installdir := $(stageddir)/_test_build
bowtie2_test_native_build: bowtie2_install

bowtie2_test_build: args := make -f SK8/SK8.mk usermode=1 bowtie2_src=$(bowtie2_src) bowtie2_ext=$(bowtie2_ext) bowtie2_package=$(bowtie2_package) bowtie2_version=$(bowtie2_version) recursive=$(recursive) \
	bowtie2_install bowtie2_test
bowtie2_test_build: docker_test_build
	@$(ECHO) "\nFinished Docker build test for '$(bowtie2_package)' module." >&2
