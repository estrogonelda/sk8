############################## GENERIC PACKAGE #################################

# Info to download, configure, build, install and use 'salmon' package.
salmon_source                  := https://github.com/COMBINE-lab/salmon/archive/v1.3.0.tar.gz
salmon_source                  := https://github.com/COMBINE-lab/salmon/releases/download/v1.3.0/salmon-1.3.0_linux_x86_64.tar.gz
salmon_ext                     := .tar.gz

# Info to download, configure, build, install and use 'salmon' package.
salmon_package                 := $(shell perl -ne 'print "@{[ (split(/-/, $$_))[0] ]}"' <<< "$(notdir $(salmon_source))")
salmon_version                 := 1.3.0
salmon_id                      := $(salmon_package)-xxxxxx
salmon_distname                := $(salmon_package)-$(salmon_version)
salmon_license                 := GPLv4

# Potential installation data.
salmon_dependecies             :=
salmon_repository              := $(salmon_source)
salmon_tarball                 := $(tarballsdir)/$(salmon_distname)$(salmon_ext)
salmon_builddir                := $(tarballsdir)/$(salmon_distname)
salmon_configurefile           := $(salmon_builddir)/configure
salmon_buildfile               := $(salmon_builddir)/Makefile
salmon_execfile                := $(salmon_builddir)/bin/$(salmon_package)
salmon_installfile             := $(installdir)/$(salmon_package)


# Actual installation data.
salmon_has                     := which $(salmon_package) 2> /dev/null
salmon_get                     := $(if $(shell $(salmon_has)),,salmon_install)
salmon                         := $(if $(shell $(salmon_has)),$(shell $(salmon_has)),$(salmon_installfile))


# FORCE data.
salmon_force                   := $(if $(FORCE),salmon_force,)
salmon_status                  := $(if $(FORCE),salmon_install,$(salmon_get))
salmon                         := $(if $(FORCE),$(salmon_installfile),$(salmon))


# Verification.
salmon_status: $(salmon_status) # 'salmon_install' or none.
	@$(ECHO) "\nSystem has '$(salmon_package)' up and running."


# Install.
salmon_install: $(salmon_installfile) # $(installdir)/$(salmon_package)
	@$(ECHO) "\nSystem has '$(salmon_package)' in PATH: '$<'."

$(salmon_installfile): prefix := $(if $(DESTDIR),$(prefix),$(dir $(installdir)))
$(salmon_installfile): $(salmon_execfile) # just copy the executable file to $(installdir).
	@$(ECHO) "\nInstalling '$(salmon_package)' on directory '$(installdir)' ..."
	#$(MAKE) -C $(salmon_builddir) install DESTDIR=$(DESTDIR) prefix=$(prefix) -j
	install -d $(installdir)
	install -m 0755 -D $< $(installdir)
	install -d $(prefix)/lib
	install -m 0755 -D $(dir $<)/../lib/* $(prefix)/lib
	if [ -z "$$(perl -ne 'print if s#.*($(installdir)).*#$$1#g' <<< $${PATH})" ]; then \
		echo -e "export PATH=$(installdir):\$$PATH" >> ~/.bashrc; \
		source ~/.bashrc; \
		echo -e "WARNING: You may need to run 'source ~/.bashrc' to release the new \$$PATH." >&2; \
	elif [ "$(salmon)" != "$(salmon_installfile)" ]; then \
		echo "WARNING: Previuos installation of '$(salmon_package)' may be shadowing the new one in the \$$PATH." >&2; \
	fi
	touch $@
	@$(ECHO) "Done."


# Build.
salmon_build: $(salmon_execfile) # $(salmon_builddir)/$(salmon_package)

$(salmon_execfile): $(salmon_buildfile) # a Makefile generates the executable file.
	@$(ECHO) "\nBuilding '$(salmon_package)' on directory '$(salmon_builddir)' ..."
	#$(MAKE) -C $(salmon_builddir) -j
	touch $@
	@$(ECHO) "Done."


# Configure.
salmon_configure: $(salmon_buildfile) # $(salmon_builddir)/Makefile

$(salmon_buildfile): $(salmon_configurefile)
	@$(ECHO) "\nConfiguring '$(salmon_package)' on directory '$(salmon_builddir)' ..."
	#cd $(salmon_builddir); \
	#./configure
	touch $@
	@$(ECHO) "Done."

$(salmon_configurefile): $(salmon_tarball)
	@$(ECHO) "\nExtracting '$(salmon_package)' tarball to directory '$(salmon_builddir)' ..."
	mkdir -p $(salmon_builddir)
	tar -xzvf $< -C $(salmon_builddir) --strip-components=1
	touch $@
	@$(ECHO) "Done."


# Download.
salmon_tarball: $(salmon_tarball) # $(tarballsdir)/$(salmon_tarball).

$(salmon_tarball): $(internet_get) $(salmon_force) | $(tarballsdir)
	@$(ECHO) "\nDownloading '$(salmon_package)' tarball from '$(salmon_repository)' ..."
	wget -c $(salmon_repository) -P $(tarballsdir) -O $@
	touch $@
	@$(ECHO) "Done."


salmon_refresh: salmon_uninstall salmon_install


# Uninstall and clean.
salmon_uninstall: prefix := $(if $(DESTDIR),$(prefix),$(dir $(installdir)))
salmon_uninstall:
	@$(ECHO) "\nUninstalling '$(salmon_package)' from directory '$(installdir)' ...";
	[ -d "$(installdir)" ] || exit 1;
	#$(MAKE) -C $(salmon_builddir) uninstall DESTDIR=$(DESTDIR) prefix=$(prefix) -j
	rm -f $(installdir)/salmon
	echo "User may resolve \$$PATH in '~/.bashrc' file." >&2 
	@$(ECHO) "Done."

salmon_clean:
	@$(ECHO) "\nCleaning '$(salmon_package)' from staged directory '$(stageddir)' ...";
	rm -rf $(tarballsdir)/$(salmon_package)*
	@$(ECHO) "Done."

salmon_purge: salmon_uninstall salmon_clean


# FORCE!
salmon_force:
	@$(ECHO) "\nForce installation of '$(salmon_package)' on directory '$(installdir)'."
	-rm $(salmon_tarball)
	@$(ECHO) "Done."


# Test.
salmon_test:
	@$(ECHO) "\nTesting package '$(salmon_package)' ...";
	if [ -z "$$($(salmon_package) --version 2> /dev/null)" ]; then \
		echo "Package '$(salmon_package)' is not properly running." >&2; \
		exit 1; \
	else \
		echo "PASS!"; \
	fi
	@$(ECHO) "Done."



############################## DEBUG CODE ######################################

ifeq ($(dbg_salmon),yes)
$(info ****************************** DEBUG GENERIC PACK ******************************)
$(info Precursor variables for a 'salmon' package source.)
$(info workdir:                       $(workdir).)
$(info stageddir:                     $(stageddir).)
$(info tarballsdir:                   $(tarballsdir).)
$(info DESTDIR:                       $(DESTDIR).)
$(info installdir:                    $(installdir).)
$(info usermode:                      $(usermode).)
$(info src:                           $(src).)
$(info ext:                           $(ext).)
$(info )

$(info Info to download, configure, build, install and use 'salmon' package.)
$(info salmon_source:                $(salmon_source).)
$(info salmon_ext:                   $(salmon_ext).)
$(info )

$(info salmon_package:               $(salmon_package).)
$(info salmon_version:               $(salmon_version).)
$(info salmon_id:                    $(salmon_id).)
$(info salmon_distname:              $(salmon_distname).)
$(info salmon_license:               $(salmon_license).)
$(info )

$(info Potential installation data.)
$(info salmon_dependecies:           $(salmon_dependecies).)
$(info salmon_repository:            $(salmon_repository).)
$(info salmon_tarball:               $(salmon_tarball).)
$(info salmon_builddir:              $(salmon_builddir).)
$(info salmon_configurefile:         $(salmon_configurefile).)
$(info salmon_buildfile:             $(salmon_buildfile).)
$(info salmon_execfile:              $(salmon_execfile).)
$(info salmon_installfile:           $(salmon_installfile).)


$(info Actual installation data.)
$(info salmon_has:                   $(salmon_has).)
$(info salmon_get:                   $(salmon_get).)
$(info salmon:                       $(salmon).)
$(info )


$(info Status and FORCE data.)
$(info salmon_force:                 $(salmon_force).)
$(info salmon_status:                $(salmon_status).)
$(info ********************************************************************************)
endif
