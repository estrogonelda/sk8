# Non-standalone run verification.
this                           := php.mk
ifeq ($(notdir $(firstword $(MAKEFILE_LIST))),$(this))
      $(error ERROR: Module '$(this)' cannot run in standalone mode, \
      it needs to be loaded by 'SK8')
endif

# Info to download, configure, build, install and use 'php' package.
php_src                    := https://www.php.net/distributions/php-8.0.3.tar.gz
php_ext                    := .tar.gz

php_package                := $(shell perl -ne 'print "@{[ (split(/-/, $$_))[0] ]}"' <<< "$(notdir $(php_src))")
php_version                := $(shell perl -pe 's/^$(php_package)-(.*?)\$(php_ext)/$$1/g' <<< "$(notdir $(php_src))")
php_id                     := $(php_package)-xxxxxx
php_distname               := $(php_package)-$(php_version)
php_license                := GPLv4

# Potential installation data.
php_repository             := $(php_src)
php_tarball                := $(tarballsdir)/$(php_distname)$(php_ext)
php_builddir               := $(tarballsdir)/$(php_distname)
php_configurefile          := $(php_builddir)/configure
php_buildfile              := $(php_builddir)/Makefile
php_execfile               := $(php_builddir)/$(php_package)
php_installfile            := $(installdir)/$(php_package)


# Actual installation data.
php_has                    := $(shell which $(php_package) 2> /dev/null)
php_get                    := $(if $(php_has),,php_install)
php                        := $(if $(php_has),$(php_has),$(php_installfile))


# FORCE data.
php_force                  := $(if $(FORCE),php_force,)
php_status                 := $(if $(FORCE),php_install,$(php_get))
php                        := $(if $(FORCE),$(php_installfile),$(php))


# Include required modules.
#include $(addprefix $(sk8_rootdir)/modules/, gcc.mk)
php_deps                   := $(if $(recursive),php_deps,)
php_depscheck              := $(gcc_status) $(php_deps)


# Usage.
php_help:
	@$(ECHO) "\n\
	NAME\n\
		\n\
		php_package - $(php_package), version $(php_version)\n\
		\n\
	SYNOPSIS\n\
		\n\
		$$ sk8 [make_option ...] [variable=value ...] <php_target ...>\n\
		\n\
	DESCRIPTION\n\
		\n\
		This is an engine to download, configure, build, install and test packages\n\
		in a php way. So, the package must be compliant with the GNU Coding\n\
		Standards in order to be handled by SK8.\n\
		Now, the package in question is '$(php_distname)'.\n\
		\n\
	OPTIONS\n\
		\n\
		Targets:\n\
		\n\
		php_help                   Print this help text.\n\
		php_version                Print '$(php_package)' package info.\n\
		php_status                 Verifies '$(php_package)' current installation status.\n\
		php_run                    Run '$(php_package)' with the arguments given in 'args' variable.\n\
		php_install                Install '$(php_package)' binary in '$$(prefix)/bin' directory.\n\
		php_build                  Compile the '$(php_package)' binary in the build directory.\n\
		php_configure              Configure '$(php_package)' build files for this system.\n\
		php_tarball                Download the '$(php_package)' tarball from its repository: $(php_src)\n\
		php_uninstall              Uninstall '$(php_package)' from the '$$(prefix)/bin' directory.\n\
		php_clear                  Remove '$(php_package)' tarball from tarball''s directory.\n\
		php_purge                  Uninstall and clear '$(php_package)' from the system.\n\
		php_refresh                Uninstall and then re-install '$(php_package)' again.\n\
		php_deps                   Install a '$(php_package)' dependency recursivelly as passed in 'args' variable.\n\
		php_test                   Test '$(php_package)' installation.\n\
		php_test_build             Test '$(php_package)' build process, natively.\n\
		php_test_build_conda       Test '$(php_package)' build process in a new Conda environment.\n\
		php_test_build_docker      Test '$(php_package)' build process inside a vanilla Docker container.\n\
		php_test_build_aws         Test '$(php_package)' build process inside an AWS instance. NOT WORKING!!\n\
		php_create_img             Create a Docker image for '$(php_package)', based on '$(image)' image.\n\
		php_create_dockerfile      Create a Dockerfile to build an image for '$(php_package)'.\n\
		php_create_module          Create a pre-filled SK8 module for '$(php_package)' package.\n\
		\n\
		conda_run                      Run '$(php_package)' in a new Conda environment. See 'conda_help' for more info.\n\
		docker_run                     Run '$(php_package)' in a vanilla Docker image. See 'docker_help' for more info.\n\
		aws_run                        Run '$(php_package)' in an AWS instance. See 'aws_help' for more info. NOT WORKING!!\n\
		\n\
		Variables:\n\
		\n\
		args                           String of arguments passed to '$(php_package)' when running.               [ '' ]\n\
		prefix                         Path to the directory where to put the built '$(php_packages)' binaries.   [ /usr/local ]\n\
		DESTDIR                        Path to an alternative staged installation directory.                          [ '' ]\n\
		workdir                        The current directory.                                                         [ . ]\n\
		stageddir                      Path to a staged directory for tarballs and package builds.                    [ ./.local ]\n\
		usermode                       Boolean to switch between local or system-wide installations paths.            [ '' ]\n\
		recursive                      Boolean to enable recursive dependency installations.                          [ '' ]\n\
		FORCE                          Boolean to force a fresh installation over an existent one.                    [ '' ]\n\
		\n\
		src                            URL of the '$(php_package)' repository.                                    [ '$(php_src)' ]\n\
		ext                            The tarball extension. Only for extensions other than '.tar.gz'.               [ '.tar.gz' ]\n\
		php_package                The name of the package, derived from its tarball''s name.                     [ '$(php_package)' ]\n\
		php_version                The version of the package, derived from its tarball''s name.                  [ '$(php_version)' ]\n\
		\n\
		Make options:\n\
		\n\
		\n\
		NOTE: Some packages cannot build phpally. So, user must test the chosen target first.\n\
		For a list of known phpally installable packages, pleas read the\n\
		'$(sk8_rootdir)/modules/modules_urls.txt' file.\n\
		\n\
		\n\
	COPYRIGHT\n\
		\n\
		$(sk8_package), version $(sk8_version) for GNU/Linux.\n\
		Copyright (C) Bards Agrotechnologies, 2011-2021.\n\
		\n\
	SEE ALSO\n\
		\n\
		Search for 'help', 'docker_help', 'conda_help' and 'miscellaneus'\n\
		for other sources of command line help or visit <$(basename $(sk8_url))>\n\
		for full documentation."

php_version:
	@$(ECHO) "\n\
	Package:    $(php_package)\n\
	Version:    $(php_version)\n\
	Id:         $(php_id)\n\
	License:    $(php_license)\n\
	\n\
	Status:     $(php_status)\n\
	Source:     $(php_src)\n\
	Installdir: $(installdir)\n\
	\n\
	Runned with $(sk8_package), version $(sk8_version) for GNU/Linux.\n\
	Copyright (C) Bards Agrotechnologies, 2011-2021."

php_run: $(php_status)
	@$(ECHO) "\nRunning '$(php)' with arguments '$(args)' ..." >&2
	$(php) $(args)
	@$(ECHO) "Done." >&2


# Verification.
php_status: $(php_status) # 'php_install' or none.
	@$(ECHO) "\nSystem has '$(php_package)' up and running." >&2


# Install.
php_install: $(php_installfile) # $(installdir)/$(php_package)
	@$(ECHO) "\nSystem has '$(php_package)' in PATH: '$<'." >&2

$(php_installfile): $(php_execfile) | $(installdir) # just copy the executable file to $(installdir).
	@$(ECHO) "\nInstalling '$(php_package)' on directory '$(installdir)' ..." >&2
	$(MAKE) -C $(php_builddir) install DESTDIR=$(DESTDIR) prefix=$(dir $(installdir)) -j4
	if [ -z "$$(perl -ne 'print if s#.*$(installdir).*#AA#g' <<< $${PATH})" ]; then \
		echo -e "export PATH=$(installdir):\$$PATH" >> ~/.bashrc; \
		source ~/.bashrc; \
		echo -e "WARNING: You may need to run 'source ~/.bashrc' to release the new \$$PATH."; \
	elif [ "$(php)" != "$(php_installfile)" ]; then \
		echo "WARNING: Previuos installation of '$(php_package)' may be shadowing the new one in the \$$PATH."; \
	fi
	touch $@
	@$(ECHO) "Done." >&2


# Build.
php_build: $(php_execfile) # $(php_builddir)/$(php_package)

$(php_execfile): $(php_buildfile) | $(gcc_status) # a Makefile generates the executable file.
	@$(ECHO) "\nBuilding '$(php_package)' on directory '$(php_builddir)' ..." >&2
	$(MAKE) -C $(php_builddir) -j4
	touch $@
	@$(ECHO) "Done." >&2


# Configure.
php_configure: $(php_buildfile) # $(php_builddir)/Makefile

$(php_buildfile): $(php_configurefile)
	@$(ECHO) "\nConfiguring '$(php_package)' on directory '$(php_builddir)' ..." >&2
	cd $(php_builddir); \
	./configure --prefix=$(dir $(installdir)) $(php_args)
	touch $@
	@$(ECHO) "Done." >&2

$(php_configurefile): $(php_tarball) | $(php_builddir)
	@$(ECHO) "\nExtracting '$(php_package)' tarball to directory '$(php_builddir)' ..." >&2
	if [ "$(php_ext)" == ".tar.gz" ]; then \
		tar -xzvf $< -C $(php_builddir) --strip-components=1; \
	else \
		tar -xvf $< -C $(php_builddir) --strip-components=1; \
	fi
	touch $@
	@$(ECHO) "Done." >&2

$(php_builddir):
	@$(ECHO) "\nCreating directory '$(php_builddir)' ..." >&2
	mkdir -p $(php_builddir)
	@$(ECHO) "Done." >&2


# Download.
php_tarball: $(php_tarball) # $(tarballsdir)/$(php_tarball).

$(php_tarball): $(php_force) | $(tarballsdir) $(internet_status) $(php_depscheck)
	@$(ECHO) "\nDownloading '$(php_package)' tarball from '$(php_repository)' ..." >&2
	wget -c $(php_repository) -P $(tarballsdir) -O $@
	touch $@
	@$(ECHO) "Done." >&2

php_deps:
	@$(ECHO) "\nRecursively installing dependecies for '$(php_distname)' ..." >&2
	[ -n "$(args)" ] || ( echo "ERROR: Variable 'args' can not be empty." >&2; exit 1 )
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) recursive= $(args)
	@$(ECHO) "Done." >&2

php_force:
	@$(ECHO) "\nForce reinstallation of '$(php_package)' on directory '$(installdir)'." >&2
	rm -f $(php_tarball)
	@$(ECHO) "Done." >&2


php_refresh: php_uninstall
	if [ -z "$(usermode)" ]; then \
		$(MAKE) -f $(firstword $(MAKEFILE_LIST)) DESTDIR=$(DESTDIR) prefix=$(prefix) php_install; \
	else \
		$(MAKE) -f $(firstword $(MAKEFILE_LIST)) DESTDIR=$(DESTDIR) prefix=$(prefix) php_install usermode=1; \
	fi


# Uninstall and clean.
php_purge: php_uninstall php_clean

php_uninstall:
	@$(ECHO) "\nUninstalling '$(php_package)' from directory '$(installdir)' ..." >&2
	[ -d "$(installdir)" ] || exit 1;
	if [ -f "$(php_buildfile)_no" ]; then \
		$(MAKE) -C $(php_builddir) uninstall DESTDIR=$(DESTDIR) prefix=$(dir $(installdir)) -j4; \
	else \
		rm -rf $(php_installfile)*; \
		rm -rf $(installdir)/phar*; \
		rm -rf $(dir $(installdir))/php; \
		rm -rf $(dir $(installdir))/lib/php; \
		rm -rf $(dir $(installdir))/include/php; \
	fi
	@$(ECHO) "User must resolve \$$PATH in '~/.bashrc' file." >&2
	@$(ECHO) "Done." >&2

php_clean:
	@$(ECHO) "\nCleaning '$(php_package)' from staged directory '$(stageddir)' ..." >&2
	rm -rf $(tarballsdir)/$(php_package)*
	@$(ECHO) "Done." >&2


# Tests.
php_test:
	@$(ECHO) "\nTesting '$(php_package)' ..." >&2
	if [ -n "$(is_lib)" ]; then \
		if [ ! -s "$$(find $(dir $(installdir))/lib -type f -name '$(php_package)*')" ]; then \
			echo "Library '$(php_package)' is not properly installed."; \
			exit 7; \
		fi \
	else \
		if [ -z "$$($(php_package) --version 2> /dev/null)" ]; then \
			echo "Package '$(php_package)' is not properly running."; \
			exit 7; \
		fi \
	fi
	@$(ECHO) "Package '$(php_package)' test status: PASS!"
	@$(ECHO) "Done." >&2


php_test_build_native: _install := $(basedir)/_install_$(php_distname)
php_test_build_native:
	@$(ECHO) "\nTesting native build of '$(php_package)' module." >&2
	[ -n "$(_install)" ] || ( echo "ERROR: Variable '_install' is empty."; exit 7 )
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) \
		stageddir=$(_install) \
		usermode=1 \
		php_src=$(php_src) \
		php_ext=$(php_ext) \
		php_package=$(php_package) \
		php_version=$(php_version) \
		php_args=$(php_args) \
		recursive=$(recursive) args="$(args)" \
		php_install php_test
	[ -n "$(keep_install)" ] || rm -rf $(_install)
	@$(ECHO) "Done." >&2
	@$(ECHO) "\nFinished native build test for '$(php_package)' module." >&2

php_test_build_conda: conda_test_build
	@$(ECHO) "\nFinished Conda build test for '$(php_package)' module." >&2

php_test_build_docker: override args := make -f SK8/SK8.mk \
	module=$(addprefix SK8/modules/, $(notdir $(module))) \
	usermode=1 \
	php_src=$(php_src) \
	php_ext=$(php_ext) \
	php_package=$(php_package) \
	php_version=$(php_version) \
	php_args=$(php_args) \
	recursive=$(recursive) args="$(args)" \
	php_install php_test
php_test_build_docker: docker_test_build
	@$(ECHO) "\nFinished Docker build test for '$(php_package)' module." >&2

