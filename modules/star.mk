# Non-standalone run verification.
this                           := star.mk
ifeq ($(notdir $(firstword $(MAKEFILE_LIST))),$(this))
      $(error ERROR: Module '$(this)' cannot run in standalone mode, \
      it needs to be loaded by 'SK8')
endif

# Info to download, configure, build, install and use 'star' package.
star_src                    := https://github.com/alexdobin/STAR/archive/2.7.9a.tar.gz
star_ext                    := .tar.gz

star_package                := STAR
star_version                := 2.7.9a
star_id                     := $(star_package)-xxxxxx
star_distname               := $(star_package)-$(star_version)
star_license                := GPLv4

# Potential installation data.
star_repository             := $(star_src)
star_tarball                := $(tarballsdir)/$(star_distname)$(star_ext)
star_builddir               := $(tarballsdir)/$(star_distname)
star_configurefile          := $(star_builddir)/configure.fk
star_buildfile              := $(star_builddir)/source/Makefile
star_execfile               := $(star_builddir)/source/$(star_package)
star_installfile            := $(installdir)/$(star_package)


# Actual installation data.
star_has                    := $(shell which $(star_package) 2> /dev/null)
star_get                    := $(if $(star_has),,star_install)
star                        := $(if $(star_has),$(star_has),$(star_installfile))


# FORCE data.
star_force                  := $(if $(FORCE),star_force,)
star_status                 := $(if $(FORCE),star_install,$(star_get))
star                        := $(if $(FORCE),$(star_installfile),$(star))


# Include required modules.
#include $(addprefix $(sk8_rootdir)/modules/, gcc.mk)
star_deps                   := $(if $(recursive),star_deps,)
star_depscheck              := $(gpp_status) star_deps


# Usage.
star_help:
	@$(ECHO) "\n\
	NAME\n\
		\n\
		star_package - $(star_package), version $(star_version)\n\
		\n\
	SYNOPSIS\n\
		\n\
		$$ sk8 [make_option ...] [variable=value ...] <star_target ...>\n\
		\n\
	DESCRIPTION\n\
		\n\
		This is an engine to download, configure, build, install and test packages\n\
		in a star way. So, the package must be compliant with the GNU Coding\n\
		Standards in order to be handled by SK8.\n\
		Now, the package in question is '$(star_distname)'.\n\
		\n\
	OPTIONS\n\
		\n\
		Targets:\n\
		\n\
		star_help                   Print this help text.\n\
		star_version                Print '$(star_package)' package info.\n\
		star_status                 Verifies '$(star_package)' current installation status.\n\
		star_run                    Run '$(star_package)' with the arguments given in 'args' variable.\n\
		star_install                Install '$(star_package)' binary in '$$(prefix)/bin' directory.\n\
		star_build                  Compile the '$(star_package)' binary in the build directory.\n\
		star_configure              Configure '$(star_package)' build files for this system.\n\
		star_tarball                Download the '$(star_package)' tarball from its repository: $(star_src)\n\
		star_uninstall              Uninstall '$(star_package)' from the '$$(prefix)/bin' directory.\n\
		star_clear                  Remove '$(star_package)' tarball from tarball''s directory.\n\
		star_purge                  Uninstall and clear '$(star_package)' from the system.\n\
		star_refresh                Uninstall and then re-install '$(star_package)' again.\n\
		star_deps                   Install a '$(star_package)' dependency recursivelly as passed in 'args' variable.\n\
		star_test                   Test '$(star_package)' installation.\n\
		star_test_build             Test '$(star_package)' build process, natively.\n\
		star_test_build_conda       Test '$(star_package)' build process in a new Conda environment.\n\
		star_test_build_docker      Test '$(star_package)' build process inside a vanilla Docker container.\n\
		star_test_build_aws         Test '$(star_package)' build process inside an AWS instance. NOT WORKING!!\n\
		star_create_img             Create a Docker image for '$(star_package)', based on '$(image)' image.\n\
		star_create_dockerfile      Create a Dockerfile to build an image for '$(star_package)'.\n\
		star_create_module          Create a pre-filled SK8 module for '$(star_package)' package.\n\
		\n\
		conda_run                      Run '$(star_package)' in a new Conda environment. See 'conda_help' for more info.\n\
		docker_run                     Run '$(star_package)' in a vanilla Docker image. See 'docker_help' for more info.\n\
		aws_run                        Run '$(star_package)' in an AWS instance. See 'aws_help' for more info. NOT WORKING!!\n\
		\n\
		Variables:\n\
		\n\
		args                           String of arguments passed to '$(star_package)' when running.               [ '' ]\n\
		prefix                         Path to the directory where to put the built '$(star_packages)' binaries.   [ /usr/local ]\n\
		DESTDIR                        Path to an alternative staged installation directory.                          [ '' ]\n\
		workdir                        The current directory.                                                         [ . ]\n\
		stageddir                      Path to a staged directory for tarballs and package builds.                    [ ./.local ]\n\
		usermode                       Boolean to switch between local or system-wide installations paths.            [ '' ]\n\
		recursive                      Boolean to enable recursive dependency installations.                          [ '' ]\n\
		FORCE                          Boolean to force a fresh installation over an existent one.                    [ '' ]\n\
		\n\
		src                            URL of the '$(star_package)' repository.                                    [ '$(star_src)' ]\n\
		ext                            The tarball extension. Only for extensions other than '.tar.gz'.               [ '.tar.gz' ]\n\
		star_package                The name of the package, derived from its tarball''s name.                     [ '$(star_package)' ]\n\
		star_version                The version of the package, derived from its tarball''s name.                  [ '$(star_version)' ]\n\
		\n\
		Make options:\n\
		\n\
		\n\
		NOTE: Some packages cannot build starally. So, user must test the chosen target first.\n\
		For a list of known starally installable packages, pleas read the\n\
		'$(sk8_rootdir)/modules/modules_urls.txt' file.\n\
		\n\
		\n\
	COPYRIGHT\n\
		\n\
		$(sk8_package), version $(sk8_version) for GNU/Linux.\n\
		Copyright (C) Bards Agrotechnologies, 2011-2021.\n\
		\n\
	SEE ALSO\n\
		\n\
		Search for 'help', 'docker_help', 'conda_help' and 'miscellaneus'\n\
		for other sources of command line help or visit <$(basename $(sk8_url))>\n\
		for full documentation."

star_version:
	@$(ECHO) "\n\
	Package:    $(star_package)\n\
	Version:    $(star_version)\n\
	Id:         $(star_id)\n\
	License:    $(star_license)\n\
	\n\
	Status:     $(star_status)\n\
	Source:     $(star_src)\n\
	Installdir: $(installdir)\n\
	\n\
	Runned with $(sk8_package), version $(sk8_version) for GNU/Linux.\n\
	Copyright (C) Bards Agrotechnologies, 2011-2021."

star_run: $(star_status)
	@$(ECHO) "\nRunning '$(star)' with arguments '$(args)' ..." >&2
	$(star) $(args)
	@$(ECHO) "Done." >&2


# Verification.
star_status: $(star_status) # 'star_install' or none.
	@$(ECHO) "\nSystem has '$(star_package)' up and running." >&2


# Install.
star_install: $(star_installfile) # $(installdir)/$(star_package)
	@$(ECHO) "\nSystem has '$(star_package)' in PATH: '$<'." >&2

$(star_installfile): $(star_execfile) | $(installdir) # just copy the executable file to $(installdir).
	@$(ECHO) "\nInstalling '$(star_package)' on directory '$(installdir)' ..." >&2
	#$(MAKE) -C $(star_builddir) install DESTDIR=$(DESTDIR) prefix=$(dir $(installdir)) -j4
	install -t $(installdir) -m 755 $<
	if [ -z "$$(perl -ne 'print if s#.*$(installdir).*#AA#g' <<< $${PATH})" ]; then \
		echo -e "export PATH=$(installdir):\$$PATH" >> ~/.bashrc; \
		source ~/.bashrc; \
		echo -e "WARNING: You may need to run 'source ~/.bashrc' to release the new \$$PATH."; \
	elif [ "$(star)" != "$(star_installfile)" ]; then \
		echo "WARNING: Previuos installation of '$(star_package)' may be shadowing the new one in the \$$PATH."; \
	fi
	touch $@
	@$(ECHO) "Done." >&2


# Build.
star_build: $(star_execfile) # $(star_builddir)/$(star_package)

$(star_execfile): $(star_buildfile) | $(gcc_status) # a Makefile generates the executable file.
	@$(ECHO) "\nBuilding '$(star_package)' on directory '$(star_builddir)' ..." >&2
	$(MAKE) -C $(star_builddir)/source $(star_package) -j4
	touch $@
	@$(ECHO) "Done." >&2


# Configure.
star_configure: $(star_buildfile) # $(star_builddir)/Makefile

$(star_buildfile): $(star_configurefile)
	@$(ECHO) "\nConfiguring '$(star_package)' on directory '$(star_builddir)' ..." >&2
	#cd $(star_builddir); \
	#./configure --prefix=$(dir $(installdir)) $(star_args)
	touch $@
	@$(ECHO) "Done." >&2

$(star_configurefile): $(star_tarball) | $(star_builddir)
	@$(ECHO) "\nExtracting '$(star_package)' tarball to directory '$(star_builddir)' ..." >&2
	if [ "$(star_ext)" == ".tar.gz" ]; then \
		tar -xzvf $< -C $(star_builddir) --strip-components=1; \
	else \
		tar -xvf $< -C $(star_builddir) --strip-components=1; \
	fi
	touch $@
	@$(ECHO) "Done." >&2

$(star_builddir):
	@$(ECHO) "\nCreating directory '$(star_builddir)' ..." >&2
	mkdir -p $(star_builddir)
	@$(ECHO) "Done." >&2


# Download.
star_tarball: $(star_tarball) # $(tarballsdir)/$(star_tarball).

$(star_tarball): $(star_force) | $(tarballsdir) $(internet_status) $(star_depscheck)
	@$(ECHO) "\nDownloading '$(star_package)' tarball from '$(star_repository)' ..." >&2
	wget -c $(star_repository) -P $(tarballsdir) -O $@
	touch $@
	@$(ECHO) "Done." >&2

star_deps:
	@$(ECHO) "\nRecursively installing dependecies for '$(star_distname)' ..." >&2
	[ -n "$(args)_" ] || ( echo "ERROR: Variable 'args' can not be empty." >&2; exit 1 )
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) \
		usermode=$(usermode) \
		generic_src=https://zlib.net/zlib-1.2.11.tar.gz \
		generic_ext=.tar.gz \
		generic_package=zlib \
		generic_version=1.2.11 \
		recursive= \
		generic_install
	export CPATH=$(dir $(installdir))/include
	@$(ECHO) "Done." >&2

star_force:
	@$(ECHO) "\nForce reinstallation of '$(star_package)' on directory '$(installdir)'." >&2
	rm -f $(star_tarball)
	@$(ECHO) "Done." >&2


star_refresh: star_uninstall
	if [ -z "$(usermode)" ]; then \
		$(MAKE) -f $(firstword $(MAKEFILE_LIST)) DESTDIR=$(DESTDIR) prefix=$(prefix) star_install; \
	else \
		$(MAKE) -f $(firstword $(MAKEFILE_LIST)) DESTDIR=$(DESTDIR) prefix=$(prefix) star_install usermode=1; \
	fi


# Uninstall and clean.
star_purge: star_uninstall star_clean

star_uninstall:
	@$(ECHO) "\nUninstalling '$(star_package)' from directory '$(installdir)' ..." >&2
	[ -d "$(installdir)" ] || exit 1;
	if [ -f "$(star_buildfile)_no" ]; then \
		$(MAKE) -C $(star_builddir) uninstall DESTDIR=$(DESTDIR) prefix=$(dir $(installdir)) -j4; \
	else \
		rm -rf $(star_installfile)*; \
	fi
	@$(ECHO) "User must resolve \$$PATH in '~/.bashrc' file." >&2
	@$(ECHO) "Done." >&2

star_clean:
	@$(ECHO) "\nCleaning '$(star_package)' from staged directory '$(stageddir)' ..." >&2
	rm -rf $(tarballsdir)/$(star_package)*
	@$(ECHO) "Done." >&2


# Tests.
star_test:
	@$(ECHO) "\nTesting '$(star_package)' ..." >&2
	if [ -n "$(is_lib)" ]; then \
		if [ ! -s "$$(find $(dir $(installdir))/lib -type f -name '$(star_package)*')" ]; then \
			echo "Library '$(star_package)' is not properly installed."; \
			exit 7; \
		fi \
	else \
		if [ -z "$$($(star_package) --version 2> /dev/null)" ]; then \
			echo "Package '$(star_package)' is not properly running."; \
			exit 7; \
		fi \
	fi
	@$(ECHO) "Package '$(star_package)' test status: PASS!"
	@$(ECHO) "Done." >&2


star_test_build_native: _install := $(basedir)/_install_$(star_distname)
star_test_build_native:
	@$(ECHO) "\nTesting native build of '$(star_package)' module." >&2
	[ -n "$(_install)" ] || ( echo "ERROR: Variable '_install' is empty."; exit 7 )
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) \
		stageddir=$(_install) \
		usermode=1 \
		star_src=$(star_src) \
		star_ext=$(star_ext) \
		star_package=$(star_package) \
		star_version=$(star_version) \
		star_args=$(star_args) \
		recursive=$(recursive) args="$(args)" \
		star_install star_test
	[ -n "$(keep_install)" ] || rm -rf $(_install)
	@$(ECHO) "Done." >&2
	@$(ECHO) "\nFinished native build test for '$(star_package)' module." >&2

star_test_build_conda: conda_test_build
	@$(ECHO) "\nFinished Conda build test for '$(star_package)' module." >&2

star_test_build_docker: override args := make -f SK8/SK8.mk \
	module=$(addprefix SK8/modules/, $(notdir $(module))) \
	usermode=1 \
	star_src=$(star_src) \
	star_ext=$(star_ext) \
	star_package=$(star_package) \
	star_version=$(star_version) \
	star_args=$(star_args) \
	recursive=$(recursive) args="$(args)" \
	star_install star_test
star_test_build_docker: docker_more_opts := -e CPATH=/home/lion/.local/include
star_test_build_docker: docker_test_build
	@$(ECHO) "\nFinished Docker build test for '$(star_package)' module." >&2

