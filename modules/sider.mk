############################## sideRETRO PACKAGE ###############################

# Info to download, configure, build, install and use 'sider' package.
sider_src                      ?= https://github.com/galantelab/sideRETRO/archive/0.14.1.tar.gz
sider_ext                      ?= .tar.gz

sider_package                  := sider
sider_version                  := 0.14.1
sider_id                       := $(sider_package)-xxxxxx
sider_distname                 := $(sider_package)-$(sider_version)
sider_license                  := GPLv4

# Potential installation data.
sider_repository               := $(sider_src)
sider_tarball                  := $(tarballsdir)/$(sider_distname)$(sider_ext)
sider_builddir                 := $(tarballsdir)/$(sider_distname)
sider_configurefile            := $(sider_builddir)/meson.build
sider_buildfile                := $(sider_builddir)/build/build.ninja
sider_execfile                 := $(sider_builddir)/build/src/$(sider_package)
sider_installfile              := $(installdir)/$(sider_package)


# Actual installation data.
sider_has                      := $(shell which $(sider_package) 2> /dev/null)
sider_get                      := $(if $(sider_has),,sider_install)
sider                          := $(if $(sider_has),$(sider_has),$(sider_installfile))


# FORCE data.
sider_force                    := $(if $(FORCE),sider_force,)
sider_status                   := $(if $(FORCE),sider_install,$(sider_get))
sider                          := $(if $(FORCE),$(sider_installfile),$(sider))


# Include required modules.
include $(addprefix $(sk8_rootdir)/modules/, ninja.mk meson.mk)
sider_depscheck                := $(ninja_status) $(meson_status)


# Verification.
sider_status: $(sider_status) # 'sider_install' or none.
	@$(ECHO) "\nSystem has '$(sider_package)' up and running."


# Install.
sider_install: $(sider_installfile) # $(installdir)/$(sider_package)
	@$(ECHO) "\nSystem has '$(sider_package)' in PATH: '$<'."

$(sider_installfile): $(sider_execfile) # just copy the executable file to $(installdir).
	@$(ECHO) "\nInstalling '$(sider_package)' on directory '$(installdir)' ..."
	install -d $(installdir)
	install -m 755 $< $(installdir)
	if [ -z "$$(perl -ne 'print if s#.*$(installdir).*#AA#g' <<< $${PATH})" ]; then \
		echo -e "export PATH=$(installdir):\$$PATH" >> ~/.bashrc; \
		source ~/.bashrc; \
		echo -e "WARNING: You may need to run 'source ~/.bashrc' to release the new \$$PATH."; \
	elif [ "$(sider)" != "$(sider_installfile)" ]; then \
		echo "WARNING: Previuos installation of '$(sider_package)' may be shadowing the new one in the \$$PATH."; \
	fi
	touch $@
	@$(ECHO) "Done."


# Build.
sider_build: $(sider_execfile) # $(sider_builddir)/$(sider_package)

$(sider_execfile): $(sider_buildfile) # a Makefile generates the executable file.
	@$(ECHO) "\nBuilding '$(sider_package)' on directory '$(sider_builddir)' ..."
	ninja -C $(sider_builddir)/build
	touch $@
	@$(ECHO) "Done."


# Configure.
sider_configure: $(sider_buildfile) # $(sider_builddir)/Makefile

$(sider_buildfile): $(sider_configurefile)
	@$(ECHO) "\nConfiguring '$(sider_package)' on directory '$(sider_builddir)' ..."
	cd $(sider_builddir); \
	meson build
	touch $@
	@$(ECHO) "Done."

$(sider_configurefile): $(sider_tarball)
	@$(ECHO) "\nExtracting '$(sider_package)' tarball to directory '$(sider_builddir)' ..."
	mkdir -p $(sider_builddir)
	tar -xzvf $< -C $(sider_builddir) --strip-components=1
	touch $@
	@$(ECHO) "Done."


# Download.
sider_tarball: $(sider_tarball) # $(tarballsdir)/$(sider_tarball).

$(sider_tarball): $(internet_status) $(sider_force) | $(tarballsdir) $(sider_depscheck)
	@$(ECHO) "\nDownloading '$(sider_package)' tarball from '$(sider_repository)' ..."
	wget -c $(sider_repository) -P $(tarballsdir) -O $@
	touch $@
	@$(ECHO) "Done."


sider_refresh: sider_uninstall
	if [ -z "$(usermode)" ]; then \
		$(MAKE) -f $(firstword $(MAKEFILE_LIST)) DESTDIR=$(DESTDIR) prefix=$(prefix) sider_install; \
	else \
		$(MAKE) -f $(firstword $(MAKEFILE_LIST)) DESTDIR=$(DESTDIR) prefix=$(prefix) sider_install usermode=1; \
	fi


# Uninstall and clean.
sider_purge: sider_uninstall sider_clean

sider_uninstall:
	@$(ECHO) "\nUninstalling '$(sider_package)' from directory '$(installdir)' ...";
	[ -d "$(installdir)" ] || exit 1;
	if [ -f "$(sider_buildfile)_no" ]; then \
		$(MAKE) -C $(sider_builddir) uninstall DESTDIR=$(DESTDIR) prefix=$(dir $(installdir)) -j4; \
	else \
		rm -rf $(sider_installfile)*; \
	fi
	@$(ECHO) "Done."

sider_clean:
	@$(ECHO) "\nCleaning '$(sider_package)' from staged directory '$(stageddir)' ...";
	rm -rf $(tarballsdir)/$(sider_package)*
	@$(ECHO) "Done."


# FORCE!
sider_force:
	@$(ECHO) "\nForce installation of '$(sider_package)' on directory '$(installdir)'."
	rm -rf $(sider_tarball)
	@$(ECHO) "Done."


# Test.
sider_test:
	@$(ECHO) "\nTesting package '$(sider_package)' ...";
	if [ -z "$$($(sider_package) --version 2> /dev/null)" ]; then \
		echo "Package '$(sider_package)' is not properly running." >&2; \
		exit 1; \
	else \
		echo "PASS!"; \
	fi
	@$(ECHO) "Done."


sider_test_build: args := make -f SK8/SK8.mk module=SK8/modules/sider.mk usermode=1 \
	sider_src=$(sider_src) sider_ext=$(sider_ext) sider_package=$(sider_package) sider_version=$(sider_version) sider_args=$(sider_args) \
	recursive=$(recursive) \
	sider_install sider_test
sider_test_build: docker_test_build
	@$(ECHO) "\nFinished Docker build test for '$(sider_package)' module." >&2




############################## DEBUG CODE ######################################

ifeq ($(dbg_sider),yes)
$(info ****************************** DEBUG GENERIC PACK ******************************)
$(info Precursor variables for a 'sider' package source.)
$(info workdir:                       $(workdir).)
$(info stageddir:                     $(stageddir).)
$(info tarballsdir:                   $(tarballsdir).)
$(info DESTDIR:                       $(DESTDIR).)
$(info installdir:                    $(installdir).)
$(info usermode:                      $(usermode).)
$(info src:                           $(src).)
$(info ext:                           $(ext).)
$(info )

$(info Info to download, configure, build, install and use 'sider' package.)
$(info sider_source:                $(sider_source).)
$(info sider_ext:                   $(sider_ext).)
$(info )

$(info sider_package:               $(sider_package).)
$(info sider_version:               $(sider_version).)
$(info sider_id:                    $(sider_id).)
$(info sider_distname:              $(sider_distname).)
$(info sider_license:               $(sider_license).)
$(info )

$(info Potential installation data.)
$(info sider_dependecies:           $(sider_dependecies).)
$(info sider_repository:            $(sider_repository).)
$(info sider_tarball:               $(sider_tarball).)
$(info sider_builddir:              $(sider_builddir).)
$(info sider_configurefile:         $(sider_configurefile).)
$(info sider_buildfile:             $(sider_buildfile).)
$(info sider_execfile:              $(sider_execfile).)
$(info sider_installfile:           $(sider_installfile).)


$(info Actual installation data.)
$(info sider_has:                   $(sider_has).)
$(info sider_get:                   $(sider_get).)
$(info sider:                       $(sider).)
$(info )


$(info Status and FORCE data.)
$(info sider_force:                 $(sider_force).)
$(info sider_status:                $(sider_status).)
$(info ********************************************************************************)
endif
