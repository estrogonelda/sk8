############################## HEADER ##########################################
# SK8 module for package 'zlib'.
################################################################################



############################## PREAMBLE ########################################
# Non-standalone run verification.
this                           := zlib.mk
ifeq ($(notdir $(firstword $(MAKEFILE_LIST))),$(this))
      $(error ERROR: Module '$(this)' cannot run in standalone mode, \
      it needs to be loaded by 'SK8')
endif

# Info to download, configure, build, install and use 'zlib' package.
zlib_src                    ?= https://zlib.net/zlib-1.2.12.tar.gz
zlib_ext                    ?= .tar.gz

zlib_package                ?= $(shell perl -ne 'print "@{[ (split(/-/, $$_))[0] ]}"' <<< "$(notdir $(zlib_src))")
zlib_version                ?= $(shell perl -pe 's/^$(zlib_package)-(.*?)\$(zlib_ext)/$$1/g' <<< "$(notdir $(zlib_src))")
zlib_id                     ?= $(zlib_package)-$(shell echo $$RANDOM)
zlib_license                ?= GPLv4
zlib_distname               ?= $(zlib_package)-$(zlib_version)

# Potential installation data.
zlib_repository             ?= $(zlib_src)
zlib_tarball                ?= $(tarballsdir)/$(zlib_distname)$(zlib_ext)
zlib_builddir               ?= $(tarballsdir)/$(zlib_distname)
zlib_configurefile          ?= $(zlib_builddir)/configure
zlib_buildfile              ?= $(zlib_builddir)/Makefile
zlib_execfile               ?= $(zlib_builddir)/$(zlib_package)

zlib_prefix                 ?= $(if $(supprefix),$(dir $(installdir))/$(zlib_package),$(dir $(installdir)))
zlib_install_bin            ?= $(zlib_prefix)/bin/$(zlib_package)
zlib_install_lib            ?= $(zlib_prefix)/lib/lib$(zlib_package).a
zlib_install_include        ?= $(zlib_prefix)/include/$(zlib_package).h
zlib_install_share          ?= $(zlib_prefix)/share/man

zlib_bins                   = $(wildcard $(zlib_builddir)/bin/*)
zlib_libs                   = $(wildcard $(zlib_builddir)/lib/lib*)
zlib_includes               = $(wildcard $(zlib_builddir)/include/*.h)


# Actual installation data.
ifeq ($(islib),)
zlib_installfile            ?= $(zlib_install_bin)
zlib_check                  ?= $(shell which $(zlib_package) 2> /dev/null)
else
zlib_installfile            ?= $(zlib_install_lib)
zlib_check                  ?= $(abspath $(strip $(wildcard $(basename $(zlib_installfile)).*)))
endif

zlib_get                    ?= $(if $(zlib_check),,zlib_install)
zlib                        ?= $(or $(zlib_check), $(zlib_installfile))


# FORCE data.
zlib_force                  ?= $(if $(FORCE),zlib_force,)
zlib_status                 ?= $(if $(FORCE),zlib_install,$(zlib_get))
zlib                        ?= $(if $(FORCE),$(zlib_installfile),$(zlib))


# Include required modules.
#include $(addprefix $(sk8_rootdir)/modules/, gcc.mk)

zlib_deps_status             ?= $(if $(recursive),zlib_deps,)
zlib_deps                    ?= $(gcc_status)



############################## MAIN ############################################
# Usage.
zlib_help:
	@$(ECHO) "\n\
	NAME\n\
		\n\
		zlib_package - $(zlib_package), version $(zlib_version)\n\
		\n\
	SYNOPSIS\n\
		\n\
		$$ sk9 [make_option ...] [variable=value ...] <zlib_target ...>\n\
		\n\
	DESCRIPTION\n\
		\n\
		This is an engine to download, configure, build, install and test packages\n\
		in a zlib way. So, the package must be compliant with the GNU Coding\n\
		Standards in order to be handled by SK8.\n\
		Now, the package in question is '$(zlib_distname)'.\n\
		\n\
	OPTIONS\n\
		\n\
		Targets:\n\
		\n\
		zlib_help                   Print this help text.\n\
		zlib_version                Print '$(zlib_package)' package version.\n\
		zlib_status                 Verifies '$(zlib_package)' current installation status.\n\
		zlib_run                    Run '$(zlib_package)' with the arguments given in the 'args' variable.\n\
		zlib_install                Install '$(zlib_package)' binary in '\$$(prefix)/bin' directory.\n\
		zlib_build                  Compile the '$(zlib_package)' binary in the build directory.\n\
		zlib_configure              Configure '$(zlib_package)' build files for this system.\n\
		zlib_tarball                Download the '$(zlib_package)' tarball from its repository: $(zlib_src)\n\
		zlib_uninstall              Uninstall '$(zlib_package)' from the '\$$(prefix)/bin' directory.\n\
		zlib_clear                  Remove '$(zlib_package)' tarball from tarball''s directory.\n\
		zlib_purge                  Uninstall and clear '$(zlib_package)' from the system.\n\
		zlib_refresh                Uninstall and then re-install '$(zlib_package)' again.\n\
		zlib_deps                   Install a '$(zlib_package)' dependency recursivelly as passed by the 'args' variable.\n\
		zlib_test                   Test '$(zlib_package)' installation.\n\
		zlib_test_build_native      Test '$(zlib_package)' build process, natively.\n\
		zlib_test_build_conda       Test '$(zlib_package)' build process in a new Conda environment.\n\
		zlib_test_build_docker      Test '$(zlib_package)' build process inside a vanilla Docker container.\n\
		zlib_test_build_aws         Test '$(zlib_package)' build process inside an AWS instance. NOT WORKING!!\n\
		zlib_create_img             Create a Docker image for '$(zlib_package)', based on '$(image)' image.\n\
		zlib_create_dockerfile      Create a Dockerfile to build an image for '$(zlib_package)'.\n\
		zlib_create_module          Create a pre-filled SK8 module for '$(zlib_package)' package.\n\
		\n\
		conda_run                      Run '$(zlib_package)' in a new Conda environment. See 'conda_help' for more info.\n\
		docker_run                     Run '$(zlib_package)' in a vanilla Docker image. See 'docker_help' for more info.\n\
		aws_run                        Run '$(zlib_package)' in an AWS instance. See 'aws_help' for more info. NOT WORKING!!\n\
		\n\
		Variables:\n\
		\n\
		args                           String of arguments passed to '$(zlib_package)' when running.               [ '' ]\n\
		prefix                         Path to the directory where to put the built '$(zlib_packages)' binaries.   [ /usr/local ]\n\
		DESTDIR                        Path to an alternative staged installation directory.                          [ '' ]\n\
		workdir                        The current directory.                                                         [ . ]\n\
		stageddir                      Path to a staged directory for tarballs and package builds.                    [ ./.local ]\n\
		usermode                       Boolean to switch between local or system-wide installations paths.            [ '' ]\n\
		recursive                      Boolean to enable recursive dependency installations.                          [ '' ]\n\
		FORCE                          Boolean to force a fresh installation over an existent one.                    [ '' ]\n\
		\n\
		src                            URL of the '$(zlib_package)' repository.                                    [ '$(zlib_src)' ]\n\
		ext                            The tarball extension. Only for extensions other than '.tar.gz'.               [ '.tar.gz' ]\n\
		zlib_package                The name of the package, derived from its tarball''s name.                     [ '$(zlib_package)' ]\n\
		zlib_version                The version of the package, derived from its tarball''s name.                  [ '$(zlib_version)' ]\n\
		\n\
		Make options:\n\
		\n\
		\n\
		NOTE: Some packages cannot build zlibally. So, user must test the chosen target first.\n\
		For a list of known zlibally installable packages, please read the\n\
		'$(sk8_rootdir)/modules/modules_urls.txt' file.\n\
		\n\
		\n\
	COPYRIGHT\n\
		\n\
		$(sk8_package), version $(sk8_version) for GNU/Linux.\n\
		Copyright (C) Bards Agrotechnologies, 2011-2021.\n\
		\n\
	SEE ALSO\n\
		\n\
		Search for 'help', 'docker_help', 'conda_help' and 'miscellaneus'\n\
		for other sources of command line help or visit <$(basename $(sk8_source))>\n\
		for full documentation."

zlib_manual:
	@$(ECHO) "\n$(zlib_package) v$(zlib_version) for GNU/Linux.\n\
	Please, see <$(zlib_src)> for full documentation." >&2

zlib_version:
	@$(ECHO) "\n\
	Package:    $(zlib_package)\n\
	Version:    $(zlib_version)\n\
	Id:         $(zlib_id)\n\
	License:    $(zlib_license)\n\
	\n\
	Status:     $(zlib_status)\n\
	Source:     $(zlib_src)\n\
	Installdir: $(installdir)\n\
	\n\
	Runned with $(sk8_package), version $(sk8_version) for GNU/Linux.\n\
	Copyright (C) Bards Agrotechnologies, 2011-2021."


zlib_run: $(zlib_status)
	@$(ECHO) "\nRunning '$(zlib)' with arguments '$(args)' ..." >&2
	$(zlib) $(args)
	@$(ECHO) "Done." >&2


# Verification.
zlib_status: $(zlib_status) # 'zlib_install' or none.
	@$(ECHO) "\nSystem has '$(zlib_package)' up and running." >&2


# Install.
zlib_install: $(zlib_installfile) # $(installdir)/$(zlib_package)
	@$(ECHO) "\nSystem has '$(zlib_package)' in PATH: '$<'." >&2

$(zlib_installfile): $(zlib_execfile) | $(installdir) # just copy the executable file to $(installdir).
	@$(ECHO) "\nInstalling '$(zlib_package)' on directory '$(zlib_prefix)' ..." >&2
	$(MAKE) -C $(zlib_builddir) install DESTDIR=$(DESTDIR) prefix=$(zlib_prefix) -j4
	if [ -z "$$(perl -ne 'print if s#.*$(installdir).*#AA#g' <<< $${PATH})" ]; then \
		echo -e "export PATH=$(installdir):\$$PATH" >> ~/.bashrc; \
		source ~/.bashrc; \
		echo -e "WARNING: You may need to run 'source ~/.bashrc' to release the new \$$PATH."; \
	elif [ "$(zlib)" != "$(zlib_installfile)" ]; then \
		echo "WARNING: Previuos installation of '$(zlib_package)' may be shadowing the new one in the \$$PATH."; \
	fi
	touch $@
	@$(ECHO) "Done." >&2


# Build.
zlib_build: $(zlib_execfile) # $(zlib_builddir)/$(zlib_package)

$(zlib_execfile): $(zlib_buildfile) | $(gcc_status) # a Makefile generates the executable file.
	@$(ECHO) "\nBuilding '$(zlib_package)' on directory '$(zlib_builddir)' ..." >&2
	$(MAKE) -C $(zlib_builddir) -j4
	touch $@
	@$(ECHO) "Done." >&2


# Configure.
zlib_configure: $(zlib_buildfile) # $(zlib_builddir)/Makefile

$(zlib_buildfile): zlib_configure_cmd := ./configure --prefix=$(dir $(installdir)) $(zlib_args)
$(zlib_buildfile): $(zlib_configurefile)
	@$(ECHO) "\nConfiguring '$(zlib_package)' on directory '$(zlib_builddir)' ..." >&2
	cd $(zlib_builddir); \
	$(zlib_configure_cmd)
	touch $@
	@$(ECHO) "Done." >&2

$(zlib_configurefile): $(zlib_tarball) | $(zlib_builddir)
	@$(ECHO) "\nExtracting '$(zlib_package)' tarball to directory '$(zlib_builddir)' ..." >&2
	if [ "$(zlib_ext)" == ".tar.gz" ]; then \
		tar -xzvf $< -C $(zlib_builddir) --strip-components=1; \
	elif [ "$(zlib_ext)" == ".tar.xz" ]; then \
		tar -xJvf $< -C $(zlib_builddir) --strip-components=1; \
	else \
		tar -xvf $< -C $(zlib_builddir) --strip-components=1; \
	fi
	[ -s "$@" ] || { echo "ERROR: No file '$@' was extracted." >&2; exit 8; }
	touch $@
	@$(ECHO) "Done." >&2

$(zlib_builddir):
	@$(ECHO) "\nCreating directory '$(zlib_builddir)' ..." >&2
	mkdir -p $(zlib_builddir)
	@$(ECHO) "Done." >&2


# Download.
zlib_tarball: $(zlib_tarball) # $(tarballsdir)/$(zlib_tarball).

$(zlib_tarball): $(zlib_force) | $(tarballsdir) $(internet_status) $(zlib_deps_status) # none or 'zlib_deps'
	@$(ECHO) "\nDownloading '$(zlib_package)' tarball from '$(zlib_repository)' ..." >&2
	$(call downloader_cmd,$(dwlr),download,$(zlib_repository),$@,)
	touch $@
	@$(ECHO) "Done." >&2

zlib_deps: $(zlib_deps)
	@$(ECHO) "\nRecursively installing dependecies for '$(zlib_distname)' ..." >&2
	if [ -n "$(args)" ]; then \
		$(MAKE) -f $(firstword $(MAKEFILE_LIST)) recursive= $(args); \
	fi
	@$(ECHO) "Done." >&2

zlib_force:
	@$(ECHO) "\nForce reinstallation of '$(zlib_package)' on directory '$(installdir)'." >&2
	rm -f $(zlib_tarball)
	@$(ECHO) "Done." >&2


zlib_refresh: zlib_uninstall
	if [ -z "$(usermode)" ]; then \
		$(MAKE) -f $(firstword $(MAKEFILE_LIST)) DESTDIR=$(DESTDIR) prefix=$(prefix) zlib_install; \
	else \
		$(MAKE) -f $(firstword $(MAKEFILE_LIST)) DESTDIR=$(DESTDIR) prefix=$(prefix) zlib_install usermode=1; \
	fi


# Uninstall and clean.
zlib_purge: zlib_uninstall zlib_clean

zlib_uninstall:
	@$(ECHO) "\nUninstalling '$(zlib_package)' from directory '$(installdir)' ..." >&2
	[ -d "$(installdir)" ] || exit 1;
	if [ -f "$(zlib_buildfile)" ]; then \
		$(MAKE) -C $(zlib_builddir) uninstall DESTDIR=$(DESTDIR) prefix=$(dir $(installdir)) -j4; \
	else \
		rm -f $(zlib_install_bin); \
		rm -f $(zlib_install_include); \
		rm -f $(dir $(zlib_install_lib))/pkgconfig/$(zlib_package).pc; \
		rm -rf $(basename $(zlib_install_lib)).*; \
		rm -rf $(zlib_installfile)*; \
	fi
	@$(ECHO) "User must resolve \$$PATH in '~/.bashrc' file." >&2
	@$(ECHO) "Done." >&2

zlib_clean:
	@$(ECHO) "\nCleaning '$(zlib_package)' from staged directory '$(stageddir)' ..." >&2
	rm -rf $(tarballsdir)/$(zlib_package)*
	@$(ECHO) "Done." >&2


# Unit Test.
zlib_test: alt_name := $(or $(zlib_alt_name),$(zlib_package))
zlib_test: $(zlib_status)
	$(ECHO) "\nTesting installation of '$(zlib_distname)' package ..." >&2
	if [ -f "$(zlib_buildfile)" ]; then \
		if [ -n "$$(grep 'installcheck' $(zlib_buildfile))" ]; then \
			$(MAKE) -C $(zlib_builddir) installcheck; \
		else \
			$(MAKE) -C $(zlib_builddir) check; \
		fi \
	else \
		if [ -n "$(islib)" ]; then \
			echo "int main() {}" | cc -xc - -L$(zlib_installfile) -l$(alt_name) -o /dev/null; \
		else \
			$(zlib_installfile) --version; \
		fi \
	fi >&2
	#$(call test_lib,$(alt_name),$(zlib_prefix),$(args))
	$(ECHO) "Done." >&2


# Build tests.
args2 = $(MAKE) -f $(c_basedir)/SK8/$(notdir $(firstword $(MAKEFILE_LIST))) \
	usermode=1 \
	module=$(module) \
	recursive=$(recursive) \
	islib=$(islib) \
	zlib_src=$(zlib_src) \
	zlib_ext=$(zlib_ext) \
	zlib_package=$(zlib_package) \
	zlib_version=$(zlib_version) \
	zlib_alt_name=$(zlib_alt_name) \
	zlib_args='$(zlib_args)' \
	args='$(or $(args),--version)' \
	zlib_test


zlib_test_native: override args := $(args2)
zlib_test_native: native_test_build | $(srctestsdir)
	$(ECHO) "\nFinished native build test for '$(zlib_package)' package." >&2


zlib_test_conda: override args := $(args2)
zlib_test_conda: conda_test_build | $(srctestsdir)
	$(ECHO) "\nFinished Conda build test for '$(zlib_package)' package." >&2


zlib_test_docker: override args := $(args2)
zlib_test_docker: docker_test_build | $(srctestsdir)
	$(ECHO) "\nFinished Docker build test for '$(zlib_package)' package." >&2


zlib_test_all: zlib_test_docker zlib_test_conda zlib_test_native


.PHONY: zlib_help zlib_version zlib_manual zlib_force



############################## DOCUMENTATION ###################################
# Documentation was already written in the targets above.
