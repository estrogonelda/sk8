# Info to download, configure, build, install and use 'perl' package.
perl_src                    := https://www.cpan.org/src/5.0/perl-5.32.0.tar.gz
perl_ext                    := .tar.gz

perl_package                := $(shell perl -ne 'print "@{[ (split(/-/, $$_))[0] ]}"' <<< "$(notdir $(perl_src))")
perl_version                := $(shell perl -pe 's/^$(perl_package)-(.*?)\$(perl_ext)/$$1/g' <<< "$(notdir $(perl_src))")
perl_id                     := $(perl_package)-xxxxxx
perl_distname               := $(perl_package)-$(perl_version)
perl_license                := GPLv4

# Potential installation data.
perl_repository             := $(perl_src)
perl_tarball                := $(tarballsdir)/$(perl_distname)$(perl_ext)
perl_builddir               := $(tarballsdir)/$(perl_distname)
perl_configurefile          := $(perl_builddir)/configure
perl_buildfile              := $(perl_builddir)/Makefile
perl_execfile               := $(perl_builddir)/$(perl_package)
perl_installfile            := $(installdir)/$(perl_package)


# Actual installation data.
perl_has                    := $(shell which $(perl_package) 2> /dev/null)
perl_get                    := $(if $(perl_has),,perl_install)
perl                        := $(if $(perl_has),$(perl_has),$(perl_installfile))


# FORCE data.
perl_force                  := $(if $(FORCE),perl_force,)
perl_status                 := $(if $(FORCE),perl_install,$(perl_get))
perl                        := $(if $(FORCE),$(perl_installfile),$(perl))


# Include required modules.
#include $(addprefix $(sk8_rootdir)/modules/, gcc.mk)
#perl_deps                   := $(if $(recursive),perl_deps,)
perl_depscheck              := $(gcc_status) $(perl_deps)


# Verification.
perl_status: $(perl_status) # 'perl_install' or none.
	@$(ECHO) "\nSystem has '$(perl_package)' up and running." >&2


# Install.
perl_install: $(perl_installfile) # $(installdir)/$(perl_package)
	@$(ECHO) "\nSystem has '$(perl_package)' in PATH: '$<'." >&2

$(perl_installfile): $(perl_execfile) # just copy the executable file to $(installdir).
	@$(ECHO) "\nInstalling '$(perl_package)' on directory '$(installdir)' ..." >&2
	$(MAKE) -C $(perl_builddir) install DESTDIR=$(DESTDIR) prefix=$(dir $(installdir)) -j4
	if [ -z "$$(perl -ne 'print if s#.*$(installdir).*#AA#g' <<< $${PATH})" ]; then \
		echo -e "export PATH=$(installdir):\$$PATH" >> ~/.bashrc; \
		source ~/.bashrc; \
		echo -e "WARNING: You may need to run 'source ~/.bashrc' to release the new \$$PATH."; \
	elif [ "$(perl)" != "$(perl_installfile)" ]; then \
		echo "WARNING: Previuos installation of '$(perl_package)' may be shadowing the new one in the \$$PATH."; \
	fi
	touch $@
	@$(ECHO) "Done." >&2


# Build.
perl_build: $(perl_execfile) # $(perl_builddir)/$(perl_package)

$(perl_execfile): $(perl_buildfile) | $(gcc_status) # a Makefile generates the executable file.
	@$(ECHO) "\nBuilding '$(perl_package)' on directory '$(perl_builddir)' ..." >&2
	$(MAKE) -C $(perl_builddir) -j4
	touch $@
	@$(ECHO) "Done." >&2


# Configure.
perl_configure: $(perl_buildfile) # $(perl_builddir)/Makefile

$(perl_buildfile): $(perl_configurefile)
	@$(ECHO) "\nConfiguring '$(perl_package)' on directory '$(perl_builddir)' ..." >&2
	cd $(perl_builddir); \
	./configure.gnu --prefix=$(dir $(installdir)) $(perl_args)
	touch $@
	@$(ECHO) "Done." >&2

$(perl_configurefile): $(perl_tarball)
	@$(ECHO) "\nExtracting '$(perl_package)' tarball to directory '$(perl_builddir)' ..." >&2
	mkdir -p $(perl_builddir)
	if [ "$(perl_ext)" == ".tar.gz" ]; then \
		tar -xzvf $< -C $(perl_builddir) --strip-components=1; \
	else \
		tar -xvf $< -C $(perl_builddir) --strip-components=1; \
	fi
	touch $@
	@$(ECHO) "Done." >&2


# Download.
perl_tarball: $(perl_tarball) # $(tarballsdir)/$(perl_tarball).

$(perl_tarball): $(internet_status) $(perl_force) | $(tarballsdir) $(perl_depscheck)
	@$(ECHO) "\nDownloading '$(perl_package)' tarball from '$(perl_repository)' ..." >&2
	wget -c $(perl_repository) -P $(tarballsdir) -O $@
	touch $@
	@$(ECHO) "Done." >&2

perl_deps:
	@$(ECHO) "\nRecursively installing dependecies for '$(perl_distname)' ..." >&2
	[ -n "$(args)" ] || ( echo "ERROR: Variable 'args' can not be empty." >&2; exit 1 )
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) recursive= $(args)
	@$(ECHO) "Done." >&2

perl_force:
	@$(ECHO) "\nForce reinstallation of '$(perl_package)' on directory '$(installdir)'." >&2
	rm -f $(perl_tarball)
	@$(ECHO) "Done." >&2


perl_refresh: perl_uninstall
	if [ -z "$(usermode)" ]; then \
		$(MAKE) -f $(firstword $(MAKEFILE_LIST)) DESTDIR=$(DESTDIR) prefix=$(prefix) perl_install; \
	else \
		$(MAKE) -f $(firstword $(MAKEFILE_LIST)) DESTDIR=$(DESTDIR) prefix=$(prefix) perl_install usermode=1; \
	fi


# Uninstall and clean.
perl_purge: perl_uninstall perl_clean

perl_uninstall:
	@$(ECHO) "\nUninstalling '$(perl_package)' from directory '$(installdir)' ..." >&2
	[ -d "$(installdir)" ] || exit 1;
	if [ -f "$(perl_buildfile)" ]; then \
		$(MAKE) -C $(perl_builddir) uninstall DESTDIR=$(DESTDIR) prefix=$(dir $(installdir)) -j4; \
	else \
		rm -rf $(perl_installfile)*; \
	fi
	@$(ECHO) "User must resolve \$$PATH in '~/.bashrc' file." >&2
	@$(ECHO) "Done." >&2

perl_clean:
	@$(ECHO) "\nCleaning '$(perl_package)' from staged directory '$(stageddir)' ..." >&2
	rm -rf $(tarballsdir)/$(perl_package)*
	@$(ECHO) "Done." >&2


# Tests.
perl_test:
	@$(ECHO) "\nTesting '$(perl_package)' ..." >&2
	if [ -z "$$($(perl_package) --version 2> /dev/null)" ]; then \
		echo "Package '$(perl_package)' is not properly running." >&2; \
		exit 1; \
	else \
		echo "PASS!" \
	fi
	@$(ECHO) "Done." >&2

perl_test_build: args := make -f SK8/SK8.mk usermode=1 perl_install
perl_test_build: docker_test_build
	@$(ECHO) "\nFinished Docker build test for '$(perl_package)' module." >&2
