############################## HEADER ##########################################
# SK8 module for package 'gzip'.
################################################################################



############################## PREAMBLE ########################################
# Non-standalone run verification.
this                           := gzip.mk
ifeq ($(notdir $(firstword $(MAKEFILE_LIST))),$(this))
      $(error ERROR: Module '$(this)' cannot run in standalone mode, \
      it needs to be loaded by 'SK8')
endif

# Info to download, configure, build, install and use 'gzip' package.
gzip_src                    ?= https://ftp.gnu.org/gnu/gzip/gzip-1.12.tar.gz
gzip_ext                    ?= .tar.gz

gzip_package                ?= $(shell perl -ne 'print "@{[ (split(/-/, $$_))[0] ]}"' <<< "$(notdir $(gzip_src))")
gzip_version                ?= $(shell perl -pe 's/^$(gzip_package)-(.*?)\$(gzip_ext)/$$1/g' <<< "$(notdir $(gzip_src))")
gzip_id                     ?= $(gzip_package)-$(shell echo $$RANDOM)
gzip_license                ?= GPLv4
gzip_distname               ?= $(gzip_package)-$(gzip_version)

# Potential installation data.
gzip_repository             ?= $(gzip_src)
gzip_tarball                ?= $(tarballsdir)/$(gzip_distname)$(gzip_ext)
gzip_builddir               ?= $(tarballsdir)/$(gzip_distname)
gzip_configurefile          ?= $(gzip_builddir)/configure
gzip_buildfile              ?= $(gzip_builddir)/Makefile
gzip_execfile               ?= $(gzip_builddir)/$(gzip_package)

gzip_prefix                 ?= $(if $(supprefix),$(dir $(installdir))/$(gzip_package),$(dir $(installdir)))
gzip_install_bin            ?= $(gzip_prefix)/bin/$(gzip_package)
gzip_install_lib            ?= $(gzip_prefix)/lib/lib$(gzip_package).a
gzip_install_include        ?= $(gzip_prefix)/include/$(gzip_package).h
gzip_install_share          ?= $(gzip_prefix)/share/man

gzip_bins                   = $(wildcard $(gzip_builddir)/bin/*)
gzip_libs                   = $(wildcard $(gzip_builddir)/lib/lib*)
gzip_includes               = $(wildcard $(gzip_builddir)/include/*.h)


# Actual installation data.
ifeq ($(islib),)
gzip_installfile            ?= $(gzip_install_bin)
gzip_check                  ?= $(shell which $(gzip_package) 2> /dev/null)
else
gzip_installfile            ?= $(gzip_install_lib)
gzip_check                  ?= $(abspath $(strip $(wildcard $(basename $(gzip_installfile)).*)))
endif

gzip_get                    ?= $(if $(gzip_check),,gzip_install)
gzip                        ?= $(or $(gzip_check), $(gzip_installfile))


# FORCE data.
gzip_force                  ?= $(if $(FORCE),gzip_force,)
gzip_status                 ?= $(if $(FORCE),gzip_install,$(gzip_get))
gzip                        ?= $(if $(FORCE),$(gzip_installfile),$(gzip))


# Include required modules.
#include $(addprefix $(sk8_rootdir)/modules/, gcc.mk)

gzip_deps_status             ?= $(if $(recursive),gzip_deps,)
gzip_deps                    ?= $(gcc_status)



############################## MAIN ############################################
# Usage.
gzip_help:
	@$(ECHO) "\n\
	NAME\n\
		\n\
		gzip_package - $(gzip_package), version $(gzip_version)\n\
		\n\
	SYNOPSIS\n\
		\n\
		$$ sk9 [make_option ...] [variable=value ...] <gzip_target ...>\n\
		\n\
	DESCRIPTION\n\
		\n\
		This is an engine to download, configure, build, install and test packages\n\
		in a gzip way. So, the package must be compliant with the GNU Coding\n\
		Standards in order to be handled by SK8.\n\
		Now, the package in question is '$(gzip_distname)'.\n\
		\n\
	OPTIONS\n\
		\n\
		Targets:\n\
		\n\
		gzip_help                   Print this help text.\n\
		gzip_version                Print '$(gzip_package)' package version.\n\
		gzip_status                 Verifies '$(gzip_package)' current installation status.\n\
		gzip_run                    Run '$(gzip_package)' with the arguments given in the 'args' variable.\n\
		gzip_install                Install '$(gzip_package)' binary in '\$$(prefix)/bin' directory.\n\
		gzip_build                  Compile the '$(gzip_package)' binary in the build directory.\n\
		gzip_configure              Configure '$(gzip_package)' build files for this system.\n\
		gzip_tarball                Download the '$(gzip_package)' tarball from its repository: $(gzip_src)\n\
		gzip_uninstall              Uninstall '$(gzip_package)' from the '\$$(prefix)/bin' directory.\n\
		gzip_clear                  Remove '$(gzip_package)' tarball from tarball''s directory.\n\
		gzip_purge                  Uninstall and clear '$(gzip_package)' from the system.\n\
		gzip_refresh                Uninstall and then re-install '$(gzip_package)' again.\n\
		gzip_deps                   Install a '$(gzip_package)' dependency recursivelly as passed by the 'args' variable.\n\
		gzip_test                   Test '$(gzip_package)' installation.\n\
		gzip_test_build_native      Test '$(gzip_package)' build process, natively.\n\
		gzip_test_build_conda       Test '$(gzip_package)' build process in a new Conda environment.\n\
		gzip_test_build_docker      Test '$(gzip_package)' build process inside a vanilla Docker container.\n\
		gzip_test_build_aws         Test '$(gzip_package)' build process inside an AWS instance. NOT WORKING!!\n\
		gzip_create_img             Create a Docker image for '$(gzip_package)', based on '$(image)' image.\n\
		gzip_create_dockerfile      Create a Dockerfile to build an image for '$(gzip_package)'.\n\
		gzip_create_module          Create a pre-filled SK8 module for '$(gzip_package)' package.\n\
		\n\
		conda_run                      Run '$(gzip_package)' in a new Conda environment. See 'conda_help' for more info.\n\
		docker_run                     Run '$(gzip_package)' in a vanilla Docker image. See 'docker_help' for more info.\n\
		aws_run                        Run '$(gzip_package)' in an AWS instance. See 'aws_help' for more info. NOT WORKING!!\n\
		\n\
		Variables:\n\
		\n\
		args                           String of arguments passed to '$(gzip_package)' when running.               [ '' ]\n\
		prefix                         Path to the directory where to put the built '$(gzip_packages)' binaries.   [ /usr/local ]\n\
		DESTDIR                        Path to an alternative staged installation directory.                          [ '' ]\n\
		workdir                        The current directory.                                                         [ . ]\n\
		stageddir                      Path to a staged directory for tarballs and package builds.                    [ ./.local ]\n\
		usermode                       Boolean to switch between local or system-wide installations paths.            [ '' ]\n\
		recursive                      Boolean to enable recursive dependency installations.                          [ '' ]\n\
		FORCE                          Boolean to force a fresh installation over an existent one.                    [ '' ]\n\
		\n\
		src                            URL of the '$(gzip_package)' repository.                                    [ '$(gzip_src)' ]\n\
		ext                            The tarball extension. Only for extensions other than '.tar.gz'.               [ '.tar.gz' ]\n\
		gzip_package                The name of the package, derived from its tarball''s name.                     [ '$(gzip_package)' ]\n\
		gzip_version                The version of the package, derived from its tarball''s name.                  [ '$(gzip_version)' ]\n\
		\n\
		Make options:\n\
		\n\
		\n\
		NOTE: Some packages cannot build gzipally. So, user must test the chosen target first.\n\
		For a list of known gzipally installable packages, please read the\n\
		'$(sk8_rootdir)/modules/modules_urls.txt' file.\n\
		\n\
		\n\
	COPYRIGHT\n\
		\n\
		$(sk8_package), version $(sk8_version) for GNU/Linux.\n\
		Copyright (C) Bards Agrotechnologies, 2011-2021.\n\
		\n\
	SEE ALSO\n\
		\n\
		Search for 'help', 'docker_help', 'conda_help' and 'miscellaneus'\n\
		for other sources of command line help or visit <$(basename $(sk8_source))>\n\
		for full documentation."

gzip_manual:
	@$(ECHO) "\n$(gzip_package) v$(gzip_version) for GNU/Linux.\n\
	Please, see <$(gzip_src)> for full documentation." >&2

gzip_version:
	@$(ECHO) "\n\
	Package:    $(gzip_package)\n\
	Version:    $(gzip_version)\n\
	Id:         $(gzip_id)\n\
	License:    $(gzip_license)\n\
	\n\
	Status:     $(gzip_status)\n\
	Source:     $(gzip_src)\n\
	Installdir: $(installdir)\n\
	\n\
	Runned with $(sk8_package), version $(sk8_version) for GNU/Linux.\n\
	Copyright (C) Bards Agrotechnologies, 2011-2021."


gzip_run: $(gzip_status)
	@$(ECHO) "\nRunning '$(gzip)' with arguments '$(args)' ..." >&2
	$(gzip) $(args)
	@$(ECHO) "Done." >&2


# Verification.
gzip_status: $(gzip_status) # 'gzip_install' or none.
	@$(ECHO) "\nSystem has '$(gzip_package)' up and running." >&2


# Install.
gzip_install: $(gzip_installfile) # $(installdir)/$(gzip_package)
	@$(ECHO) "\nSystem has '$(gzip_package)' in PATH: '$<'." >&2

$(gzip_installfile): $(gzip_execfile) | $(installdir) # just copy the executable file to $(installdir).
	@$(ECHO) "\nInstalling '$(gzip_package)' on directory '$(gzip_prefix)' ..." >&2
	$(MAKE) -C $(gzip_builddir) install DESTDIR=$(DESTDIR) prefix=$(gzip_prefix) -j4
	if [ -z "$$(perl -ne 'print if s#.*$(installdir).*#AA#g' <<< $${PATH})" ]; then \
		echo -e "export PATH=$(installdir):\$$PATH" >> ~/.bashrc; \
		source ~/.bashrc; \
		echo -e "WARNING: You may need to run 'source ~/.bashrc' to release the new \$$PATH."; \
	elif [ "$(gzip)" != "$(gzip_installfile)" ]; then \
		echo "WARNING: Previuos installation of '$(gzip_package)' may be shadowing the new one in the \$$PATH."; \
	fi
	touch $@
	@$(ECHO) "Done." >&2


# Build.
gzip_build: $(gzip_execfile) # $(gzip_builddir)/$(gzip_package)

$(gzip_execfile): $(gzip_buildfile) | $(gcc_status) # a Makefile generates the executable file.
	@$(ECHO) "\nBuilding '$(gzip_package)' on directory '$(gzip_builddir)' ..." >&2
	$(MAKE) -C $(gzip_builddir) -j4
	touch $@
	@$(ECHO) "Done." >&2


# Configure.
gzip_configure: $(gzip_buildfile) # $(gzip_builddir)/Makefile

$(gzip_buildfile): gzip_configure_cmd := ./configure --prefix=$(dir $(installdir)) $(gzip_args)
$(gzip_buildfile): $(gzip_configurefile)
	@$(ECHO) "\nConfiguring '$(gzip_package)' on directory '$(gzip_builddir)' ..." >&2
	cd $(gzip_builddir); \
	$(gzip_configure_cmd)
	touch $@
	@$(ECHO) "Done." >&2

$(gzip_configurefile): $(gzip_tarball) | $(gzip_builddir)
	@$(ECHO) "\nExtracting '$(gzip_package)' tarball to directory '$(gzip_builddir)' ..." >&2
	if [ "$(gzip_ext)" == ".tar.gz" ]; then \
		tar -xzvf $< -C $(gzip_builddir) --strip-components=1; \
	elif [ "$(gzip_ext)" == ".tar.xz" ]; then \
		tar -xJvf $< -C $(gzip_builddir) --strip-components=1; \
	else \
		tar -xvf $< -C $(gzip_builddir) --strip-components=1; \
	fi
	[ -s "$@" ] || { echo "ERROR: No file '$@' was extracted." >&2; exit 8; }
	touch $@
	@$(ECHO) "Done." >&2

$(gzip_builddir):
	@$(ECHO) "\nCreating directory '$(gzip_builddir)' ..." >&2
	mkdir -p $(gzip_builddir)
	@$(ECHO) "Done." >&2


# Download.
gzip_tarball: $(gzip_tarball) # $(tarballsdir)/$(gzip_tarball).

$(gzip_tarball): $(gzip_force) | $(tarballsdir) $(internet_status) $(gzip_deps_status) # none or 'gzip_deps'
	@$(ECHO) "\nDownloading '$(gzip_package)' tarball from '$(gzip_repository)' ..." >&2
	$(call downloader_cmd,$(dwlr),download,$(gzip_repository),$@,)
	touch $@
	@$(ECHO) "Done." >&2

gzip_deps: $(gzip_deps)
	@$(ECHO) "\nRecursively installing dependecies for '$(gzip_distname)' ..." >&2
	if [ -n "$(args)" ]; then \
		$(MAKE) -f $(firstword $(MAKEFILE_LIST)) recursive= $(args); \
	fi
	@$(ECHO) "Done." >&2

gzip_force:
	@$(ECHO) "\nForce reinstallation of '$(gzip_package)' on directory '$(installdir)'." >&2
	rm -f $(gzip_tarball)
	@$(ECHO) "Done." >&2


gzip_refresh: gzip_uninstall
	if [ -z "$(usermode)" ]; then \
		$(MAKE) -f $(firstword $(MAKEFILE_LIST)) DESTDIR=$(DESTDIR) prefix=$(prefix) gzip_install; \
	else \
		$(MAKE) -f $(firstword $(MAKEFILE_LIST)) DESTDIR=$(DESTDIR) prefix=$(prefix) gzip_install usermode=1; \
	fi


# Uninstall and clean.
gzip_purge: gzip_uninstall gzip_clean

gzip_uninstall:
	@$(ECHO) "\nUninstalling '$(gzip_package)' from directory '$(installdir)' ..." >&2
	[ -d "$(installdir)" ] || exit 1;
	if [ -f "$(gzip_buildfile)" ]; then \
		$(MAKE) -C $(gzip_builddir) uninstall DESTDIR=$(DESTDIR) prefix=$(dir $(installdir)) -j4; \
	else \
		rm -f $(gzip_install_bin); \
		rm -f $(gzip_install_include); \
		rm -f $(dir $(gzip_install_lib))/pkgconfig/$(gzip_package).pc; \
		rm -rf $(basename $(gzip_install_lib)).*; \
		rm -rf $(gzip_installfile)*; \
	fi
	@$(ECHO) "User must resolve \$$PATH in '~/.bashrc' file." >&2
	@$(ECHO) "Done." >&2

gzip_clean:
	@$(ECHO) "\nCleaning '$(gzip_package)' from staged directory '$(stageddir)' ..." >&2
	rm -rf $(tarballsdir)/$(gzip_package)*
	@$(ECHO) "Done." >&2


# Unit Test.
gzip_test: alt_name := $(or $(gzip_alt_name),$(gzip_package))
gzip_test: $(gzip_status)
	$(ECHO) "\nTesting installation of '$(gzip_distname)' package ..." >&2
	if [ -f "$(gzip_buildfile)" ]; then \
		if [ -n "$$(grep 'installcheck' $(gzip_buildfile))" ]; then \
			$(MAKE) -C $(gzip_builddir) installcheck; \
		else \
			$(MAKE) -C $(gzip_builddir) check; \
		fi \
	else \
		if [ -n "$(islib)" ]; then \
			echo "int main() {}" | cc -xc - -L$(gzip_installfile) -l$(alt_name) -o /dev/null; \
		else \
			$(gzip_installfile) --version; \
		fi \
	fi >&2
	#$(call test_lib,$(alt_name),$(gzip_prefix),$(args))
	$(ECHO) "Done." >&2


# Build tests.
args2 = $(MAKE) -f $(c_basedir)/SK8/$(notdir $(firstword $(MAKEFILE_LIST))) \
	FORCE=yes \
	usermode=1 \
	islib= \
	recursive= \
	module=/home/lion/SK8/modules/$(this) \
	gzip_src=$(gzip_src) \
	gzip_ext=$(gzip_ext) \
	gzip_package=$(gzip_package) \
	gzip_version=$(gzip_version) \
	gzip_alt_name=$(gzip_alt_name) \
	gzip_args='$(gzip_args)' \
	args='$(or $(args),--version)' \
	gzip_test


gzip_test_native: override args := $(args2)
gzip_test_native: native_test_build | $(srctestsdir)
	$(ECHO) "\nFinished native build test for '$(gzip_package)' package." >&2


gzip_test_conda: override args := $(args2)
gzip_test_conda: conda_test_build | $(srctestsdir)
	$(ECHO) "\nFinished Conda build test for '$(gzip_package)' package." >&2


gzip_test_docker: override args := $(args2)
gzip_test_docker: docker_test_build | $(srctestsdir)
	$(ECHO) "\nFinished Docker build test for '$(gzip_package)' package." >&2


gzip_test_all: gzip_test_docker gzip_test_conda gzip_test_native


.PHONY: gzip_help gzip_version gzip_manual gzip_force



############################## DOCUMENTATION ###################################
# Documentation was already written in the targets above.
