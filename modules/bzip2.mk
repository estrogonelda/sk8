############################## HEADER ##########################################
# SK8 module for package 'bzip2'.
################################################################################



############################## PREAMBLE ########################################
# Non-standalone run verification.
this                           := bzip2.mk
ifeq ($(notdir $(firstword $(MAKEFILE_LIST))),$(this))
      $(error ERROR: Module '$(this)' cannot run in standalone mode, \
      it needs to be loaded by 'SK8')
endif

# Info to download, configure, build, install and use 'bzip2' package.
bzip2_src                    ?= https://sourceware.org/pub/bzip2/bzip2-1.0.8.tar.gz
bzip2_ext                    ?= .tar.gz

bzip2_package                ?= $(shell perl -ne 'print "@{[ (split(/-/, $$_))[0] ]}"' <<< "$(notdir $(bzip2_src))")
bzip2_version                ?= $(shell perl -pe 's/^$(bzip2_package)-(.*?)\$(bzip2_ext)/$$1/g' <<< "$(notdir $(bzip2_src))")
bzip2_id                     ?= $(bzip2_package)-$(shell echo $$RANDOM)
bzip2_license                ?= GPLv4
bzip2_distname               ?= $(bzip2_package)-$(bzip2_version)

# Potential installation data.
bzip2_repository             ?= $(bzip2_src)
bzip2_tarball                ?= $(tarballsdir)/$(bzip2_distname)$(bzip2_ext)
bzip2_builddir               ?= $(tarballsdir)/$(bzip2_distname)
bzip2_configurefile          ?= $(bzip2_builddir)/configure
bzip2_buildfile              ?= $(bzip2_builddir)/Makefile
bzip2_execfile               ?= $(bzip2_builddir)/$(bzip2_package)

bzip2_prefix                 ?= $(if $(supprefix),$(dir $(installdir))/$(bzip2_package),$(dir $(installdir)))
bzip2_install_bin            ?= $(bzip2_prefix)/bin/$(bzip2_package)
bzip2_install_lib            ?= $(bzip2_prefix)/lib/lib$(bzip2_package).a
bzip2_install_include        ?= $(bzip2_prefix)/include/$(bzip2_package).h
bzip2_install_share          ?= $(bzip2_prefix)/share/man

bzip2_bins                   = $(wildcard $(bzip2_builddir)/bin/*)
bzip2_libs                   = $(wildcard $(bzip2_builddir)/lib/lib*)
bzip2_includes               = $(wildcard $(bzip2_builddir)/include/*.h)


# Actual installation data.
ifeq ($(islib),)
bzip2_installfile            ?= $(bzip2_install_bin)
bzip2_check                  ?= $(shell which $(bzip2_package) 2> /dev/null)
else
bzip2_installfile            ?= $(bzip2_install_lib)
bzip2_check                  ?= $(abspath $(strip $(wildcard $(basename $(bzip2_installfile)).*)))
endif

bzip2_get                    ?= $(if $(bzip2_check),,bzip2_install)
bzip2                        ?= $(or $(bzip2_check), $(bzip2_installfile))


# FORCE data.
bzip2_force                  ?= $(if $(FORCE),bzip2_force,)
bzip2_status                 ?= $(if $(FORCE),bzip2_install,$(bzip2_get))
bzip2                        ?= $(if $(FORCE),$(bzip2_installfile),$(bzip2))


# Include required modules.
#include $(addprefix $(sk8_rootdir)/modules/, gcc.mk)

bzip2_deps_status             ?= $(if $(recursive),bzip2_deps,)
bzip2_deps                    ?= $(gcc_status)



############################## MAIN ############################################
# Usage.
bzip2_help:
	@$(ECHO) "\n\
	NAME\n\
		\n\
		bzip2_package - $(bzip2_package), version $(bzip2_version)\n\
		\n\
	SYNOPSIS\n\
		\n\
		$$ sk9 [make_option ...] [variable=value ...] <bzip2_target ...>\n\
		\n\
	DESCRIPTION\n\
		\n\
		This is an engine to download, configure, build, install and test packages\n\
		in a bzip2 way. So, the package must be compliant with the GNU Coding\n\
		Standards in order to be handled by SK8.\n\
		Now, the package in question is '$(bzip2_distname)'.\n\
		\n\
	OPTIONS\n\
		\n\
		Targets:\n\
		\n\
		bzip2_help                   Print this help text.\n\
		bzip2_version                Print '$(bzip2_package)' package version.\n\
		bzip2_status                 Verifies '$(bzip2_package)' current installation status.\n\
		bzip2_run                    Run '$(bzip2_package)' with the arguments given in the 'args' variable.\n\
		bzip2_install                Install '$(bzip2_package)' binary in '\$$(prefix)/bin' directory.\n\
		bzip2_build                  Compile the '$(bzip2_package)' binary in the build directory.\n\
		bzip2_configure              Configure '$(bzip2_package)' build files for this system.\n\
		bzip2_tarball                Download the '$(bzip2_package)' tarball from its repository: $(bzip2_src)\n\
		bzip2_uninstall              Uninstall '$(bzip2_package)' from the '\$$(prefix)/bin' directory.\n\
		bzip2_clear                  Remove '$(bzip2_package)' tarball from tarball''s directory.\n\
		bzip2_purge                  Uninstall and clear '$(bzip2_package)' from the system.\n\
		bzip2_refresh                Uninstall and then re-install '$(bzip2_package)' again.\n\
		bzip2_deps                   Install a '$(bzip2_package)' dependency recursivelly as passed by the 'args' variable.\n\
		bzip2_test                   Test '$(bzip2_package)' installation.\n\
		bzip2_test_build_native      Test '$(bzip2_package)' build process, natively.\n\
		bzip2_test_build_conda       Test '$(bzip2_package)' build process in a new Conda environment.\n\
		bzip2_test_build_docker      Test '$(bzip2_package)' build process inside a vanilla Docker container.\n\
		bzip2_test_build_aws         Test '$(bzip2_package)' build process inside an AWS instance. NOT WORKING!!\n\
		bzip2_create_img             Create a Docker image for '$(bzip2_package)', based on '$(image)' image.\n\
		bzip2_create_dockerfile      Create a Dockerfile to build an image for '$(bzip2_package)'.\n\
		bzip2_create_module          Create a pre-filled SK8 module for '$(bzip2_package)' package.\n\
		\n\
		conda_run                      Run '$(bzip2_package)' in a new Conda environment. See 'conda_help' for more info.\n\
		docker_run                     Run '$(bzip2_package)' in a vanilla Docker image. See 'docker_help' for more info.\n\
		aws_run                        Run '$(bzip2_package)' in an AWS instance. See 'aws_help' for more info. NOT WORKING!!\n\
		\n\
		Variables:\n\
		\n\
		args                           String of arguments passed to '$(bzip2_package)' when running.               [ '' ]\n\
		prefix                         Path to the directory where to put the built '$(bzip2_packages)' binaries.   [ /usr/local ]\n\
		DESTDIR                        Path to an alternative staged installation directory.                          [ '' ]\n\
		workdir                        The current directory.                                                         [ . ]\n\
		stageddir                      Path to a staged directory for tarballs and package builds.                    [ ./.local ]\n\
		usermode                       Boolean to switch between local or system-wide installations paths.            [ '' ]\n\
		recursive                      Boolean to enable recursive dependency installations.                          [ '' ]\n\
		FORCE                          Boolean to force a fresh installation over an existent one.                    [ '' ]\n\
		\n\
		src                            URL of the '$(bzip2_package)' repository.                                    [ '$(bzip2_src)' ]\n\
		ext                            The tarball extension. Only for extensions other than '.tar.gz'.               [ '.tar.gz' ]\n\
		bzip2_package                The name of the package, derived from its tarball''s name.                     [ '$(bzip2_package)' ]\n\
		bzip2_version                The version of the package, derived from its tarball''s name.                  [ '$(bzip2_version)' ]\n\
		\n\
		Make options:\n\
		\n\
		\n\
		NOTE: Some packages cannot build bzip2ally. So, user must test the chosen target first.\n\
		For a list of known bzip2ally installable packages, please read the\n\
		'$(sk8_rootdir)/modules/modules_urls.txt' file.\n\
		\n\
		\n\
	COPYRIGHT\n\
		\n\
		$(sk8_package), version $(sk8_version) for GNU/Linux.\n\
		Copyright (C) Bards Agrotechnologies, 2011-2021.\n\
		\n\
	SEE ALSO\n\
		\n\
		Search for 'help', 'docker_help', 'conda_help' and 'miscellaneus'\n\
		for other sources of command line help or visit <$(basename $(sk8_source))>\n\
		for full documentation."

bzip2_manual:
	@$(ECHO) "\n$(bzip2_package) v$(bzip2_version) for GNU/Linux.\n\
	Please, see <$(bzip2_src)> for full documentation." >&2

bzip2_version:
	@$(ECHO) "\n\
	Package:    $(bzip2_package)\n\
	Version:    $(bzip2_version)\n\
	Id:         $(bzip2_id)\n\
	License:    $(bzip2_license)\n\
	\n\
	Status:     $(bzip2_status)\n\
	Source:     $(bzip2_src)\n\
	Installdir: $(installdir)\n\
	\n\
	Runned with $(sk8_package), version $(sk8_version) for GNU/Linux.\n\
	Copyright (C) Bards Agrotechnologies, 2011-2021."


bzip2_run: $(bzip2_status)
	@$(ECHO) "\nRunning '$(bzip2)' with arguments '$(args)' ..." >&2
	$(bzip2) $(args)
	@$(ECHO) "Done." >&2


# Verification.
bzip2_status: $(bzip2_status) # 'bzip2_install' or none.
	@$(ECHO) "\nSystem has '$(bzip2_package)' up and running." >&2


# Install.
bzip2_install: $(bzip2_installfile) # $(installdir)/$(bzip2_package)
	@$(ECHO) "\nSystem has '$(bzip2_package)' in PATH: '$<'." >&2

$(bzip2_installfile): $(bzip2_execfile) | $(installdir) # just copy the executable file to $(installdir).
	@$(ECHO) "\nInstalling '$(bzip2_package)' on directory '$(bzip2_prefix)' ..." >&2
	$(MAKE) -C $(bzip2_builddir) install DESTDIR=$(DESTDIR) PREFIX=$(bzip2_prefix) -j4
	if [ -z "$$(perl -ne 'print if s#.*$(installdir).*#AA#g' <<< $${PATH})" ]; then \
		echo -e "export PATH=$(installdir):\$$PATH" >> ~/.bashrc; \
		source ~/.bashrc; \
		echo -e "WARNING: You may need to run 'source ~/.bashrc' to release the new \$$PATH."; \
	elif [ "$(bzip2)" != "$(bzip2_installfile)" ]; then \
		echo "WARNING: Previuos installation of '$(bzip2_package)' may be shadowing the new one in the \$$PATH."; \
	fi
	touch $@
	@$(ECHO) "Done." >&2


# Build.
bzip2_build: $(bzip2_execfile) # $(bzip2_builddir)/$(bzip2_package)

$(bzip2_execfile): $(bzip2_buildfile) | $(gcc_status) # a Makefile generates the executable file.
	@$(ECHO) "\nBuilding '$(bzip2_package)' on directory '$(bzip2_builddir)' ..." >&2
	$(MAKE) -C $(bzip2_builddir) -j4
	touch $@
	@$(ECHO) "Done." >&2


# Configure.
bzip2_configure: $(bzip2_buildfile) # $(bzip2_builddir)/Makefile

$(bzip2_buildfile): bzip2_configure_cmd := ./configure --prefix=$(dir $(installdir)) $(bzip2_args)
$(bzip2_buildfile): $(bzip2_configurefile)
	@$(ECHO) "\nConfiguring '$(bzip2_package)' on directory '$(bzip2_builddir)' ..." >&2
	#cd $(bzip2_builddir); \
	#$(bzip2_configure_cmd)
	touch $@
	@$(ECHO) "Done." >&2

$(bzip2_configurefile): $(bzip2_tarball) | $(bzip2_builddir)
	@$(ECHO) "\nExtracting '$(bzip2_package)' tarball to directory '$(bzip2_builddir)' ..." >&2
	if [ "$(bzip2_ext)" == ".tar.gz" ]; then \
		tar -xzvf $< -C $(bzip2_builddir) --strip-components=1; \
	elif [ "$(bzip2_ext)" == ".tar.xz" ]; then \
		tar -xJvf $< -C $(bzip2_builddir) --strip-components=1; \
	else \
		tar -xvf $< -C $(bzip2_builddir) --strip-components=1; \
	fi
	#[ -s "$@" ] || { echo "ERROR: No file '$@' was extracted." >&2; exit 8; }
	touch $@
	@$(ECHO) "Done." >&2

$(bzip2_builddir):
	@$(ECHO) "\nCreating directory '$(bzip2_builddir)' ..." >&2
	mkdir -p $(bzip2_builddir)
	@$(ECHO) "Done." >&2


# Download.
bzip2_tarball: $(bzip2_tarball) # $(tarballsdir)/$(bzip2_tarball).

$(bzip2_tarball): $(bzip2_force) | $(tarballsdir) $(internet_status) $(bzip2_deps_status) # none or 'bzip2_deps'
	@$(ECHO) "\nDownloading '$(bzip2_package)' tarball from '$(bzip2_repository)' ..." >&2
	$(call downloader_cmd,$(dwlr),download,$(bzip2_repository),$@,)
	touch $@
	@$(ECHO) "Done." >&2

bzip2_deps: $(bzip2_deps)
	@$(ECHO) "\nRecursively installing dependecies for '$(bzip2_distname)' ..." >&2
	if [ -n "$(args)" ]; then \
		$(MAKE) -f $(firstword $(MAKEFILE_LIST)) recursive= $(args); \
	fi
	@$(ECHO) "Done." >&2

bzip2_force:
	@$(ECHO) "\nForce reinstallation of '$(bzip2_package)' on directory '$(installdir)'." >&2
	rm -f $(bzip2_tarball)
	@$(ECHO) "Done." >&2


bzip2_refresh: bzip2_uninstall
	if [ -z "$(usermode)" ]; then \
		$(MAKE) -f $(firstword $(MAKEFILE_LIST)) DESTDIR=$(DESTDIR) prefix=$(prefix) bzip2_install; \
	else \
		$(MAKE) -f $(firstword $(MAKEFILE_LIST)) DESTDIR=$(DESTDIR) prefix=$(prefix) bzip2_install usermode=1; \
	fi


# Uninstall and clean.
bzip2_purge: bzip2_uninstall bzip2_clean

bzip2_uninstall:
	@$(ECHO) "\nUninstalling '$(bzip2_package)' from directory '$(installdir)' ..." >&2
	[ -d "$(installdir)" ] || exit 1;
	if [ -f "$(bzip2_buildfile)" ]; then \
		$(MAKE) -C $(bzip2_builddir) uninstall DESTDIR=$(DESTDIR) prefix=$(dir $(installdir)) -j4; \
	else \
		rm -f $(bzip2_install_bin); \
		rm -f $(bzip2_install_include); \
		rm -f $(dir $(bzip2_install_lib))/pkgconfig/$(bzip2_package).pc; \
		rm -rf $(basename $(bzip2_install_lib)).*; \
		rm -rf $(bzip2_installfile)*; \
	fi
	@$(ECHO) "User must resolve \$$PATH in '~/.bashrc' file." >&2
	@$(ECHO) "Done." >&2

bzip2_clean:
	@$(ECHO) "\nCleaning '$(bzip2_package)' from staged directory '$(stageddir)' ..." >&2
	rm -rf $(tarballsdir)/$(bzip2_package)*
	@$(ECHO) "Done." >&2


# Unit Test.
bzip2_test: alt_name := $(or $(bzip2_alt_name),$(bzip2_package))
bzip2_test: $(bzip2_status)
	$(ECHO) "\nTesting installation of '$(bzip2_distname)' package ..." >&2
	if [ -f "$(bzip2_buildfile)" ]; then \
		if [ -n "$$(grep 'installcheck' $(bzip2_buildfile))" ]; then \
			$(MAKE) -C $(bzip2_builddir) installcheck; \
		else \
			$(MAKE) -C $(bzip2_builddir) check; \
		fi \
	else \
		if [ -n "$(islib)" ]; then \
			echo "int main() {}" | cc -xc - -L$(bzip2_installfile) -l$(alt_name) -o /dev/null; \
		else \
			$(bzip2_installfile) --version; \
		fi \
	fi >&2
	#$(call test_lib,$(alt_name),$(bzip2_prefix),$(args))
	$(ECHO) "Done." >&2


# Build tests.
args2 = $(MAKE) -f $(c_basedir)/SK8/$(notdir $(firstword $(MAKEFILE_LIST))) \
	FORCE=yes \
	usermode=1 \
	islib= \
	recursive= \
	module=/home/lion/SK8/modules/$(this) \
	bzip2_src=$(bzip2_src) \
	bzip2_ext=$(bzip2_ext) \
	bzip2_package=$(bzip2_package) \
	bzip2_version=$(bzip2_version) \
	bzip2_alt_name=$(bzip2_alt_name) \
	bzip2_args='$(bzip2_args)' \
	args='$(or $(args),--version)' \
	bzip2_test


bzip2_test_native: override args := $(args2)
bzip2_test_native: native_test_build | $(srctestsdir)
	$(ECHO) "\nFinished native build test for '$(bzip2_package)' package." >&2


bzip2_test_conda: override args := $(args2)
bzip2_test_conda: conda_test_build | $(srctestsdir)
	$(ECHO) "\nFinished Conda build test for '$(bzip2_package)' package." >&2


bzip2_test_docker: override args := $(args2)
bzip2_test_docker: docker_test_build | $(srctestsdir)
	$(ECHO) "\nFinished Docker build test for '$(bzip2_package)' package." >&2


bzip2_test_all: bzip2_test_docker bzip2_test_conda bzip2_test_native


.PHONY: bzip2_help bzip2_version bzip2_manual bzip2_force



############################## DOCUMENTATION ###################################
# Documentation was already written in the targets above.
