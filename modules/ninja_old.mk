############################## NINJA PACKAGE ###################################

# Info to download, configure, build, install and use 'ninja' package.
ninja_src                      := https://github.com/ninja-build/ninja/releases/download/v1.10.0/ninja-linux.zip
ninja_ext                      := .zip

ninja_package                  := $(shell perl -ne 'print "@{[ (split(/-/, $$_))[0] ]}"' <<< "$(notdir $(ninja_src))")
ninja_version                  := 1.10.0
ninja_id                       := $(ninja_package)-xxxxxx
ninja_distname                 := $(ninja_package)-$(ninja_version)
ninja_license                  := GPLv4

# Potential installation data.
ninja_dependecies            := bash make gcc
ninja_repository             := $(ninja_src)
ninja_tarball                := $(tarballsdir)/$(ninja_distname)$(ninja_ext)
ninja_builddir               := $(tarballsdir)/$(ninja_distname)
ninja_configurefile          := $(ninja_builddir)/configure
ninja_buildfile              := $(ninja_builddir)/Makefile
ninja_execfile               := $(ninja_builddir)/$(ninja_package)
ninja_installfile            := $(installdir)/$(ninja_package)


# Actual installation data.
ninja_has                    := $(shell which $(ninja_package) 2> /dev/null)
ninja_get                    := $(if $(ninja_has),,ninja_install)
ninja                        := $(if $(ninja_has),$(ninja_has),$(ninja_installfile))


# FORCE data.
ninja_force                  := $(if $(FORCE),ninja_force,)
ninja_status                 := $(if $(FORCE),ninja_install,$(ninja_get))
ninja                        := $(if $(FORCE),$(ninja_installfile),$(ninja))


# Verification.
ninja_status: $(ninja_status) # 'ninja_install' or none.
	@$(ECHO) "\nSystem has '$(ninja_package)' up and running."


# Install.
ninja_install: $(ninja_installfile) # $(installdir)/$(ninja_package)
	@$(ECHO) "\nSystem has '$(ninja_package)' in PATH: '$<'."

$(ninja_installfile): $(ninja_execfile) # just copy the executable file to $(installdir).
	@$(ECHO) "\nInstalling '$(ninja_package)' on directory '$(installdir)' ..."
	install -d $(installdir)
	install -m 755 $< $(installdir)
	if [ -z "$$(perl -ne 'print if s#.*$(installdir).*#AA#g' <<< $${PATH})" ]; then \
		echo -e "export PATH=$(installdir):\$$PATH" >> ~/.bashrc; \
		source ~/.bashrc; \
		echo -e "WARNING: You may need to run 'source ~/.bashrc' to release the new \$$PATH."; \
	elif [ "$(ninja)" != "$(ninja_installfile)" ]; then \
		echo "WARNING: Previuos installation of '$(ninja_package)' may be shadowing the new one in the \$$PATH."; \
	fi
	touch $@
	@$(ECHO) "Done."


# Build.
ninja_build: $(ninja_execfile) # $(ninja_builddir)/$(ninja_package)

$(ninja_execfile): $(ninja_tarball) # a Makefile generates the executable file.
	@$(ECHO) "\nBuilding '$(ninja_package)' on directory '$(ninja_builddir)' ..."
	unzip $< -d $(ninja_builddir)
	touch $@
	@$(ECHO) "Done."


# Download.
ninja_tarball: $(ninja_tarball) # $(tarballsdir)/$(ninja_tarball).

$(ninja_tarball): $(internet_get) $(ninja_force) | $(tarballsdir)
	@$(ECHO) "\nDownloading '$(ninja_package)' tarball from '$(ninja_repository)' ..."
	wget -c $(ninja_repository) -P $(tarballsdir) -O $@
	touch $@
	@$(ECHO) "Done."


ninja_refresh: ninja_uninstall
	if [ -z "$(usermode)" ]; then \
		$(MAKE) -f $(firstword $(MAKEFILE_LIST)) DESTDIR=$(DESTDIR) prefix=$(prefix) ninja_install; \
	else \
		$(MAKE) -f $(firstword $(MAKEFILE_LIST)) DESTDIR=$(DESTDIR) prefix=$(prefix) ninja_install usermode=1; \
	fi


# Uninstall and clean.
ninja_purge: ninja_uninstall ninja_clean

ninja_uninstall:
	@$(ECHO) "\nUninstalling '$(ninja_package)' from directory '$(installdir)' ...";
	[ -d "$(installdir)" ] || exit 1;
	if [ -f "$(ninja_buildfile)" ]; then \
		$(MAKE) -C $(ninja_builddir) uninstall DESTDIR=$(DESTDIR) prefix=$(dir $(installdir)) -j4; \
	else \
		rm -rf $(ninja_installfile)*; \
	fi
	echo "User may resolve \$$PATH in '~/.bashrc' file." >&2 
	@$(ECHO) "Done."

ninja_clean:
	@$(ECHO) "\nCleaning '$(ninja_package)' from staged directory '$(stageddir)' ...";
	rm -rf $(tarballsdir)/$(ninja_package)*
	@$(ECHO) "Done."


# FORCE!
ninja_force:
	@$(ECHO) "\nForce installation of '$(ninja_package)' on directory '$(installdir)'."
	rm -rf $(ninja_tarball)
	@$(ECHO) "Done."


# Test.
ninja_test:
	@$(ECHO) "\nTesting package '$(ninja_package)' ...";
	if [ -z "$$($(ninja_package) --version 2> /dev/null)" ]; then \
		echo "Package '$(ninja_package)' is not properly running."; \
		exit 1; \
	else \
		echo "PASS!"; \
	fi
	@$(ECHO) "Done."



############################## DEBUG CODE ######################################

ifeq ($(dbg_ninja),yes)
$(info ****************************** DEBUG GENERIC PACK ******************************)
$(info Precursor variables for a 'ninja' package source.)
$(info workdir:                       $(workdir).)
$(info stageddir:                     $(stageddir).)
$(info tarballsdir:                   $(tarballsdir).)
$(info DESTDIR:                       $(DESTDIR).)
$(info installdir:                    $(installdir).)
$(info usermode:                      $(usermode).)
$(info src:                           $(src).)
$(info ext:                           $(ext).)
$(info )

$(info Info to download, configure, build, install and use 'ninja' package.)
$(info ninja_source:                $(ninja_source).)
$(info ninja_ext:                   $(ninja_ext).)
$(info )

$(info ninja_package:               $(ninja_package).)
$(info ninja_version:               $(ninja_version).)
$(info ninja_id:                    $(ninja_id).)
$(info ninja_distname:              $(ninja_distname).)
$(info ninja_license:               $(ninja_license).)
$(info )

$(info Potential installation data.)
$(info ninja_dependecies:           $(ninja_dependecies).)
$(info ninja_repository:            $(ninja_repository).)
$(info ninja_tarball:               $(ninja_tarball).)
$(info ninja_builddir:              $(ninja_builddir).)
$(info ninja_configurefile:         $(ninja_configurefile).)
$(info ninja_buildfile:             $(ninja_buildfile).)
$(info ninja_execfile:              $(ninja_execfile).)
$(info ninja_installfile:           $(ninja_installfile).)


$(info Actual installation data.)
$(info ninja_has:                   $(ninja_has).)
$(info ninja_get:                   $(ninja_get).)
$(info ninja:                       $(ninja).)
$(info )


$(info Status and FORCE data.)
$(info ninja_force:                 $(ninja_force).)
$(info ninja_status:                $(ninja_status).)
$(info ********************************************************************************)
endif
