############################## HEADER ##########################################
#
# SK8 module for Binformatics.
#
################################################################################





############################## PREAMBLE ########################################

# Non-standalone run verification.
this                           := biotools.mk
ifeq ($(notdir $(firstword $(MAKEFILE_LIST))),$(this))
      $(error ERROR: Module '$(this)' cannot run in standalone mode, \
      it needs to be loaded by 'SK8')
endif

# Include required modules.
$(warning MESSAGE: $(this) => Modules included: \
	$(addprefix $(sk8_rootdir)/modules/, parallel.mk samtools.mk bwa.mk))
include $(addprefix $(sk8_rootdir)/modules/, parallel.mk samtools.mk bwa.mk)


# Default variables for user interaction.
usermode                       := 1
data_mode                      ?= batch
data_type                      ?= tp
data_sample                    ?= all
data_control                   ?= solid tissue normal
data_control2                  ?=
data_control3                  ?=
data_treated                   ?= primary tumor
data_treated2                  ?=
data_treated3                  ?=
paired                         ?= no

ifeq ($(words $(data_sample)),1)
      radical                  ?= $(data_sample)
else
      radical                  ?= $(firstword $(data_sample))
endif

manifest                       ?= $(if $(inputfl),,$(radical).manifest)
manifest_type                  ?= tcga_manifest


# Miscelaneous data.
misc_dir                       ?= $(datadir)/misc
gene_universe                  ?= $(misc_dir)/universe.tsv
blacklist                      ?= $(misc_dir)/blacklist.bed
cosmic                         ?= $(misc_dir)/cosmic_cancergenes.tsv
chr_sizes                      ?= $(misc_dir)/chr_sizes.tsv

scripts_dir                    ?= $(rdir)/scripts
analyzer                       ?= $(scripts_dir)/rtc_analyser.pl
manifest_gen                   ?= $(scripts_dir)/manifest_gen.sh
payload_gen                    ?= $(scripts_dir)/payload_gen.sh


# UCSC.
ucsc_latest_u                  := https://hgdownload.soe.ucsc.edu/goldenPath/hg38/bigZips/latest/hg38.fa.gz
ucsc_latest_chrom_sizes_u      := https://hgdownload.soe.ucsc.edu/goldenPath/hg38/bigZips/latest/hg38.chrom.sizes
ucsc_hg38_u                    := https://hgdownload.soe.ucsc.edu/goldenPath/hg38/bigZips/hg38.fa.gz
ucsc_hg19_u                    := https://hgdownload.soe.ucsc.edu/goldenPath/hg19/bigZips/hg19.fa.gz
ucsc_urls                      := $(ucsc_latest_u) $(ucsc_latest_chrom_sizes_u) $(ucsc_hg19_u)

ucsc_dir                       ?= $(datadir)/ucsc
ucsc_latest                    ?= $(ucsc_dir)/$(notdir $(ucsc_latest_u))
ucsc_latest_chrom_sizes        ?= $(ucsc_dir)/$(notdir $(ucsc_latest_chrom_sizes_u))
ucsc_hg38                      ?= $(ucsc_dir)/$(notdir $(ucsc_hg38_u))
ucsc_hg19                      ?= $(ucsc_dir)/$(notdir $(ucsc_hg19_u))
ucsc_all                       ?= $(ucsc_latest) $(ucsc_hg19)
genome_ref                     ?= $(ucsc_latest)
genome_idx                     ?= $(genome_ref).fai


# TCGA.
tcga_endpoint_status           := https://api.gdc.cancer.gov/status
tcga_endpoint_manifest         := https://api.gdc.cancer.gov/manifest
tcga_endpoint_files            := https://api.gdc.cancer.gov/files
tcga_endpoint_data             := https://api.gdc.cancer.gov/data

tcga_dir                       ?= $(datadir)/tcga
tcga_radical                   := $(data_sample)
tcga_payload                   := $(tcga_radical).payl
tcga_precursor                 := $(tcga_payload:.payl=.prec)
tcga_manifest                  := $(tcga_payload:.payl=.manifest)
tcga_associations              := $(tcga_payload:.payl=.assoc)
tcga_tsv                       := $(tcga_payload:.payl=.tsv)
tcga_expr                      := $(tcga_payload:.payl=.expr)


# TCGA status verification.
tcga_has                       := $(shell echo `$(call downloader_cmd,$(dwlr),status,$(tcga_endpoint_status))`)
tcga_status                    := $(if $(tcga_has),,$(warning WARNING: TCGA server is unreachable!))


# GENCODE.
# NOTE: gencode v22 is for TCGA RNA-seq data and v26 is for GTEx.
gencode_latest_u               := ftp://ftp.ebi.ac.uk/pub/databases/gencode/Gencode_human/release_35/gencode.v35.annotation.gtf.gz
gencode_v22_u                  := ftp://ftp.ebi.ac.uk/pub/databases/gencode/Gencode_human/release_22/gencode.v22.annotation.gtf.gz
gencode_v26_collab_u           := https://storage.googleapis.com/gtex_analysis_v8/reference/gencode.v26.GRCh38.genes.gtf
gencode_urls                   := $(gencode_latest_u) $(gencode_v22_u) $(gencode_v26_collab_u)

gencode_dir                    ?= $(datadir)/gencode
gencode_latest                 ?= $(gencode_dir)/$(notdir $(gencode_latest_u))
gencode_v22                    ?= $(gencode_dir)/$(notdir $(gencode_v22_u))
gencode_v26                    ?= $(gencode_dir)/$(notdir $(gencode_v26_collab_u))
gencode_all                    ?= $(gencode_latest) $(gencode_v22)
annotation_ref                 ?= $(gencode_latest)

# ENSEMBL.
ensembl_transcriptome_v103_u   := http://ftp.ensembl.org/pub/release-103/fasta/homo_sapiens/cdna/Homo_sapiens.GRCh38.cdna.all.fa.gz
ensembl_transcriptome_v103     ?= $(gencode_dir)/$(notdir $(ensembl_transcriptome_v103_u))
transcriptome_ref              ?= $(ensembl_transcriptome_v103)


# GTEx.
gtex_attributes_u              := https://storage.googleapis.com/gtex_analysis_v8/annotations/GTEx_Analysis_v8_Annotations_SampleAttributesDS.txt
gtex_reads_u                   := https://storage.googleapis.com/gtex_analysis_v8/rna_seq_data/GTEx_Analysis_2017-06-05_v8_RNASeQCv1.1.9_gene_reads.gct.gz
gtex_tpm_u                     := https://storage.googleapis.com/gtex_analysis_v8/rna_seq_data/GTEx_Analysis_2017-06-05_v8_RNASeQCv1.1.9_gene_tpm.gct.gz
gtex_medians_u                 := https://storage.googleapis.com/gtex_analysis_v8/rna_seq_data/GTEx_Analysis_2017-06-05_v8_RNASeQCv1.1.9_gene_median_tpm.gct.gz

gtex_urls := $(gtex_attributes_u) $(gtex_reads_u) $(gtex_tpm_u) $(gtex_medians_u)

gtex_dir                       ?= $(datadir)/gtex
gtex_attributes                ?= $(gtex_dir)/$(notdir $(gtex_attributes_u))
gtex_reads                     ?= $(gtex_dir)/$(notdir $(gtex_reads_u))
gtex_tpm                       ?= $(gtex_dir)/$(notdir $(gtex_tpm_u))
gtex_medians                   ?= $(gtex_dir)/$(notdir $(gtex_medians_u))
gtex_all                       ?= $(gtex_attributes) $(gtex_reads) $(gtex_tpm) $(gtex_medians)

gtex_table                     := gtex_table.tsv
gtex_BoE                       := gtex_BoE.tsv
tau_up                         := 0.75
tau_down                       := 0.4
tpm_cutoff                     := 0.05


# GDC.
gdc_dir                        ?= $(datadir)/gdc
gdc_token                      ?= $(gdc_dir)/gdc_token.txt
gdc_client                     ?= $(gdc_dir)/gdc-client
token                          ?= $(gdc_token)

1KG_dir                       ?= $(datadir)/1KG


# Operational vars.
jobs                           ?= 10
step                           ?= 10
bwa                            ?= $(shell which bwa 2> /dev/null)
samtools                       ?= $(shell which samtools 2> /dev/null)
STAR                           ?= $(shell which STAR 2> /dev/null)
salmon                         ?= $(shell which salmon 2> /dev/null)
kallisto                       ?= $(shell which kallisto 2> /dev/null)
aligner                        ?= $(bwa)


lst                            := $(radical).lst
plt                            := $(radical).png
expr                           := $(radical)_tpm.tsv
mexpr                          := $(radical)_mexpr.tsv
deg                            := $(radical)_deg.tsv
gsea                           := $(radical)_gsea.tsv
dwl                            := $(addprefix $(outputsdir)/, $(notdir $(inputfl)))



############################## MACRO DEFINITIONS ###############################

define rec
      $(filter %$(1)$(sfx), $(inputfl))
endef


define opt
{                                                                              \
    "op":"=",                                                                  \
    "content":{                                                                \
        "field":"cases.project.project_id",                                    \
        "value":["TCGA-$(data_sample)"]                                        \
     }                                                                         \
},
endef

ifeq ($(data_sample),all)
      opt :=
endif


define payload_wgs
{                                                                              \
    "filters":{                                                                \
        "op":"and",                                                            \
        "content":[                                                            \
            {                                                                  \
                "op":"=",                                                      \
                "content":{                                                    \
                    "field":"cases.project.program.name",                      \
                    "value":["TCGA"]                                           \
                }                                                              \
            },$(opt)                                                           \
            {                                                                  \
                "op":"in",                                                     \
                "content":{                                                    \
                    "field":"cases.samples.sample_type",                       \
                    "value":["$(data_control)","$(data_treated)","$(data_treated2)"] \
                }                                                              \
            },                                                                 \
            {                                                                  \
                "op":"=",                                                      \
                "content":{                                                    \
                    "field":"files.data_format",                               \
                    "value":["bam"]                                            \
                }                                                              \
            },                                                                 \
            {                                                                  \
                "op":"=",                                                      \
                "content":{                                                    \
                    "field":"files.data_category",                             \
                    "value":["sequencing reads"]                               \
                }                                                              \
            },                                                                 \
            {                                                                  \
                "op":"=",                                                      \
                "content":{                                                    \
                    "field":"files.data_type",                                 \
                    "value":["Aligned Reads"]                                  \
                }                                                              \
            },                                                                 \
            {                                                                  \
                "op":"=",                                                      \
                "content":{                                                    \
                    "field":"files.experimental_strategy",                     \
                    "value":["WGS"]                                            \
                }                                                              \
            }                                                                  \
        ]                                                                      \
    },                                                                         \
    "format":"tsv",                                                            \
    "size":"90000",                                                            \
    "fields":"file_id,file_name,cases.case_id,cases.samples.sample_type,cases.project.project_id,cases.samples.submitter_id" \
}
endef

define payload_wxs
{                                                                              \
    "filters":{                                                                \
        "op":"and",                                                            \
        "content":[                                                            \
            {                                                                  \
                "op":"=",                                                      \
                "content":{                                                    \
                    "field":"cases.project.program.name",                      \
                    "value":["TCGA"]                                           \
                }                                                              \
            },$(opt)                                                           \
            {                                                                  \
                "op":"in",                                                     \
                "content":{                                                    \
                    "field":"cases.samples.sample_type",                       \
                    "value":["$(data_control)","$(data_treated)","$(data_treated2)"] \
                }                                                              \
            },                                                                 \
            {                                                                  \
                "op":"=",                                                      \
                "content":{                                                    \
                    "field":"files.data_format",                               \
                    "value":["bam"]                                            \
                }                                                              \
            },                                                                 \
            {                                                                  \
                "op":"=",                                                      \
                "content":{                                                    \
                    "field":"files.data_category",                             \
                    "value":["sequencing reads"]                               \
                }                                                              \
            },                                                                 \
            {                                                                  \
                "op":"=",                                                      \
                "content":{                                                    \
                    "field":"files.data_type",                                 \
                    "value":["Aligned Reads"]                                  \
                }                                                              \
            },                                                                 \
            {                                                                  \
                "op":"=",                                                      \
                "content":{                                                    \
                    "field":"files.experimental_strategy",                     \
                    "value":["WXS"]                                            \
                }                                                              \
            }                                                                  \
        ]                                                                      \
    },                                                                         \
    "format":"tsv",                                                            \
    "size":"90000",                                                            \
    "fields":"file_id,file_name,cases.case_id,cases.samples.sample_type,cases.project.project_id,cases.samples.submitter_id" \
}
endef

define payload_wts
{                                                                              \
    "filters":{                                                                \
        "op":"and",                                                            \
        "content":[                                                            \
            {                                                                  \
                "op":"=",                                                      \
                "content":{                                                    \
                    "field":"cases.project.program.name",                      \
                    "value":["TCGA"]                                           \
                }                                                              \
            },$(opt)                                                           \
            {                                                                  \
                "op":"in",                                                     \
                "content":{                                                    \
                    "field":"cases.samples.sample_type",                       \
                    "value":["$(data_control)","$(data_treated)","$(data_treated2)"] \
                }                                                              \
            },                                                                 \
            {                                                                  \
                "op":"=",                                                      \
                "content":{                                                    \
                    "field":"files.data_format",                               \
                    "value":["bam"]                                            \
                }                                                              \
            },                                                                 \
            {                                                                  \
                "op":"=",                                                      \
                "content":{                                                    \
                    "field":"files.data_category",                             \
                    "value":["sequencing reads"]                               \
                }                                                              \
            },                                                                 \
            {                                                                  \
                "op":"=",                                                      \
                "content":{                                                    \
                    "field":"files.data_type",                                 \
                    "value":["Aligned Reads"]                                  \
                }                                                              \
            },                                                                 \
            {                                                                  \
                "op":"=",                                                      \
                "content":{                                                    \
                    "field":"files.experimental_strategy",                     \
                    "value":["RNA-Seq"]                                        \
                }                                                              \
            }                                                                  \
        ]                                                                      \
    },                                                                         \
    "format":"tsv",                                                            \
    "size":"90000",                                                            \
    "fields":"file_id,file_name,cases.case_id,cases.samples.sample_type,cases.project.project_id,cases.samples.submitter_id" \
}
endef

define payload_tp
{                                                                              \
    "filters":{                                                                \
        "op":"and",                                                            \
        "content":[                                                            \
            {                                                                  \
                "op":"=",                                                      \
                "content":{                                                    \
                    "field":"cases.project.program.name",                      \
                    "value":["TCGA"]                                           \
                }                                                              \
            },$(opt)                                                           \
            {                                                                  \
                "op":"in",                                                     \
                "content":{                                                    \
                    "field":"cases.samples.sample_type",                       \
                    "value":["$(data_control)","$(data_treated)","$(data_treated2)"] \
                }                                                              \
            },                                                                 \
            {                                                                  \
                "op":"=",                                                      \
                "content":{                                                    \
                    "field":"files.analysis.workflow_type",                    \
                    "value":["STAR - Counts"]                                  \
                }                                                              \
            },                                                                 \
            {                                                                  \
                "op":"=",                                                      \
                "content":{                                                    \
                    "field":"files.data_category",                             \
                    "value":["transcriptome profiling"]                        \
                }                                                              \
            },                                                                 \
            {                                                                  \
                "op":"=",                                                      \
                "content":{                                                    \
                    "field":"files.data_type",                                 \
                    "value":["Gene Expression Quantification"]                 \
                }                                                              \
            },                                                                 \
            {                                                                  \
                "op":"=",                                                      \
                "content":{                                                    \
                    "field":"files.experimental_strategy",                     \
                    "value":["RNA-Seq"]                                        \
                }                                                              \
            }                                                                  \
        ]                                                                      \
    },                                                                         \
    "format":"tsv",                                                            \
    "size":"90000",                                                            \
    "fields":"file_id,file_name,cases.case_id,cases.samples.sample_type,cases.project.project_id,cases.samples.submitter_id" \
}
endef

# Decide payload type.
payload_gen                    := $(payload_tp)
ifeq ($(data_type),wgs)
      payload_gen              := $(payload_wgs)
endif
ifeq ($(data_type),wxs)
      payload_gen              := $(payload_wxs)
endif
ifeq ($(data_type),wts)
      payload_gen              := $(payload_wts)
endif
$(if $(filter $(data_type), wgs wxs wts tp),,\
	$(error ERROR: Unsuported data_type '$(data_type)'))


# Manifest filter. Never use comments in scripts like this (in the same line).
define manifest_gen
		chomp;                                                                 \
		print "CASE_ID\tTUMOR_TYPE\tSAMPLE_TYPE\tSUBMITTER_ID\tFILE_ID\tFILE_NAME\tID\n" if /file_id/; \
		next if /file_id/;                                                     \
		                                                                       \
		@groups = ("Solid Tissue Normal", "Blood Derived Normal",              \
			"Primary Tumor", "Metastatic");                                    \
		if ( "$(data_sample)" eq "all" ) {                                     \
			push @{ $$a{$$F[0]}{$$F[2]} } => $$_;                              \
		}                                                                      \
		else {                                                                 \
			push @{ $$a{$$F[0]}{$$F[2]} } => $$_ if $$F[1] =~ /TCGA-$(data_sample)/; \
		}                                                                      \
		$$gp{$$F[2]}++;                                                        \
		                                                                       \
		END {                                                                  \
			$$num = scalar keys %gp;                                           \
			foreach my $$k ( sort keys %a ) {                                  \
				$$j = 0;                                                       \
				$$prev = 0;                                                    \
				$$mn = 0;                                                      \
				$$mx = 0;                                                      \
				%prs = {};                                                     \
				@arr = ();                                                     \
				foreach my $$g ( @groups ) {                                   \
					next unless grep { $$_ eq $$g } keys %gp;                  \
					push @arr => $$g;                                          \
					@{ $$prs{$$g} } = sort { (split "\t", $$a)[5] cmp (split "\t", $$b)[5] } @{ $$a{$$k}{$$g} }; \
					$$mn = min($$prev, $$#{ $$a{$$k}{$$g} });                  \
					$$mx = max($$prev, $$#{ $$a{$$k}{$$g} });                  \
					$$prev = $$#{ $$a{$$k}{$$g} };                             \
				}                                                              \
				if ( $$mn != -1 and $$mn != $$mx ) {                           \
					$$ch_min = "";                                             \
					$$ch_max = "";                                             \
					for my $$g ( @arr ) {                                      \
						$$ch_min = $$g if $$#{ $$prs{$$g} } == $$mn;           \
						$$ch_max = $$g if $$#{ $$prs{$$g} } == $$mx;           \
					}                                                          \
					AGAIN: if ( $$ch_min and $$ch_max ) {                      \
						$$ptrn = (split "\t", @{ $$prs{$$ch_min} }[0])[5];     \
						$$ptrn = (split "-", $$ptrn)[0];                       \
						unless ( length($$ptrn) > 9                            \
							or @{ $$prs{$$ch_max} }[0] =~ /$$ptrn/             \
							or $$j > $$mx ) {                                  \
							$$tp = shift @{ $$prs{$$ch_max} };                 \
							push @{ $$prs{$$ch_max} } => $$tp;                 \
						}                                                      \
					}                                                          \
				}                                                              \
				PONGA: foreach my $$idx ( 0..$$mx ) {                          \
					for my $$g ( @arr ) {                                      \
						print "$$prs{$$g}->[$$idx]\n" if "$(paired)" ne "no"   \
							and $$idx <= $$mn;                                 \
						                                                       \
						if ( $$prs{$$g}->[$$idx] ) {                           \
							print STDERR (split /\t/, $$prs{$$g}->[$$idx])[5]; \
						}                                                      \
						else { print STDERR "."; }                             \
						print STDERR "\t" unless $$g eq $$arr[-1];             \
					}                                                          \
					print STDERR "\n";                                         \
				}                                                              \
				                                                               \
				unless ( "$(paired)" ne "no" ) {                               \
					foreach ( sort keys %{ $$a{$$k} } ) {                      \
						print join("\n", @{ $$a{$$k}{$$_} }), "\n" if @{ $$a{$$k}{$$_} }; \
					}                                                          \
				}                                                              \
			}                                                                  \
		}
endef


define tpm_gen
	BEGIN {                                                                \
		$$ck = 0;                                                          \
	}                                                                      \
		                                                                   \
	$$act = $$ARGV, $$ck++ unless $$ck;                                    \
	next if /^#/;                                                          \
		                                                                   \
	if ( $$act eq $$ARGV ) {                                               \
		if ( $$F[2] eq "gene" ) {                                          \
			$$ensembl = $$1 if $$F[8] =~ /gene_id "(.*?)";/;               \
			$$name = $$1 if $$F[8] =~ /gene_name "(.*?)";/;                \
			$$type = $$1 if $$F[8] =~ /gene_type "(.*?)";/;                \
			$$genes{$$ensembl} = [ $$name, 1, $$type, $$F[0] ];            \
			$$gene_ck = 0;                                                 \
		}                                                                  \
		elsif ( $$F[2] eq "transcript" ) {                                 \
			if ( $$gene_ck ) {                                             \
				$$gene_keep = $$gene_acm if $$gene_acm > $$gene_keep;      \
			}                                                              \
			else {                                                         \
				$$genes{$$gene_prev}->[1] = max($$gene_keep, 1) if defined $$gene_prev; \
				$$genes{$$gene_prev}->[1] = max($$gene_acm, 1) if defined $$gene_prev and $$gene_keep == 0; \
				$$gene_keep = 0;                                           \
			}                                                              \
			$$gene_prev = $$ensembl;                                       \
			$$gene_ck++;                                                   \
			$$gene_acm = 0;                                                \
		}                                                                  \
		elsif ( $$F[2] eq "exon" ) {                                       \
			$$gene_acm += abs($$F[3] - $$F[4]);                            \
		}                                                                  \
		else {}                                                            \
	}                                                                      \
	else {                                                                 \
		if ( $$ARGV =~ /GTEx/ or $$ARGV =~ /.dev.fd/ ) {                   \
			next if scalar(@F) < 3;                                        \
			if ( /^Name/ ) {                                               \
				@names = @F[2..$$#F];                                      \
				chomp $$names[-1];                                         \
				next;                                                      \
			}                                                              \
				                                                           \
			next unless exists $$genes{$$F[0]};                            \
				                                                           \
			$$rpk = $$genes{$$F[0]}->[1];                                  \
			next if $$rpk == 0;                                            \
				                                                           \
			push @gns => $$F[0];                                           \
			foreach ( 0..$$#names ) {                                      \
				$$mtx{$$names[$$_]}{$$F[0]} = $$F[$$_+2]/$$rpk;            \
				$$mtx{$$names[$$_]}{"acm"} += $$F[$$_+2]/$$rpk;            \
			}                                                              \
		}                                                                  \
		else {                                                             \
			next if /^_/;                                                  \
			next unless exists $$genes{$$F[0]};                            \
				                                                           \
			push @names => (split(m#/#, $$ARGV))[-1] if $$argv ne $$ARGV;  \
			$$argv = $$ARGV;                                               \
			push @gns => $$F[0] if scalar @names == 1;                     \
				                                                           \
			$$rpk = $$genes{$$F[0]}->[1];                                  \
			$$mtx{$$names[-1]}{$$F[0]} = $$F[1]/$$rpk;                     \
			$$mtx{$$names[-1]}{"acm"} += $$F[1]/$$rpk;                     \
		}                                                                  \
	}                                                                      \
		                                                                   \
	END {                                                                  \
		$$mtx{$$_}{"acm"} /= 1000000 for @names;                           \
		print "## Annotation file: $$act\n";                               \
		print "GENE_ID\tGENE_NAME\tGENE_TYPE\tGENE_LENGTH\t", join("\t", sort @names), "\n"; \
		foreach my $$gn ( sort @gns ) {                                    \
			$$xtra = "";                                                   \
			$$xtra = "(MT)" if $$genes{$$gn}->[3] eq "chrM";               \
			print "$$gn\t$$genes{$$gn}->[0]\t$$genes{$$gn}->[2]$$xtra\t$$genes{$$gn}->[1]"; \
			foreach my $$nm ( sort @names ) {                              \
				$$res = 0;                                                 \
				if ( $$mtx{$$nm}{"acm"} < 1e-6 ) {                         \
					print "\t-1";                                          \
				}                                                          \
				elsif ( ($$mtx{$$nm}{$$gn} / $$mtx{$$nm}{"acm"}) < 1e-4 ) { \
					print "\t0";                                           \
				}                                                          \
				else {                                                     \
					$$res = $$mtx{$$nm}{$$gn} / $$mtx{$$nm}{"acm"};        \
					printf("\t%06.4f", $$res);                             \
				}                                                          \
			}                                                              \
			print "\n";                                                    \
		}                                                                  \
	}
endef


define quantify
	perl -ane '                                                                \
		BEGIN { $$ck = 0; }                                                    \
		$$act = $$ARGV, $$ck++ unless $$ck;                                    \
		                                                                       \
		if ( $$act eq $$ARGV ) {                                               \
			next if /^Name/;                                                   \
			next if /^_/;                                                      \
			push @{ $$c{$$F[0]} } => $$F[1];                                   \
		}                                                                      \
		else {                                                                 \
			if ( /^GENE_ID/ ) {                                                \
				$$pos = 0;                                                     \
				for ( 4..$$#F ) {                                              \
					$$pos = $$_, last if $$act eq $$F[$$_];                    \
				}                                                              \
			}                                                                  \
			next if /^#/ or (! $$pos);                                         \
			next unless exists $$c{$$F[0]};                                    \
			                                                                   \
			push @{ $$c{$$F[0]} } => @F[0, 3, 3, $$pos];                       \
		}                                                                      \
		                                                                       \
		END {                                                                  \
			print "Name\tLength\tEffectiveLength\tTPM\tNumReads\n";            \
			print join("\t", @{ $$c{$$_} }[1..4, 0]), "\n"                     \
				foreach sort keys %c;                                          \
		}'
endef



############################## MAIN TARGETS ####################################

biotools_help:
	@$(ECHO) "\nUnder development. Arguments: '$(args)'." >&2

biotools_version:
	@$(ECHO) "\nUnder development. Arguments: '$(args)'." >&2

biotools_test:
	@$(ECHO) "\nA simple test for Biotools module. Arguments: '$(args)'." >&2

biotools_rec: $(manifest)
	@$(ECHO) "\nRecursively calling SK8 with commands '$(args)'." >&2
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) $(args)
	@$(ECHO) "Done." >&2



############################## COMMONS #########################################

manifest: $(manifest_type) # 'tcga_manifest' or '1KG_manifest'. 
	$(ECHO) "\nManifest complete: $^." >&2


# TCGA files manifest.
tcga_manifest: $(tcga_manifest)

$(tcga_manifest): $(tcga_precursor)
	$(ECHO) "\nCreating manifest file '$@' from precursor file '$<' ..." >&2
	$(PERL) -F"\t" -MList::Util=min,max \
		-ane '$(manifest_gen)' $< > $@ 2> $(tcga_associations)
	$(PERL) -i -F"\t" -lane 'print join("\t", @F[0..2,4,5,3])' $@
	[ -s "$(tcga_associations)" ] || ( echo "ERROR: Creation failed for file '$@'." >&2; exit 7 )
	$(ECHO) "Done." >&2


tcga_precursor: $(tcga_precursor)

$(tcga_precursor): $(tcga_payload) $(internet_get)
	$(ECHO) "\nCreating precursor file '$@' from GDC payload file '$<' ..." >&2
	$(call downloader_cmd,$(dwlr),query,$(tcga_endpoint_files),$@,$<)
	[ -s "$@" ] || ( echo "ERROR: Download of precursor file '$@' failed." >&2; exit 7 )
	$(ECHO) "Done." >&2


tcga_payload: $(tcga_payload)

.SECONDEXPANSION:
$(tcga_payload): $(manifest_FORCE)
	$(ECHO) "\nCreating GDC payload file for data type '$(data_type)' and tumor type '$(data_sample)' ..." >&2
	cat <<< '$(payload_gen)' > $@
	$(ECHO) "Done." >&2

tcga_status:
	$(ECHO) "\nTCGA server status." >&2
	$(call downloader_cmd,$(dwlr),status,$(tcga_endpoint_status))
	$(ECHO) "Done" >&2


# UCSC files manifest (1K genomes).
1KG_manifest: $(1KG_manifest)
	@$(ECHO) "\nUnsupported manifest type." >&2
	exit 7


manifest_FORCE:
	$(ECHO) "\nForcing payload file '$(tcga_payload)' recriation." >&2
	rm -f $(tcga_payload)
	$(ECHO) "Done." >&2



############################## CONTINUOUS MODE #################################
ifeq ($(batch),)


ifneq ($(MAKELEVEL),0)
ifneq ($(manifest),)
      dwl := $(addprefix $(outputsdir)/, \
		$(shell perl -F'\t' -lane 'next if /^CASE_ID/; print $$F[4]' "$(manifest)" 2> /dev/null))
endif

$(if $(dwl),,$(error ERROR: Either manifest file and input list are empty.))
endif


extr                           := $(basename $(dwl))

ifneq ($(filter %.bam, $(dwl)),)
dln                            := $(dwl:.bam=_dealigned_R1.fastq)
aln                            := $(dln:_dealigned_R1.fastq=_realigned.bam)
srt                            := $(if $(realign), $(aln:.bam=_sorted.bam), $(dwl:.bam=_sorted.bam))
endif

ifneq ($(filter %.fastq, $(dwl)),)
aln                            := $(filter %_aligned.bam, $(dwl:1.fastq=_aligned.bam))
srt                            := $(aln:.bam=_sorted.bam)
endif

ifneq ($(filter %.counts.gz, $(dwl)),)
quant                          := $(extr:.counts=.sf)
endif

idx                            := $(srt:.bam=.bam.bai)


analyze: $(or $(manifest),$(inputfl))
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) anlz

anlz: $(anlz)
	@$(ECHO) "\nAll analysis complete for files '$^'." >&2

$(anlz): $($(precursors)) | $($(tool)_status)
	@$(ECHO) "\nAnalyzing file '$@' from '$^' ..." >&2
	[ -n "$(tool)" ] || ( echo "ERROR: No tool specified for analysis with '$@'." >&2; exit 7 )
	$(tool) $(tool_args)
	@$(ECHO) "Done." >&2


sort: $(manifest)
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) srt

srt: $(srt)
	@$(ECHO) "\nAll sortings complete of files '$^'." >&2

ifneq ($(filter %.bam, $(dwl)),)
$(srt): %_sorted.bam: %.bam | $(samtools_status)
	@$(ECHO) "\nSorting file '$@' from '$^' ..." >&2
	if [ "$(sort_bam)" == "never" ]; then \
		echo "BAM file '$<' wont be sort."; \
		ln -s $< $@; \
	elif [ "$(sort_bam)" == "queryname" ]; then \
		samtools sort -n -O BAM -m 1G -@4 $< -o $@; \
	elif [ -n "$(sort_bam)" ]; then \
		samtools sort -O BAM -m 1G -@4 $< -o $@; \
	elif [ -n "$$(samtools view -H $< 2> /dev/null | perl -le '$$a = <>; print 1 if $$a =~ /SO:coordinate/')" ]; then \
		echo "BAM File '$<' was already sorted by coordinate."; \
		ln -s $< $@; \
	else \
		samtools sort -O BAM -m 1G -@4 $< -o $@; \
	fi
	@$(ECHO) "Done." >&2

else
$(srt): %_sorted.bam: %_aligned.bam | $(samtools_status)
	@$(ECHO) "\nSorting file '$@' from '$^' ..." >&2
	if [ "$(sort_bam)" == "never" ]; then \
		echo "BAM file '$<' wont be sort."; \
		ln -s $< $@; \
	elif [ "$(sort_bam)" == "name" ]; then \
		samtools sort -n -O BAM -m 1G -@4 $< -o $@; \
	elif [ -n "$(sort_bam)" ]; then \
		samtools sort -O BAM -m 1G -@4 $< -o $@; \
	elif [ -n "$$(samtools view -H $< 2> /dev/null | perl -le '$$a = <>; print 1 if $$a =~ /SO:coordinate/')" ]; then \
		echo "BAM File '$<' was already sorted by coordinate."; \
		ln -s $< $@; \
	else \
		samtools sort -O BAM -m 1G -@4 $< -o $@; \
	fi
	@$(ECHO) "Done." >&2

endif


align: $(manifest)
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) aln

aln: $(aln)
	@$(ECHO) "\nAll alignments complete for files '$^'." >&2

ifneq ($(filter %.bam, $(dwl)),)
$(aln): %_realigned.bam: $(genome_ref) %_dealigned_R1.fastq | $(bwa_status) $(samtools_status)
	@$(ECHO) "\nRealigning file '$@' from '$^' ..." >&2
	R2=$(@:_realigned.bam=_dealigned_R2.fastq); \
	[ -s $${R2} ] || R2=''; \
	if [ "$(aligner)" == "bwa" ]; then \
		$(bwa) mem -t$(jobs) $^ $${R2} | samtools sort -O BAM -m 1G -@4 -o $@; \
	else \
		$(star) -t$(jobs) $^ $${R2} | samtools view -b -o $@; \
	fi
	[ -s "$@" ] || ( echo "ERROR: Alignment failed for file '$@'." >&2; exit 7 )
	@$(ECHO) "Done." >&2

else
$(aln): %_aligned.bam: $(genome_ref) %1.fastq %2.fastq | $(bwa_status) $(samtools_status)
	@$(ECHO) "\nAligning file '$@' from '$^' ..." >&2
	if [ "$(aligner)" == "bwa" ]; then \
		$(bwa) mem -t$(jobs) $^ | samtools sort -O BAM -m 1G -@4 -o $@ -; \
	else \
		$(star) -t$(jobs) $^ | samtools view -b -o $@ -; \
	fi
	[ -s "$@" ] || ( echo "ERROR: Alignment failed for file '$@'." >&2; exit 7 )
	@$(ECHO) "Done." >&2

endif


dealign: $(manifest)
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) dln

dln: $(dln)
	@$(ECHO) "\nAll dealignments complete for files '$^'." >&2

$(dln): %_dealigned_R1.fastq: %.bam | $(samtools_status)
	@$(ECHO) "\nExtracting file '$@' from '$<' ..." >&2
	if [ -n "$$(samtools view -H $< 2> /dev/null | perl -le '$$a = <>; print 1 if $$a =~ /SO:queryname/')" ]; then \
		$(samtools) fastq -1 $@ -2 $(@:1.fastq=2.fastq) $< > $@; \
	else \
		$(samtools) sort -n -O BAM -m 1G -@4 $< | $(samtools) fastq -1 $@ -2 $(@:1.fastq=2.fastq) > $@; \
	fi
	[ -s "$@" ] || ( echo "ERROR: Dealignment failed for file '$@'." >&2; exit 7 )
	@$(ECHO) "Done." >&2


list: $(manifest)
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) lst

lst: $(lst)
	@$(ECHO) "\nAll lists complete for files '$^'." >&2

$(lst): $(if $(list_choice), $($(list_choice)), $(dwl))
	@$(ECHO) "\nCreating list file '$@' from files '$^' ..." >&2
	[ -n "$^" ] || ( echo "ERROR: No file specified for '$@'." >&2; exit 7 )
	for i in $^; do echo "$$i"; done > $@
	@$(ECHO) "Done." >&2


index: $(manifest)
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) idx

idx: $(idex)
	@$(ECHO) "\nAll indexes complete for files: '$^'." >&2

$(idx): %.bam.bai: %.bam
	$(ECHO) "\nCreating index file '$@' from BAM file '$<' ..." >&2
	if [ -s "$$(readlink -f $<).bai" ]; then \
		ln -sf "$$(readlink -f $<).bai" $@; \
	else \
		$(samtools) index -b -@4 $< $@; \
	fi
	[ -s "$@" ] || ( rm -f $@; echo "ERROR: Indexing produced an empty file '$@'." >&2; exit 7 )
	$(ECHO) "Done." >&2


extract: $(manifest)
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) extr

extr: $(extr)
	@$(ECHO) "\nAll extractions complete for files: '$^'." >&2

$(extr): %: %.gz
	@$(ECHO) "\nExtracting file '$@' from gzipped file '$<' ..." >&2
	gunzip -fk $<
	[ -s "$@" ] || ( rm -f $@; echo "ERROR: Extraction produced an empty file '$@'." >&2; exit 7 )
	@$(ECHO) "Done." >&2


downloads: $(manifest)
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) dwl

dwl: $(dwl)
	@$(ECHO) "\nAll downloads complete for files: '$^'." >&2

ifneq ($(manifest),)
$(dwl): $(manifest) | $(token) $(outputsdir)
	@$(ECHO) "\nDownloading file '$@' from manifest file '$<' ..." >&2
	[ -s "$<" ] || ( echo "ERROR: Manifest file '$<' is empty." >&2; exit 7 )
	id=$$(grep `basename $@` $< | cut -f4); \
	$(call downloader_cmd,$(dwlr),download_token,$(tcga_endpoint_data)/$${id},$@)
	[ -s "$@" ] || ( echo "ERROR: Downloaded file '$@' is empty" >&2; exit 7 )
	touch $@
	@$(ECHO) "Done." >&2


else
$(dwl): | $(inputfl) $(outputsdir)
	@$(ECHO) "\nLinking file '$@' from input list '$(inputfl)' ..." >&2
	inst=$(filter %$(notdir $@), $(inputfl)); \
	if [ "$@" != "$${inst}" ]; then \
		ln -fs `readlink -f $${inst}` $@; \
	else \
		touch $@; \
	fi
	@$(ECHO) "Done." >&2

endif



############################## BATCH MODE ######################################
else

num_lines                      := $(words $(inputfl) ponga)
ifneq ($(MAKELEVEL),0)
ifneq ($(manifest),)
      num_lines                := $(shell wc -l $(manifest) | awk '{ print $$1 }')
endif
endif

sfx_end                        := $(shell perl -e '$$a = $(num_lines) - 1; $$b = $(step); $$k = $$a / $$b; $$k = int($$a/$$b) + 1 if $$a % $$b; print $$k;')
sfx_list                       := $(shell seq -f "%04g" 1 $(sfx_end))
dwl1                           := $(addprefix $(outputsdir)/, $(sfx_list))
dwl2                           := $(addsuffix /, $(dwl1))
dwl3                           := $(join $(dwl2), $(sfx_list))
pdwl                           := $(addsuffix _$(radical).pdwl, $(dwl3))

dwl                            := $(pdwl:.pdwl=.dwl)
lst                            := $(pdwl:.pdwl=.lst)
dln                            := $(pdwl:.pdwl=.dln)
aln                            := $(pdwl:.pdwl=.aln)
srt                            := $(pdwl:.pdwl=.srt)
idx                            := $(pdwl:.pdwl=.idx)
db                             := $(pdwl:.pdwl=.db)
vcf                            := $(pdwl:.pdwl=.vcf)


analyze: $(or $(manifest),$(inputfl))
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) anlz

anlz: $(anlz)
	@$(ECHO) "\nAll analysis complete for files '$^'." >&2

$(anlz): $($(precursors)) | $($(tool)_status)
	@$(ECHO) "\nAnalyzing file '$@' from '$^' ..." >&2
	[ -n "$(tool)" ] || ( echo "ERROR: No tool specified for analysis with '$@'." >&2; exit 7 )
	$(tool) $(tool_args)
	@$(ECHO) "Done." >&2


index: $(manifest)
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) idx batch=1

idx: $(idx)

$(idx): %.idx: $(if $(realign),%.srt,%.dwl) | $(samtools_status)
	$(ECHO) "\nCreating index file '$@' from BAM file '$<' ..." >&2
	while read fl; do \
		if [ -s "$$(readlink -f $${fl}).bai" ]; then \
			ln -sf "$$(readlink -f $${fl}).bai" $${fl}.bai; \
		else \
			$(samtools) index -b -@4 $${fl} $${fl}.bai; \
		fi \
	done < $<
	find $(*D) -mindepth 1 -maxdepth 1 -type f -name "*.bai" > $@
	[ -s "$@" ] || ( echo "ERROR: Empty list file '$@'." >&2; exit 7 )
	[ -n "$(keep_files)" ] || rm -rf $$(< $<)
	$(ECHO) "Done." >&2


sort: $(manifest)
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) srt batch=1

srt: $(srt)

$(srt): %.srt: $(if $(realign),%.aln,%.dwl) | $(samtools_status)
	@$(ECHO) "\nSorting file '$@' from '$^' ..." >&2
	while read fl; do \
		if [ "$(sort_bam)" == "never" ]; then \
			echo "BAM file '$<' wont be sort."; \
			ln -s $${fl} $${fl%.*}_sorted.bam; \
		elif [ "$(sort_bam)" == "queryname" ]; then \
			samtools sort -n -O BAM -m 1G -@4 $${fl} -o $${fl%.*}_sorted.bam; \
		elif [ -n "$(sort_bam)" ]; then \
			samtools sort -O BAM -m 1G -@4 $${fl} -o $${fl%.*}_sorted.bam; \
		elif [ -n "$$(samtools view -H $< 2> /dev/null | perl -le '$$a = <>; print 1 if $$a =~ /SO:coordinate/')" ]; then \
			echo "BAM File '$<' was already sorted by coordinate."; \
			ln -s $${fl} $${fl%.*}; \
		else \
			samtools sort -O BAM -m 1G -@4 $${fl} -o $${fl%.*}_sorted.bam; \
		fi \
	done < $<
	find $(*D) -mindepth 1 -maxdepth 1 -type f -name "*_sorted.bam" > $@
	[ -s "$@" ] || ( echo "ERROR: Empty list file '$@'." >&2; exit 7 )
	[ -n "$(keep_files)" ] || rm -rf $$(< $<)
	@$(ECHO) "Done." >&2


align: $(manifest)
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) aln batch=1

aln: $(aln)

tst := $(or $(filter %.bam, $(inputfl)), $(filter %.bam, $(shell tail -n1 $(manifest) | cut -f5)))
ifneq ($(tst),)
$(aln): %.aln: %.dln | $(genome_ref) $(genome_idx) $($(aligner)_status) $(samtools_status)
	@$(ECHO) "\nRealigning files from list '$<' ..."
	parallel -j $(jobs) -N2 bwa mem $(genome_ref) {1} {2} '|' samtools view -bo {1.}_realigned.bam :::: $<; \
	[ $$? -eq 0 ] || ( echo "ERROR: Realignment failed for file '$@'." >&2; exit 7; )
	find $(*D) -mindepth 1 -maxdepth 1 -type f -name "*_realigned.bam" > $@
	[ -s "$@" ] || ( echo "ERROR: Empty list file '$@'." >&2; exit 7 )
	[ -n "$(keep_files)" ] || rm -rf $$(< $<)
	@$(ECHO) "Done." >&2

else
$(aln): %.aln: %.dwl | $(genome_ref) $($(aligner)_status)
	@$(ECHO) "\nAligning files from list '$<' ..."
	if [ "$${aligner}" == "bwa" ]; then \
		parallel $(jobs) bwa $(genome_ref) -1 {} -2 {= s/1.fastq/2.fastq/g =} '|' samtools -bo $@_aligned.bam :::: $<; \
		[ $$? -eq 0 ] || ( echo "ERROR: Dealignment failed for file '$@'." >&2; exit 7; ); \
	else \
		parallel $(jobs) star $(genome_ref) -1 {} -2 {= s/1.fastq/2.fastq/g =} -o $@ :::: $<; \
		[ $$? -eq 0 ] || ( echo "ERROR: Dealignment failed for file '$@'." >&2; exit 7; ); \
	fi
	find $(*D) -mindepth 1 -maxdepth 1 -type f -name "*_aligned.bam" > $@
	[ -s "$@" ] || ( echo "ERROR: Empty list file '$@'." >&2; exit 7 )
	[ -n "$(keep_files)" ] || rm -rf $$(< $<)
	@$(ECHO) "Done." >&2

endif


dealign: $(manifest)
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) dln batch=1

dln: $(dln)

$(dln): %.dln: %.dwl
	@$(ECHO) "\nDealigning files from list '$<' ..."
	parallel -j $(jobs) samtools sort -n -O BAM -m 1G {} '|' \
		samtools fastq -1 {.}_dealigned_R1.fastq -2 {.}_dealigned_R2.fastq '>' {.}_dealigned_R1.fastq :::: $<; \
	[ $$? -eq 0 ] || ( echo "ERROR: Dealignment failed for file '$@'." >&2; exit 7; )
	find $(*D) -mindepth 1 -maxdepth 1 -type f -name "*.fastq" | sort > $@
	[ -s "$@" ] || ( echo "ERROR: Empty list file '$@'." >&2; exit 7 )
	[ -n "$(keep_files)" ] || rm -rf $$(< $<)
	@$(ECHO) "Done." >&2



downloads: $(manifest)
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) dwl batch=1

dwl: $(dwl)
	@$(ECHO) "\nAll downloads complete: $^."

ifneq ($(manifest),)
$(dwl): %.dwl: %.pdwl | $(token) $(parallel_status)
	@$(ECHO) "\nDownloading files by ID from list '$<' ..." >&2
	tk=$$(< $(token)); \
	mn="$(abspath $(manifest))"; \
	cd $(*D); \
	if [[ "$(dwlr)" =~ curl ]]; then \
		parallel -q -j $(jobs) curl --remote-name --remote-header-name --header "X-Auth-Token: $${tk}" "$(tcga_endpoint_data)/{}" :::: $<; \
		[ $$? -eq 0 ] || ( echo "ERROR: Download fail in parallel." >&2; exit 7; ); \
	else \
		parallel -q -j $(jobs) wget -c --header "X-Auth-Token: $${tk}" "$(tcga_endpoint_data)/{}" -O {}.k :::: $<; \
		[ $$? -eq 0 ] || ( echo "ERROR: Download fail in parallel." >&2; exit 7; ); \
		for fl in $(*D)/*.k; do \
			new=$$(grep `basename $${fl%.*}` $${mn} | cut -f5); \
			mv $${fl} $(*D)/$${new}; \
		done \
	fi
	sfx=`tail -n1 $(manifest) | cut -f5`; \
	sfx=$${sfx//*.}; \
	[ -n "$$(find $(*D) -mindepth 1 -maxdepth 1 -type f -name *.$${sfx} -size -1M)" ] && ( echo "ERROR: File '$@' was corrupted." >&2; exit 7 ); \
	find $(*D) -mindepth 1 -maxdepth 1 -type f -name "*.$${sfx}" > $@
	[ -s "$@" ] || ( echo "ERROR: Empty list file '$@'." >&2; exit 7 )
	@$(ECHO) "Done." >&2


else
$(dwl): %.dwl: %.pdwl
	@$(ECHO) "\nLinking files '$@' from list '$^' ..." >&2
	while read file1 file2 file3; do \
		ln -fs `readlink -f $${file1}` $(*D)/link_`basename $${file1}`; \
		if [ -f "${file2}" ]; then \
			ln -fs `readlink -f $${file2}` $(*D)/link_`basename $${file2}`; \
		fi \
	done < $<
	find $(*D) -mindepth 1 -maxdepth 1 -type l -name "link_*" > $@
	[ -s "$@" ] || ( echo "ERROR: Empty list file '$@'." >&2; exit 7 )
	@$(ECHO) "Done." >&2

endif


pre_downloads: $(manifest)
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) pdwl batch=1

pdwl: $(pdwl)
	@$(ECHO) "\nAll pre downloads complete: $^."

ifneq ($(manifest),)
$(pdwl): %.pdwl: $(manifest)
	@$(ECHO) "\nCreating pre-download file '$@' from manifest file '$<' ..." >&2
	instance=$$(sed -r -n -e 's|.*/([0-9]{4})_$(radical).pdwl|\1|p' <<< "$@"); \
	[ -z "$${instance}" ] && echo "ERROR: Instance parsing error." && exit 1; \
	inst=$$(perl -ne '$$k = $$_; $$k = substr($$_,1) if ($$_ < 1000 and $$_ >= 100); $$k = substr($$_,2) if ($$_ < 100 and $$_ >= 10); $$k = substr($$_,3) if $$_ < 10; print $$k;' <<< "$${instance}"); \
	[ -z "$${inst}" ] && echo "ERROR: Instance parsing error." && exit 1; \
	mkdir -p $(*D); \
	if [ "$${inst}" -eq "$(sfx_end)" ]; then \
		if (( ($(num_lines)-1) % $(step) )); then \
			tail -n $$((($(num_lines)-1) % $(step))) $< | cut -f4 > $@; \
		else \
			tail $< | cut -f4 > $@; \
		fi \
	else \
		head -n $$(($${inst}*$(step) + 1)) $< | tail -n $(step) | cut -f4 > $@; \
	fi
	@$(ECHO) "Done." >&2

else
slice = $(shell perl -lane \
	'$$F[0] =~ s/.*(\d{4}).*/$$1/g; print "@F[($(step)*(int($$F[0]) - 1) + 1)..$(step)*int($$F[0])]"' <<< "$(1) $(2)")

.SECONDEXPANSION:
$(pdwl): %.pdwl: $$(call slice, $$*, $(inputfl))
	@$(ECHO) "\nCreating pre-download file '$@' from input list '$^'..." >&2
	mkdir -p $(*D);
	for i in $^; do echo "$$i"; done > $@
	@$(ECHO) "Done." >&2

endif

endif # Batch mode.



############################## 1K GENOMES ######################################

test_data_genomes:

1KG_endpoint                   := http://ftp.1000genomes.ebi.ac.uk/vol1/ftp/phase1/data/
hg00096_u                      := $(1KG_endpoint)/HG00096/alignment/HG00096.chrom20.ILLUMINA.bwa.GBR.low_coverage.20101123.bam
hg00097_u                      := $(1KG_endpoint)/HG00096/alignment/HG00096.chrom20.ILLUMINA.bwa.GBR.low_coverage.20101123.bam
hg00099_u                      := $(1KG_endpoint)/HG00096/alignment/HG00096.chrom20.ILLUMINA.bwa.GBR.low_coverage.20101123.bam
hg00100_u                      := $(1KG_endpoint)/HG00096/alignment/HG00096.chrom20.ILLUMINA.bwa.GBR.low_coverage.20101123.bam
hg00101_u                      := $(1KG_endpoint)/HG00096/alignment/HG00096.chrom20.ILLUMINA.bwa.GBR.low_coverage.20101123.bam



############################## UCSC ############################################

genome_ref: $(genome_ref)
genome_idx: $(genome_idx)
annotation_ref: $(annotation_ref)

$(genome_idx): %.fai: % | $(bwa_status)
	$(ECHO) "\nIndexing FASTA file '$<' ..." >&2
	$(bwa) index -p $* $<
	$(ECHO) "Done." >&2

$(ucsc_all): %.fa.gz: | $(internet_status) $(ucsc_dir)
	@$(ECHO) "\nDownloading UCSC genome file '$(notdir $@)' from '$(filter %$(notdir $@), $(ucsc_urls))' ..." >&2
	if [ "$(dwlr)" -eq "curl" ]; then \
		$(CURL) $(filter %$(notdir $@), $(ucsc_urls)) -o $@; \
	else \
		$(WGET) $(filter %$(notdir $@), $(ucsc_urls)) -O $@; \
	fi
	@$(ECHO) "Done." >&2


gene_universe: $(gene_universe)

$(gene_universe): $(annotation_ref) | $(misc_dir)
	$(ECHO) "\nCreating Gene Universe file '$@' from annotation file '$<' ..." >&2
	perl -F"\t" -lane \
		'next if /^#/ or $$F[2] ne "gene"; $$id = $$1 if $$F[8] =~ /gene_id "(.*?)";/; $$nm = $$1 if $$F[8] =~ /gene_name "(.*?)";/; $$tp = $$1 if $$F[8] =~ /gene_type "(.*?)";/; print "$$id\t$$nm\t$$tp"' \
		<(zcat $<) > $@
	$(ECHO) "Done." >&2

$(gencode_all): %.gtf.gz: | $(internet_status) $(gencode_dir)
	@$(ECHO) "\nDownloading UCSC genome file '$(notdir $@)' from '$(filter %$(notdir $@), $(ucsc_urls))' ..." >&2
	if [ "$(dwlr)" -eq "curl" ]; then \
		$(CURL) $(filter %$(notdir $@), $(gencode_urls)) -o $@; \
	else \
		$(WGET) $(filter %$(notdir $@), $(gencode_urls)) -O $@; \
	fi
	@$(ECHO) "Done." >&2

$(ucsc_dir) $(gencode_dir) $(1KG_dir) $(tcga_dir) $(gdc_dir) $(ensembl_dir) \
$(gtex_dir) $(misc_dir):
	$(ECHO) "\nCreating directory '$@' ..." >&2
	mkdir -p $@
	$(ECHO) "Done." >&2



############################## SALMON ##########################################

ifneq ($(filter salmon_%, $(MAKECMDGOALS)),)

ifeq ($(inputfl),)
      salmon_inputs            := $(shell perl -lane 'next if /^CASE/; print $$F[4]' $(manifest) 2> /dev/null)
else
      salmon_inputs            := $(if $(ctrl), $(dln), $(inputfl))
endif


salmon_inputs1                 += $(filter %1.fastq, $(salmon_inputs))
salmon_inputs2                 += $(filter %2.fastq, $(salmon_inputs))

#salmon_inputs1                 += $(wildcard *1.fastq.gz, $(salmon_inputs))
#salmon_inputs2                 += $(wildcard $(patsubst %1.fastq.gz, %2.fastq.gz, $(salmon_inputs1)))

ifneq ($(words (salmon_inputs1)),$(words $(salmon_inputs2)))
      $(error ERROR: No matching number of FASTq files.)
endif

salmon_quant                   := $(addprefix $(outputsdir)/, $(patsubst %.fastq,%,$(notdir $(salmon_inputs1))))
salmon_idx                     ?= $(gencode_dir)/Hs_salmon_idx


salmon_quant: $(salmon_quant)

$(salmon_quant): %: %1.fastq %2.fastq | $(salmon_idx)
	@$(ECHO) "\nQuantifying transcripts expression in files '$^' ..." >&2
	[ -n "$^" ] || ( echo "ERROR: Argument list is empty." >&2; exit 7 )
	salmon quant \
		-i $(salmon_idx) \
		-l A \
		-p $(jobs) \
		--validateMappings \
		-1 $< \
		-2 $(<:1.fastq=2.fastq) \
		-o $@
	[ -d "$@" ] || ( echo "ERROR: Nonexistent directory '$@'." >&2; exit 7 )
	@$(ECHO) "Done." >&2


salmon_index: $(salmon_idx)

$(salmon_idx): $(transcriptome_ref) | $(salmon_status)
	@$(ECHO) "\nCreating transcripts index from file '$<' ..." >&2
	salmon index -t $< -i $@
	@$(ECHO) "Done." >&2


ensembl_transcriptome: $(transcriptome_ref)

$(transcriptome_ref): | $(internet_status)
	@$(ECHO) "\nDownloading reference transcriptome file '$@' ..." >&2
	curl $(ensembl_transcriptome_v103_u) -o $@
	@$(ECHO) "Done." >&2

endif



############################## GETx ############################################


define perl_medians_by_tissue
	BEGIN { $$pos = 0; }                                                       \
	next if /^#/ or scalar @F < 3;                                             \
	                                                                           \
	if ( /^Name/ ) {                                                           \
		foreach ( 2..$$#F ) {                                                  \
			$$pos = $$_, last if $$F[$$_] eq "$(args)";                        \
		}                                                                      \
		die "Nonexistent tissue <$(args)>." unless $$pos;                      \
	}                                                                          \
	                                                                           \
	print join("\t", @F[0, 1, $$pos]), "\n";
endef

define perl_protein_coding_medians_by_tissue
	BEGIN { $$ck = 0; $$pos = 0; }                                             \
	$$act = $$ARGV, $$ck++ unless $$ck;                                        \
	next if /^#/ or scalar @F < 3;                                             \
	                                                                           \
	if ( $$act eq $$ARGV ) {                                                   \
		next unless $$F[2] eq "gene";                                          \
		next unless $$F[8] =~ /protein_coding/;                                \
		$$name = $$1 if $$F[8] =~ /gene_name "(.*?)";/;                        \
		next unless $$1;                                                       \
		$$gn{$$name} = 0;                                                      \
	}                                                                          \
	else {                                                                     \
		if ( /^Name/ ) {                                                       \
			foreach ( 2..$$#F ) {                                              \
				$$pos = $$_, last if $$F[$$_] eq "$(args)";                    \
			}                                                                  \
			die "Nonexistent tissue <$(args)>." unless $$pos;                  \
			print join("\t", @F[0, 1, $$pos]), "\n";                           \
			next;                                                              \
		}                                                                      \
		print join("\t", @F[0, 1, $$pos]), "\n" if exists $$gn{$$F[1]} and $$gn{$$F[1]} == 0; \
		$$gn{$$F[1]}++ if exists $$gn{$$F[1]};                                 \
	}
endef

define perl_boe
	next if /^#/ or scalar @F < 3; \
	@names = @F, next if /^Name/; \
	chomp $$F[-1]; \
	\
	@idx = sort { $$F[$$b] <=> $$F[$$a] } 2..$$#F; \
	$$cut = $(tpm_cutoff); \
	$$mx = max @F[2..$$#F]; \
	$$boe = -1; \
	$$boe = sum(map { $$_ >= $$cut ? 1 - ($$_/$$mx) : 1 } @F[2..$$#F])/(scalar @F - 3) if $$mx >= $$cut; \
	\
	$$add = ""; \
	$$tp = "missing"; \
	if ( $$boe > $(tau_up) ) { \
		$$tp = "specific"; \
		$$add = join "|",  @names[ grep { $$F[$$_] >= $$cut } @idx ]; \
	} \
	elsif ( $$boe < $(tau_down) and $$boe != -1 ) { \
		$$tp = "housekeeping"; \
		$$add = join "|",  @names[ grep { $$F[$$_] == 0 } @idx ]; \
	} \
	elsif ( $$boe != -1 ) { \
		$$tp = "middle_range"; \
		$$add = join "|", @names[ @idx[0..9] ]; \
	} \
	else {} \
	$$gn{$$F[0]} = [ $$F[0], $$F[1], $$boe, $$tp, $$add ]; \
	\
	END { \
		print join("\t", @{ $$gn{$$_} }) \
			for sort { $$gn{$$b}->[2] cmp $$gn{$$a}->[2] } keys %gn; \
	}
endef


gtex_get_all: $(gtex_attributes) $(gtex_reads) $(gtex_tpm) $(gtex_medians)


gtex_breadth_of_expression: $(gtex_BoE)

$(gtex_BoE): $(gtex_medians)
	@$(ECHO) "\nCalculating Breadth of Expression for genes in GTEx data: '$<' ..." >&2
	perl -F"\t" -MList::Util=sum,max -lane '$(perl_boe)' <(zcat $<) > $@
	@$(ECHO) "Done." >&2


gtex_calibrate: $(gtex_table)

$(gtex_table): $(gencode_v26) $(gtex_reads)
	@$(ECHO) "\nCalibrating TPM algorithm for GTEx data ..."
	perl -F"\t" -MList::Util=max -ane '$(tpm_gen)' $< <(zcat $(gtex_reads)) > $@
	@$(ECHO) "Done."


gtex_protein_coding_medians_by_tissue: $(gencode_v26) $(gtex_medians)
	@$(ECHO) "\nTaking GTEx protein coding genes expression medians for tissues: '$(args)' ..." >&2
	[ -z "$(args)" ] \
		&& echo "ERROR: Empty variable: 'args' for tissue name." >&2 && exit 1
	perl -F"\t" -ane '$(perl_protein_coding_medians_by_tissue)' $< <(zcat $(gtex_medians))
	@$(ECHO) "Done." >&2


gtex_medians_by_tissue: $(gtex_medians)
	@$(ECHO) "\nTaking GTEx gene expression medians for tissues: '$(args)' ..." >&2
	[ -z "$(args)" ] \
		&& echo "ERROR: Empty variable: 'args' for tissue name." >&2 && exit 1
	zcat $< | perl -F"\t" -ane '$(perl_medians_by_tissue)'
	@$(ECHO) "Done." >&2


gtex_tissues: $(gtex_medians)
	@$(ECHO) "\nTaking GTEx tissues names from '$<' ..." >&2
	zcat $< | perl -F"\t" -ane 'print join("\n", @F[2..$$#F]) if /^Name/'
	@$(ECHO) "Done." >&2

$(gtex_attributes) $(gtex_reads) $(gtex_tpm) $(gtex_medians):
	@$(ECHO) "\nDownloading GTEx data '$@' ..."
	wget $(filter %$(notdir $@), $(gtex_urls)) -P $(gtex_dir)
	touch $@
	@$(ECHO) "Done."



############################## MISCELLANEOUS ###################################

manifest_of_power              := tcga_power.manifest
manifest_wgs_all               := tcga_wgs_all.manifest
manifest_wgs_all_p             := tcga_wgs_all_p.manifest
manifest_wxs_all               := tcga_wxs_all.manifest
manifest_wxs_all_p             := tcga_wxs_all_p.manifest
manifest_wts_all               := tcga_wts_all.manifest
manifest_wts_all_p             := tcga_wts_all_p.manifest
manifest_tp_all                := tcga_tp_all.manifest
manifest_tp_all_p              := tcga_tp_all_p.manifest


rtc_table                      := rtc_table.tsv
rtc_extracted_manifest         := rtc_extracted.manifest
rtc_combined_manifest          := rtc_combined.manifest
rtc_combined_manifest2         := rtc_combined2.manifest
rtc_combined_matrix            := rtc_combined_matrix.tsv
rtc_match                      := rtc_expression_match.manifest
rtc_expression                 := rtc_expression.tsv
rtc_expression_soma            := rtc_expression_soma.tsv
rtc_expression_table           := rtc_expression_table.tsv
rtc_parentals                  := rtc_parentals.tsv
rtc_hosts                      := rtc_hosts.tsv
rtc_patterns                   := rtc_patterns.tsv
rtc_targets                    := rtc_targets.tsv
rtc_models                     := rtc_models.Rdata
mretro                         := $(radical)_mretro.tsv


define R_header
	options(tidyverse.quiet = T); \
	options(DESeq2.quiet = T); \
	library(tidyverse); \
	args <- commandArgs(trailingOnly = T);
endef

define R_import
	tbl <- read.table(args[1], header = T, sep = "\t");
endef

define R_device
	pdf($1, height = 21*0.3, width = 29.7*0.5); \
	print(g1); \
	dev.off();
endef

define R_gsea
	$(R_header) \
	library(clusterProfiler); \
	 \
	print(paste("Importing files:", args)); \
	res <- read.table(args[1], header = T, sep = "\t"); \
	tbl <- read.table(args[2], header = T, sep = "\t"); \
	tbl <- tbl[!tbl$$INFO %% 2, ]; \
	tbl <- tbl[!tbl$$INDIV == ".", ]; \
	str(tbl); \
	 \
	ids <- sub("(.*?)\\.\\d*", "\\1", rownames(res)); \
	geneList <- res$$log2FoldChange; \
	names(geneList) <- ids; \
	geneList <- sort(geneList, decreasing = TRUE); \
	 \
	eg = bitr(ids, fromType = "ENSEMBL", toType = c("ENTREZID", "SYMBOL"), OrgDb = "org.Hs.eg.db"); \
	geneList <- geneList[names(geneList) %in% unique(eg$$ENSEMBL)]; \
	k <- bitr(names(geneList), fromType = "ENSEMBL", toType = "ENTREZID", OrgDb = "org.Hs.eg.db"); \
	 \
	x <- NA; \
	nm <- names(geneList); \
	for (i in nm) \
		x <- c(x, which(k$$ENSEMBL == i)[1]); \
	 \
	names(geneList) <- k[x[!is.na(x)], 2]; \
	 \
	wpgmtfile <- system.file("extdata/wikipathways-20180810-gmt-Homo_sapiens.gmt", package = "clusterProfiler"); \
	wp2gene <- read.gmt(wpgmtfile); \
	str(wp2gene); \
	wp2gene <- wp2gene %>% tidyr::separate(term, c("name","version","wpid","org"), "%"); \
	wpid2gene <- wp2gene %>% dplyr::select(wpid, gene); \
	wpid2name <- wp2gene %>% dplyr::select(wpid, name); \
	 \
	ewp <- enricher(unique(names(geneList)), TERM2GENE = wpid2gene, TERM2NAME = wpid2name); \
	ewp <- setReadable(ewp, org.Hs.eg.db, keyType = "ENTREZID"); \
	write.table(as.data.frame(ewp), file = "$(gsea)", quote = F, sep = "\t");
endef
	
define R_DEG
	options(DESeq2.quiet = T); \
	library("DESeq2"); \
	args <- commandArgs(trailingOnly = T); \
	print(paste("Arguments:", args)); \
	\
	tbl <- read.table(args[1], header = T, sep = "\t"); \
	tbl <- tbl[order(tbl$$FILE_NAME), ]; \
	tumor <- sub("TCGA-(.*?)$$", "\\1", tbl$$TUMOR_TYPE[1]); \
	\
	genes <- read.table(args[2], sep = "\t"); \
	names(genes) <- c("ID", "NAME", "TYPE"); \
	\
	directory <- args[3]; \
	sampleFiles <- sort(grep(".counts$$", list.files(directory), value = T)); \
	\
	\
	if ( length(tbl$$ID) != length(sampleFiles) ) \
		exit(); \
	\
	sampleCondition <- sub("(.*treated).*","\\1",sampleFiles); \
	sampleTable <- data.frame(sampleName = sampleFiles, \
					fileName = sampleFiles, \
					condition = factor(tbl$$SAMPLE_TYPE, levels = c("Solid Tissue Normal", "Primary Tumor")) \
					); \
	\
	dds <- DESeqDataSetFromHTSeqCount(sampleTable = sampleTable, directory = directory, design = ~ condition); \
	keep <- rowSums(counts(dds)) > 10; \
	dds <- dds[keep,]; \
	\
	dds <- DESeq(dds); \
	res <- results(dds, alpha = 0.05); \
	\
	pv <- !is.na(res$$pvalue); \
	pdj <- !is.na(res$$padj); \
	relev <- !is.na(res$$log2FoldChange); \
	res <- res[pdj & relev & pv, ]; \
	\
	resOrd <- res[order(res$$log2FoldChange), ]; \
	write.table(as.data.frame(resOrd), file = paste0(tumor, "_deg.tsv"), quote = F, sep = "\t");
endef


define R_plot
	options(tidyverse.quiet = T);
	library(tidyverse);
	args <- commandArgs(trailingOnly = TRUE);
	
	man <- read.table(args[1], header = T, sep = "\t");
	expr <- read_tsv(args[2], comment = "#");
	a <- strsplit(args[3], split = " ")[[1]];
	
	nm <- names(expr)[-(1:4)];
	ord <- order(nm);
	guide <- data.frame( type = ifelse(man$$SAMPLE_TYPE == "Solid Tissue Normal", "Normal", "Tumor"),
		indiv = man$$FILE_NAME
		);
	guide <- guide[guide$$indiv %in% paste0(nm, ".gz"), ];
	guide <- guide[order(guide$$indiv), ];
	
	gene_ord <- order(a);
	ptn <- paste("^", a[gene_ord], "$$", sep = "", collapse = "|");
	mtx <- expr %>% filter(str_detect(GENE_NAME, ptn));
	expr <- NULL;
	
	genes <- factor(mtx$$GENE_NAME);
	cl <- length(genes);
	mtx <- t(mtx[, -(1:4)]);
	mtx <- mtx[ord, ];
	mtx <- as.data.frame(matrix(as.numeric(mtx), ncol = 1));
	mtx <- cbind(mtx, as.factor(rep(genes, each = length(ord))));
	mtx <- cbind(mtx, as.factor(rep(guide$$type, cl)));
	mtx <- cbind(mtx, rep(guide$$indiv, cl));
	colnames(mtx) <- c("DATA", "GENE", "TYPE", "INDIV");
	
	pdf("$(data_sample).pdf", height = 21*0.3, width = 29.7*0.5);
	ggplot(mtx, aes(x = TYPE, y = DATA, fill = TYPE)) + geom_boxplot() + labs(x = "$(data_sample)", y = "TPM", fill = "Tissue") + facet_wrap(~ GENE, nrow = 1, scales = "free");
	dev.off();
endef


define R_volcano
	options(tidyverse.quiet = T);
	library(tidyverse);
	require("ggrepel");
	args <- commandArgs(trailingOnly = TRUE);
	
	genes <- read.table(args[1], header = T, sep = "\t");
	genes$$Significant <- ifelse(genes$$padj < 0.05, "FDR < 0.05", "Not Sig");
	
	g <- ggplot(genes, aes(x = log2FoldChange, y = -log10(pvalue))) +
	geom_point(aes(color = Significant)) +
	scale_color_manual(values = c("red", "grey")) +
	theme_bw(base_size = 12) + theme(legend.position = "bottom")
	
	pdf("$(data_sample).pdf", height = 21*0.3, width = 29.7*0.5);
	print(g);
	dev.off();
endef


define perl_most_expressed
	$$act = $$ARGV, $$ck++ unless $$ck; \
	next if /^#/; \
	\
	if ( $$act eq $$ARGV ) { \
		next if /^CASE_ID/; \
		$$k = $$1 if $$F[4] =~ /(.*?)\.gz/; \
		$$n{$$k}++ if $$F[2] eq "Solid Tissue Normal"; \
		$$t{$$k}++ if $$F[2] eq "Primary Tumor"; \
	} \
	else { \
		@names = @F, next if /^GENE_ID/; \
		next if $$F[2] ne "protein_coding"; \
		@nacm = (); \
		@tacm = (); \
		foreach ( 4..$$#F ) { \
			push @nacm => $$F[$$_] if exists $$n{$$names[$$_]}; \
			push @tacm => $$F[$$_] if exists $$t{$$names[$$_]}; \
		} \
		$$nc{$$F[1]} = median(@nacm); \
		$$tc{$$F[1]} = median(@tacm); \
	} \
	\
	END { \
		print join("\n", map { "$$_\t$$nc{$$_}" } sort { $$nc{$$b} <=> $$nc{$$a} } keys %nc), "\n"; \
		print STDERR join("\n", map { "$$_\t$$tc{$$_}" } sort { $$tc{$$b} <=> $$tc{$$a} } keys %tc), "\n"; \
	}
endef

define perl_somatics
	next if /^#/ or $F[10] & 1; \
	next unless $$F[10] & 64; \
	\
	$$n = "Normal"; \
	$$n = "Tumor" if sum(@F[23, 24]); \
	$$a{$$F[15]} = $F[16]; \
	$$b{$$F[5]} = $F[16]; \
	\
	END { \
		print "$$_\t$a{$$_}\t$$n" for sort keys %a; \
		delete $$b{"none"}; \
		print STDERR "$$_\t$b{$$_}\t$$n" for sort keys %b; \
	}
endef

define perl_individuals
	$$act = $$ARGV, $$ck++ unless $$ck; \
	next if /^#/; \
	\
	if ( $$act eq $$ARGV ) { \
		next unless $$F[2] eq "gene"; \
		\
		$$id = $$1 if $$F[8] =~ /gene_id "(.*?)";/; \
		$$name = $$1 if $$F[8] =~ /gene_name "(.*?)";/; \
		$$type = $$1 if $$F[8] =~ /gene_type "(.*?)";/; \
		$$genes{$$id} = [ $$name, $$type ]; \
	} \
	else { \
		next if $$F[10] & 1 or $$F[31] eq "."; \
		\
		$$tst = "$(with_polymorphics)"; \
		$$mk = 0; \
		$$mk++ if $$F[10] & 64; \
		$$rtc{$$F[0]} = [ $$F[5], $$F[15], $$F[16], $$mk ] if $$mk or $$tst; \
		push @{ $$ind{$$_} } => $$F[0] for split /\|/, $$F[31]; \
	} \
	\
	END { \
		@pc = sort \
			uniq map { $$genes{$$_}->[0] } \
			grep { $$genes{$$_}->[1] eq "protein_coding" } keys %genes; \
		push @zr => 0 for 0..$$#pc; \
		print "INDIV\t", join("\t", @pc); \
		print STDERR "INDIV\t", join("\t", @pc); \
		foreach my $$i ( sort keys %ind ) { \
			@z_par = @zr; \
			@z_host = @zr; \
			foreach my $$pos ( 0..$$#pc ) { \
				$$z_par[$$pos]++ if grep { $$rtc{$$_}->[1] eq $$pc[$$pos] } @{ $$ind{$$i} }; \
				$$z_host[$$pos]++ if grep { $$rtc{$$_}->[0] eq $$pc[$$pos] } @{ $$ind{$$i} }; \
			} \
			print "$$i\t", join("\t", @z_par); \
			print STDERR "$$i\t", join("\t", @z_host); \
		} \
	}
endef

define perl_expression_match
	$$act = $$ARGV, $$ck++ unless $$ck;                                        \
	next if /^#/ or /^\s*$$/ or /^ID/ or /^CASE/;                              \
	chomp;                                                                     \
	                                                                           \
	if ( $$act eq $$ARGV ) {                                                   \
		next if $$F[10] & 1 or $$F[-1] eq "." or $$F[9] < 10;                  \
		next unless $$F[10] & 64;                                              \
		push @{ $$ind{$$F[-1]} } => [ @F ];                                    \
	}                                                                          \
	elsif ( $$ARGV =~ /wxs_all_p/ ) {                                          \
		push @{ $$cases{$$F[0]} } => [ @F ] if exists $$ind{$$F[4]};           \
	}                                                                          \
	else {                                                                     \
		$$prt{$$F[4]} = [ @F ] if exists $$cases{$$F[0]};                      \
	}                                                                          \
	                                                                           \
	END {                                                                      \
		print join("\t", @{ $$prt{$$_} }) for sort { $$prt{$$a}->[0] cmp $$prt{$$b}->[0] } keys %prt; \
	}
endef

define perl_expression_soma
	$$act = $$ARGV, $$ck++ unless $$ck;                                        \
	next if /^#/ or /^\s*$$/ or /^ID/ or /^CASE/;                              \
	                                                                           \
	                                                                           \
	if ( $$act eq $$ARGV ) {                                                   \
		next if $$F[10] & 1 or $$F[-1] eq "." or $$F[9] < 10;                  \
		next unless $$F[10] & 64 and ( $$F[4] eq "intronic" or $$F[4] eq "exonic" ); \
		                                                                       \
		$$tumors{$$F[16]} = "";                                                \
		push @{ $$ind{$$F[-1]} } => [ @F ];                                    \
	}                                                                          \
	elsif ( $$ARGV =~ /wxs_all_p/ ) {                                          \
		push @{ $$cases{$$F[0]} } => [ @F ] if exists $$ind{$$F[4]};           \
	}                                                                          \
	elsif ( $$ARGV =~ /tp_all/ ) {                                             \
		my $$type = $$F[1] =~ /TCGA-(.*?)$$/ ? $$1 : ".";                      \
		push @{ $$types{$$type}{$$F[2]} } => $$F[4] if exists $$tumors{$$type}; \
		next if $$F[2] =~ /Normal/;                                            \
		push @{ $$expr{$$F[4]} } => [ @F ] if exists $$cases{$$F[0]};          \
	}                                                                          \
	elsif ( $$ARGV =~ /gtex/ ) {                                               \
		$$boe{$$F[1]} = [ @F ] unless defined $$boe{$$F[1]};                   \
	}                                                                          \
	elsif ( $$ARGV =~ /degao/ ) {                                              \
		my $$mt = "";                                                          \
		$$mt = (grep { $$F[1] eq $$boe{$$_}->[0] } keys %boe)[0] if exists $$tumors{$$F[0]}; \
		$$deg{$$F[0]}{$$mt} = [ @F ] if $$mt;                                  \
	}                                                                          \
	else {                                                                     \
		if ( /^GENE_ID/ ) {                                                    \
			@hd = @F[0..3];                                                    \
			push @hd => (split(m#/#, $$_))[-1] . ".gz" for @F[4..$$#F];        \
			next;                                                              \
		}                                                                      \
		                                                                       \
		@x = ();                                                               \
		foreach my $$i ( keys %ind ) {                                         \
			push @x => map { $$_->[15] } grep { $$F[1] eq "$$_->[5]" } @{ $$ind{$$i} }; \
		}                                                                      \
		if ( @x ) {                                                            \
			$$all{$$_}++ for ( $$F[1], @x );                                   \
		}                                                                      \
		                                                                       \
		                                                                       \
		foreach my $$tp ( keys %types ) {                                      \
			next if $$tp eq ".";                                               \
			if ( ! $$ct ) {                                                    \
				for my $$a ( @{ $$types{$$tp}{"Primary Tumor"} } ) {           \
					push @{ $$pos{$$tp} } => grep {$$_ > 0 } map { $$a =~ /$$hd[$$_]/ ? $$_: 0 } 4..$$#hd; \
				}                                                              \
			}                                                                  \
			                                                                   \
			my @ch = @{ $$pos{$$tp} };                                         \
			$$genes{$$tp}{$$F[1]}{"tumor"} = {                                 \
				"n"       => scalar(@ch),                                      \
				"median"  => median(@F[@ch]),                                  \
				"mean"    => mean(@F[@ch]),                                    \
				"std"     => stddev(@F[@ch]),                                  \
				"norm"    => 0,                                                \
				"pos"     => [ @ch ],                                          \
				"val"     => [ @F ],                                           \
			};                                                                 \
		}                                                                      \
		$$ct++;                                                                \
	}                                                                          \
	                                                                           \
	END {                                                                      \
		delete $$types{"."};                                                   \
		$$ex = (keys %genes)[0];                                               \
		print STDERR "IND ", scalar keys %ind, "."; \
		print STDERR "CASES ", scalar keys %cases, "."; \
		print STDERR "EXPR ", scalar keys %expr, "."; \
		print STDERR "BOE ", scalar keys %boe, "."; \
		print STDERR "ALL ", scalar keys %all, "."; \
		print STDERR "GENES $$ct."; \
		print STDERR "GENES hash ", scalar keys %{ $$genes{$$ex} }, "."; \
		foreach my $$g ( keys %{ $$genes{$$ex} } ) {                           \
			unless ( exists $$all{$$g} ) {                                     \
				delete $$genes{$$_}{$$g} for keys %genes;                      \
			}                                                                  \
		}                                                                      \
		print STDERR "Ah la2 ", scalar keys %{ $$genes{$$ex} }, "."; \
	                                                                           \
		                                                                       \
		print "CHROM\tPOS\tPOL\tREG\tHOST\tCASE\tFILE\tSAMPLE\tSUBSAMPLE\tINFO\tN\tMEDIAN\tMEAN\tSTD\tNORMALITY\tTAU\tTISSUE\tDIFF_EXPR\tVAL\tZ\tPARENTAL\tMEDIANp\tMEANp\tSTDp\tNORMALITYp\tTAUp\tTISSUEp\tDIFF_EXPRp\tVALp\tZp\tCORR\tREL";                                                                \
		my @out;                                                               \
		foreach my $$i ( keys %ind ) {                                         \
			foreach my $$ins ( @{ $$ind{$$i} } ) {                             \
				my @vct = @{ $$ins };                                          \
				my $$host = $$vct[5];                                          \
				my $$tumor = $$vct[16];                                        \
				die "ERROR: Unexistent gene \"$$host\"." unless exists $$genes{$$tumor}{$$host}; \
				                                                               \
				my $$reg = $$vct[4];                                           \
				my $$par = $$vct[15];                                          \
				my $$tissue = "Tumor";                                         \
				$$tissue = "Normal" if sum(@vct[21, 22]);                      \
				my $$rel = ".";                                                \
				                                                               \
				my $$cs = "none";                                              \
				for my $$k ( keys %cases ) {                                   \
					$$cs = (map { $$_->[0] } grep { $$i eq $$_->[4] } @{ $$cases{$$k} })[0]; \
					last if length($$cs) > 4;                                  \
				}                                                              \
				die "ERROR: File \"$$i\" must have a case ID." if $$cs eq "none"; \
				                                                               \
				my @exp = ();                                                  \
				for my $$k ( keys %expr ) {                                    \
					next if $$expr{$$k}->[0][2] !~ /$$tissue/;                 \
					push @exp => $$k if grep { $$cs eq $$_->[0] } @{ $$expr{$$k} }; \
				}                                                              \
				next unless @exp;                                              \
				                                                               \
				my @pos = ();                                                  \
				for my $$p ( @exp ) {                                          \
					push @pos => (grep { $$_ > 0 } map { $$p =~ /$$hd[$$_]/ ? $$_ : 0 } 4..$$#hd)[0]; \
				}                                                              \
				                                                               \
				$$boe_tau_h = exists $$boe{$$host} ? $$boe{$$host}->[2] : -1;  \
				$$boe_spec_h = exists $$boe{$$host} ? (grep { $$_ !~ /Cells/ } split(/\|/, $$boe{$$host}->[4]))[0] . "." : "."; \
				$$boe_tau_p = exists $$boe{$$par} ? $$boe{$$par}->[2] : -1;    \
				$$boe_spec_h = exists $$boe{$$par} ? (grep { $$_ !~ /Cells/ } split(/\|/, $$boe{$$par}->[4]))[0] . "." : "."; \
				                                                               \
				                                                               \
				$$valh = @{ $$genes{$$tumor}{$$host}{"tumor"}->{"val"} }[$$pos[0]]; \
				$$meanh = $$genes{$$tumor}{$$host}{"tumor"}->{mean};           \
				$$stdh = $$genes{$$tumor}{$$host}{"tumor"}->{std};             \
				$$zh = $$stdh > 0 ? ($$valh - $$meanh)/$$stdh : 0;             \
				$$diffh = exists $$deg{$$tumor}{$$host} ? $$deg{$$tumor}{$$host}->[-1] : -1; \
				                                                               \
				$$valp = @{ $$genes{$$tumor}{$$par}{"tumor"}->{"val"} }[$$pos[0]]; \
				$$meanp = $$genes{$$tumor}{$$par}{"tumor"}->{mean};            \
				$$stdp = $$genes{$$tumor}{$$par}{"tumor"}->{std};              \
				$$zp = $$stdp > 0 ? ($$valp - $$meanp)/$$stdp : 0;             \
				$$diffp = exists $$deg{$$tumor}{$$par} ? $$deg{$$tumor}{$$par}->[-1] : -1; \
				                                                               \
				@po = @{ $$genes{$$tumor}{$$host}{"tumor"}->{pos} };           \
				@c1 = @{ $$genes{$$tumor}{$$host}{"tumor"}->{val} }[@po];      \
				@c2 = @{ $$genes{$$tumor}{$$par}{"tumor"}->{val} }[@po];       \
				$$corr = correlation( \@c1, \@c2 );                            \
				$$corr = $$corr eq "n/a" ? 0 : $$corr;                         \
				                                                               \
				my $$dt = [                                                    \
					$$genes{$$tumor}{$$host}{"tumor"}->{n},                    \
					$$genes{$$tumor}{$$host}{"tumor"}->{median},               \
					$$genes{$$tumor}{$$host}{"tumor"}->{mean},                 \
					$$genes{$$tumor}{$$host}{"tumor"}->{std},                  \
					$$genes{$$tumor}{$$host}{"tumor"}->{norm},                 \
					$$boe_tau_h,                                               \
					$$boe_spec_h,                                              \
					$$diffh,                                                   \
					$$valh,                                                    \
					$$zh,                                                      \
					$$par,                                                     \
					$$genes{$$tumor}{$$par}{"tumor"}->{median},                \
					$$genes{$$tumor}{$$par}{"tumor"}->{mean},                  \
					$$genes{$$tumor}{$$par}{"tumor"}->{std},                   \
					$$genes{$$tumor}{$$par}{"tumor"}->{norm},                  \
					$$boe_tau_p,                                               \
					$$boe_spec_p,                                              \
					$$diffp,                                                   \
					$$valp,                                                    \
					$$zp,                                                      \
					$$corr,                                                    \
					join("|", @exp),                                           \
				];                                                             \
				push @out => [ @vct[1..5], $$cs, $$i, $$tumor, $$tissue, $$vct[10], @{ $$dt } ]; \
			}                                                                  \
		}                                                                      \
		                                                                       \
		print join("\t", @{ $$_ }) for sort { $$a->[0] cmp $$b->[0] || $$a->[1] cmp $$b->[1] || $$a->[5] cmp $$b->[5] } @out; \
	}
endef


define perl_targets
	$$act = $$ARGV, $$ck++ unless $$ck; \
	next if /^#/; \
	\
	if ( $$act eq $$ARGV ) { \
		next unless $$F[2] eq "gene"; \
		next unless $$F[8] =~ /protein_coding/; \
		\
		$$id = $$1 if $$F[8] =~ /gene_id "(.*?)";/; \
		$$name = $$1 if $$F[8] =~ /gene_name "(.*?)";/; \
		$$type = $$1 if $$F[8] =~ /gene_type "(.*?)";/; \
		$$genes{$$id} = [ $$name, $$type ]; \
		$$ck = -1;
	} \
	elsif ( $$ARGV =~ /GTEx.*Attributes/ ) { \
		next if /^SAMPID/; \
		$$sample{$$F[0]}++ if $$F[6] eq "Whole Blood"; \
	} \
	elsif ( $$ARGV =~ /\/dev\/fd\/.*/ and $$ck < 0 ) { \
		next if scalar @F < 3; \
		\
		if ( /^Name/ ) { \
			@names = @F; \
			push @pos => 0 for 0..$$#F; \
			$$pos[$$_] = exists $$sample{$$F[$$_]} ? $$_ : 0 for 0..$$#F; \
		} \
		\
		next unless grep { $$F[1] eq $$genes{$$_}->[0] } keys %genes; \
		$$tpm{$$F[1]} = [ @F[@pos] ]; \
	} \
	elsif ( $$ARGV =~ /rtc_parentals/ ) { \
		next if /^INDIV/; \
		push @ord_ptn => $$F[0]; \
	} \
	elsif ( $$ARGV =~ /rtc_expression.assoc/ and $$ck < 0 ) { \
		$$fl = join("|", @F[2..$$#F]); \
		push @ord_tgt => $$F[1] if grep { $$_ =~ // } @ord_ptn; \
	} \
	else { last; } \
	END { print scalar keys %tpm, " -- ", scalar keys %expr; }
endef

define perl_combine_manifest2
	die "Variable args is empty." unless "$(args)";                            \
	                                                                           \
	$$act = $$ARGV, $$ct++ unless $$ct;                                        \
	next if /^#/ or /^ID/ or /^CASE/;                                          \
	my $$arg = "$(args)";                                                      \
	                                                                           \
	if ( $$act eq $$ARGV ) {                                                   \
		next if $$F[-1] eq "." or $$F[10] & 1 or $$F[9] < 10;                  \
		                                                                       \
		if ( $$F[16] eq $$arg or $$arg eq "all" ) {                            \
			my @id = split /\|/, $$F[-1];                                      \
			push @{ $$ind{$$_} } => [ 0, 0, 0, @F[1, 2, 15] ] for @id;         \
			                                                                   \
			if ( $$F[10] & 64 ) {                                              \
				$$ind{$$_}->[-1][0]++ for @id;                                 \
			}                                                                  \
			elsif ( $$F[10] & 32 ) {                                           \
				$$ind{$$_}->[-1][1]++ for @id;                                 \
			}                                                                  \
			else {                                                             \
				$$ind{$$_}->[-1][2]++ for @id;                                 \
			}                                                                  \
		}                                                                      \
	}                                                                          \
	elsif ( $$ARGV =~ /wxs_all_p/ ) {                                          \
		my $$type = $$F[1] =~ /^TCGA-(\w{2,4})/ ? $$1 : ".";                   \
		next if $$type eq "." or ( $$arg ne $$type and $$arg ne "all" );       \
		                                                                       \
		push @{ $$cases{$$F[0]} } => [ @F ] if exists $$ind{$$F[4]};           \
	}                                                                          \
	else {                                                                     \
		$$res{$$F[4]} = [ @F ] if exists $$cases{$$F[0]};                      \
	}                                                                          \
	                                                                           \
	END {           	                                                       \
		my @out;                                                               \
		foreach my $$i ( keys %res ) {                                         \
			my $$tissue = $$res{$$i}->[2];                                     \
			my $$cs = "";                                                      \
			foreach my $$c ( keys %cases ) {                                   \
				$$cs = $$c, last if grep { $$_->[0] eq $$res{$$i}->[0] } @{ $$cases{$$c} }; \
			}                                                                  \
			next unless $$cs;                                                  \
			                                                                   \
			my @id = map { $$_->[4] } grep { $$_->[2] eq $$tissue } @{ $$cases{$$cs} }; \
			next unless @id;                                                   \
			                                                                   \
			my $$soma = scalar grep { $$_->[0] > 0 } @{ $$ind{$$id[0]} };      \
			my $$germ = scalar grep { $$_->[1] > 0 } @{ $$ind{$$id[0]} };      \
			my $$poly = scalar grep { $$_->[2] > 0 } @{ $$ind{$$id[0]} };      \
			my @ins;                                                           \
			$$ins[0] = join("|", map { join ":", @{ $$_ }[3..5] } grep { $$_->[0] > 0 } @{ $$ind{$$id[0]} }); \
			$$ins[1] = join("|", map { join ":", @{ $$_ }[3..5] } grep { $$_->[1] > 0 } @{ $$ind{$$id[0]} }); \
			$$ins[2] = join("|", map { join ":", @{ $$_ }[3..5] } grep { $$_->[2] > 0 } @{ $$ind{$$id[0]} }); \
			print STDERR "Exomes related to transcriptome $$i (case_id $$cs) => ", join("|", @id), "."; \
			                                                                   \
			push @out => [ @{ $$res{$$i} }[0..$$#{ $$res{$$i} }-1], $$id[0], $$soma, $$germ, $$poly, join("@", @ins) ]; \
			@{ $$cases{$$cs} } = grep { $$_->[4] ne $$id[0] } @{ $$cases{$$cs} }; \
		}                                                                      \
		                                                                       \
		my $$sm = 0;                                                           \
		$$sm += scalar grep { $$_->[0] > 0 } @{ $$ind{$$_} } for keys %ind;    \
		print STDERR "Counts for \"$$arg\": Files = ", scalar keys %ind, "; Cases = ", scalar keys %cases, "; Somatic Insertions = ", $$sm, "; Transcriptomes assoc = ", scalar @out, "."; \
		print join("\t", @{ $$_ }) for sort { $$a->[1] cmp $$b->[1] || $$b->[6] <=> $$a->[6] || $$b->[7] <=> $$a->[7] || $$b->[8] <=> $$a->[8] } @out; \
	}
endef

define perl_mretro
	next if /^#/ or /^ID/ or $$F[-1] eq "." or $$F[10] & 1 or $$F[9] < 10;     \
	                                                                           \
	next unless sum(@F[23..24]);                                               \
	$$ch = length("$(args)") > 0 ? $$F[5] : $$F[15];                           \
	next if $$ch eq "none";                                                    \
	                                                                           \
	$$rtc{$$ch}{$$F[16]} = [ 0, 0, 0 ] unless exists $$rtc{$$ch}{$$F[16]};     \
	                                                                           \
	if ( $$F[10] & 64 ) {                                                      \
		$$rtc{$$ch}{$$F[16]}->[0]++;                                           \
	}                                                                          \
	if ( $$F[10] & 32 ) {                                                      \
		$$rtc{$$ch}{$$F[16]}->[1]++;                                           \
	}                                                                          \
	else {                                                                     \
		$$rtc{$$ch}{$$F[16]}->[2] += sum(@F[23..24]);                          \
	}                                                                          \
	                                                                           \
																			   \
	END {                                                                      \
		print "GENE\tTUMOR\tSOMATICS\tGERMLINES\tPOLYMORPHICS";                \
		foreach my $$r ( sort keys %rtc ) {                                    \
			print join("\t", ( $$r, $$_, @{ $$rtc{$$r}{$$_} } )) for sort keys %{ $$rtc{$$r} }; \
		}                                                                      \
	}
endef

define perl_stats
	next if /^#/;                                                              \
	                                                                           \
	if ( $$ARGV =~ /manifest/ ) {                                              \
		$$ind{$$F[4]} = [ @F ];                                                \
		                                                                       \
		$$group = $$F[1] =~ /TCGA-(.*?)/ ? $$1 : "none";                       \
		next if $$group eq "none";                                             \
		$$groups{$$group}++;                                                   \
	}                                                                          \
	else {                                                                     \
		die "ERROR: Incorrect file type.\n" unless /^GENE_ID/;                 \
		@hd = @F, next if /^GENE_ID/;                                          \
		                                                                       \
		if ( defined %groups ) {                                               \
			foreach my $$gp ( keys %groups ) {                                 \
				$$idx{$$gp} grep {} @evwr;                                     \
			}                                                                  \
		}                                                                      \
		else {                                                                 \
		}                                                                      \
	}                                                                          \
	                                                                           \
	END {                                                                      \
		print "## Expression statistics for 1 group in TPM.\n";                \
		print "GENE_ID\tGENE_SYMBOL\tGENE_TYPE\tGENE_LENGTH\tGENE_BoE\tN\tMEDIAN\tMEAN\tSTD\tNORMALITY\n"; \
		print join("\t", @{ $$genes{$$_} }), "\n" for sort keys %genes;        \
	}
endef

define perl_genome_summary
	BEGIN { $$last_chr = ""; $$last_type = ""; $$last_gene = ""; $$exonic_area = 0; } \
	\
	\
	$$act = $$ARGV, $$ct++ unless $$ct; \
	if ( $$act eq $$ARGV ) { \
		next if /^#/ or length($$F[0]) > 5; \
		if ( $$F[2] ne "gene" and $$F[2] ne "transcript" and $$F[2] ne "exon" ) { \
			next; \
		} \
		elsif ( $$F[2] eq "transcript" ) { \
			$$isof++; \
			next; \
		} \
		elsif ( $$F[2] eq "exon" ) { \
			$$ex++; \
			$$exonic_area += $$F[4] - $$F[3]; \
			$$last_chr = $$chr; \
			$$last_type = $$type; \
			$$last_gene = $$id; \
			push @{ $$tree{$$chr} } => { \
				"exon_start" => $$F[3], \
				"exon_end" => $$F[4], \
				"gene_start" => $$genes{$$chr}{all}{$$id}->{start}, \
				"gene_end" => $$genes{$$chr}{all}{$$id}->{end}, \
				"gene_type" => $$type, \
				}; \
			next; \
		} \
		\
		$$chr = $$F[0]; \
		$$type = $$F[8] =~ /gene_type "(.*?)";/ ? $$1 : "unknown"; \
		$$id = $$F[8] =~ /gene_id "(.*?)";/ ? $$1 : "unknown"; \
		$$name = $$F[8] =~ /gene_name "(.*?)";/ ? $$1 : "unknown"; \
		\
		$$genes{$$chr}{$$type}{$$id} = { \
			"id" => $$id, \
			"name" => $$name, \
			"start" => $$F[3], \
			"end" => $$F[4], \
			"size" => $$F[4] - $$F[3], \
			"isoforms" => 0, \
			"exons" => 0, \
			"exonic_area" => 0, \
			"intronic_area" => 0, \
			"genic_area" => $$F[4] - $$F[3], \
			}; \
		$$genes{$$chr}{$$type}{n}++; \
		$$genes{$$chr}{$$type}{genic_area} += $$genes{$$chr}{$$type}{$$id}->{size}; \
		$$genes{$$chr}{all}{n}++; \
		$$genes{$$chr}{all}{genic_area} += $$genes{$$chr}{$$type}{$$id}->{size}; \
		$$genes{$$chr}{all}{$$id} = $$genes{$$chr}{$$type}{$$id}; \
		\
		if ( $$last_chr ) { \
			$$ex ||= 1; \
			$$isof ||= 1; \
			$$genes{$$last_chr}{all}{$$last_gene}->{isoforms} = $$isof; \
			$$genes{$$last_chr}{all}{$$last_gene}->{exons} = $$ex/$$isof; \
			$$genes{$$last_chr}{all}{$$last_gene}->{exonic_area} = $$exonic_area/$$isof; \
			$$genes{$$last_chr}{all}{$$last_gene}->{intronic_area} = $$genes{$$last_chr}{all}{$$last_gene}->{genic_area} - $$genes{$$last_chr}{all}{$$last_gene}->{exonic_area}; \
			$$genes{$$last_chr}{$$last_type}{$$last_gene}->{isoforms} = $$isof; \
			$$genes{$$last_chr}{$$last_type}{$$last_gene}->{exons} = $$ex/$$isof; \
			$$genes{$$last_chr}{$$last_type}{$$last_gene}->{exonic_area} = $$exonic_area/$$isof; \
			$$genes{$$last_chr}{$$last_type}{$$last_gene}->{intronic_area} = $$genes{$$last_chr}{$$type}{$$last_gene}->{genic_area} - $$genes{$$last_chr}{$$type}{$$last_gene}->{exonic_area}; \
		} \
		\
		$$ex = 0; \
		$$isof = 0; \
		$$exonic_area = 0; \
	} \
	elsif ( $$ARGV =~ /rtc\.tsv/ ) { \
		next if /^#/ or /^ID/ or $$F[10] & 1 or $$F[9] < 10 or !($$F[10] & 64); \
		push @{ $$mei{$$F[16]}{normal}{$$F[1]} } => [ $$F[2], $$F[15], $$F[4], $$F[5], $$F[-1] ] if $$F[21] + $$F[22] > 0; \
		push @{ $$mei{$$F[16]}{tumor}{$$F[1]} } => [ $$F[2], $$F[15], $$F[4], $$F[5], $$F[-1] ] if $$F[21] + $$F[22] == 0; \
	} \
	else { \
		chomp $$F[1]; \
		$$genes{$$F[0]}{info} = $$F[1]; \
	} \
	\
	END { \
		die "Could not parse MEI table.\n" unless %mei; \
		\
		foreach my $$chr ( keys %genes ) { \
			foreach my $$tp ( keys %{ $$genes{$$chr} } ) { \
				next if $$tp eq "info" or $$genes{$$chr}{$$tp}{n} == 0; \
				my $$lim = 0; \
				my $$isoforms = 0; \
				my $$exons = 0; \
				my $$exonic_area = 0; \
				my $$intronic_area = 0; \
				foreach my $$id ( sort { $$genes{$$chr}{$$tp}{$$a}->{start} <=> $$genes{$$chr}{$$tp}{$$b}->{start} } keys %{ $$genes{$$chr}{$$tp} } ) { \
					next if $$id eq "n" or $$id eq "genic_area"; \
					my %gene = %{ $$genes{$$chr}{$$tp}{$$id} }; \
					\
					my $$disc = 0; \
					if ( $$gene{end} - $$lim < 0 ) { \
						$$disc = $$gene{size}; \
					} \
					elsif ( $$gene{start} - $$lim < 0 ) { \
						$$disc = $$lim - $$gene{start}; \
					} \
					$$genes{$$chr}{$$tp}{genic_area} -= $$disc; \
					$$isoforms += $$gene{isoforms}; \
					$$exons += $$gene{exons}; \
					$$exonic_area += $$gene{exonic_area}; \
					$$intronic_area += $$gene{intronic_area}; \
					\
					$$lim = $$gene{end} if $$gene{end} > $$lim; \
				} \
				$$genes{$$chr}{$$tp}{mean_isoforms} = $$isoforms / $$genes{$$chr}{$$tp}{n}; \
				$$genes{$$chr}{$$tp}{mean_exons} = $$exons / $$genes{$$chr}{$$tp}{n}; \
				$$genes{$$chr}{$$tp}{intergenic_area} = $$genes{$$chr}{info} - $$genes{$$chr}{genic_area}; \
				$$genes{$$chr}{$$tp}{genic_density} = $$genes{$$chr}{$$tp}{genic_area} / $$genes{$$chr}{info}; \
				$$genes{$$chr}{$$tp}{exonic_density} = $$exonic_area / $$genes{$$chr}{info}; \
				$$genes{$$chr}{$$tp}{intronic_density} = $$intronic_area / $$genes{$$chr}{info}; \
				$$genes{$$chr}{$$tp}{intergenic_density} = 1 - $$genes{$$chr}{$$tp}{genic_density}; \
			} \
		} \
		print STDERR "Chegou??"; \
		\
		@hd = ( "CHR", \
			"SIZE", \
			"GENES", \
			"MEAN_ISOFORMS", \
			"MEAN_EXONS", \
			"GENIC_DENSITY", \
			"EXONIC_DENSITY", \
			"INTRONIC_DENSITY", \
			"INTERGENIC_DENSITY", \
			"SAMPLE", \
			"MEI_EXONICS", \
			"MEI_INTRONICS", \
			"MEI_NEARS", \
			"MEI_INTERGENICS", \
			"STAT", \
			"PC_GENES", \
			"PC_MEAN_ISOFORMS", \
			"PC_MEAN_EXONS", \
			"PC_GENIC_DENSITY", \
			"PC_EXONIC_DENSITY", \
			"PC_INTRONIC_DENSITY", \
			"PC_INTERGENIC_DENSITY", \
			"PC_MEI_EXONICS", \
			"PC_MEI_INTRONICS", \
			"PC_MEI_NEARS", \
			"PC_MEI_INTERGENICS", \
			"PC_STAT", \
			); \
		\
		print join("\t", @hd); \
		foreach my $$tm ( sort keys %mei ) { \
			foreach my $$chr ( sort keys %genes ) { \
				$$wd = 2000; $$exo = 0; $$intro = 0; $$near = 0; $$inter = 0; \
				foreach my $$rtc ( @{ $$mei{$$tm}{tumor}{$$chr} } ) { \
					if ( grep { $$rtc->[0] >= $$_->{gene_start} and $$rtc->[0] <= $$_->{gene_end} } @{ $$tree{$$chr} } ) { \
						if ( grep { $$rtc->[0] >= $$_->{exon_start} and $$rtc->[0] <= $$_->{exon_end} } @{ $$tree{$$chr} } ) { \
							$$exo++; \
						} \
						else { $$intro++; } \
					} \
					elsif ( grep { $$rtc->[0] >= $$_->{gene_start} - $$wd and $$rtc->[0] <= $$_->{gene_end} + $$wd } @{ $$tree{$$chr} } ) { \
						$$near++; \
					} \
					else { $$inter++; } \
				} \
				$$pc_exo = scalar grep { $$_->[2] eq "exonic" } @{ $$mei{$$tm}{tumor}{$$chr} }; \
				$$pc_intro = scalar grep { $$_->[2] eq "intronic" } @{ $$mei{$$tm}{tumor}{$$chr} }; \
				$$pc_near = scalar grep { $$_->[2] eq "near" } @{ $$mei{$$tm}{tumor}{$$chr} }; \
				$$pc_inter = scalar grep { $$_->[2] eq "intergenic" } @{ $$mei{$$tm}{tumor}{$$chr} }; \
				\
				foreach my $$tp ( keys %{ $$genes{$$chr} } ) { \
					next unless $$tp eq "protein_coding"; \
					\
					@out = ( $$chr, \
						$$genes{$$chr}{info}, \
						$$genes{$$chr}{all}{n}, \
						$$genes{$$chr}{all}{mean_isoforms}, \
						$$genes{$$chr}{all}{mean_exons}, \
						$$genes{$$chr}{all}{genic_density}, \
						$$genes{$$chr}{all}{exonic_density}, \
						$$genes{$$chr}{all}{intronic_density}, \
						$$genes{$$chr}{all}{intergenic_density}, \
						$$tm, \
						$$exo, \
						$$intro, \
						$$near, \
						$$inter, \
						"unsig", \
						$$genes{$$chr}{$$tp}{n}, \
						$$genes{$$chr}{$$tp}{mean_isoforms}, \
						$$genes{$$chr}{$$tp}{mean_exons}, \
						$$genes{$$chr}{$$tp}{genic_density}, \
						$$genes{$$chr}{$$tp}{exonic_density}, \
						$$genes{$$chr}{$$tp}{intronic_density}, \
						$$genes{$$chr}{$$tp}{intergenic_density}, \
						$$pc_exo, \
						$$pc_intro, \
						$$pc_near, \
						$$pc_inter, \
						"unsig", \
						); \
					\
					print join("\t", @out); \
				} \
			} \
		} \
	}
endef

define perl_extracted_manifest
	die "Variable args is empty." unless "$(args)";                            \
	                                                                           \
	$$act = $$ARGV, $$ct++ unless $$ct;                                        \
	my $$arg = "$(args)";                                                      \
	                                                                           \
	if ( $$act eq $$ARGV ) {                                                   \
		next if /^#/ or /^ID/ or $$F[-1] eq "." or $$F[10] & 1 or $$F[9] < 10; \
		                                                                       \
		if ( $$F[16] eq $$arg or $$arg eq "all" ) {                            \
			my @id = split /\|/, $$F[-1];                                      \
			push @{ $$ind{$$_} } => [ 0, 0, 0, @F[1, 2, 4, 5, 15] ] for @id;   \
			                                                                   \
			if ( $$F[10] & 64 ) {                                              \
				$$ind{$$_}->[-1][0]++ for @id;                                 \
			}                                                                  \
			elsif ( $$F[10] & 32 ) {                                           \
				$$ind{$$_}->[-1][1]++ for @id;                                 \
			}                                                                  \
			else {                                                             \
				$$ind{$$_}->[-1][2]++ for @id;                                 \
			}                                                                  \
		}                                                                      \
	}                                                                          \
	else {                                                                     \
		my $$type = $$F[1] =~ /^TCGA-(.*?)$$/ ? $$1 : ".";                     \
		next if $$type eq ".";                                                 \
		                                                                       \
		$$info{$$F[4]} = [ @F ] if exists $$ind{$$F[4]};                       \
	}                                                                          \
	                                                                           \
	END {           	                                                       \
		my @out;                                                               \
		foreach my $$k ( keys %ind ) {                                         \
			@vct = @{ $$ind{$$k} };                                            \
			@soma = (); @germ = (); @poly = ();                                \
			push @soma => join(":", @{ $$_ }[3..7]) for grep { $$_->[0] > 0 } @vct; \
			push @germ => join(":", @{ $$_ }[3..7]) for grep { $$_->[1] > 0 } @vct; \
			push @poly => join(":", @{ $$_ }[3..7]) for grep { $$_->[2] > 0 } @vct; \
			$$s = join("|", @soma); \
			$$g = join("|", @germ); \
			$$p = join("|", @poly); \
			push @out => [ \
				@{ $$info{$$k} }, \
				scalar(@soma), \
				scalar(@germ), \
				scalar(@poly), \
				join("&", ( $$s, $$g, $$p, )), \
			]; \
		} \
		print STDERR "Size: ", scalar @out; \
		@hd = qw/CASE_ID TUMOR_TYPE SAMPLE_TYPE FILE_ID FILE_NAME SUBMITTER_ID SOMATICS GEMLINES POLYMORPHICS INFO/; \
		print join("\t", @hd); \
		print join("\t", @{ $$_ }) for sort { \
			$$a->[1] cmp $$b->[1] \
			|| $$b->[2] cmp $$a->[2] \
			|| $$b->[6] <=> $$a->[6] \
			|| $$b->[7] <=> $$a->[7] \
			|| $$b->[8] <=> $$a->[8] \
		} @out; \
	}
endef

define perl_combined_manifest
	die "Variable args is empty." unless "$(args)";                            \
	                                                                           \
	$$act = $$ARGV, $$ct++ unless $$ct;                                        \
	my $$arg = "$(args)";                                                      \
	                                                                           \
	if ( $$act eq $$ARGV ) {                                                   \
		next if /^#/ or /^CASE_ID/;                                            \
		                                                                       \
		$$ind{$$F[4]} = [ @F ] if $$F[1] =~ $$arg or $$arg eq "all";           \
	}                                                                          \
	else {                                                                     \
		next if /^#/ or /^CASE_ID/;                                            \
		my $$type = $$F[1] =~ /^TCGA-(.*?)$$/ ? $$1 : ".";                     \
		next if $$type eq "." or ( $$arg ne "all" and $$F[1] !~ $$arg );       \
		                                                                       \
		@tst = grep { $$F[0] eq $$ind{$$_}->[0] and $$F[2] eq $$ind{$$_}->[2] } keys %ind; \
		if ( @tst ) {                                                          \
			$$prt{$$F[4]} = [ @F ];                                            \
			push @{ $$prt{$$F[4]} } => @{ $$ind{$$tst[0]} }[4,6..9];           \
			delete $$ind{$$tst[0]};                                            \
		}                                                                      \
	}                                                                          \
	                                                                           \
	END {           	                                                       \
		print STDERR "Ind: ", scalar keys %ind, ", Prt: ", scalar keys %prt;   \
		@hd = qw/CASE_ID TUMOR_TYPE SAMPLE_TYPE FILE_ID FILE_NAME SUBMITTER_ID EXOME SOMATICS GEMLINES POLYMORPHICS INFO/; \
		print join("\t", @hd); \
		print join("\t", @{ $$prt{$$_} }) for sort {                           \
			$$prt{$$a}->[1] cmp $$prt{$$b}->[1]                                \
			|| $$prt{$$b}->[2] cmp $$prt{$$a}->[2]                             \
			|| $$prt{$$b}->[6] <=> $$prt{$$a}->[6]                             \
			|| $$prt{$$b}->[7] <=> $$prt{$$a}->[7]                             \
			|| $$prt{$$b}->[8] <=> $$prt{$$a}->[8]                             \
		} keys %prt;                                                           \
	}
endef


define perl_expression_STAR
	$$act = $$ARGV, $$ct++ unless $$ct;                                        \
	                                                                           \
	if ( $$act eq $$ARGV ) {                                                   \
		next if /^#/ or /^CASE_ID/;                                            \
		$$idx{$$F[5]} = $$F[6];                                                \
	}                                                                          \
	else {                                                                     \
		next if /^#/ or /^gene_id/ or /^N_/;  \
		$$file = (split(/\//, $$ARGV))[-1]; \
		$$gn{$$F[0]}{$$file} = [ $$F[1], $$F[2], $$F[-3] ]; \
	} \
	$$last = $$ARGV; \
	\
	END { \
		$$g1 = (keys %gn)[0]; \
		@ord = sort keys %{ $$gn{$$g1} }; \
		@hord = map { exists $$idx{$$_} ? $$idx{$$_} : $$_ } @ord; \
		@hd = ( "GENE_ID", "GENE_SYMBOL", "GENE_TYPE", @hord ); \
		print join("\t", @hd); \
		foreach my $$id ( sort keys %gn ) { \
			my $$sym = $$gn{$$id}{$$last}->[0]; \
			my $$type = $$gn{$$id}{$$last}->[1]; \
			my @out = ( $$id, $$sym, $$type ); \
			push @out => $$gn{$$id}{$$_}->[2] for @ord; \
			print join("\t", @out); \
		} \
	}
endef

tcga_expression_star := tcga_expression_star.tsv


expression_star: $(manifest)
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) expr_star

expr_star: $(tcga_expression_star)

$(tcga_expression_star): $(manifest) $(dwl)
	@$(ECHO) "\nCalculating expression matrix based on '$<'." >&2
	perl -F"\t" -ane '$(perl_expression_STAR)' $^ > $@
	@$(ECHO) "Done." >&2


genome_summary := Hs_genome_summary.tsv

genome_summary: $(genome_summary)

$(genome_summary): $(annotation_ref) $(rtc_table) $(chr_sizes)
	@$(ECHO) "\nGenerating genomic summary based on files '$^'." >&2
	perl -F"\t" -lane '$(perl_genome_summary)' $^ > $@
	@$(ECHO) "Done." >&2


transpose: $(args)
	@$(ECHO) "\nTransposing files '$(args)'." >&2
	perl -F"\t" -lane '\
		$$num = @F if $$k ne $$ARGV; \
		$$k = $$ARGV if $$k ne $$ARGV;
		$$ck++, die "ERROR: Inconsistent file '$$ARGV'.\n" unless $$num == @F; \
		push @{ $$files{$$ARGV} } => [ @F ]; \
		\
		END { \
			die if $$ck; \
			for my $$fl ( keys %files ) { \
				$$num = $$#{ $$files{$$fl} }; \
				open my $$fh, '>', "$$fl.trans" or die "ERROR!\n"; \
				for my $$i ( 0..$$num ) { \
					for my $$j ( 0..$$#{ $$files{$$fl}->[$$i] } ); \
						print $$fh "$$files{$$fl}->[$$j][$$i]\t"; \
					} \
					print $$fh "\n"; \
				} \
				close $$fh
			} \
		}' $(args)
	@$(ECHO) "Done." >&2


renderize: $(renderfile)
	@$(ECHO) "\nRendering document from file '$(renderfile)' with arguments '$(args)'." >&2
	[ -n "$(args)" ] || ( echo "ERROR: Variable 'args' is empty." && exit 7 )
	R --vanilla --slave -e 'library(rmarkdown); render("$<");' --args $(args)
	@$(ECHO) "Done." >&2


rtc_model: $(rtc_model)

$(rtc_model): $(rtc_ind) $(rct_targets)


rtc_targets: $(rtc_targets)

$(rtc_targets): $(annotation_ref) $(gtex_attributes) $(gtex_tpm) $(rtc_parentals) $(manifest_rtc_aux) $(rtc_expression)
	@$(ECHO) "\nGenerating Targets and Patterns table of RTC insertions by individual from '$(rtc_table)' file." >&2
	perl -F"\t" -MList::Util=sum -lane '$(perl_targets)' \
		<(zcat $<) $(gtex_attributes) <(zcat $(gtex_tpm)) \
		$(rtc_parentals) $(manifest_rtc_aux) $(rtc_expression)
	@$(ECHO) "Done." >&2


rtc_individuals: $(rtc_parentals)

#$(rtc_parentals): $(annotation_ref) $(rtc_table)
#	@$(ECHO) "\nGenerating table of RTC insertions by individual from '$(rtc_table)' file." >&2
#	perl -F"\t" -MList::Util=sum -MList::MoreUtils=uniq -lane \
#		'$(perl_individuals)' <(zcat $<) $(rtc_table) > $@ 2> $(rtc_hosts)
#	@$(ECHO) "Done." >&2


rtc_expression_somatics: $(rtc_expression_soma)

$(rtc_expression_soma): $(rtc_table) $(rtc_expression) $(gtex_BoE) $(degao)
	@$(ECHO) "\nGenerating table of expression of hosts apporting somatics RTCs from file '$<'." >&2
	perl -F"\t" \
		-MStatistics::Basic=median,mean,stddev,correlation \
		-MStatistics::Normality=shapiro_wilk_test \
		-MList::Util=sum \
		-lane \
		'$(perl_expression_soma)' \
			$< \
			$(gtex_BoE) \
			$(degao) \
			$(datadir)/tcga_wxs_all_p.manifest \
			$(datadir)/tcga_tp_all.manifest \
			$(rtc_expression) > $@
	@$(ECHO) "Done." >&2


rtc_expression_match: $(rtc_expression)

$(rtc_expression): $(rtc_match)
	@$(ECHO) "\nCalculate expression table for RTC manifest file: '$<'." >&2
	[ -s "$<" ] || ( echo "ERROR: Variable 'rtc_match' is empty." >&2; exit 7 )
	#cp $< $(datadir)/tmp.manifest
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) \
		-j$(jobs) \
		manifest=$(datadir)/tmp.manifest \
		expr
	mv all_tpm.tsv $@
	perl -i -lne 'print if /^GENE/ or /protein_coding/' $@
	@$(ECHO) "Done." >&2

define olalaoh
		$$act = $$ARGV, $$ct++ unless $$ct; \
		next if /^#/ or /^ID/ or /^CASE/; \
		\
		if ( $$act eq $$ARGV ) { \
			$$tumors{$$F[16]}++; \
		} \
		else { \
			$$tp = $$F[1] =~ /TCGA-(.*?)$$/ ? $$1 : "."; \
			$$tp = "." unless $$tp; \
			print join("\t", @F) if exists $$tumors{$$tp}; \
		}
endef

rtc_expression_manifest: $(rtc_match)

$(rtc_match): $(rtc_table) trick $(manifest)
	@$(ECHO) "\nGenerating expression manifest that match cases in '$<' file." >&2
	[ -n "$(rtc_table)" ] || ( echo "ERROR: Variable 'rtc_table' is empty." >&2; exit 7 )
	cp $(manifest) $(datadir)/tcga_tp_all.manifest
	rm -f $(manifest:.manifest=.payl)
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) \
		module=$(sk8_rootdir)/modules/biotools.mk \
		data_sample=all \
		data_type=wxs \
		data_control="Blood Derived Normal" \
		paired=yes \
		manifest
	[ -s "$(manifest)" ] || ( echo "ERROR: Manifest generation failed for '$@'." >&2; exit 7 )
	cp $(manifest) $(datadir)/tcga_wxs_all_p.manifest
	rm -f $(manifest:.manifest=.payl)
	#perl -F"\t" -lane '$(perl_expression_match)' \
	#	$< \
	#	$(datadir)/tcga_wxs_all_p.manifest \
	#	$(datadir)/tcga_tp_all.manifest > $@
	perl -F"\t" -lane '$(olalaoh)' $< $(datadir)/tcga_tp_all.manifest > $@
	@$(ECHO) "Done." >&2


rtc_retro_higher: mretro
	#$(MAKE) -f $(firstword $(MAKEFILE_LIST)) mretro

mretro: $(mretro)

$(mretro): $(rtc_table)
	@$(ECHO) "\nCalculating list of the most retrocopied parental genes in file: '$<'." >&2
	perl -F"\t" -MList::Util=sum -lane '$(perl_mretro)' $< > $@
	@$(ECHO) "Done." >&2


rtc_expression_higher: $(manifest)
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) rtc_mexpr

rtc_mexpr: $(rtc_mexpr)

$(rtc_mexpr): %_normals.tsv: $(manifest) $(rtc_table)
	@$(ECHO) "\nCreating lists of most expressed coding genes for tumors in file: '$<'." >&2
	#[ -n "$(args)" ] || ( echo "ERROR: Variable 'args' is empty." >&2; exit 7 )
	perl -F"\t" -MStatistics::Basic=median -ane '$(perl_most_expressed)' $^ > $@ 2> $(@:_normals.tsv=_tumors.tsv)
	@$(ECHO) "Done." >&2


define perl_rtc_parentals
	chomp $$F[-1]; \
	$$tm = $$F[1] =~ /TCGA-(.*?)$$/ ? $$1 : "unknown"; \
	$$ind{"$$F[4]|$$F[5]"} = [ $$tm, $$F[-1] ]; \
	$$genes{$$_}++ for grep { /^[[:alpha:]]/ and ! /^chr/ } split(/&|:|\|/, $$F[-1]); \
	\
	END { \
		my $$ct = 0; \
		my @out; \
		my @g; \
		my @gn = sort keys %genes; \
		my @zr; push @zr => 0 for 0..$$#gn; \
		print join("\t", ( "ID", "TUMOR", @gn )), "\n"; \
		foreach my $$i ( sort keys %ind ) { \
			next unless scalar(split(/&/, $$ind{$$i}->[1])) == 3; \
			( $$soma, $$germ, $$poly ) = split(/&/, $$ind{$$i}->[1]); \
			my @idx = @zr; \
			for my $$x ( grep { /^[[:alpha:]]/ and ! /^chr/ } split(/:|\|/, $$soma) ) { \
				@g = grep { $$gn[$$_] eq $$x } 0..$$#gn; \
			} \
			$$idx[$$_]++ for @g; \
			for my $$x ( grep { /^[[:alpha:]]/ and ! /^chr/ } split(/:|\|/, $$germ) ) { \
				@g = grep { $$gn[$$_] eq $$x } 0..$$#gn; \
			} \
			$$idx[$$_] += 2 for @g; \
			for my $$x ( grep { /^[[:alpha:]]/ and ! /^chr/ } split(/:|\|/, $$poly) ) { \
				@g = grep { $$gn[$$_] eq $$x } 0..$$#gn; \
			} \
			$$idx[$$_] += 4 for @g; \
			print join("\t", ( $$i, $$ind{$$i}->[0], @idx )), "\n"; \
			$$ct++; \
		} \
		print STDERR scalar @gn, " -- ", scalar @zr, " -- $$ct.\n"; \
	}
endef


rtc_parentals: $(rtc_parentals)

$(rtc_parentals): $(rtc_combined_manifest)
	@$(ECHO) "\nGenerating MEI matrix for cases in '$<' file." >&2
	perl -F"\t" -MList::Uniq=uniq -ane '$(perl_rtc_parentals)' $< > $@
	@$(ECHO) "Done." >&2


rtc_expression_table: $(rtc_expression_table)

$(rtc_expression_table): $(rtc_combined_manifest)
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) -j$(jobs) expr manifest=$<


ifeq ($(MAKELEVEL),0)
rtc_combined_manifest: $(rtc_combined_manifest)

$(rtc_combined_manifest): $(rtc_extracted_manifest) $(manifest)
	$(ECHO) "\nGenerating manifest file combined from files '$^'." >&2
	[ -n "$(args)" ] || ( echo "ERROR: Variable 'args' is empty." >&2; exit 7 )
	perl -F"\t" -lane '$(perl_combined_manifest)' $^ > $@
	$(ECHO) "Done." >&2

endif


rtc_extracted_manifest: $(rtc_extracted_manifest)

$(rtc_extracted_manifest): $(rtc_table) $(manifest_wxs_all_p)
	$(ECHO) "\nGenerating manifest file enriched with data from '$(rtc_table)'." >&2
	[ -n "$(args)" ] || ( echo "ERROR: Variable 'args' is empty." >&2; exit 7 )
	perl -F"\t" -lane '$(perl_extracted_manifest)' $^ > $@
	$(ECHO) "Done." >&2


rtc_combined_manifest2: $(rtc_combined_manifest2)

$(rtc_combined_manifest2): $(rtc_table) $(manifest_wxs_all_p) $(manifest) # This last manifest determines the combination.
	@$(ECHO) "\nGenerating expression manifest that match cases in '$<' file." >&2
	[ -n "$(args)" ] || ( echo "ERROR: Variable 'args' is empty." >&2; exit 7 )
	perl -F"\t" -lane '$(perl_combine_manifest2)' $^ > $@
	@$(ECHO) "Done." >&2


$(manifest_wxs_all_p):
	@$(ECHO) "\nGenerating manifest file for all paired WXS data." >&2
	#cp $(manifest) $@
	@$(ECHO) "Done." >&2

$(manifest_wts_all):
	@$(ECHO) "\nGenerating manifest file for all WTS data." >&2
	#cp $(manifest) $@
	@$(ECHO) "Done." >&2


trick:
	@$(ECHO) "\nSome cleanings ..." >&2
	rm -f $(manifest:.manifest=.payl)
	@$(ECHO) "Done." >&2

$(rtc_table):
	@$(ECHO) "\nUser MUST provide a TSV file in variable 'rtc_table'." >&2
	exit 7


tcga_volcano: $(degao)
	@$(ECHO) "\nPloting a Volcano plot for tumor types: '$(data_sample)'." >&2
	R --vanilla --slave --args $< <<< '$(R_volcano)'
	@$(ECHO) "Done." >&2
	

tcga_expression_boxplot: $(manifest)
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) plt

plt: $(plt)

$(plt): %.png: $(manifest) %_tpm.tsv
	@$(ECHO) "\nGenerating TCGA gene expression boxplot for genes: '$(args)'." >&2
	[ -z "$(args)" ] \
		&& echo "ERROR: Empty variable: 'args' for gene names." >&2 && exit 1
	R --vanilla --slave --args $< $(expr) "$(args)" <<< '$(R_plot)'
	@$(ECHO) "Done." >&2


tcga_gsea: $(gsea)

$(gsea): $(deg) $(rtc_table) | $(R_status)# Pq rtc_table?
	@$(ECHO) "\nCalculating Gene Set Enrichment for RTCs of tumor types: '$(data_sample)'." >&2
	R --vanilla --slave --args $^ <<< '$(R_gsea)'
	@$(ECHO) "Done." >&2


tcga_deg: extract
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) deg

deg: $(deg)

$(deg): $(manifest) $(gene_universe) $(extr)
	@$(ECHO) "\nCreating lists of Differential Expressed Genes for tumor types: '$(data_sample)'." >&2
	R --vanilla --slave --args $< $(gene_universe) $(dir $(firstword $(extr))) <<< '$(R_DEG)'
	@$(ECHO) "Done." >&2


tcga_expression_stats: $(stats)

$(stats): $(manifest) $(expr)
	@$(ECHO) "\nCalculating statistics for expression table and manifest file: '$^' ..." >&2
	perl -F"\t" -MStatistics::Basic -ane '$(perl_stats)' $^ > $@
	@$(ECHO) "Done." >&2


expr2 := $(radical)_TPM.tsv
#gencode_v36 := /run/media/leonel/43697b55-725f-4c06-ab74-35e2a4ff375b/Workbench/data/gencode/gencode.v36.annotation.gtf.gz 

tcga_expression2: $(manifest)
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) -j$(jobs) expr2


define tpm_gen2
	next if /^#/ or /^N/; \
	\
	push @spl => (split(/\//, $$ARGV))[-1] if $$act ne $$ARGV; \
	$$tb{$$F[0]}{$$ARGV} = $$F[-3]; \
	$$rel{$$F[0]} = $$F[1]; \
	$$act = $$ARGV if $$act ne $$ARGV; \
	\
	END { \
		print "# Based on Gencode v36\n"; \
		print "GENE_ID\tGENE_NAME\t", join("\t", sort @spl), "\n"; \
		foreach my $$g ( sort { $$rel{$$a} cmp $$rel{$$b} } keys %tb ) { \
			print "$$g\t$$rel{$$g}"; \
			print "\t$$tb{$$g}{$$_}" for sort keys %{ $$tb{$$g} }; \
			print "\n"; \
		} \
	}
endef

expr2: $(expr2)

$(expr2): $(dwl)
	@$(ECHO) "\nCalculating expression table: '$@' ..." >&2
	perl -F"\t" -ane '$(tpm_gen2)' $^ > $@
	@$(ECHO) "Done." >&2


tcga_expression: $(manifest)
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) -j$(jobs) expr

expr: $(expr)

$(expr): $(gencode_v22) $(extr)
	@$(ECHO) "\nCalculating expression table: '$@' ..." >&2
	perl -F"\t" -MList::Util=max -ane '$(tpm_gen)' <(zcat $<) $(extr) > $@
	@$(ECHO) "Done." >&2


quantify: $(manifest)
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) quant

quant: $(quant)

$(quant): %.sf: %.counts $(expr)
	@$(ECHO) "\nCreating quantification file '$@' from '$(expr)' TPM table ..."
	$(quantify) $< $(expr) > $@
	@$(ECHO) "Done."



############################## TESTS ###########################################

envdir                         := $(sk8_rootdir)/tests/biotools


biotools_test_all: test_TCGA_manifest

biotools_test_general: biotools_set_env
	$(MAKE) -C $(sk8_rootdir)/tests/biotools -f $(sk8_mainfile) \
		module=$(sk8_rootdir)/modules/$(this) \
		$(args)
	# Some test here ...


biotools_test_TCGA_manifest: biotools_set_env | $(internet_status)
	$(ECHO) "\nTesting target '$@' in environment '$(env)' at '$(envdir)'." >&2
	$(MAKE) -C $(sk8_rootdir)/tests/biotools -f $(sk8_mainfile) \
		module=$(sk8_rootdir)/modules/biotools.mk \
		data_type=wxs \
		data_sample=GBM \
		data_control="blood derived normal" \
		paired=yes \
		manifest
	[ -s "GBM.manifest" -a "$(wc -l 'GBM.manifest')" == 17453 ] \
		|| ( echo "ERROR: Test '$@' Failed! File corrupted." >&2; exit 7; )
	[ -n "$(keep_files)" ] || rm -rf $(envdir)
	$(ECHO) "Done." >&2


biotools_test_1KG_manifest: $(manifest)

test_sort: $(or $(manifest),$(inputfl))
	$(ECHO) "\nTesting target '$@' in environment '$(env)' at '$(envdir)'." >&2
	$(MAKE) -C $(sk8_rootdir)/tests/biotools -f $(sk8_mainfile) \
		module=$(sk8_rootdir)/modules/$(this) \
	$(ECHO) "Done." >&2


biotools_set_env: | $(envdir)


$(envdir):
	$(MKDIR) $@


############################## OTHERS ##########################################

############################## sideRETRO #######################################
ifneq ($(filter sider_%, $(MAKECMDGOALS)),)

include $(sk8_rootdir)/modules/sider.mk


sider_db                       := $(db)
sider_vcf                      := $(vcf)

sider_ps_opts                  := -a $(annotation_ref) -c 2000000 -Q 20 -F 0.9 -t $(jobs)
sider_mc_opts                  := -B $(blacklist) -c 2000000 -x 1000000 -g 5 -I
sider_vcf_opts                 := -r $(genome_ref)


sider_all: sider_final
	@$(ECHO) "\nAll done for sider." >&2



sider_results: $(manifest)
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) sider_vcf batch=1

sider_vcf: $(sider_vcf)

$(sider_vcf): %.vcf: %.db
	@$(ECHO) "\nCreating VCF file '$@' from database '$<' ..."
	cd $(*D); \
	$(sider) vcf $(sider_vcf_opts) -p $(*F) $<
	@$(ECHO) "Done."


sider_databases: $(manifest)
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) sider_db batch=1

sider_db: $(sider_db)
	@$(ECHO) "\nAll databases complete: $^"

$(sider_db): %.db: %.dwl | $(sider_status) $(blacklist) $(annotation_ref)
	@$(ECHO) "\nCreating database '$@' from list file '$<' ..."
	cd $(*D); \
	$(sider) ps $(sider_ps_opts) -p $(*F) -i $<
	$(sider) mc $(sider_mc_opts) $@
	[ -n "$$(find $(*D) -type f -name '*.db' -size +10M)" ] || exit 7
	[ -n "$(keep_files)" ] || rm -rf $(*D)/*.bam
	@$(ECHO) "Done."

endif

ifneq ($(filter melt_%, $(MAKECMDGOALS)),)
      include $(sk8_rootdir)/modules/melt.mk
endif

ifneq ($(filter scramble_%, $(MAKECMDGOALS)),)
      include $(sk8_rootdir)/modules/scramble.mk
endif

ifneq ($(filter squire_%, $(MAKECMDGOALS)),)
      include $(sk8_rootdir)/modules/squire.mk
endif



############################## DEBUG ###########################################

ifeq ($(dbg_biotools),yes)
$(info )
$(info ############################## BIOTOOLS DEBUG ##################################)
$(info General paths.)
$(info CURDIR:                        $(CURDIR).)
$(info PWD:                           $(PWD).)
$(info ./:                            $(abspath .).)
$(info workdir:                       $(workdir).)
$(info sk8_rootdir:                   $(sk8_rootdir).)
$(info sk8_modules:                   $(MAKEFILE_LIST).)
$(info LIBRARY_PATH:                  $(LIBRARY_PATH))
$(info CPATH:                         $(CPATH))
$(info PATH:                          $(PATH))
$(info )

$(info Build paths.)
$(info DESTDIR:                       $(DESTDIR).)
$(info prefix:                        $(prefix).)
$(info installdir:                    $(installdir).)
$(info stageddir:                     $(stageddir).)
$(info rootdir:                       $(rootdir).)
$(info srcdir:                        $(srcdir).)
$(info builddir:                      $(builddir).)
$(info datadir:                       $(datadir).)
$(info )

$(info Environment paths.)
$(info switch:                        $(switch).)
$(info capsule:                       $(capsule).)
$(info basedir:                       $(basedir).)
$(info configdir:                     $(configdir).)
$(info inputsdir:                     $(inputsdir).)
$(info outputsdir:                    $(outputsdir).)
$(info datadir:                       $(datadir).)
$(info referencedir:                  $(referencedir).)
$(info annotationdir:                 $(annotationdir).)
$(info extradir:                      $(extradir).)
$(info tmpdir:                        $(tmpdir).)
$(info infrastructure:                $(infrastructure).)
$(info )

$(info Runtime data.)
$(info default goal:                  $(DEFAULT_GOAL))
$(info runmode:                       $(runmode))
$(info data_sample:                   $(data_sample).)
$(info data_type:                     $(data_type).)
$(info data_mode:                     $(datamode).)
$(info data_control:                  $(data_control).)
$(info data_treated:                  $(data_treated).)
$(info pairing:                       $(pairing).)
$(info radical:                       $(radical).)
$(info manifest:                      $(manifest).)
$(info manifest_type:                 $(manifest_type).)
$(info Command Line:                  $(CMD).)
$(info )

$(info Configuration files.)
$(info configfile: $(configfile).)
$(info dft_config: $(dft_config).)
$(info cfg: $(cfg).)
$(info )

$(info Input data.)
$(info suffix:                        $(sfx).)
$(info inputfl:                       $(inputfl).)
$(info args:                          $(args).)
$(info )

$(info Misc data.)
$(info hg19:                          $(hg19).)
$(info hg38:                          $(hg38).)
$(info genome_ref:                    $(genome_ref).)
$(info annotation_ref:                $(annotation_ref).)
$(info Genes blacklist:               $(blacklist).)
$(info GDC_token:                     $(gdc_token).)
$(info tcga_status:                   $(tcga_status).)
$(info sider:                         $(sider).)
$(info sider_ps_opts:                 $(sider_ps_opts).)
$(info sider_mc_opts:                 $(sider_mc_opts).)
$(info sider_vcf_opts:                $(sider_vcf_opts).)
$(info )

$(info More misc data.)
$(info dwl:                           $(dwl).)
$(info lst:                           $(lst).)
$(info db:                            $(db).)
$(info vcf:                           $(vcf).)
$(info tsv:                           $(tsv).)
$(info pdf:                           $(pdf).)
$(info ################################################################################)
endif
