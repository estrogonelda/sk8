############################## HEADER ##########################################
# SK8 module for package 'samtools'.
################################################################################



############################## PREAMBLE ########################################
# Non-standalone run verification.
this                           := samtools.mk
ifeq ($(notdir $(firstword $(MAKEFILE_LIST))),$(this))
      $(error ERROR: Module '$(this)' cannot run in standalone mode, \
      it needs to be loaded by 'SK8')
endif

# Info to download, configure, build, install and use 'samtools' package.
samtools_src                    ?= https://github.com/samtools/samtools/releases/download/1.15.1/samtools-1.15.1.tar.bz2
samtools_ext                    ?= .tar.bz2

samtools_package                ?= $(shell perl -ne 'print "@{[ (split(/-/, $$_))[0] ]}"' <<< "$(notdir $(samtools_src))")
samtools_version                ?= $(shell perl -pe 's/^$(samtools_package)-(.*?)\$(samtools_ext)/$$1/g' <<< "$(notdir $(samtools_src))")
samtools_id                     ?= $(samtools_package)-$(shell echo $$RANDOM)
samtools_license                ?= GPLv4
samtools_distname               ?= $(samtools_package)-$(samtools_version)

# Potential installation data.
samtools_repository             ?= $(samtools_src)
samtools_tarball                ?= $(tarballsdir)/$(samtools_distname)$(samtools_ext)
samtools_builddir               ?= $(tarballsdir)/$(samtools_distname)
samtools_configurefile          ?= $(samtools_builddir)/configure
samtools_buildfile              ?= $(samtools_builddir)/Makefile
samtools_execfile               ?= $(samtools_builddir)/$(samtools_package)

samtools_prefix                 ?= $(if $(supprefix),$(dir $(installdir))/$(samtools_package),$(dir $(installdir)))
samtools_install_bin            ?= $(samtools_prefix)/bin/$(samtools_package)
samtools_install_lib            ?= $(samtools_prefix)/lib/lib$(samtools_package).a
samtools_install_include        ?= $(samtools_prefix)/include/$(samtools_package).h
samtools_install_share          ?= $(samtools_prefix)/share/man

samtools_bins                   = $(wildcard $(samtools_builddir)/bin/*)
samtools_libs                   = $(wildcard $(samtools_builddir)/lib/lib*)
samtools_includes               = $(wildcard $(samtools_builddir)/include/*.h)


# Actual installation data.
ifeq ($(islib),)
samtools_installfile            ?= $(samtools_install_bin)
samtools_check                  ?= $(shell which $(samtools_package) 2> /dev/null)
else
samtools_installfile            ?= $(samtools_install_lib)
samtools_check                  ?= $(abspath $(strip $(wildcard $(basename $(samtools_installfile)).*)))
endif

samtools_get                    ?= $(if $(samtools_check),,samtools_install)
samtools                        ?= $(or $(samtools_check), $(samtools_installfile))


# FORCE data.
samtools_force                  ?= $(if $(FORCE),samtools_force,)
samtools_status                 ?= $(if $(FORCE),samtools_install,$(samtools_get))
samtools                        ?= $(if $(FORCE),$(samtools_installfile),$(samtools))


# Include required modules.
include $(addprefix $(sk8_rootdir)/modules/, gzip.mk bzip2.mk xz.mk)

samtools_deps_status             ?= $(if $(recursive),samtools_deps,)
samtools_deps                    ?= $(gcc_status)



############################## MAIN ############################################
# Usage.
samtools_help:
	@$(ECHO) "\n\
	NAME\n\
		\n\
		samtools_package - $(samtools_package), version $(samtools_version)\n\
		\n\
	SYNOPSIS\n\
		\n\
		$$ sk9 [make_option ...] [variable=value ...] <samtools_target ...>\n\
		\n\
	DESCRIPTION\n\
		\n\
		This is an engine to download, configure, build, install and test packages\n\
		in a samtools way. So, the package must be compliant with the GNU Coding\n\
		Standards in order to be handled by SK8.\n\
		Now, the package in question is '$(samtools_distname)'.\n\
		\n\
	OPTIONS\n\
		\n\
		Targets:\n\
		\n\
		samtools_help                   Print this help text.\n\
		samtools_version                Print '$(samtools_package)' package version.\n\
		samtools_status                 Verifies '$(samtools_package)' current installation status.\n\
		samtools_run                    Run '$(samtools_package)' with the arguments given in the 'args' variable.\n\
		samtools_install                Install '$(samtools_package)' binary in '\$$(prefix)/bin' directory.\n\
		samtools_build                  Compile the '$(samtools_package)' binary in the build directory.\n\
		samtools_configure              Configure '$(samtools_package)' build files for this system.\n\
		samtools_tarball                Download the '$(samtools_package)' tarball from its repository: $(samtools_src)\n\
		samtools_uninstall              Uninstall '$(samtools_package)' from the '\$$(prefix)/bin' directory.\n\
		samtools_clear                  Remove '$(samtools_package)' tarball from tarball''s directory.\n\
		samtools_purge                  Uninstall and clear '$(samtools_package)' from the system.\n\
		samtools_refresh                Uninstall and then re-install '$(samtools_package)' again.\n\
		samtools_deps                   Install a '$(samtools_package)' dependency recursivelly as passed by the 'args' variable.\n\
		samtools_test                   Test '$(samtools_package)' installation.\n\
		samtools_test_build_native      Test '$(samtools_package)' build process, natively.\n\
		samtools_test_build_conda       Test '$(samtools_package)' build process in a new Conda environment.\n\
		samtools_test_build_docker      Test '$(samtools_package)' build process inside a vanilla Docker container.\n\
		samtools_test_build_aws         Test '$(samtools_package)' build process inside an AWS instance. NOT WORKING!!\n\
		samtools_create_img             Create a Docker image for '$(samtools_package)', based on '$(image)' image.\n\
		samtools_create_dockerfile      Create a Dockerfile to build an image for '$(samtools_package)'.\n\
		samtools_create_module          Create a pre-filled SK8 module for '$(samtools_package)' package.\n\
		\n\
		conda_run                      Run '$(samtools_package)' in a new Conda environment. See 'conda_help' for more info.\n\
		docker_run                     Run '$(samtools_package)' in a vanilla Docker image. See 'docker_help' for more info.\n\
		aws_run                        Run '$(samtools_package)' in an AWS instance. See 'aws_help' for more info. NOT WORKING!!\n\
		\n\
		Variables:\n\
		\n\
		args                           String of arguments passed to '$(samtools_package)' when running.               [ '' ]\n\
		prefix                         Path to the directory where to put the built '$(samtools_packages)' binaries.   [ /usr/local ]\n\
		DESTDIR                        Path to an alternative staged installation directory.                          [ '' ]\n\
		workdir                        The current directory.                                                         [ . ]\n\
		stageddir                      Path to a staged directory for tarballs and package builds.                    [ ./.local ]\n\
		usermode                       Boolean to switch between local or system-wide installations paths.            [ '' ]\n\
		recursive                      Boolean to enable recursive dependency installations.                          [ '' ]\n\
		FORCE                          Boolean to force a fresh installation over an existent one.                    [ '' ]\n\
		\n\
		src                            URL of the '$(samtools_package)' repository.                                    [ '$(samtools_src)' ]\n\
		ext                            The tarball extension. Only for extensions other than '.tar.gz'.               [ '.tar.gz' ]\n\
		samtools_package                The name of the package, derived from its tarball''s name.                     [ '$(samtools_package)' ]\n\
		samtools_version                The version of the package, derived from its tarball''s name.                  [ '$(samtools_version)' ]\n\
		\n\
		Make options:\n\
		\n\
		\n\
		NOTE: Some packages cannot build samtoolsally. So, user must test the chosen target first.\n\
		For a list of known samtoolsally installable packages, please read the\n\
		'$(sk8_rootdir)/modules/modules_urls.txt' file.\n\
		\n\
		\n\
	COPYRIGHT\n\
		\n\
		$(sk8_package), version $(sk8_version) for GNU/Linux.\n\
		Copyright (C) Bards Agrotechnologies, 2011-2021.\n\
		\n\
	SEE ALSO\n\
		\n\
		Search for 'help', 'docker_help', 'conda_help' and 'miscellaneus'\n\
		for other sources of command line help or visit <$(basename $(sk8_source))>\n\
		for full documentation."

samtools_manual:
	@$(ECHO) "\n$(samtools_package) v$(samtools_version) for GNU/Linux.\n\
	Please, see <$(samtools_src)> for full documentation." >&2

samtools_version:
	@$(ECHO) "\n\
	Package:    $(samtools_package)\n\
	Version:    $(samtools_version)\n\
	Id:         $(samtools_id)\n\
	License:    $(samtools_license)\n\
	\n\
	Status:     $(samtools_status)\n\
	Source:     $(samtools_src)\n\
	Installdir: $(installdir)\n\
	\n\
	Runned with $(sk8_package), version $(sk8_version) for GNU/Linux.\n\
	Copyright (C) Bards Agrotechnologies, 2011-2021."


samtools_run: $(samtools_status)
	@$(ECHO) "\nRunning '$(samtools)' with arguments '$(args)' ..." >&2
	$(samtools) $(args)
	@$(ECHO) "Done." >&2


# Verification.
samtools_status: $(samtools_status) # 'samtools_install' or none.
	@$(ECHO) "\nSystem has '$(samtools_package)' up and running." >&2


# Install.
samtools_install: $(samtools_installfile) # $(installdir)/$(samtools_package)
	@$(ECHO) "\nSystem has '$(samtools_package)' in PATH: '$<'." >&2

$(samtools_installfile): $(samtools_execfile) | $(installdir) # just copy the executable file to $(installdir).
	@$(ECHO) "\nInstalling '$(samtools_package)' on directory '$(samtools_prefix)' ..." >&2
	$(MAKE) -C $(samtools_builddir) install DESTDIR=$(DESTDIR) prefix=$(samtools_prefix) -j4
	if [ -z "$$(perl -ne 'print if s#.*$(installdir).*#AA#g' <<< $${PATH})" ]; then \
		echo -e "export PATH=$(installdir):\$$PATH" >> ~/.bashrc; \
		source ~/.bashrc; \
		echo -e "WARNING: You may need to run 'source ~/.bashrc' to release the new \$$PATH."; \
	elif [ "$(samtools)" != "$(samtools_installfile)" ]; then \
		echo "WARNING: Previuos installation of '$(samtools_package)' may be shadowing the new one in the \$$PATH."; \
	fi
	touch $@
	@$(ECHO) "Done." >&2


# Build.
samtools_build: $(samtools_execfile) # $(samtools_builddir)/$(samtools_package)

$(samtools_execfile): $(samtools_buildfile) | $(gcc_status) # a Makefile generates the executable file.
	@$(ECHO) "\nBuilding '$(samtools_package)' on directory '$(samtools_builddir)' ..." >&2
	$(MAKE) -C $(samtools_builddir) -j4
	touch $@
	@$(ECHO) "Done." >&2


# Configure.
samtools_configure: $(samtools_buildfile) # $(samtools_builddir)/Makefile

$(samtools_buildfile): samtools_configure_cmd := ./configure --prefix=$(dir $(installdir)) $(samtools_args)
$(samtools_buildfile): $(samtools_configurefile)
	@$(ECHO) "\nConfiguring '$(samtools_package)' on directory '$(samtools_builddir)' ..." >&2
	cd $(samtools_builddir); \
	$(samtools_configure_cmd)
	touch $@
	@$(ECHO) "Done." >&2

$(samtools_configurefile): $(samtools_tarball) | $(samtools_builddir)
	@$(ECHO) "\nExtracting '$(samtools_package)' tarball to directory '$(samtools_builddir)' ..." >&2
	if [ "$(samtools_ext)" == ".tar.gz" ]; then \
		tar -xzvf $< -C $(samtools_builddir) --strip-components=1; \
	elif [ "$(samtools_ext)" == ".tar.xz" ]; then \
		tar -xJvf $< -C $(samtools_builddir) --strip-components=1; \
	else \
		tar -xvf $< -C $(samtools_builddir) --strip-components=1; \
	fi
	[ -s "$@" ] || { echo "ERROR: No file '$@' was extracted." >&2; exit 8; }
	touch $@
	@$(ECHO) "Done." >&2

$(samtools_builddir):
	@$(ECHO) "\nCreating directory '$(samtools_builddir)' ..." >&2
	mkdir -p $(samtools_builddir)
	@$(ECHO) "Done." >&2


# Download.
samtools_tarball: $(samtools_tarball) # $(tarballsdir)/$(samtools_tarball).

$(samtools_tarball): $(samtools_force) | $(tarballsdir) $(internet_status) $(samtools_deps_status) # none or 'samtools_deps'
	@$(ECHO) "\nDownloading '$(samtools_package)' tarball from '$(samtools_repository)' ..." >&2
	$(call downloader_cmd,$(dwlr),download,$(samtools_repository),$@,)
	touch $@
	@$(ECHO) "Done." >&2

samtools_deps: CPATH := $(basedir)/.local/include
samtools_deps: LIBRARR_PATH := $(basedir)/.local/lib
samtools_deps: $(samtools_deps)
	@$(ECHO) "\nRecursively installing dependecies for '$(samtools_distname)' ..." >&2
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) \
		module=$(basedir)/SK8/modules/zlib.mk \
		usermode=$(usermode) \
		recursive= \
		islib=1 \
		zlib_install
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) \
		module=$(basedir)/SK8/modules/gzip.mk \
		usermode=$(usermode) \
		recursive= \
		gzip_install
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) \
		module=$(basedir)/SK8/modules/bzip2.mk \
		usermode=$(usermode) \
		recursive= \
		bzip2_install
	$(MAKE) -f $(firstword $(MAKEFILE_LIST)) \
		module=$(basedir)/SK8/modules/xz.mk \
		usermode=$(usermode) \
		recursive= \
		xz_install
	@$(ECHO) "Done." >&2

samtools_force:
	@$(ECHO) "\nForce reinstallation of '$(samtools_package)' on directory '$(installdir)'." >&2
	rm -f $(samtools_tarball)
	@$(ECHO) "Done." >&2


samtools_refresh: samtools_uninstall
	if [ -z "$(usermode)" ]; then \
		$(MAKE) -f $(firstword $(MAKEFILE_LIST)) DESTDIR=$(DESTDIR) prefix=$(prefix) samtools_install; \
	else \
		$(MAKE) -f $(firstword $(MAKEFILE_LIST)) DESTDIR=$(DESTDIR) prefix=$(prefix) samtools_install usermode=1; \
	fi


# Uninstall and clean.
samtools_purge: samtools_uninstall samtools_clean

samtools_uninstall:
	@$(ECHO) "\nUninstalling '$(samtools_package)' from directory '$(installdir)' ..." >&2
	[ -d "$(installdir)" ] || exit 1;
	if [ -f "$(samtools_buildfile)" ]; then \
		$(MAKE) -C $(samtools_builddir) uninstall DESTDIR=$(DESTDIR) prefix=$(dir $(installdir)) -j4; \
	else \
		rm -f $(samtools_install_bin); \
		rm -f $(samtools_install_include); \
		rm -f $(dir $(samtools_install_lib))/pkgconfig/$(samtools_package).pc; \
		rm -rf $(basename $(samtools_install_lib)).*; \
		rm -rf $(samtools_installfile)*; \
	fi
	@$(ECHO) "User must resolve \$$PATH in '~/.bashrc' file." >&2
	@$(ECHO) "Done." >&2

samtools_clean:
	@$(ECHO) "\nCleaning '$(samtools_package)' from staged directory '$(stageddir)' ..." >&2
	rm -rf $(tarballsdir)/$(samtools_package)*
	@$(ECHO) "Done." >&2


# Unit Test.
samtools_test: alt_name := $(or $(samtools_alt_name),$(samtools_package))
samtools_test: $(samtools_status)
	$(ECHO) "\nTesting installation of '$(samtools_distname)' package ..." >&2
	if [ -f "$(samtools_buildfile)" ]; then \
		if [ -n "$$(grep 'installcheck' $(samtools_buildfile))" ]; then \
			$(MAKE) -C $(samtools_builddir) installcheck; \
		else \
			$(MAKE) -C $(samtools_builddir) check; \
		fi \
	else \
		if [ -n "$(islib)" ]; then \
			echo "int main() {}" | cc -xc - -L$(samtools_installfile) -l$(alt_name) -o /dev/null; \
		else \
			$(samtools_installfile) --version; \
		fi \
	fi >&2
	#$(call test_lib,$(alt_name),$(samtools_prefix),$(args))
	$(ECHO) "Done." >&2


# Build tests.
args2 = $(MAKE) -f $(c_basedir)/SK8/$(notdir $(firstword $(MAKEFILE_LIST))) \
	usermode=1 \
	module=$(c_basedir)/SK8/modules/samtools.mk \
	recursive=1 \
	islib= \
	samtools_src=$(samtools_src) \
	samtools_ext=$(samtools_ext) \
	samtools_package=$(samtools_package) \
	samtools_version=$(samtools_version) \
	samtools_args='$(samtools_args)' \
	samtools_alt_name=$(samtools_alt_name) \
	args='$(or $(args),--version)' \
	samtools_test


samtools_test_native: override args := $(args2)
samtools_test_native: native_test_build | $(srctestsdir)
	$(ECHO) "\nFinished native build test for '$(samtools_package)' package." >&2


samtools_test_conda: override args := $(args2)
samtools_test_conda: conda_test_build | $(srctestsdir)
	$(ECHO) "\nFinished Conda build test for '$(samtools_package)' package." >&2


samtools_test_docker: override args := $(args2)
samtools_test_docker: docker_test_build | $(srctestsdir)
	$(ECHO) "\nFinished Docker build test for '$(samtools_package)' package." >&2


samtools_test_all: samtools_test_docker samtools_test_conda samtools_test_native


.PHONY: samtools_help samtools_version samtools_manual samtools_force



############################## DOCUMENTATION ###################################
# Documentation was already written in the targets above.
